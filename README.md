目前网关只实现了转发功能，未实现过滤功能

## GateServer

网关服务端

编译：

进入Server/文档下

sudo make

运行：

进入Server/bin文档下

sudo ./GatewayServer

使用iptables 配置网桥（“gatebr0”）到内网网卡的转发和SNAT

## GateClient

网关客户端

编译：

sudo make

运行：

进入Client/bin文档下

sudo ./GatewayClient

配置默认网关 为服务端网桥(“gatebr0”)的地址(代码中默认值为192.168.159.172)

## gmssl

gmssl 源码 （网关运行需要gmssl环境）

以及一个tls连接demo

## 安全网关部分.docx

初始设计文档

## 网关部分设计文档.docx

代码实际对应的设计文档

文档中 的veth pair 在代码中已不再使用

