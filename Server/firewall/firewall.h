
/*
struct KerFunction exchangeMsgK(void *smsg, unsigned int slen); //内核交换数据结构

struct KerFunction addRule(char *after,char *name,char *sip,char *dip,unsigned int sport,unsigned int dport,u_int8_t proto,unsigned int log,unsigned int action); 
// 新增过滤规则，端口范围（高2字节为最小值低2字节为最大值）
struct KerFunction delRule(char *name); //删除过滤规则
struct KerFunction getAllRules(void); //获取规则
struct KerFunction addNATRule(char *sip,char *natIP,unsigned short minport,unsigned short maxport); //添加NAT规则
struct KerFunction delNATRule(int num); //删除NAT规则
struct KerFunction getAllNATRules(void); //获取NAT规则
struct KerFunction setDefaultAction(unsigned int action); //默认规则
struct KerFunction getLogs(unsigned int num); // num=0获取所有日志
struct KerFunction getAllConns(void); //获取所有连接

int IPstr2IPint(const char *ipStr, unsigned int *ip, unsigned int *mask);
int IPint2IPstr(unsigned int ip, unsigned int mask, char *ipStr);
int IPint2IPstrNoMask(unsigned int ip, char *ipStr);
int IPint2IPstrWithPort(unsigned int ip, unsigned short port, char *ipStr);
int showRules(struct IPRule *rules, int len);
int showNATRules(struct NATRule *rules, int len);
int showLogs(struct IPLog *logs, int len);
int showConns(struct ConnLog *logs, int len);

void dealResponseAtCmd(struct KerFunction rsp);
*/
