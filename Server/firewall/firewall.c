
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <linux/types.h>
#include <linux/in.h>
#include <linux/netfilter.h>
#include <linux/netlink.h>

#define REQ_GETAllIPRules 1
#define REQ_ADDIPRule 2
#define REQ_DELIPRule 3
#define REQ_SETAction 4
#define REQ_GETAllIPLogs 5
#define REQ_GETAllConns 6
#define REQ_ADDNATRule 7
#define REQ_DELNATRule 8
#define REQ_GETNATRules 9
#define RSP_Only_Head 10
#define RSP_MSG 11
#define RSP_IPRules 12
#define RSP_IPLogs 13
#define RSP_NATRules 14
#define RSP_ConnLogs 15
#define NAT_TYPE_NO 0
#define NAT_TYPE_SRC 1
#define NAT_TYPE_DEST 2

#define uint8_t unsigned char
#define NETLINK_MYFW 17
#define MAX_PAYLOAD (1024 * 256)
#define ERROR_CODE_EXIT -1
#define ERROR_CODE_EXCHANGE -2 // 与内核交换信息失败
#define ERROR_CODE_WRONG_IP -11 // 错误的IP格式
#define ERROR_CODE_NO_SUCH_RULE -12


struct IPRule {
    char name[10];
    unsigned int saddr;
    unsigned int smask;
    unsigned int daddr;
    unsigned int dmask;
    unsigned int sport; // 源端口范围（高2字节为最小值低2字节为最大值）
    unsigned int dport; // 目的端口范围
    u_int8_t protocol;
    unsigned int action;
    unsigned int log;
    struct IPRule* next;
};

struct IPLog {
    long tm;
    unsigned int saddr;
    unsigned int daddr;
    unsigned short sport;
    unsigned short dport;
    u_int8_t protocol;
    unsigned int len;
    unsigned int action;
    struct IPLog* next;
};

struct NATRule { // NAT规则：源IP端口转换
    unsigned int saddr; // 原始源IP
    unsigned int smask; // 原始源IP掩码
    unsigned int daddr; // NAT源IP

    unsigned short sport; // 最小端口
    unsigned short dport; // 最大端口
    unsigned short nowPort; // 当前使用端口
    struct NATRule* next;
};

struct ConnLog {
    unsigned int saddr;
    unsigned int daddr;
    unsigned short sport;
    unsigned short dport;
    u_int8_t protocol;
    int natType;
    struct NATRule nat; // NAT记录
};

struct UserReq {
    unsigned int tp;
    char ruleName[10];
    union {
        struct IPRule ipRule;
        struct NATRule natRule;
        unsigned int defaultAction;
        unsigned int num;
    } msg;
};

struct KerFunctionHeader {
    unsigned int bodyTp;
    unsigned int arrayLen;
};

struct KerFunction { //内核回应数据包
    int code; // 小于0代表请求失败，大于0为数据包长度
    void *data; // 回应包指针
    struct KerFunctionHeader *header; //指向数据包头部
    void *body; // 指向数据包中的Body
};


struct KerFunction exchangeMsgK(void *smsg, unsigned int slen); //内核交换数据结构

struct KerFunction addRule(char *after,char *name,char *sip,char *dip,unsigned int sport,unsigned int dport,u_int8_t proto,unsigned int log,unsigned int action);
// 新增过滤规则，端口范围（高2字节为最小值低2字节为最大值）
struct KerFunction delRule(char *name); //删除过滤规则
struct KerFunction getAllRules(void); //获取规则
struct KerFunction addNATRule(char *sip,char *natIP,unsigned short minport,unsigned short maxport); //添加NAT规则
struct KerFunction delNATRule(int num); //删除NAT规则
struct KerFunction getAllNATRules(void); //获取NAT规则
struct KerFunction setDefaultAction(unsigned int action); //默认规则
struct KerFunction getLogs(unsigned int num); // num=0获取所有日志
struct KerFunction getAllConns(void); //获取所有连接

int IPstr2IPint(const char *ipStr, unsigned int *ip, unsigned int *mask);
int IPint2IPstr(unsigned int ip, unsigned int mask, char *ipStr);
int IPint2IPstrNoMask(unsigned int ip, char *ipStr);
int IPint2IPstrWithPort(unsigned int ip, unsigned short port, char *ipStr);
int showRules(struct IPRule *rules, int len);
int showNATRules(struct NATRule *rules, int len);
int showLogs(struct IPLog *logs, int len);
int showConns(struct ConnLog *logs, int len);

void dealResponseAtCmd(struct KerFunction rsp);

struct KerFunction exchangeMsgK(void *smsg, unsigned int slen) {
	struct sockaddr_nl local;
	struct sockaddr_nl kpeer;
	struct KerFunction rsp;
	int dlen, kpeerlen = sizeof(struct sockaddr_nl);
	// init socket
	int skfd = socket(PF_NETLINK, SOCK_RAW, NETLINK_MYFW);
	if (skfd < 0) {
		rsp.code = ERROR_CODE_EXCHANGE;
		return rsp;
	}
	// bind
	memset(&local, 0, sizeof(local));
	local.nl_family = AF_NETLINK;
	local.nl_pid = getpid();
	local.nl_groups = 0;
	if (bind(skfd, (struct sockaddr *) &local, sizeof(local)) != 0) {
		close(skfd);
		rsp.code = ERROR_CODE_EXCHANGE;
		return rsp;
	}
	memset(&kpeer, 0, sizeof(kpeer));
	kpeer.nl_family = AF_NETLINK;
	kpeer.nl_pid = 0;
	kpeer.nl_groups = 0;
	// set send msg
	struct nlmsghdr *message=(struct nlmsghdr *)malloc(NLMSG_SPACE(slen)*sizeof(uint8_t));
	if(!message) {
		close(skfd);
		rsp.code = ERROR_CODE_EXCHANGE;
		return rsp;
	}
	memset(message, '\0', sizeof(struct nlmsghdr));
	message->nlmsg_len = NLMSG_SPACE(slen);
	message->nlmsg_flags = 0;
	message->nlmsg_type = 0;
	message->nlmsg_seq = 0;
	message->nlmsg_pid = local.nl_pid;
	memcpy(NLMSG_DATA(message), smsg, slen);
	// send msg
	if (!sendto(skfd, message, message->nlmsg_len, 0, (struct sockaddr *) &kpeer, sizeof(kpeer))) {
		close(skfd);
		free(message);
		rsp.code = ERROR_CODE_EXCHANGE;
		return rsp;
	}
	// recv msg
	struct nlmsghdr *nlh = (struct nlmsghdr *)malloc(NLMSG_SPACE(MAX_PAYLOAD)*sizeof(uint8_t));
	if (!nlh) {
		close(skfd);
		free(message);
		rsp.code = ERROR_CODE_EXCHANGE;
		return rsp;
	}
	if (!recvfrom(skfd, nlh, NLMSG_SPACE(MAX_PAYLOAD), 0, (struct sockaddr *) &kpeer, (socklen_t *)&kpeerlen)) {
		close(skfd);
		free(message);
		free(nlh);
		rsp.code = ERROR_CODE_EXCHANGE;
		return rsp;
	}
	dlen = nlh->nlmsg_len - NLMSG_SPACE(0);
	rsp.data = malloc(dlen);
	if(!(rsp.data)) {
		close(skfd);
		free(message);
		free(nlh);
		rsp.code = ERROR_CODE_EXCHANGE;
		return rsp;
	}
	memset(rsp.data, 0, dlen);
	memcpy(rsp.data, NLMSG_DATA(nlh), dlen);
	rsp.code = dlen - sizeof(struct KerFunctionHeader);
	if(rsp.code < 0) {
		rsp.code = ERROR_CODE_EXCHANGE;
	}
	rsp.header = (struct KerFunctionHeader*)rsp.data;
	rsp.body = rsp.data + sizeof(struct KerFunctionHeader);
	// over
	close(skfd);
	free(message);
	free(nlh);
	return rsp;
}

struct KerFunction addRule(char *after,char *name,char *sip,char *dip,unsigned int sport,unsigned int dport,u_int8_t proto,unsigned int log,unsigned int action) {
	struct UserReq req;
    struct KerFunction rsp;
	// form rule
	struct IPRule rule;
	if(IPstr2IPint(sip,&rule.saddr,&rule.smask)!=0) {
		rsp.code = ERROR_CODE_WRONG_IP;
		return rsp;
	}
	if(IPstr2IPint(dip,&rule.daddr,&rule.dmask)!=0) {
		rsp.code = ERROR_CODE_WRONG_IP;
		return rsp;
	}
	rule.saddr = rule.saddr;
	rule.daddr = rule.daddr;
	rule.sport = sport;
	rule.dport = dport;
	rule.log = log;
	rule.action = action;
	rule.protocol = proto;
	strncpy(rule.name, name, 10);
	// form req
	req.tp = REQ_ADDIPRule;
	req.ruleName[0]=0;
	strncpy(req.ruleName, after, 10);
	req.msg.ipRule = rule;
	// exchange
	return exchangeMsgK(&req, sizeof(req));
}

struct KerFunction delRule(char *name) {
	struct UserReq req;
	// form request
	req.tp = REQ_DELIPRule;
	strncpy(req.ruleName, name, 10);
	// exchange
	return exchangeMsgK(&req, sizeof(req));
}

struct KerFunction getAllRules(void) {
	struct UserReq req;
	// exchange msg
	req.tp = REQ_GETAllIPRules;
	return exchangeMsgK(&req, sizeof(req));
}

struct KerFunction addNATRule(char *sip,char *natIP,unsigned short minport,unsigned short maxport) {
	struct UserReq req;
	struct KerFunction rsp;
	// form rule
	struct NATRule rule;
	if(IPstr2IPint(natIP,&rule.daddr,&rule.smask)!=0) {
		rsp.code = ERROR_CODE_WRONG_IP;
		return rsp;
	}
	if(IPstr2IPint(sip,&rule.saddr,&rule.smask)!=0) {
		rsp.code = ERROR_CODE_WRONG_IP;
		return rsp;
	}
	rule.sport = minport;
	rule.dport = maxport;
	// form req
	req.tp = REQ_ADDNATRule;
	req.msg.natRule = rule;
	// exchange
	return exchangeMsgK(&req, sizeof(req));
}

struct KerFunction delNATRule(int num) {
	struct UserReq req;
	struct KerFunction rsp;
	if(num < 0) {
		rsp.code = ERROR_CODE_NO_SUCH_RULE;
		return rsp;
	}
	req.tp = REQ_DELNATRule;
	req.msg.num = num;
	// exchange
	return exchangeMsgK(&req, sizeof(req));
}

struct KerFunction getAllNATRules(void) {
	struct UserReq req;
	// exchange msg
	req.tp = REQ_GETNATRules;
	return exchangeMsgK(&req, sizeof(req));
}

struct KerFunction setDefaultAction(unsigned int action) {
	struct UserReq req;
	// form request
	req.tp = REQ_SETAction;
	req.msg.defaultAction = action;
	// exchange
	return exchangeMsgK(&req, sizeof(req));
}

struct KerFunction getLogs(unsigned int num) {
	struct UserReq req;
	// exchange msg
	req.msg.num = num;
	req.tp = REQ_GETAllIPLogs;
	return exchangeMsgK(&req, sizeof(req));
}

struct KerFunction getAllConns(void) {
	struct UserReq req;
	// exchange msg
	req.tp = REQ_GETAllConns;
	return exchangeMsgK(&req, sizeof(req));
}

int IPstr2IPint(const char *ipStr, unsigned int *ip, unsigned int *mask){
	// init
	int p = -1, count = 0;
	unsigned int len = 0, tmp = 0, r_mask = 0, r_ip = 0,i;
	for(i = 0; i < strlen(ipStr); i++){
		if(!(ipStr[i]>='0' && ipStr[i]<='9') && ipStr[i]!='.' && ipStr[i]!='/') {
			return -1;
		}
	}
	// 获取掩码
	for(i = 0; i < strlen(ipStr); i++){
        if(p != -1){
            len *= 10;
            len += ipStr[i] - '0';
        }
        else if(ipStr[i] == '/')
            p = i;
    }
	if(len > 32 || (p>=0 && p<7)) {
		return -1;
	}
    if(p != -1){
        if(len)
            r_mask = 0xFFFFFFFF << (32 - len);
    }
    else r_mask = 0xFFFFFFFF;
	// 获取IP
    for(i = 0; i < (p>=0 ? p : strlen(ipStr)); i++){
        if(ipStr[i] == '.'){
            r_ip = r_ip | (tmp << (8 * (3 - count)));
            tmp = 0;
            count++;
            continue;
        }
        tmp *= 10;
        tmp += ipStr[i] - '0';
		if(tmp>256 || count>3)
			return -2;
    }
    r_ip = r_ip | tmp;
	*ip = r_ip;
	*mask = r_mask;
    return 0;
}

int IPint2IPstr(unsigned int ip, unsigned int mask, char *ipStr) {
    unsigned int i,ips[4],maskNum = 32;
    if(ipStr == NULL) {
        return -1;
    }
	if(mask == 0)
		maskNum = 0;
	else {
		while((mask & 1u) == 0) {
                	maskNum--;
                	mask >>= 1;
        	}
	}
    for(i=0;i<4;i++) {
        ips[i] = ((ip >> ((3-i)*8)) & 0xFFU);
    }
	sprintf(ipStr, "%u.%u.%u.%u/%u", ips[0], ips[1], ips[2], ips[3], maskNum);
	return 0;
}

int IPint2IPstrNoMask(unsigned int ip, char *ipStr) {
    unsigned int i,ips[4];
    if(ipStr == NULL) {
        return -1;
    }
    for(i=0;i<4;i++) {
        ips[i] = ((ip >> ((3-i)*8)) & 0xFFU);
    }
	sprintf(ipStr, "%u.%u.%u.%u", ips[0], ips[1], ips[2], ips[3]);
	return 0;
}

int IPint2IPstrWithPort(unsigned int ip, unsigned short port, char *ipStr) {
    if(port == 0) {
        return IPint2IPstrNoMask(ip, ipStr);
    }
    unsigned int i,ips[4];
    if(ipStr == NULL) {
        return -1;
    }
    for(i=0;i<4;i++) {
        ips[i] = ((ip >> ((3-i)*8)) & 0xFFU);
    }
	sprintf(ipStr, "%u.%u.%u.%u:%u", ips[0], ips[1], ips[2], ips[3], port);
	return 0;
}

void dealResponseAtCmd(struct KerFunction rsp) {
	// 判断错误码
	switch (rsp.code) {
	case ERROR_CODE_EXIT:
		exit(0);
		break;
	case ERROR_CODE_NO_SUCH_RULE:
		printf("no such rule!\n");
		return;
	case ERROR_CODE_WRONG_IP:
		printf("IP is wrong!.\n");
		return;
	}
	if(rsp.code < 0 || rsp.data == NULL || rsp.header == NULL || rsp.body == NULL) 
		return;
	// 处理数据
	switch (rsp.header->bodyTp) {
	case RSP_Only_Head:
		printf("succeed to delete %d rules.\n", rsp.header->arrayLen);
		break;
	case RSP_MSG:
		printf("kernel operation: %s\n", (char*)rsp.body);
		break;
	case RSP_IPRules:
		showRules((struct IPRule*)rsp.body, rsp.header->arrayLen);
		break;
	case RSP_NATRules:
		showNATRules((struct NATRule*)rsp.body, rsp.header->arrayLen);
		break;
	case RSP_IPLogs:
		showLogs((struct IPLog*)rsp.body, rsp.header->arrayLen);
		break;
	case RSP_ConnLogs:
		showConns((struct ConnLog*)rsp.body, rsp.header->arrayLen);
		break;
	}
	if(rsp.header->bodyTp != RSP_Only_Head && rsp.body != NULL) {
		free(rsp.data);
	}
}

void printLine(int len) {
	int i;
	for(i = 0; i < len; i++) {
		printf("-");
	}
	printf("\n");
}

int showOneRule(struct IPRule rule) {
	char saddr[25],daddr[25],sport[13],dport[13],proto[6],action[8],log[5];
	// ip
	IPint2IPstr(rule.saddr,rule.smask,saddr);
	IPint2IPstr(rule.daddr,rule.dmask,daddr);
	// port
	if(rule.sport == 0xFFFFu)
		strcpy(sport, "any");
	else if((rule.sport >> 16) == (rule.sport & 0xFFFFu))
		sprintf(sport, "only %u", (rule.sport >> 16));
	else
		sprintf(sport, "%u~%u", (rule.sport >> 16), (rule.sport & 0xFFFFu));
	if(rule.dport == 0xFFFFu)
		strcpy(dport, "any");
	else if((rule.dport >> 16) == (rule.dport & 0xFFFFu))
		sprintf(dport, "only %u", (rule.dport >> 16));
	else
		sprintf(dport, "%u~%u", (rule.dport >> 16), (rule.dport & 0xFFFFu));
	// action
	if(rule.action == NF_ACCEPT) {
		sprintf(action, "accept");
	} else if(rule.action == NF_DROP) {
		sprintf(action, "drop");
	} else {
		sprintf(action, "other");
	}
	// protocol
	if(rule.protocol == IPPROTO_TCP) {
		sprintf(proto, "TCP");
	} else if(rule.protocol == IPPROTO_UDP) {
		sprintf(proto, "UDP");
	} else if(rule.protocol == IPPROTO_ICMP) {
		sprintf(proto, "ICMP");
	} else if(rule.protocol == IPPROTO_IP) {
		sprintf(proto, "IP");
	} else {
		sprintf(proto, "other");
	}
	// log
	if(rule.log) {
		sprintf(log, "yes");
	} else {
		sprintf(log, "no");
	}
	// print
	printf("| %-*s | %-18s | %-18s | %-11s | %-11s | %-8s | %-6s | %-3s |\n", 10,
	rule.name, saddr, daddr, sport, dport, proto, action, log);
	printLine(111);
}

int showRules(struct IPRule *rules, int len) {
	int i;
	if(len == 0) {
		printf("No rules now.\n");
		return 0;
	}
	printLine(111);
	printf("| %-*s | %-18s | %-18s | %-11s | %-11s | %-8s | %-6s | %-3s |\n", 10,
	 "name", "source ipaddr", "dest ipaddr", "source port", "target port", "protocol", "action", "log");
	printLine(111);
	for(i = 0; i < len; i++) {
		showOneRule(rules[i]);
	}
	return 0;
}

int showNATRules(struct NATRule *rules, int len) {
	int i, col = 66;
	char saddr[25],daddr[25];
	if(len == 0) {
		printf("No NAT rules now.\n");
		return 0;
	}
	printLine(col);
	printf("| seq | %18s |->| %-18s | %-11s |\n", "source ip", "NAT ip", "NAT port");
	printLine(col);
	for(i = 0; i < len; i++) {
		IPint2IPstr(rules[i].saddr,rules[i].smask,saddr);
		IPint2IPstrNoMask(rules[i].daddr,daddr);
		printf("| %3d | %18s |->| %-18s | %5u~%-5u |\n", i, saddr, daddr, rules[i].sport, rules[i].dport);
		printLine(col);
	}
	return 0;
}

int showOneLog(struct IPLog log) {
	struct tm * timeinfo;
	char saddr[25],daddr[25],proto[6],action[8],tm[21];
	// ip
	IPint2IPstrWithPort(log.saddr, log.sport, saddr);
	IPint2IPstrWithPort(log.daddr, log.dport, daddr);
	// action
	if(log.action == NF_ACCEPT) {
		sprintf(action, "[accept]");
	} else if(log.action == NF_DROP) {
		sprintf(action, "[drop]");
	} else {
		sprintf(action, "[unknown]");
	}
	// protocol
	if(log.protocol == IPPROTO_TCP) {
		sprintf(proto, "TCP");
	} else if(log.protocol == IPPROTO_UDP) {
		sprintf(proto, "UDP");
	} else if(log.protocol == IPPROTO_ICMP) {
		sprintf(proto, "ICMP");
	} else if(log.protocol == IPPROTO_IP) {
		sprintf(proto, "IP");
	} else {
		sprintf(proto, "other");
	}
	// time
	timeinfo = localtime(&log.tm);
	sprintf(tm, "%4d-%02d-%02d %02d:%02d:%02d",
		1900 + timeinfo->tm_year, 1 + timeinfo->tm_mon, timeinfo->tm_mday, timeinfo->tm_hour, timeinfo->tm_min, timeinfo->tm_sec);
	// print
	printf("[%s] %-9s %s->%s protocol=%s length=%uB\n",
		tm, action, saddr, daddr, proto, log.len);
}

int showLogs(struct IPLog *logs, int len) {
	int i;
	if(len == 0) {
		printf("No logs now.\n");
		return 0;
	}
	printf("sum: %d\n", len);
	for(i = 0; i < len; i++) {
		showOneLog(logs[i]);
	}
	return 0;
}

int showOneConn(struct ConnLog log) {
	struct tm * timeinfo;
	char saddr[25],daddr[25],proto[6];
	// ip
	IPint2IPstrWithPort(log.saddr,log.sport,saddr);
	IPint2IPstrWithPort(log.daddr,log.dport,daddr);
	// protocol
	if(log.protocol == IPPROTO_TCP) {
		sprintf(proto, "TCP");
	} else if(log.protocol == IPPROTO_UDP) {
		sprintf(proto, "UDP");
	} else if(log.protocol == IPPROTO_ICMP) {
		sprintf(proto, "ICMP");
	} else if(log.protocol == IPPROTO_IP) {
		sprintf(proto, "any");

	} else {
		sprintf(proto, "other");
	}
	printf("| %-11s |  %-21s |  %-21s | Established |\n",proto, saddr, daddr);
	if(log.natType == NAT_TYPE_SRC) {
		IPint2IPstrWithPort(log.nat.daddr, log.nat.dport, saddr);
		printf("| %-11s |=>%-21s |  %-21c | %11c |\n", "NAT", saddr, ' ', ' ');
	} else if(log.natType == NAT_TYPE_DEST) {
		IPint2IPstrWithPort(log.nat.daddr, log.nat.dport, daddr);
		printf("| %-11s |  %-21c |=>%-21s | %11c |\n", "NAT", ' ', daddr, ' ');
	}
}

int showConns(struct ConnLog *logs, int len) {
	int i, col = 79;
	if(len == 0) {
		printf("No connections now.\n");
		return 0;
	}
	printf("connection num: %d\n", len);
	printLine(col);
	printf("| %-11s |  %-21s |  %-21s | %11s |\n", "protocol", "source addr", "dest addr", "connect");
	printLine(col);
	for(i = 0; i < len; i++) {
		showOneConn(logs[i]);
	}
	printLine(col);
	return 0;
}

// 过滤规则时的用户交互
struct KerFunction cmdAddRule() {
	struct KerFunction empty;
	char after[10],name[10],saddr[25],daddr[25],sport[15],dport[15],protoS[6];
	unsigned short sportMin,sportMax,dportMin,dportMax;
	unsigned int action = NF_DROP, log = 0, proto, i;
	empty.code = ERROR_CODE_EXIT;
	// 前序规则名
	printf("rule sequence after name(input enter is the first one): ");
	for(i=0;;i++) {
		if(i>10) {
			printf("name is too long.\n");
			return empty;
		}
		after[i] = getchar();
		if(after[i] == '\n' || after[i] == '\r') {
			after[i] = '\0';
			break;
		}
	}
	// 规则名
	printf("input rule name (less than %d): ", 10);
	scanf("%s",name);
	if(strlen(name)==0 || strlen(name)>10) {
		printf("name is too long or too short.\n");
		return empty;
	}
	// 源IP
	printf("input source ip (eg: 11.4.5.14) or with mask (eg: 11.4.5.14/32): ");
	scanf("%s",saddr);
	// 源端口
	printf("input source port range (eg: 114-514) or any: ");
	scanf("%s",sport);
	if(strcmp(sport, "any") == 0) {
		sportMin = 0,sportMax = 0xFFFFu;
	} else {
		sscanf(sport,"%hu-%hu",&sportMin,&sportMax);
	}
	if(sportMin > sportMax) {
		printf("wrong input!\n");
		return empty;
	}
	// 目的IP
	printf("input dest ip (eg: 11.4.5.14) or with mask (eg: 11.4.5.14/32): ");
	scanf("%s",daddr);
	// 目的端口
	printf("input dest port range (eg: 114-514) or any: ");
	scanf("%s",dport);
	if(strcmp(dport, "any") == 0) {
		dportMin = 0,dportMax = 0xFFFFu;
	} else {
		sscanf(dport,"%hu-%hu",&dportMin,&dportMax);
	}
	if(dportMin > dportMax) {
		printf("wrong input!\n");
		return empty;
	}
	// 协议
	printf("input protocol (TCP/UDP/ICMP/any): ");
	scanf("%s",protoS);
	if(strcmp(protoS,"TCP")==0)
		proto = IPPROTO_TCP;
	else if(strcmp(protoS,"UDP")==0)
		proto = IPPROTO_UDP;
	else if(strcmp(protoS,"ICMP")==0)
		proto = IPPROTO_ICMP;
	else if(strcmp(protoS,"any")==0)
		proto = IPPROTO_IP;
	else {
		printf("wrong input!\n");
		return empty;
	}
	// 动作
	printf("input action (0: drop 1: accept): ");
	scanf("%d",&action);
	// 是否记录日志
	printf("record log? (1:yes 0: no): ");
	scanf("%u",&log);
	return addRule(after,name,saddr,daddr,
		(((unsigned int)sportMin << 16) | (((unsigned int)sportMax) & 0xFFFFu)),
		(((unsigned int)dportMin << 16) | (((unsigned int)dportMax) & 0xFFFFu)),proto,log,action);
}

struct KerFunction cmdAddNATRule() {
	struct KerFunction empty;
	char saddr[25],daddr[25],port[15];
	unsigned short portMin,portMax;
	empty.code = ERROR_CODE_EXIT;
	printf("only for source nat\n");
	// 源IP
	printf("input source ip and mask (eg: 11.4.5.14/32): ");
	scanf("%s",saddr);
	// NAT IP
	printf("input NAT ip (eg: 192.168.19.19): ");
	scanf("%s",daddr);
	// 目的端口
	printf("input NAT port range (eg: 114-514) or any: ");
	scanf("%s",port);
	if(strcmp(port, "any") == 0) {
		portMin = 0,portMax = 0xFFFFu;
	} else {
		sscanf(port,"%hu-%hu",&portMin,&portMax);
	}
	if(portMin > portMax) {
		printf("the min port > max port.\n");
		return empty;
	}
	return addNATRule(saddr,daddr,portMin,portMax);
}

void wrongCommand() {
	printf("invalid command!\n");
	printf("valid commands are here:\n");
	printf("1. add rule: 	   ./firewall add rule\n");
	printf("2. delete rule:    ./firewall del rule name\n");
	printf("3. default rule:   ./firewall default rule accept/drop\n");
	printf("4. list rules:     ./firewall ls rule \n");
	printf("5. add nat rule:   ./firewall add nat\n");
	printf("6. del nat rule:   ./firewall del nat name\n");
	printf("7. list nat rules: ./firewall ls nat\n");
	printf("8. list logs: 	   ./firewall ls log (num)\n");
	printf("9. list connects:  ./firewall ls connect\n");
	printf("./firewall add [rule/nat]\n");
	printf("./firewall del [rule/nat] name\n");
	printf("./firewall ls [rule/nat/log/connect]\n");
	printf("./firewall default rule [accept/drop]\n");
		
}

void ChooseOption(){
	int op,num;
	char str[11];
	printf("firewall functions and commands are here:\n");
	printf("1. add rule: 	   ./firewall add rule\n");
	printf("2. delete rule:    ./firewall del rule name\n");
	printf("3. default rule:   ./firewall default rule accept/drop\n");
	printf("4. list rules:     ./firewall ls rule \n");
	printf("5. add nat rule:   ./firewall add nat\n");
	printf("6. del nat rule:   ./firewall del nat name\n");
	printf("7. list nat rules: ./firewall ls nat\n");
	printf("8. list logs: 	   ./firewall ls log (num)\n");
	printf("9. list connects:  ./firewall ls connect\n");
	printf("please input function number(other number exit)!\n");
	
		struct KerFunction rsp;
		rsp.code = ERROR_CODE_EXIT;
		scanf("%d",&op);
		getchar();
		switch (op){						
			case 1:
				rsp = cmdAddRule();
				dealResponseAtCmd(rsp);
				break;

			case 2:
				printf("input rule name:\n");
				scanf("%s",str);
				getchar();
				if(strlen(str)>10) printf("rule name is too long!");
				else rsp = delRule(str);
				dealResponseAtCmd(rsp);
				break;

			case 3:
				printf("input default rule(accept or drop):\n");
				scanf("%s",str);
				getchar();
				if(strcmp(str, "accept")==0) rsp = setDefaultAction(NF_ACCEPT);
				else if(strcmp(str, "drop")==0) rsp = setDefaultAction(NF_DROP);
				else printf("please input \"accept\" or \"drop\".\n");
				dealResponseAtCmd(rsp);
				break;

			case 4:
				rsp = getAllRules();
				dealResponseAtCmd(rsp);
				break;

			case 5:
				rsp = cmdAddNATRule();
				dealResponseAtCmd(rsp);
				break;

			case 6:
				printf("input rule number:\n");
				scanf("%d", &num);
				getchar();
				rsp = delNATRule(num);
				dealResponseAtCmd(rsp);
				break;

			case 7:
				rsp = getAllNATRules();
				dealResponseAtCmd(rsp);
				break;

			case 8:
				printf("input list log number(0: all):\n");
				scanf("%d", &num);
				getchar();
				if(num>=0) rsp = getLogs(num);
				dealResponseAtCmd(rsp);
				break;

			case 9:
				rsp = getAllConns();
				dealResponseAtCmd(rsp);
				break;

			default:
				dealResponseAtCmd(rsp);
				break;
			
		}	
	exit(0);
}

int firewall_main(int argc, char *argv[]) {
	if(argc<3) {
		ChooseOption();
		return 0;
	}
	struct KerFunction rsp;
	rsp.code = ERROR_CODE_EXIT;
	// 过滤规则相关
	if(strcmp(argv[2], "rule")==0 || argv[2][0] == 'r') {
		if(strcmp(argv[1], "ls")==0 || strcmp(argv[1], "list")==0) {
		// 列出所有过滤规则
			rsp = getAllRules();
		} else if(strcmp(argv[1], "del")==0) {
		// 删除过滤规则
			if(argc < 4)
				printf("please input rule name!\n");
			else if(strlen(argv[3])>10)
				printf("rule name is too long!");
			else
				rsp = delRule(argv[3]);
		} else if(strcmp(argv[1], "add")==0) {
		// 添加过滤规则
			rsp = cmdAddRule();
		} else if(strcmp(argv[1], "default")==0) {
		// 设置默认规则
			if(argc < 4)
				printf("please input default rule(accept or drop)!\n");
			else if(strcmp(argv[3], "accept")==0)
				rsp = setDefaultAction(NF_ACCEPT);
			else if(strcmp(argv[3], "drop")==0)
				rsp = setDefaultAction(NF_DROP);
			else
				printf("please input \"accept\" or \"drop\".\n");
		} else 
			wrongCommand();
	} else if(strcmp(argv[2], "nat")==0 || argv[2][0] == 'n') {
		if(strcmp(argv[1], "ls")==0 || strcmp(argv[1], "list")==0) {
		// 列出所有NAT规则
			rsp = getAllNATRules();
		} else if(strcmp(argv[1], "del")==0) {
		// 删除NAT规则
			if(argc < 4)
				printf("please input rule number!\n");
			else {
				int num;
				sscanf(argv[3], "%d", &num);
				rsp = delNATRule(num);
			}
		} else if(strcmp(argv[1], "add")==0) {
		// 添加NAT规则
			rsp = cmdAddNATRule();
		} else {
			wrongCommand();
		}
	} else if(strcmp(argv[1], "ls")==0 || argv[1][0] == 'l') {
	// 展示相关
		if(strcmp(argv[2],"log")==0 || argv[2][0] == 'l') {
		// 过滤日志
			unsigned int num = 0;
			if(argc > 3)
				sscanf(argv[3], "%u", &num);
			rsp = getLogs(num);
		} else if(strcmp(argv[2],"connet")==0 || argv[2][0] == 'c') {
		// 连接状态
			rsp = getAllConns();
		} else if(strcmp(argv[2],"rule")==0 || argv[2][0] == 'r') {
		// 已有过滤规则
			rsp = getAllRules();
		} else if(strcmp(argv[2],"nat")==0 || argv[2][0] == 'n') {
		// 已有NAT规则
			rsp = getAllNATRules();
		} else
			wrongCommand();
	} else 
		wrongCommand();
	dealResponseAtCmd(rsp);
}
