#ifndef SERVERTHREAD_H
#define SERVERTHREAD_H

#include <QObject>
#include <QVector>
#include <gmssl/tls.h>

struct _connect_data{ //用于查看链接数量
    int net_fd;
    int tap_id;
    char srcaddr[25];
    unsigned short srcport;
    unsigned short family;
    char briaddr[25];
    unsigned short briport;
    char connect_time[25];
};
extern struct _connect_data connectstorage[100];

extern int connect_num;

class ServerThread : public QObject
{
    Q_OBJECT
public:
    explicit ServerThread(QObject *parent = nullptr);
    void output(quint8 type, QString exp);
    void gate_init();
    int do_start(QString ip,QString port,QString gatename,bool debug);
    int do_clean(QString gatename);
    //void recv_option(QString port,QString gatename,bool debug);
    void stop_bridge();
    void close_net(int net_fd);
    static int tap_init(char *tap_name);
    static void* process(void* args);
    static void process_quit(TLS_CONNECT* conn,int tap_fd,int tap_id,unsigned char *ipmsg);


signals:
    void send_msg(QVector<QString>msg);

private:
   QString port;
   QString gatename;
   int debug;
   //int stop_flag;
   int sock_fd;

};
/*
extern struct connect_info
{
  int fd_num;
  struct sockaddr_in ipaddr;
}conn_info[100];


extern int conn_num;
*/
#endif // SERVERTHREAD_H
