extern "C"{
#include <unistd.h>
#include <net/if.h>
#include <linux/if_tun.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <arpa/inet.h>
#include <sys/time.h>
#include <sys/epoll.h>
#include <errno.h>
#include <stdarg.h>
#include <pthread.h>
#include <gmssl/mem.h>
#include <gmssl/sm2.h>
#include <gmssl/tls.h>
#include <gmssl/error.h>
#include "../GateServer/bridgeAPI.h"
#include "../GateServer/ifManage.h"
#include "../GateServer/tuntapAPI.h"
}
#include "serverthread.h"
#include <QTime>

#define PORT 6000
#define MAXNUM 15000
#define DATETIME qPrintable (QDateTime::currentDateTime().toString("yyyy-MM-dd HH:mm:ss"))

QVector<QString> msg(100);
QString str;

struct _connect_data connectstorage[100];

int connect_num;


typedef struct tapandip
{
    int tap;
    unsigned char ip[4];
    struct tapandip* next;
} tapandip;

typedef struct
{
    pthread_mutex_t mutex;
    tapandip *volatile front;
    tapandip *volatile rear;
}tpqueue;

typedef struct pargs
{
    int tap_id;
    // int net_fd;
    TLS_CONNECT *conn;
    unsigned char ip[4];
}pargs;

extern tpqueue tpq;

char *progname;
tpqueue tpq;
int debug=0;
int stop_flag=0;


/* buffer for reading from tun/tap interface, must be >= 1500 */
#define BUFSIZE 2000
extern int debug;
extern char br_name[IFNAMSIZ];

char br_name[IFNAMSIZ] = "gatebr0";
// char veth_br[IFNAMSIZ] = "vethbr0";
// char veth_nat[IFNAMSIZ] = "vethnat0";
unsigned char br_ip[4] = {10,222,0,1};
unsigned char br_mask[4]={255,255,0,0};
unsigned int br_port=6000;

static void my_debug(char *msg, ...)
{

    if(debug)
    {
        va_list argp;
        va_start(argp, msg);
        vfprintf(stderr, msg, argp);
        va_end(argp);
    }
}

static size_t tls_read_n(TLS_CONNECT *conn, char *buf, size_t n)
{
    size_t nread=0, left = n;

    while (left > 0)
    {
        int rv;
        rv = tls_recv(conn, (uint8_t *)buf, left, &nread);
        if ( rv != 1)
        {
            if (rv < 0)
                return rv;
            else
                return 0;
        }
        else
        {
            if(nread==0)
                return 0;
            left -= nread;
            buf += nread;
        }
    }
    return n;
}

/**************************************************************************
 * cread: read routine that checks for errors and exits if an error is    *
 *        returned.                                                       *
 **************************************************************************/
static int cread(int fd, char *buf, int n)
{

    int nread;

    if ((nread = read(fd, buf, n)) < 0)
        perror("Reading data");
    return nread;
}

/**************************************************************************
 * cwrite: write routine that checks for errors and exits if an error is  *
 *         returned.                                                      *
 **************************************************************************/
static int cwrite(int fd, char *buf, int n)
{

    int nwrite;

    if ((nwrite = write(fd, buf, n)) < 0){
        perror("Writing data");
    }
    return nwrite;
}

/**************************************************************************
 * read_n: ensures we read exactly n bytes, and puts them into "buf".     *
 *         (unless EOF, of course)                                        *
 **************************************************************************/
static int read_n(int fd, char *buf, int n)
{

    int nread, left = n;

    while (left > 0)
    {
        if ((nread = cread(fd, buf, left)) <= 0)
        {
            return 0;
        }
        else if(nread<0)
            return nread;
        else
        {
            left -= nread;
            buf += nread;
        }
    }
    return n;
}

int ServerThread::tap_init(char *tap_name)
{
    ServerThread *temp = new ServerThread;
    int tap_state;
    /* initialize tap interface */
    int tap_fd;
    if ((tap_fd = tun_alloc(tap_name, IFF_TAP | IFF_NO_PI)) < 0)
    {
        printf("Error connecting to tap interface %s!\n", tap_name);
        str="Error connecting to tap interface";
        str=str+tap_name;
        temp->output(1,str);
        return -1;
    }

    //启动虚拟网卡
    if((tap_state=net_eth_state_is_up(tap_name))==0)
    {
        if(net_eth_state(tap_name,"up")!=0)
        {
            perror("up tap failed");
            str="up tap failed!";
            temp->output(1,str);
            return -1;
        }
    }
    else if(tap_state==-1)
    {
        perror("net_eth_state_is_up()");
        str="net_eth_state_is_up error!";
        temp->output(1,str);
        return -1;
    }


    //虚拟网卡加到桥上
    if(br_add_interface(br_name,tap_name)!=0)
    {
        perror("br_add_interface(tap)");
        str="br_add_interface(tap) error!";
        temp->output(1,str);
        return -1;
    }
    printf("init %s and add it to %s\n", tap_name,br_name);
    str="init ";
    str=str+tap_name+" and add it to "+br_name;
    temp->output(1,str);
    return tap_fd;
}

void ServerThread::process_quit(TLS_CONNECT* conn,int tap_fd,int tap_id,unsigned char *ipmsg)
{
    ServerThread *temp = new ServerThread;
    if(tap_fd>=0) close(tap_fd);
    close(conn->sock);
    tls_cleanup(conn);
    pthread_mutex_lock(&(tpq.mutex));
    if(tpq.rear==NULL)
    {
        while((tpq.rear=(tapandip*)malloc(sizeof(tapandip)))==NULL);
        tpq.rear->next=NULL;
        tpq.rear->tap=tap_id;
        tpq.rear->ip[0]=ipmsg[0];
        tpq.rear->ip[1]=ipmsg[1];
        tpq.rear->ip[2]=ipmsg[2];
        tpq.rear->ip[3]=ipmsg[3];
        tpq.front=tpq.rear;
    }
    else
    {
        while((tpq.rear->next=(tapandip*)malloc(sizeof(tapandip)))==NULL);
        tpq.rear=tpq.rear->next;
        tpq.rear->next=NULL;
        tpq.rear->tap=tap_id;
        tpq.rear->ip[0]=ipmsg[0];
        tpq.rear->ip[1]=ipmsg[1];
        tpq.rear->ip[2]=ipmsg[2];
        tpq.rear->ip[3]=ipmsg[3];
    }
    pthread_mutex_unlock(&(tpq.mutex));
    printf("thread:%lu quit...\n",pthread_self());
    str="thread: "+QString::number(pthread_self())+" quit...";
    temp->output(2,str);
    pthread_exit(NULL);
}

void* ServerThread::process(void* args)
{
    ServerThread *temp = new ServerThread;
    printf("thread:%lu start...\n",pthread_self());
    str="thread: "+QString::number(pthread_self())+" start...";
    temp->output(2,str);

    pargs* a= (pargs*)args;
    TLS_CONNECT conn = *(a->conn);
    free(a->conn);
    int tap_id=a->tap_id;
    unsigned char ipmsg[4];
    ipmsg[0]=a->ip[0];
    ipmsg[1]=a->ip[1];
    ipmsg[2]=a->ip[2];
    ipmsg[3]=a->ip[3];
    free(a);

    char tap_name[IFNAMSIZ] = "";
    sprintf(tap_name,"gtap%d",tap_id);

    int tap_fd;
    if((tap_fd=tap_init(tap_name))<0)
    {
        process_quit(&conn,tap_fd,tap_id,ipmsg);
    }

    size_t sentlen;
    if(tls_send(&conn, ipmsg, 4, &sentlen) != 1)
    {
        fprintf(stderr,"send ip:%d.%d.%d.%d failed\n",ipmsg[0],ipmsg[1],ipmsg[2],ipmsg[3]);
        str="send ip failed!";
        temp->output(1,str);
        process_quit(&conn,tap_fd,tap_id,ipmsg);
    }
    else if(sentlen!=4)
    {
        fprintf(stderr,"send ip:%d.%d.%d.%d failed\n",ipmsg[0],ipmsg[1],ipmsg[2],ipmsg[3]);
        str="send ip failed!";
        temp->output(1,str);
        process_quit(&conn,tap_fd,tap_id,ipmsg);
    }


    // int maxfd = (tap_fd > net_fd) ? tap_fd : net_fd;
    uint16_t nread, nwrite, plength;
    char buffer[BUFSIZE];
    unsigned long int tap2net = 0, net2tap = 0;

    //epoll
    int epfd=epoll_create(2);
    struct epoll_event ev, events[2];

    ev.data.fd=tap_fd;
    ev.events=EPOLLIN;
    epoll_ctl(epfd,EPOLL_CTL_ADD,tap_fd,&ev);

    ev.data.fd=conn.sock;
    ev.events=EPOLLIN;
    epoll_ctl(epfd,EPOLL_CTL_ADD,conn.sock,&ev);

    while (1)
    {
        int nfds=epoll_wait(epfd,events,2,-1);
        if (nfds < 0 && errno == EINTR)
        {
            continue;
        }

        if (nfds < 0)
        {
            perror("epoll_wait()");
            str="epoll_wait error!";
            temp->output(1,str);
            process_quit(&conn,tap_fd,tap_id,ipmsg);
        }
        for(int i=0;i<nfds;i++)
        {
            if(events[i].data.fd==conn.sock)
            {
                 /* data from the network: read it, and write it to the tun/tap interface.
                * We need to read the length first, and then the packet */

                /* Read length */
                nread = tls_read_n(&conn, (char *)&plength, sizeof(plength));

                if(nread>=65534)
                    process_quit(&conn,tap_fd,tap_id,ipmsg);
                if (nread == 0)
                {
                    /* ctrl-c at the other end */
                    // break;
                    process_quit(&conn,tap_fd,tap_id,ipmsg);
                }
                my_debug("ntohs(plength)=%d\n", ntohs(plength));
                str="ntohs(plength)="+QString::number(ntohs(plength));
                temp->output(3,str);

                net2tap++;

                /* read packet */
                nread = tls_read_n(&conn, buffer, ntohs(plength));
                if(nread>=65534)
                    process_quit(&conn,tap_fd,tap_id,ipmsg);
                my_debug("NET2TAP %lu: Read %d bytes from the network\n", net2tap, nread);
                str="NET2TAP "+QString::number(net2tap)+" Read "+QString::number(nread)+" bytes from the network";
                temp->output(3,str);

                /* now buffer[] contains a full packet or frame, write it into the tun/tap interface */
                nwrite = cwrite(tap_fd, buffer, nread);
                if(nwrite<0)
                    process_quit(&conn,tap_fd,tap_id,ipmsg);
                my_debug("NET2TAP %lu: Written %d bytes to the tap interface\n", net2tap, nwrite);
                str="NET2TAP "+QString::number(net2tap)+" Written "+QString::number(nwrite)+" bytes to the tap interface";
                temp->output(3,str);
            }
            else if (events[i].data.fd==tap_fd)
            {
                /* data from tun/tap: just read it and write it to the network */
                nread = cread(tap_fd, buffer, BUFSIZE);
                if(nread <0)
                    process_quit(&conn,tap_fd,tap_id,ipmsg);

                tap2net++;
                my_debug("TAP2NET %lu: Read %d bytes from the tap interface\n", tap2net, nread);
                str="TAP2NET "+QString::number(tap2net)+" Read "+QString::number(nread)+" bytes from the tap interface";
                temp->output(3,str);

                /* write length + packet */
                plength = htons(nread);
                // if((nwrite = cwrite(net_fd, (char *)&plength, sizeof(plength)))<0)
                //     process_quit(net_fd,tap_fd,tap_id,ipmsg);
                // if((nwrite = cwrite(net_fd, buffer, nread))<0)
                //     process_quit(net_fd,tap_fd,tap_id,ipmsg);

                if(tls_send(&conn, (const uint8_t *)&plength, sizeof(plength), &sentlen) != 1)
                    process_quit(&conn,tap_fd,tap_id,ipmsg);
                if(tls_send(&conn, (const uint8_t *)buffer, nread, &sentlen) != 1)
                    process_quit(&conn,tap_fd,tap_id,ipmsg);

                my_debug("TAP2NET %lu: Written %d bytes to the network\n", tap2net, nwrite);
                str="TAP2NET "+QString::number(tap2net)+" Written "+QString::number(nwrite)+" bytes to the network";
                temp->output(3,str);

            }


        }

    }
    process_quit(&conn,tap_fd,tap_id,ipmsg);

}


static void usage(void)
{
    fprintf(stderr, "Usage:\n");
    fprintf(stderr, "there is no help messsage yet\n");
    exit(1);
}


extern void close_tap(int net_fd){
    close(net_fd);
    return;
}

extern int connect_fd(int __fd, __CONST_SOCKADDR_ARG __addr, socklen_t __len){
    return connect(__fd,__addr,__len);
}

void ServerThread::output(quint8 type, QString exp){
    QString str;

    if (type == 1) {
        str = "[error] :";

    }else if (type == 2) {
        str = "[info] :";

    }else if (type == 3) {
        str = "[trans] :";

    }
    msg.push_back(QString("time [%1] %2 %3").arg(DATETIME).arg(str).arg(exp));
    emit send_msg(msg);
    msg.clear();
}

void ServerThread::gate_init()
{
    QString str;
    //创建bridge
    if(br_add_bridge(br_name)!=0)
    {
        output(1,"br_add_bridge error!");
        perror("br_add_bridge()");
        return;
    }
    str="Successfully add a bridge ";
    str=str+br_name;
    output(2,str);
    printf("Successfully add a bridge %s\n", br_name);


    //启动bridge
    int br_state;
    if((br_state=net_eth_state_is_up(br_name))==0)
    {
        if(net_eth_state(br_name,"up")!=0)
        {
            output(1,"can't up bridge!");
            fprintf(stderr, "can't up bridge %s: %s\n",
                br_name, strerror(errno));
            return;
        }
    }
    else if(br_state==-1)
    {
        output(1,"net_eth_state_error!");
        perror("net_eth_state_is_up()");
        return;
    }

    str=br_name;
    str=str+" is up now!";
    output(2,str);
    printf("%s is up now\n", br_name);


    //设置bridge的ip和掩码
    if(net_eth_set_ipv4_addr(br_name,br_ip,br_mask)!=0)
    {
        output(1,"net_eth_state_error!");
        perror("set br ip failed");
        return;
    }

    str="Successfully set ";
    str=str+br_name;
    str=str+" ip,netmask";
    output(2,str);
    printf("Successfully set %s's ip,netmask\n", br_name);

}

int ServerThread::do_start(QString ip,QString port,QString gatename,bool debug)
{
    stop_flag=0;
    int  option;
    struct sockaddr_in local, remote;
    char remote_ip[16] = ""; /* dotted quad IP string */
    int optval = 1;
    socklen_t remotelen;
    //memset(&conn_info, 0, sizeof(conn_info));
    //conn_num=0;

    if(!ip.isEmpty()){
        QByteArray ip_name=ip.toLatin1();
        strcpy(remote_ip,ip_name.data());
        br_ip[0]=0,br_ip[1]=0,br_ip[2]=0,br_ip[3]=0;
        for(int i=0,k=0;remote_ip[k]!='\0'&&k<16;k++)
        {
            if(remote_ip[k]=='.'){
            i++;
            continue;
            }
            switch(i){
            case 0:
                br_ip[0]=br_ip[0]*10+(remote_ip[k]-48);
                break;
            case 1:
                br_ip[1]=br_ip[1]*10+(remote_ip[k]-48);
                break;
            case 2:
                br_ip[2]=br_ip[2]*10+(remote_ip[k]-48);
                break;
            case 3:
                br_ip[3]=br_ip[3]*10+(remote_ip[k]-48);
                break;
            }
        }
        if(br_ip[3]!=1)br_ip[3]=1; //网关默认ip地址末位为1
    }else{
        br_ip[0]=10;br_ip[1]=222;br_ip[2]=0;br_ip[3]=1;
    }

    if(gatename.length()<16&&(!gatename.isEmpty())){
        QByteArray gate=gatename.toLatin1();
        strcpy(br_name,gate.data());
    }else strcpy(br_name,"gatebr0");

    if(!port.isEmpty()){
        br_port=port.toUShort();
    }else br_port=6000;

    if (debug==true) {
        debug=1;
    }
    else debug=0;

    QString str="set gatebridge name=";
    QByteArray tmp;
    str=str+QString::fromUtf8(br_name);
    output(2,str);

    char localbri[25];
    str=QString::number(br_ip[0])+"."+QString::number(br_ip[1])+"."+QString::number(br_ip[2])+"."+QString::number(br_ip[3]);
    tmp=str.toLatin1();
    strcpy(localbri,tmp.data());

    str="set ip="+str;
    output(2,str);
    str="set port="+QString::number(br_port);
    output(2,str);
    if(debug){
        str="using debug mode";
    }else str="not using debug mode";
    output(2,str);

    tpq.front=NULL;
    tpq.rear=NULL;
    pthread_mutex_init(&(tpq.mutex), NULL);


    if (*br_name == '\0')
    {
        printf("Must specify bridge interface name!\n");
        usage();
    }


    gate_init();

    char certfile[128] = "../GateServer/double_certs.pem";
    char signkeyfile[128] = "../GateServer/signkey.pem";
    char signpass[128] = "1234";
    char enckeyfile[128] = "../GateServer/enckey.pem";
    char encpass[128] = "1234";
    char cacertfile[128] = "../GateServer/cacert.pem";


    int server_ciphers[] = { TLS_cipher_ecc_sm4_cbc_sm3, };
    TLS_CTX ctx;
    memset(&ctx, 0, sizeof(ctx));
    if (tls_ctx_init(&ctx, TLS_protocol_tlcp, TLS_server_mode) != 1
        || tls_ctx_set_cipher_suites(&ctx, server_ciphers, sizeof(server_ciphers)/sizeof(int)) != 1
        || tls_ctx_set_tlcp_server_certificate_and_keys(&ctx, certfile, signkeyfile, signpass, enckeyfile, encpass) != 1) {
        output(1,"certificate error!");
        error_print();
        return -1;
    }
    if (cacertfile) {
        if (tls_ctx_set_ca_certificates(&ctx, cacertfile, TLS_DEFAULT_VERIFY_DEPTH) != 1) {
            output(1,"certificate error!");
            error_print();
            return -1;
        }
    }

    if ((sock_fd = socket(AF_INET, SOCK_STREAM, 0)) < 0)
    {
        output(1,"socket() error!");
        perror("socket()");
        return -1;
    }

    /* avoid EADDRINUSE error on bind() */
    if (setsockopt(sock_fd, SOL_SOCKET, SO_REUSEADDR, (char *)&optval, sizeof(optval)) < 0)
    {
        output(1,"setsockopt() error!");
        perror("setsockopt()");
        return -1;
    }

    memset(&local, 0, sizeof(local));
    local.sin_family = AF_INET;
    local.sin_addr.s_addr = htonl(INADDR_ANY);
    local.sin_port = htons(br_port);

    connect_num=1;
    connectstorage[0].net_fd=0;
    connectstorage[0].tap_id=0;
    strcpy(connectstorage[0].srcaddr,inet_ntoa(local.sin_addr));
    connectstorage[0].srcport=ntohs(local.sin_port);
    connectstorage[0].family=local.sin_family;
    strcpy(connectstorage[0].briaddr,localbri);
    connectstorage[0].briport=br_port;
    strcpy(connectstorage[0].connect_time,DATETIME);

    if (bind(sock_fd, (struct sockaddr *)&local, sizeof(local)) < 0)
    {
        output(1,"bind() error!");
        perror("bind()");
        return -1;
    }

    if (listen(sock_fd, 5) < 0)
    {
        output(1,"listen() error!");
        perror("listen()");
        return -1;
    }

    /* wait for connection request */
    remotelen = sizeof(remote);
    memset(&remote, 0, remotelen);

    int tap_id,net_fd;
    pthread_t new_thread;
    tapandip* tptmp=NULL;
    int currentnum=2;
    pargs *ags=NULL;

    while(1)
    {
        output(2,"server is waiting for client connect\n");
        if ((net_fd = accept(sock_fd, (struct sockaddr *)&remote, &remotelen)) < 0)
        {
            output(1,"accept() error!");
            perror("accept()");
            return -1;
        }

        printf("net_fd=%d\n",net_fd);
        printf("SERVER: Client connected from %s\n", inet_ntoa(remote.sin_addr));        

        TLS_CONNECT *conn = (TLS_CONNECT *)malloc(sizeof(TLS_CONNECT));
        memset(conn, 0, sizeof(TLS_CONNECT));
        if (tls_init(conn, &ctx) != 1
            || tls_set_socket(conn, net_fd) != 1) {
            output(1,"tls_init error!");
            error_print();
            return -1;
        }
        if (tls_do_handshake(conn) != 1) {
            output(1,"tls_handshake error!");
            error_print();
            return -1;
        }

        ags=(pargs*)malloc(sizeof(pargs));

        if(ags==NULL)
        {
            output(1,"malloc error!");
            perror("malloc (pargs)");
            return -1;
        }
        ags->conn=conn;
        ags->ip[0]=br_ip[0];
        ags->ip[1]=br_ip[1];
        pthread_mutex_lock(&(tpq.mutex));
        if(tpq.front==NULL)
        {
            output(2,"queue is empty.");
            printf("queue is empty.\n");
            if(currentnum>MAXNUM)
            {
                output(1,"The number of connections exceeded the limit!");
                printf("The number of connections exceeded the limit!\n");
                close_tap(net_fd);
                continue;
            }
            ags->ip[2]=(unsigned char)(currentnum/256);
            ags->ip[3]=(unsigned char)(currentnum%256);
            tap_id=currentnum;
            currentnum++;
            ags->tap_id=tap_id;
        }
        else
        {
            output(2,"queue is not empty.");
            printf("queue is not empty.\n");
            tptmp=tpq.front;
            tpq.front=tpq.front->next;
            if(tpq.front==NULL) tpq.rear=NULL;
            ags->tap_id=tptmp->tap;
            ags->ip[0]=tptmp->ip[0];
            ags->ip[1]=tptmp->ip[1];
            ags->ip[2]=tptmp->ip[2];
            ags->ip[3]=tptmp->ip[3];
            free(tptmp);
        }

        str="net_fd= ";
        str=str+QString::number(net_fd);
        output(2,str);
        str="SERVER: Client connected from ";
        str=str+inet_ntoa(remote.sin_addr);
        output(2,str);
        char briaddr[25];
        str=QString::number((unsigned short)(ags->ip[0]))+"."+QString::number((unsigned short)(ags->ip[1]))+"."+
                QString::number((unsigned short)(ags->ip[2]))+"."+QString::number((unsigned short)(ags->ip[3]));
        tmp=str.toLatin1();
        strcpy(briaddr,tmp.data());

        str="Distribute ip is "+str;
        output(2,str);
        connect_num=currentnum-1;
        connectstorage[connect_num-1].net_fd=net_fd;
        connectstorage[connect_num-1].tap_id=tap_id;
        strcpy(connectstorage[connect_num-1].srcaddr,inet_ntoa(remote.sin_addr));
        connectstorage[connect_num-1].srcport=ntohs(remote.sin_port);
        connectstorage[connect_num-1].family=remote.sin_family;
        strcpy(connectstorage[connect_num-1].briaddr,briaddr);
        connectstorage[connect_num-1].briport=br_port;
        strcpy(connectstorage[connect_num-1].connect_time,DATETIME);

        pthread_mutex_unlock(&(tpq.mutex));
        pthread_create(&new_thread,NULL,process,ags);
        pthread_detach(new_thread);
        /*if(stop_flag){
            output(2,"bridge is stopped !");
            return 0;
        }*/
    }
}

int ServerThread::do_clean(QString gatename)
{
    int option;
    int ret=0;
    char br_to_del[IFNAMSIZ];

    if(gatename.length()<16&&(!gatename.isEmpty())){
        QByteArray gate=gatename.toLatin1();
        strcpy(br_name,gate.data());
    }else strcpy(br_name,"gatebr0");
    strcpy(br_to_del,br_name);

    QString str;
    int del_res;
    del_res=br_del_bridge(br_to_del);
    switch (del_res)
    {
        case 0:
            str="Successfully delete bridge ";
            str=str+br_to_del;
            output(2,str);
            printf("Successfully delete bridge %s\n", br_to_del);
            break;

        case ENXIO:
            str=br_to_del;
            str=str+" doesn't exist; can't delete it";
            output(2,str);
            fprintf(stderr, "bridge %s doesn't exist; can't delete it\n",
                br_to_del);
            ret=1;
            break;

        case EBUSY:
            str=br_to_del;
            str=str+" is still up, try to down it...";
            output(2,str);
            printf("%s is still up, try to down it...\n",br_to_del);
            if(net_eth_state(br_to_del,"down")!=0)
            {
                output(1,"down br failed!");
                perror("down br failed!");
                ret=1;
                break;
            }
            str=br_to_del;
            str=str+" is down now";
            output(2,str);
            printf("%s is down now\n",br_to_del);
            if(br_del_bridge(br_to_del)==0)
            {
                str="Successfully delete bridge ";
                str=str+br_to_del;
                output(2,str);
                printf("Successfully delete bridge %s\n", br_to_del);
                break;
            }
            output(1,"can't delete bridge!");
            fprintf(stderr, "can't delete bridge %s: %s\n",
                br_to_del, strerror(del_res));
            ret=1;
            break;

        default:
            output(1,"can't delete bridge!");
            fprintf(stderr, "can't delete bridge %s: %s\n",
                br_to_del, strerror(del_res));
            break;
    }
    // if(my_del_veth(veth_to_del)!=0)
    // {
    //     ret=1;
    //     printf("failed to del %s!\n",veth_to_del);
    // }
    // else
    //     printf("successfully delete veth pair:%s\n",veth_to_del);
    return ret;
}


ServerThread::ServerThread(QObject *parent) : QObject(parent)
{

}


void ServerThread::stop_bridge(){

    //stop_flag=1;
    QString str="terminate connect and close sock_fd: ";
    str=str+sock_fd;
    output(2,str);
    close_tap(sock_fd);

}

void ServerThread::close_net(int net_fd){
    QString str="try to close net_fd number:";
    str=str+net_fd;
    output(2,str);
    close_tap(net_fd);
}
