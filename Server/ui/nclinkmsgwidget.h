#ifndef NCLINKMSGWIDGET_H
#define NCLINKMSGWIDGET_H

#include <QWidget>
#include <QTcpServer>
#include <QTcpSocket>

namespace Ui {
class nclinkmsgwidget;
}

class nclinkmsgwidget : public QWidget
{
    Q_OBJECT

public:
    explicit nclinkmsgwidget(QWidget *parent = nullptr);
    ~nclinkmsgwidget();

private slots:
    void on_start_server_clicked();

    void on_send_msg_clicked();

private:
    Ui::nclinkmsgwidget *ui;
    QTcpServer *tcp_server;
    QTcpSocket *tcp_socket;

};

#endif // NCLINKMSGWIDGET_H
