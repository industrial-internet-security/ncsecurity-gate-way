HEADERS += \
    $$PWD/CustomWidget.h \
    $$PWD/LoginWidget.h \
    $$PWD/ServerWidget.h \
    $$PWD/firewallwidget.h \
    $$PWD/mainwindow.h \
    $$PWD/nclinkdevwidget.h \
    $$PWD/nclinkmsgwidget.h \
    $$PWD/serverthread.h

SOURCES += \
    $$PWD/CustomWidget.cpp \
    $$PWD/LoginWidget.cpp \
    $$PWD/ServerWidget.cpp \
    $$PWD/firewallwidget.cpp \
    $$PWD/mainwindow.cpp \
    $$PWD/nclinkdevwidget.cpp \
    $$PWD/nclinkmsgwidget.cpp \
    $$PWD/serverthread.cpp
	
FORMS += \
    $$PWD/CustomWidget.ui \
    $$PWD/LoginWidget.ui \
    $$PWD/ServerWidget.ui \
    $$PWD/firewallwidget.ui \
    $$PWD/mainwindow.ui \
    $$PWD/nclinkdevwidget.ui \
    $$PWD/nclinkmsgwidget.ui
