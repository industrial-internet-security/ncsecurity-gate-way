﻿
#include<QVector>
#include "serverthread.h"
#include "ServerWidget.h"
#include "ui_ServerWidget.h"
//#include "../help/BinaryCvn.h"
#include "../help/UiSet.h"
//#include "../help/AppCfg.h"

#include <QTime>
#define DATETIME qPrintable (QDateTime::currentDateTime().toString("yyyyMMddHHmmss"))

//函数入口？
ServerWidget::ServerWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ServerWidget)
{
    ui->setupUi(this);
    setWindowFlags(Qt::FramelessWindowHint);   //去掉边框
    setAttribute(Qt::WA_StyledBackground);

    //server = new QTcpServer(this);
    //client = new QTcpSocket(this);   //实例化tcpClient
    InitWindow();
    //BuildConnect();
}

ServerWidget::~ServerWidget()
{
    //saveConfig();
    delete ui;
}
void ServerWidget::InitWindow()
{

    //IP输入框正则
    QRegExp regExp("^((2[0-4]\\d|25[0-5]|[01]?\\d\\d?)\\.){3}(2[0-4]\\d|25[0-5]|[01]?\\d\\d?)$");
    QValidator *validator = new QRegExpValidator(regExp, ui->ip);
    ui->ip->setValidator(validator);
    ui->port->setValidator(new QIntValidator(0, 65536,ui->port));

    //创建表头
    QStandardItemModel *rule_table = new QStandardItemModel();
    rule_table->setHorizontalHeaderItem(0, new QStandardItem(QObject::tr("seq")));
    rule_table->setHorizontalHeaderItem(1, new QStandardItem(QObject::tr("net_fd")));
    rule_table->setHorizontalHeaderItem(2, new QStandardItem(QObject::tr("tap_id")));
    rule_table->setHorizontalHeaderItem(3, new QStandardItem(QObject::tr("src_addr")));
    rule_table->setHorizontalHeaderItem(4, new QStandardItem(QObject::tr("src_port")));
    rule_table->setHorizontalHeaderItem(5, new QStandardItem(QObject::tr("family")));
    rule_table->setHorizontalHeaderItem(6, new QStandardItem(QObject::tr("bridge_addr")));
    rule_table->setHorizontalHeaderItem(7, new QStandardItem(QObject::tr("bridge_port")));
    rule_table->setHorizontalHeaderItem(8, new QStandardItem(QObject::tr("connected_time")));
    //利用setModel()方法将数据模型与QTableView绑定
    ui->tableView->setModel(rule_table);

    for(int i=0;i<9;i++){ //固定列宽
        ui->tableView->horizontalHeader()->setSectionResizeMode(i, QHeaderView::Fixed);
    }
    //设置表格的各列的宽度值
    ui->tableView->setColumnWidth(0,100);
    ui->tableView->setColumnWidth(1,100);
    ui->tableView->setColumnWidth(2,100);
    ui->tableView->setColumnWidth(3,200);
    ui->tableView->setColumnWidth(4,200);
    ui->tableView->setColumnWidth(5,200);
    ui->tableView->setColumnWidth(6,200);
    ui->tableView->setColumnWidth(7,200);
    ui->tableView->setColumnWidth(8,400);
    //设置选中时为整行选中
    ui->tableView->setSelectionBehavior(QAbstractItemView::SelectRows);
    //设置表格的单元为只读属性，即不能编辑
    ui->tableView->setEditTriggers(QAbstractItemView::NoEditTriggers);

    ServerThread *thread1 =new ServerThread;
    QThread *t1=new QThread;
    thread1->moveToThread(t1);

    connect(this,&ServerWidget::send_option,thread1,&ServerThread::do_start);
    connect(ui->startbridge,&QPushButton::clicked,this,[=](){
        if(ui->ip->text().isEmpty())ui->ip->setText("10.222.0.1");//默认网桥ip
        if(ui->port->text().isEmpty())ui->port->setText("6000");//默认网桥端口
        if(ui->gatename->text().isEmpty())ui->gatename->setText("gatebr0");//默认网桥名称
        emit send_option(ui->ip->text(),ui->port->text(),ui->gatename->text(),ui->debugmode->isChecked());
        t1->start();
    });
    connect(thread1,&ServerThread::send_msg,this,[=](QVector<QString>msg){
        msg.data();
        ui->display->append(msg.at(0));
    });

    connect(ui->cleanbridge,&QPushButton::clicked,this,[=](){
        thread1->do_clean(ui->gatename->text());
    });

    connect(ui->stopconnect, &QPushButton::clicked,this,[=](){
    if(t1->isRunning()){
        t1->terminate();
        thread1->stop_bridge();
    }
        //thread1->deleteLater();
    });

    connect(ui->flash,&QPushButton::clicked,this,[=](){
        rule_table->setRowCount(0);
        if(connect_num>0){
            for(int i=0;i<connect_num;i++){
                rule_table->setItem(i,0,new QStandardItem(QString::number(i+1)));
                rule_table->setItem(i,1,new QStandardItem(QString::number(connectstorage[i].net_fd)));
                rule_table->setItem(i,2,new QStandardItem(QString::number(connectstorage[i].tap_id)));
                rule_table->setItem(i,3,new QStandardItem(connectstorage[i].srcaddr));
                rule_table->setItem(i,4,new QStandardItem(QString::number(connectstorage[i].srcport)));
                rule_table->setItem(i,5,new QStandardItem(QString::number(connectstorage[i].family)));
                rule_table->setItem(i,6,new QStandardItem(connectstorage[i].briaddr));
                rule_table->setItem(i,7,new QStandardItem(QString::number(connectstorage[i].briport)));
                rule_table->setItem(i,8,new QStandardItem(connectstorage[i].connect_time));
            }
        }
    });


    connect(ui->save,&QPushButton::clicked,this,[=](){
        QString file_path;
        file_path="..//link_info//";
        if(!ui->save_path->text().isEmpty()){
            file_path=file_path+ui->save_path->text();
        }else {
            file_path=file_path+DATETIME;
        }file_path=file_path+".json";

        QJsonArray link_storage;
        for(int i=0;i<connect_num;i++){
            QJsonObject link_obj;
            link_obj.insert("seq",QString::number(i+1));//定位序号，方便解析时使用
            link_obj.insert("net_fd",connectstorage[i].net_fd);
            link_obj.insert("tap_id",connectstorage[i].tap_id);
            link_obj.insert("src_addr",connectstorage[i].srcaddr);
            link_obj.insert("src_port",connectstorage[i].srcport);
            link_obj.insert("family",connectstorage[i].family);
            link_obj.insert("bri_addr",connectstorage[i].briaddr);
            link_obj.insert("bri_port",connectstorage[i].briport);
            link_obj.insert("connect_time",connectstorage[i].connect_time);
            link_storage.append(link_obj);
        }

        QJsonDocument jsonDocu(link_storage);
        QByteArray jsonData = jsonDocu.toJson();

        QFile file(file_path);
        if(file.open(QIODevice::WriteOnly)){
            file.write(jsonData);
            file.close();
        }
    });
}


