#ifndef FIREWALL_H
#define FIREWALL_H

#include <QWidget>

namespace Ui {
class FirewallWidget;
}

class FirewallWidget : public QWidget
{
    Q_OBJECT

public:
    explicit FirewallWidget(QWidget *parent = nullptr);
    ~FirewallWidget();
    void initwindow();
    void option();
    void feedback(const QString text);

    void open_firewall();
    void close_firewall();
    void list_rule();
    void list_nat();
    void list_log(int num);
    void list_connect();

    char* name;
    char* prior;
    char* srcip;
    char* dstip;
    char* proto;
    unsigned int srcmk,srcptl,srcptr,dstmk,dstptl,dstptr,action=0;
    bool srcany,dstany;


private:
    Ui::FirewallWidget *ui;
};

#endif // FIREWALL_H
