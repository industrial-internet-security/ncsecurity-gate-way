#include "firewallwidget.h"
#include "ui_firewallwidget.h"
#include <QVector>
#include <QTableView>
#include <QStandardItemModel>
#include <QtWidgets>
#include <QTime>
#include <QTextEdit>

#define DATETIME qPrintable (QDateTime::currentDateTime().toString("yyyy-MM-dd HH:mm:ss"))

extern "C"{
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <linux/types.h>
#include <linux/in.h>
#include <linux/netfilter.h>
#include <linux/netlink.h>

#define REQ_GETAllIPRules 1
#define REQ_ADDIPRule 2
#define REQ_DELIPRule 3
#define REQ_SETAction 4
#define REQ_GETAllIPLogs 5
#define REQ_GETAllConns 6
#define REQ_ADDNATRule 7
#define REQ_DELNATRule 8
#define REQ_GETNATRules 9
#define RSP_Only_Head 10
#define RSP_MSG 11
#define RSP_IPRules 12
#define RSP_IPLogs 13
#define RSP_NATRules 14
#define RSP_ConnLogs 15
#define NAT_TYPE_NO 0
#define NAT_TYPE_SRC 1
#define NAT_TYPE_DEST 2

#define uint8_t unsigned char
#define NETLINK_MYFW 17
#define MAX_PAYLOAD (1024 * 256)
#define ERROR_CODE_EXIT -1
#define ERROR_CODE_EXCHANGE -2 // 与内核交换信息失败
#define ERROR_CODE_WRONG_IP -11 // 错误的IP格式
#define ERROR_CODE_NO_SUCH_RULE -12

QByteArray tmp;
int flag=0;

struct Common_Data{
    char name[10];
    char prior[10];
    char srcip[25];
    char dstip[25];
    char proto[10];
    unsigned int srcptl=0;
    unsigned int srcptr=0;
    unsigned int dstptl=0;
    unsigned int dstptr=0;
    unsigned int action=0;
    bool srcany;
    bool dstany;
};

struct rulelist{
    char name[10];
    char srcip[25];
    char dstip[25];
    char srcpt[15];
    char dstpt[15];
    char proto[10];
    char action[10];
};


struct natlist{
    char srcip[25];
    char natip[25];
    unsigned short natptr;
    unsigned short natptl;
};

struct loglist{
    char time[25];
    char action[10];
    char srcip[25];
    char dstip[25];
    char proto[10];
    unsigned int length;
};

struct conlist{
    char proto[10];
    char srcip[25];
    char dstip[25];
    char connect[15];
    int ifnat=0;
    char natsrcip[25];
    char natdstip[25];
};


struct IPRule {
    char name[10];
    unsigned int saddr;
    unsigned int smask;
    unsigned int daddr;
    unsigned int dmask;
    unsigned int sport; // 源端口范围（高2字节为最小值低2字节为最大值）
    unsigned int dport; // 目的端口范围
    u_int8_t protocol;
    unsigned int action;
    unsigned int log;
    struct IPRule* next;
};

struct IPLog {
    long tm;
    unsigned int saddr;
    unsigned int daddr;
    unsigned short sport;
    unsigned short dport;
    u_int8_t protocol;
    unsigned int len;
    unsigned int action;
    struct IPLog* next;
};

struct NATRule { // NAT规则：源IP端口转换
    unsigned int saddr; // 原始源IP
    unsigned int smask; // 原始源IP掩码
    unsigned int daddr; // NAT源IP

    unsigned short sport; // 最小端口
    unsigned short dport; // 最大端口
    unsigned short nowPort; // 当前使用端口
    struct NATRule* next;
};

struct ConnLog {
    unsigned int saddr;
    unsigned int daddr;
    unsigned short sport;
    unsigned short dport;
    u_int8_t protocol;
    int natType;
    struct NATRule nat; // NAT记录
};

struct UserReq {
    unsigned int tp;
    char ruleName[10];
    union {
        struct IPRule ipRule;
        struct NATRule natRule;
        unsigned int defaultAction;
        unsigned int num;
    } msg;
};

struct KerFunctionHeader {
    unsigned int bodyTp;
    unsigned int arrayLen;
};

struct KerFunction { //内核回应数据包
    int code; // 小于0代表请求失败，大于0为数据包长度
    void *data; // 回应包指针
    struct KerFunctionHeader *header; //指向数据包头部
    void *body; // 指向数据包中的Body
};


struct KerFunction exchangeMsgK(void *smsg, unsigned int slen); //内核交换数据结构

struct KerFunction addRule(char *after,char *name,char *sip,char *dip,unsigned int sport,unsigned int dport,u_int8_t proto,unsigned int log,unsigned int action);
// 新增过滤规则，端口范围（高2字节为最小值低2字节为最大值）
struct KerFunction delRule(char *name); //删除过滤规则
struct KerFunction getAllRules(void); //获取规则
struct KerFunction addNATRule(char *sip,char *natIP,unsigned short minport,unsigned short maxport); //添加NAT规则
struct KerFunction delNATRule(int num); //删除NAT规则
struct KerFunction getAllNATRules(void); //获取NAT规则
struct KerFunction setDefaultAction(unsigned int action); //默认规则
struct KerFunction getLogs(unsigned int num); // num=0获取所有日志
struct KerFunction getAllConns(void); //获取所有连接

int IPstr2IPint(const char *ipStr, unsigned int *ip, unsigned int *mask);
int IPint2IPstr(unsigned int ip, unsigned int mask, char *ipStr);
int IPint2IPstrNoMask(unsigned int ip, char *ipStr);
int IPint2IPstrWithPort(unsigned int ip, unsigned short port, char *ipStr);
int showRules(struct IPRule *rules, int len, struct rulelist *storage,int* storage_len);
int showNATRules(struct NATRule *rules, int len,struct natlist *storage, int *storage_len);
int showLogs(struct IPLog *logs, int len,struct loglist *storage, int *storage_len);
int showConns(struct ConnLog *logs, int len,struct conlist *storage, int *storage_len);

void dealResponseAtCmd(struct KerFunction rsp, char* response);

struct KerFunction exchangeMsgK(void *smsg, unsigned int slen) {
    struct sockaddr_nl local;
    struct sockaddr_nl kpeer;
    struct KerFunction rsp;
    int dlen, kpeerlen = sizeof(struct sockaddr_nl);
    // init socket
    int skfd = socket(PF_NETLINK, SOCK_RAW, NETLINK_MYFW);
    if (skfd < 0) {
        rsp.code = ERROR_CODE_EXCHANGE;
        return rsp;
    }
    // bind
    memset(&local, 0, sizeof(local));
    local.nl_family = AF_NETLINK;
    local.nl_pid = getpid();
    local.nl_groups = 0;
    if (bind(skfd, (struct sockaddr *) &local, sizeof(local)) != 0) {
        close(skfd);
        rsp.code = ERROR_CODE_EXCHANGE;
        return rsp;
    }
    memset(&kpeer, 0, sizeof(kpeer));
    kpeer.nl_family = AF_NETLINK;
    kpeer.nl_pid = 0;
    kpeer.nl_groups = 0;
    // set send msg
    struct nlmsghdr *message=(struct nlmsghdr *)malloc(NLMSG_SPACE(slen)*sizeof(uint8_t));
    if(!message) {
        close(skfd);
        rsp.code = ERROR_CODE_EXCHANGE;
        return rsp;
    }
    memset(message, '\0', sizeof(struct nlmsghdr));
    message->nlmsg_len = NLMSG_SPACE(slen);
    message->nlmsg_flags = 0;
    message->nlmsg_type = 0;
    message->nlmsg_seq = 0;
    message->nlmsg_pid = local.nl_pid;
    memcpy(NLMSG_DATA(message), smsg, slen);
    // send msg
    if (!sendto(skfd, message, message->nlmsg_len, 0, (struct sockaddr *) &kpeer, sizeof(kpeer))) {
        close(skfd);
        free(message);
        rsp.code = ERROR_CODE_EXCHANGE;
        return rsp;
    }
    // recv msg
    struct nlmsghdr *nlh = (struct nlmsghdr *)malloc(NLMSG_SPACE(MAX_PAYLOAD)*sizeof(uint8_t));
    if (!nlh) {
        close(skfd);
        free(message);
        rsp.code = ERROR_CODE_EXCHANGE;
        return rsp;
    }
    if (!recvfrom(skfd, nlh, NLMSG_SPACE(MAX_PAYLOAD), 0, (struct sockaddr *) &kpeer, (socklen_t *)&kpeerlen)) {
        close(skfd);
        free(message);
        free(nlh);
        rsp.code = ERROR_CODE_EXCHANGE;
        return rsp;
    }
    dlen = nlh->nlmsg_len - NLMSG_SPACE(0);
    rsp.data = malloc(dlen);
    if(!(rsp.data)) {
        close(skfd);
        free(message);
        free(nlh);
        rsp.code = ERROR_CODE_EXCHANGE;
        return rsp;
    }
    memset(rsp.data, 0, dlen);
    memcpy(rsp.data, NLMSG_DATA(nlh), dlen);
    rsp.code = dlen - sizeof(struct KerFunctionHeader);
    if(rsp.code < 0) {
        rsp.code = ERROR_CODE_EXCHANGE;
    }
    rsp.header = (struct KerFunctionHeader*)rsp.data;
    rsp.body = rsp.data + sizeof(struct KerFunctionHeader);
    // over
    close(skfd);
    free(message);
    free(nlh);
    //printf("kernel trans complete\n");
    return rsp;
}

struct KerFunction addRule(char *after,char *name,char *sip,char *dip,unsigned int sport,unsigned int dport,u_int8_t proto,unsigned int log,unsigned int action) {
    struct UserReq req;
    struct KerFunction rsp;
    // form rule
    struct IPRule rule;
    if(IPstr2IPint(sip,&rule.saddr,&rule.smask)!=0) {
        rsp.code = ERROR_CODE_WRONG_IP;
        return rsp;
    }
    if(IPstr2IPint(dip,&rule.daddr,&rule.dmask)!=0) {
        rsp.code = ERROR_CODE_WRONG_IP;
        return rsp;
    }
    rule.saddr = rule.saddr;
    rule.daddr = rule.daddr;
    rule.sport = sport;
    rule.dport = dport;
    rule.log = log;
    rule.action = action;
    rule.protocol = proto;
    strncpy(rule.name, name, 10);
    // form req
    req.tp = REQ_ADDIPRule;
    req.ruleName[0]=0;
    strncpy(req.ruleName, after, 10);
    req.msg.ipRule = rule;
    // exchange
    return exchangeMsgK(&req, sizeof(req));
}

struct KerFunction delRule(char *name) {
    struct UserReq req;
    // form request
    req.tp = REQ_DELIPRule;
    strncpy(req.ruleName, name, 10);
    // exchange
    return exchangeMsgK(&req, sizeof(req));
}

struct KerFunction getAllRules(void) {
    struct UserReq req;
    // exchange msg
    req.tp = REQ_GETAllIPRules;
    return exchangeMsgK(&req, sizeof(req));
}

struct KerFunction addNATRule(char *sip,char *natIP,unsigned short minport,unsigned short maxport) {
    struct UserReq req;
    struct KerFunction rsp;
    // form rule
    struct NATRule rule;
    if(IPstr2IPint(natIP,&rule.daddr,&rule.smask)!=0) {
        rsp.code = ERROR_CODE_WRONG_IP;
        return rsp;
    }
    if(IPstr2IPint(sip,&rule.saddr,&rule.smask)!=0) {
        rsp.code = ERROR_CODE_WRONG_IP;
        return rsp;
    }
    rule.sport = minport;
    rule.dport = maxport;
    // form req
    req.tp = REQ_ADDNATRule;
    req.msg.natRule = rule;
    // exchange
    return exchangeMsgK(&req, sizeof(req));
}

struct KerFunction delNATRule(int num) {
    struct UserReq req;
    struct KerFunction rsp;
    if(num < 0) {
        rsp.code = ERROR_CODE_NO_SUCH_RULE;
        return rsp;
    }
    req.tp = REQ_DELNATRule;
    req.msg.num = num;
    // exchange
    return exchangeMsgK(&req, sizeof(req));
}

struct KerFunction getAllNATRules(void) {
    struct UserReq req;
    // exchange msg
    req.tp = REQ_GETNATRules;
    return exchangeMsgK(&req, sizeof(req));
}

struct KerFunction setDefaultAction(unsigned int action) {
    struct UserReq req;
    // form request
    req.tp = REQ_SETAction;
    req.msg.defaultAction = action;
    // exchange
    return exchangeMsgK(&req, sizeof(req));
}

struct KerFunction getLogs(unsigned int num) {
    struct UserReq req;
    // exchange msg
    req.msg.num = num;
    req.tp = REQ_GETAllIPLogs;
    return exchangeMsgK(&req, sizeof(req));
}

struct KerFunction getAllConns(void) {
    struct UserReq req;
    // exchange msg
    req.tp = REQ_GETAllConns;
    return exchangeMsgK(&req, sizeof(req));
}

int IPstr2IPint(const char *ipStr, unsigned int *ip, unsigned int *mask){
    // init
    int p = -1, count = 0;
    unsigned int len = 0, tmp = 0, r_mask = 0, r_ip = 0,i;
    for(i = 0; i < strlen(ipStr); i++){
        if(!(ipStr[i]>='0' && ipStr[i]<='9') && ipStr[i]!='.' && ipStr[i]!='/') {
            return -1;
        }
    }
    // 获取掩码
    for(i = 0; i < strlen(ipStr); i++){
        if(p != -1){
            len *= 10;
            len += ipStr[i] - '0';
        }
        else if(ipStr[i] == '/')
            p = i;
    }
    if(len > 32 || (p>=0 && p<7)) {
        return -1;
    }
    if(p != -1){
        if(len)
            r_mask = 0xFFFFFFFF << (32 - len);
    }
    else r_mask = 0xFFFFFFFF;
    // 获取IP
    for(i = 0; i < (p>=0 ? p : strlen(ipStr)); i++){
        if(ipStr[i] == '.'){
            r_ip = r_ip | (tmp << (8 * (3 - count)));
            tmp = 0;
            count++;
            continue;
        }
        tmp *= 10;
        tmp += ipStr[i] - '0';
        if(tmp>256 || count>3)
            return -2;
    }
    r_ip = r_ip | tmp;
    *ip = r_ip;
    *mask = r_mask;
    return 0;
}

int IPint2IPstr(unsigned int ip, unsigned int mask, char *ipStr) {
    unsigned int i,ips[4],maskNum = 32;
    if(ipStr == NULL) {
        return -1;
    }
    if(mask == 0)
        maskNum = 0;
    else {
        while((mask & 1u) == 0) {
                    maskNum--;
                    mask >>= 1;
            }
    }
    for(i=0;i<4;i++) {
        ips[i] = ((ip >> ((3-i)*8)) & 0xFFU);
    }
    sprintf(ipStr, "%u.%u.%u.%u/%u", ips[0], ips[1], ips[2], ips[3], maskNum);
    return 0;
}

int IPint2IPstrNoMask(unsigned int ip, char *ipStr) {
    unsigned int i,ips[4];
    if(ipStr == NULL) {
        return -1;
    }
    for(i=0;i<4;i++) {
        ips[i] = ((ip >> ((3-i)*8)) & 0xFFU);
    }
    sprintf(ipStr, "%u.%u.%u.%u", ips[0], ips[1], ips[2], ips[3]);
    return 0;
}

int IPint2IPstrWithPort(unsigned int ip, unsigned short port, char *ipStr) {
    if(port == 0) {
        return IPint2IPstrNoMask(ip, ipStr);
    }
    unsigned int i,ips[4];
    if(ipStr == NULL) {
        return -1;
    }
    for(i=0;i<4;i++) {
        ips[i] = ((ip >> ((3-i)*8)) & 0xFFU);
    }
    sprintf(ipStr, "%u.%u.%u.%u:%u", ips[0], ips[1], ips[2], ips[3], port);
    return 0;
}

void dealResponseAtCmd(struct KerFunction rsp,char* response) {

    // 判断错误码
    switch (rsp.code) {
    case ERROR_CODE_EXIT:
        //exit(0);
        //printf("error occurs!\n");
        strcpy(response,"error occurs!");
        return;
    case ERROR_CODE_NO_SUCH_RULE:
        //printf("no such rule!\n");
        strcpy(response,"no such rule!");
        return;
    case ERROR_CODE_WRONG_IP:
        //printf("IP is wrong!.\n");
        strcpy(response,"IP is wrong!");
        return;
    }
    if(rsp.code < 0 || rsp.data == NULL || rsp.header == NULL || rsp.body == NULL){
        strcpy(response,"No rsp data!");
        return;
    }
    // 处理数据
    switch (rsp.header->bodyTp) {
    case RSP_Only_Head:
        //printf("succeed to delete %d rules.\n", rsp.header->arrayLen);
        sprintf(response, "succeed to delete %d rules.", rsp.header->arrayLen);
        break;
    case RSP_MSG:
        //printf("kernel operation: %s\n", (char*)rsp.body);
        sprintf(response, "kernel operation: %s.", (char*)rsp.body);
        break;
    case RSP_IPRules:
        //showRules((struct IPRule*)rsp.body, rsp.header->arrayLen);
        strcpy(response, "show rules.");
        break;
    case RSP_NATRules:
        //showNATRules((struct NATRule*)rsp.body, rsp.header->arrayLen);
        strcpy(response, "show  NAT rules.");
        break;
    case RSP_IPLogs:
        //showLogs((struct IPLog*)rsp.body, rsp.header->arrayLen);
        strcpy(response, "show IP logs.");
        break;
    case RSP_ConnLogs:
        //showConns((struct ConnLog*)rsp.body, rsp.header->arrayLen);
        strcpy(response, "show connect logs.");
        break;
    }
    if(rsp.header->bodyTp != RSP_Only_Head && rsp.body != NULL) {
        free(rsp.data);
    }
}

/*
void printLine(int len) {
    int i;
    for(i = 0; i < len; i++) {
        printf("-");
    }
    printf("\n");
}


int showOneRule(struct IPRule rule) {
    char saddr[25],daddr[25],sport[13],dport[13],proto[6],action[8],log[5];
    // ip
    IPint2IPstr(rule.saddr,rule.smask,saddr);
    IPint2IPstr(rule.daddr,rule.dmask,daddr);
    // port
    if(rule.sport == 0xFFFFu)
        strcpy(sport, "any");
    else if((rule.sport >> 16) == (rule.sport & 0xFFFFu))
        sprintf(sport, "only %u", (rule.sport >> 16));
    else
        sprintf(sport, "%u~%u", (rule.sport >> 16), (rule.sport & 0xFFFFu));
    if(rule.dport == 0xFFFFu)
        strcpy(dport, "any");
    else if((rule.dport >> 16) == (rule.dport & 0xFFFFu))
        sprintf(dport, "only %u", (rule.dport >> 16));
    else
        sprintf(dport, "%u~%u", (rule.dport >> 16), (rule.dport & 0xFFFFu));
    // action
    if(rule.action == NF_ACCEPT) {
        sprintf(action, "accept");
    } else if(rule.action == NF_DROP) {
        sprintf(action, "drop");
    } else {
        sprintf(action, "other");
    }
    // protocol
    if(rule.protocol == IPPROTO_TCP) {
        sprintf(proto, "TCP");
    } else if(rule.protocol == IPPROTO_UDP) {
        sprintf(proto, "UDP");
    } else if(rule.protocol == IPPROTO_ICMP) {
        sprintf(proto, "ICMP");
    } else if(rule.protocol == IPPROTO_IP) {
        sprintf(proto, "IP");
    } else {
        sprintf(proto, "other");
    }
    // log
    if(rule.log) {
        sprintf(log, "yes");
    } else {
        sprintf(log, "no");
    }
    // print
    printf("| %-*s | %-18s | %-18s | %-11s | %-11s | %-8s | %-6s | %-3s |\n", 10,
    rule.name, saddr, daddr, sport, dport, proto, action, log);
    printLine(111);
}



int showRules(struct IPRule *rules, int len) {
    int i;
    if(len == 0) {
        printf("No rules now.\n");
        return 0;
    }
    printLine(111);
    printf("| %-*s | %-18s | %-18s | %-11s | %-11s | %-8s | %-6s | %-3s |\n", 10,
     "name", "source ipaddr", "dest ipaddr", "source port", "target port", "protocol", "action", "log");
    printLine(111);
    for(i = 0; i < len; i++) {
        showOneRule(rules[i]);
    }
    return 0;
}

int showNATRules(struct NATRule *rules, int len) {
    int i, col = 66;
    char saddr[25],daddr[25];
    if(len == 0) {
        printf("No NAT rules now.\n");
        return 0;
    }
    printLine(col);
    printf("| seq | %18s |->| %-18s | %-11s |\n", "source ip", "NAT ip", "NAT port");
    printLine(col);
    for(i = 0; i < len; i++) {
        IPint2IPstr(rules[i].saddr,rules[i].smask,saddr);
        IPint2IPstrNoMask(rules[i].daddr,daddr);
        printf("| %3d | %18s |->| %-18s | %5u~%-5u |\n", i, saddr, daddr, rules[i].sport, rules[i].dport);
        printLine(col);
    }
    return 0;
}

int showOneLog(struct IPLog log) {
    struct tm * timeinfo;
    char saddr[25],daddr[25],proto[6],action[8],tm[21];
    // ip
    IPint2IPstrWithPort(log.saddr, log.sport, saddr);
    IPint2IPstrWithPort(log.daddr, log.dport, daddr);
    // action
    if(log.action == NF_ACCEPT) {
        sprintf(action, "[accept]");
    } else if(log.action == NF_DROP) {
        sprintf(action, "[drop]");
    } else {
        sprintf(action, "[unknown]");
    }
    // protocol
    if(log.protocol == IPPROTO_TCP) {
        sprintf(proto, "TCP");
    } else if(log.protocol == IPPROTO_UDP) {
        sprintf(proto, "UDP");
    } else if(log.protocol == IPPROTO_ICMP) {
        sprintf(proto, "ICMP");
    } else if(log.protocol == IPPROTO_IP) {
        sprintf(proto, "IP");
    } else {
        sprintf(proto, "other");
    }
    // time
    timeinfo = localtime(&log.tm);
    sprintf(tm, "%4d-%02d-%02d %02d:%02d:%02d",
        1900 + timeinfo->tm_year, 1 + timeinfo->tm_mon, timeinfo->tm_mday, timeinfo->tm_hour, timeinfo->tm_min, timeinfo->tm_sec);
    // print
    printf("[%s] %-9s %s->%s protocol=%s length=%uB\n",
        tm, action, saddr, daddr, proto, log.len);
}

int showLogs(struct IPLog *logs, int len) {
    int i;
    if(len == 0) {
        printf("No logs now.\n");
        return 0;
    }
    printf("sum: %d\n", len);
    for(i = 0; i < len; i++) {
        showOneLog(logs[i]);
    }
    return 0;
}

int showOneConn(struct ConnLog log) {
    struct tm * timeinfo;
    char saddr[25],daddr[25],proto[6];
    // ip
    IPint2IPstrWithPort(log.saddr,log.sport,saddr);
    IPint2IPstrWithPort(log.daddr,log.dport,daddr);
    // protocol
    if(log.protocol == IPPROTO_TCP) {
        sprintf(proto, "TCP");
    } else if(log.protocol == IPPROTO_UDP) {
        sprintf(proto, "UDP");
    } else if(log.protocol == IPPROTO_ICMP) {
        sprintf(proto, "ICMP");
    } else if(log.protocol == IPPROTO_IP) {
        sprintf(proto, "any");

    } else {
        sprintf(proto, "other");
    }
    printf("| %-11s |  %-21s |  %-21s | Established |\n",proto, saddr, daddr);
    if(log.natType == NAT_TYPE_SRC) {
        IPint2IPstrWithPort(log.nat.daddr, log.nat.dport, saddr);
        printf("| %-11s |=>%-21s |  %-21c | %11c |\n", "NAT", saddr, ' ', ' ');
    } else if(log.natType == NAT_TYPE_DEST) {
        IPint2IPstrWithPort(log.nat.daddr, log.nat.dport, daddr);
        printf("| %-11s |  %-21c |=>%-21s | %11c |\n", "NAT", ' ', daddr, ' ');
    }
}

int showConns(struct ConnLog *logs, int len) {
    int i, col = 79;
    if(len == 0) {
        printf("No connections now.\n");
        return 0;
    }
    printf("connection num: %d\n", len);
    printLine(col);
    printf("| %-11s |  %-21s |  %-21s | %11s |\n", "protocol", "source addr", "dest addr", "connect");
    printLine(col);
    for(i = 0; i < len; i++) {
        showOneConn(logs[i]);
    }
    printLine(col);
    return 0;
}

// 过滤规则时的用户交互
struct KerFunction cmdAddRule() {
    struct KerFunction empty;
    char after[10],name[10],saddr[25],daddr[25],sport[15],dport[15],protoS[6];
    unsigned short sportMin,sportMax,dportMin,dportMax;
    unsigned int action = NF_DROP, log = 0, proto, i;
    empty.code = ERROR_CODE_EXIT;
    // 前序规则名
    printf("rule sequence after name(input enter is the first one): ");
    for(i=0;;i++) {
        if(i>10) {
            printf("name is too long.\n");
            return empty;
        }
        after[i] = getchar();
        if(after[i] == '\n' || after[i] == '\r') {
            after[i] = '\0';
            break;
        }
    }
    // 规则名
    printf("input rule name (less than %d): ", 10);
    scanf("%s",name);
    if(strlen(name)==0 || strlen(name)>10) {
        printf("name is too long or too short.\n");
        return empty;
    }
    // 源IP
    printf("input source ip (eg: 11.4.5.14) or with mask (eg: 11.4.5.14/32): ");
    scanf("%s",saddr);
    // 源端口
    printf("input source port range (eg: 114-514) or any: ");
    scanf("%s",sport);
    if(strcmp(sport, "any") == 0) {
        sportMin = 0,sportMax = 0xFFFFu;
    } else {
        sscanf(sport,"%hu-%hu",&sportMin,&sportMax);
    }
    if(sportMin > sportMax) {
        printf("wrong input!\n");
        return empty;
    }
    // 目的IP
    printf("input dest ip (eg: 11.4.5.14) or with mask (eg: 11.4.5.14/32): ");
    scanf("%s",daddr);
    // 目的端口
    printf("input dest port range (eg: 114-514) or any: ");
    scanf("%s",dport);
    if(strcmp(dport, "any") == 0) {
        dportMin = 0,dportMax = 0xFFFFu;
    } else {
        sscanf(dport,"%hu-%hu",&dportMin,&dportMax);
    }
    if(dportMin > dportMax) {
        printf("wrong input!\n");
        return empty;
    }
    // 协议
    printf("input protocol (TCP/UDP/ICMP/any): ");
    scanf("%s",protoS);
    if(strcmp(protoS,"TCP")==0)
        proto = IPPROTO_TCP;
    else if(strcmp(protoS,"UDP")==0)
        proto = IPPROTO_UDP;
    else if(strcmp(protoS,"ICMP")==0)
        proto = IPPROTO_ICMP;
    else if(strcmp(protoS,"any")==0)
        proto = IPPROTO_IP;
    else {
        printf("wrong input!\n");
        return empty;
    }
    // 动作
    printf("input action (0: drop 1: accept): ");
    scanf("%d",&action);
    // 是否记录日志
    printf("record log? (1:yes 0: no): ");
    scanf("%u",&log);
    return addRule(after,name,saddr,daddr,
        (((unsigned int)sportMin << 16) | (((unsigned int)sportMax) & 0xFFFFu)),
        (((unsigned int)dportMin << 16) | (((unsigned int)dportMax) & 0xFFFFu)),proto,log,action);
}

struct KerFunction cmdAddNATRule() {
    struct KerFunction empty;
    char saddr[25],daddr[25],port[15];
    unsigned short portMin,portMax;
    empty.code = ERROR_CODE_EXIT;
    printf("only for source nat\n");
    // 源IP
    printf("input source ip and mask (eg: 11.4.5.14/32): ");
    scanf("%s",saddr);
    // NAT IP
    printf("input NAT ip (eg: 192.168.19.19): ");
    scanf("%s",daddr);
    // 目的端口
    printf("input NAT port range (eg: 114-514) or any: ");
    scanf("%s",port);
    if(strcmp(port, "any") == 0) {
        portMin = 0,portMax = 0xFFFFu;
    } else {
        sscanf(port,"%hu-%hu",&portMin,&portMax);
    }
    if(portMin > portMax) {
        printf("the min port > max port.\n");
        return empty;
    }
    return addNATRule(saddr,daddr,portMin,portMax);
}

void ChooseOption(){
    int op,num;
    char str[11];
    printf("firewall functions and commands are here:\n");
    printf("1. add rule: 	   ./firewall add rule\n");
    printf("2. delete rule:    ./firewall del rule name\n");
    printf("3. default rule:   ./firewall default rule accept/drop\n");
    printf("4. list rules:     ./firewall ls rule \n");
    printf("5. add nat rule:   ./firewall add nat\n");
    printf("6. del nat rule:   ./firewall del nat name\n");
    printf("7. list nat rules: ./firewall ls nat\n");
    printf("8. list logs: 	   ./firewall ls log (num)\n");
    printf("9. list connects:  ./firewall ls connect\n");
    printf("please input function number(other number exit)!\n");

        struct KerFunction rsp;
        rsp.code = ERROR_CODE_EXIT;
        scanf("%d",&op);
        getchar();
        switch (op){
            case 1:
                rsp = cmdAddRule();
                dealResponseAtCmd(rsp);
                break;

            case 2:
                printf("input rule name:\n");
                scanf("%s",str);
                getchar();
                if(strlen(str)>10) printf("rule name is too long!");
                else rsp = delRule(str);
                dealResponseAtCmd(rsp);
                break;

            case 3:
                printf("input default rule(accept or drop):\n");
                scanf("%s",str);
                getchar();
                if(strcmp(str, "accept")==0) rsp = setDefaultAction(NF_ACCEPT);
                else if(strcmp(str, "drop")==0) rsp = setDefaultAction(NF_DROP);
                else printf("please input \"accept\" or \"drop\".\n");
                dealResponseAtCmd(rsp);
                break;

            case 4:
                rsp = getAllRules();
                dealResponseAtCmd(rsp);
                break;

            case 5:
                rsp = cmdAddNATRule();
                dealResponseAtCmd(rsp);
                break;

            case 6:
                printf("input rule number:\n");
                scanf("%d", &num);
                getchar();
                rsp = delNATRule(num);
                dealResponseAtCmd(rsp);
                break;

            case 7:
                rsp = getAllNATRules();
                dealResponseAtCmd(rsp);
                break;

            case 8:
                printf("input list log number(0: all):\n");
                scanf("%d", &num);
                getchar();
                if(num>=0) rsp = getLogs(num);
                dealResponseAtCmd(rsp);
                break;

            case 9:
                rsp = getAllConns();
                dealResponseAtCmd(rsp);
                break;

            default:
                dealResponseAtCmd(rsp);
                break;

        }
    exit(0);
}
*/
void default_accept(char* response){
    struct KerFunction rsp;
    rsp.code = ERROR_CODE_EXIT;
    rsp=setDefaultAction(NF_ACCEPT);
    dealResponseAtCmd(rsp,response);
}

void default_drop(char* response){
    struct KerFunction rsp;
    rsp.code = ERROR_CODE_EXIT;
    rsp=setDefaultAction(NF_DROP);
    dealResponseAtCmd(rsp,response);
}

void add_rule(struct Common_Data trans, char* response){

    FirewallWidget *fire=new FirewallWidget;
    struct UserReq req;
    struct KerFunction rsp;
    struct IPRule rule;
    rsp.code = ERROR_CODE_EXIT;

    //名称是否合理？
    if(strlen(trans.name)==0){
        fire->feedback("add_rule option: rule name is wrong!");
        //printf("name is wrong!\n");
        return;
    }
    strncpy(rule.name, trans.name,10);
    //ip操作

    if(IPstr2IPint(trans.srcip,&rule.saddr,&rule.smask)!=0) {
        fire->feedback("add_rule option: src ip or mask is wrong!");
        //printf("input src ip or mask is wrong!\n");
        return;
    }
    if(IPstr2IPint(trans.dstip,&rule.daddr,&rule.dmask)!=0) {
        fire->feedback("add_rule option: dst ip or mask is wrong!");
        //printf("dst ip or mask is wrong!\n");
        return;
    }

    //端口操作
    if(trans.srcany==false){   //是否屏蔽所有端口
        if((trans.srcptl>trans.srcptr)||(trans.srcptl<0||trans.srcptl>65535)||(trans.srcptr<=0||trans.srcptr>65535)){
            fire->feedback("add_rule option: src port is wrong!");
            //printf("src port is wrong!\n");
            return;
        }
    }else{trans.srcptl=0;trans.srcptr=0xFFFFu;}
    rule.sport=((unsigned int)trans.srcptl << 16) | (((unsigned int)trans.srcptr) & 0xFFFFu);

    if(trans.dstany==false){   //是否屏蔽所有端口
        if((trans.dstptl>trans.dstptr)||(trans.dstptl<0||trans.dstptl>65535)||(trans.dstptr<=0||trans.dstptr>65535)){
            fire->feedback("add_rule option: dst port is wrong!");
            //printf("dst port is wrong!\n");
            return;
        }
    }else{trans.dstptl=0;trans.dstptr=0xFFFFu;}
    rule.dport=(trans.dstptl << 16) | (trans.dstptr & 0xFFFFu);

    //协议操作
    if(strcmp(trans.proto,"TCP")==0)
        rule.protocol= IPPROTO_TCP;
    else if(strcmp(trans.proto,"UDP")==0)
        rule.protocol = IPPROTO_UDP;
    else if(strcmp(trans.proto,"ICMP")==0)
        rule.protocol= IPPROTO_ICMP;
    else if(strcmp(trans.proto,"any")==0)
        rule.protocol = IPPROTO_IP;
    else {
        fire->feedback("add_rule option: protocol wrong input!");
        return;
    }

    //拒绝或允许
    rule.action = trans.action;
    //默认都记录日志
    rule.log=1;
    //构建req包和顺序操作
    req.tp = REQ_ADDIPRule;
    req.ruleName[0]=0;
    strncpy(req.ruleName, trans.prior,10);
    req.msg.ipRule = rule;
    //发送给内核
    rsp=exchangeMsgK(&req, sizeof(req));
    dealResponseAtCmd(rsp,response);
    return;
}

int showRules(struct IPRule *rules, int len, struct rulelist *storage,int* storage_len) {
    int i;
    if(len == 0) {
        //printf("No rules now.\n");
        return 1;
    }
    *storage_len=len;
    for(i = 0; i < len; i++) {

    strcpy((storage+i)->name,rules[i].name);
    // ip
    IPint2IPstr(rules[i].saddr,rules[i].smask,(storage+i)->srcip);
    IPint2IPstr(rules[i].daddr,rules[i].dmask,(storage+i)->dstip);
    // port
    if(rules[i].sport == 0xFFFFu)
        strcpy((storage+i)->srcpt, "any");
    else if((rules[i].sport >> 16) == (rules[i].sport & 0xFFFFu))
        sprintf((storage+i)->srcpt, "only %u", (rules[i].sport >> 16));
    else
        sprintf((storage+i)->srcpt, "%u~%u", (rules[i].sport >> 16), (rules[i].sport & 0xFFFFu));
    if(rules[i].dport == 0xFFFFu)
        strcpy((storage+i)->dstpt, "any");
    else if((rules[i].dport >> 16) == (rules[i].dport & 0xFFFFu))
        sprintf((storage+i)->dstpt, "only %u", (rules[i].dport >> 16));
    else
        sprintf((storage+i)->dstpt, "%u~%u", (rules[i].dport >> 16), (rules[i].dport & 0xFFFFu));
    // action
    if(rules[i].action == NF_ACCEPT) {
        sprintf((storage+i)->action, "accept");
    } else if(rules[i].action == NF_DROP) {
        sprintf((storage+i)->action, "drop");
    } else {
        sprintf((storage+i)->action, "other");
    }
    // protocol
    if(rules[i].protocol == IPPROTO_TCP) {
        sprintf((storage+i)->proto, "TCP");
    } else if(rules[i].protocol == IPPROTO_UDP) {
        sprintf((storage+i)->proto, "UDP");
    } else if(rules[i].protocol == IPPROTO_ICMP) {
        sprintf((storage+i)->proto, "ICMP");
    } else if(rules[i].protocol == IPPROTO_IP) {
        sprintf((storage+i)->proto, "IP");
    } else {
        sprintf((storage+i)->proto, "other");
    }

    }
    return 0;
}


void del_rule(char* name, char* response){
    struct KerFunction rsp;
    struct UserReq req;
    rsp.code = ERROR_CODE_EXIT;
    // form request
    req.tp = REQ_DELIPRule;
    strncpy(req.ruleName, name, 10);
    // exchange
    rsp=exchangeMsgK(&req, sizeof(req));
    dealResponseAtCmd(rsp,response);
    return;
}

void add_nat(struct Common_Data trans, char* response){
    
    FirewallWidget *fire=new FirewallWidget;
    struct UserReq req;
    struct KerFunction rsp;
    struct NATRule rule;
    rsp.code = ERROR_CODE_EXIT;

    //ip操作
    if(IPstr2IPint(trans.srcip,&rule.saddr,&rule.smask)!=0) {
        //fire->feedback("add_nat option: src ip or mask is wrong!");
        strcpy(response,"add_nat option: src ip or mask is wrong!");
        return;
    }
    if(IPstr2IPint(trans.dstip,&rule.daddr,&rule.smask)!=0) {
        //fire->feedback("add_nat option: dst(nat) ip or mask is wrong!");
        strcpy(response,"add_nat option: dst(nat) ip or mask is wrong!");
        return;
    }
 
    //端口操作
    if(trans.dstany==false){   //是否屏蔽所有端口
        if((trans.dstptl>trans.dstptr)||(trans.dstptl<=0||trans.dstptl>65535)||(trans.dstptr<=0||trans.dstptr>65535)){
            //fire->feedback("add_nat option: nat port is wrong!");
            strcpy(response,"add_nat option: nat port is wrong!");
            return;
        }
    }else{trans.dstptl=0;trans.dstptr=0xFFFFu;}
    rule.sport = trans.dstptl;
    rule.dport = trans.dstptr;
    
    //构建req包和顺序操作
    // form req
    req.tp = REQ_ADDNATRule;
    req.msg.natRule = rule;
    // exchange
    rsp=exchangeMsgK(&req, sizeof(req));
    dealResponseAtCmd(rsp,response);
    return;

}

int showNATRules(struct NATRule *rules, int len, struct natlist *storage, int *storage_len) {

    int i;
    if(len == 0) {
        //printf("No NAT rules now.\n");
        return 1;
    }
    *storage_len=len;
    for(i = 0; i < len; i++) {
        IPint2IPstr(rules[i].saddr,rules[i].smask,(storage+i)->srcip);
        IPint2IPstrNoMask(rules[i].daddr,(storage+i)->natip);
        (storage+i)->natptl=rules[i].sport;
        (storage+i)->natptr=rules[i].dport;
    }
    return 0;
}

void del_nat(int num,char* response){
    FirewallWidget *fire=new FirewallWidget;
    struct KerFunction rsp;
    struct UserReq req;
    rsp.code = ERROR_CODE_EXIT;
    if(num < 0) {
        //fire->feedback("del_nat option: no such rule!");
        strcpy(response,"del_nat option: no such rule!");
        return;
    }
    req.tp = REQ_DELNATRule;
    req.msg.num = num;
    rsp=exchangeMsgK(&req, sizeof(req));
    dealResponseAtCmd(rsp,response);
    return;
}

int showLogs(struct IPLog *logs, int len, struct loglist* storage, int* storage_len) {
    int i;
    if(len == 0) {
        //printf("No logs now.\n");
        return 1;
    }
    //printf("sum: %d\n", len);
     *storage_len=len;
    for(i = 0; i < len; i++) {
        struct tm * timeinfo;
        //char saddr[25],daddr[25],proto[6],action[8],tm[21];
        // ip
        IPint2IPstrWithPort(logs[i].saddr, logs[i].sport, (storage+i)->srcip);
        IPint2IPstrWithPort(logs[i].daddr, logs[i].dport, (storage+i)->dstip);
        // action
        if(logs[i].action == NF_ACCEPT) {
            sprintf((storage+i)->action, "[accept]");
        } else if(logs[i].action == NF_DROP) {
            sprintf((storage+i)->action, "[drop]");
        } else {
            sprintf((storage+i)->action, "[unknown]");
        }
        // protocol
        if(logs[i].protocol == IPPROTO_TCP) {
            sprintf((storage+i)->proto, "TCP");
        } else if(logs[i].protocol == IPPROTO_UDP) {
            sprintf((storage+i)->proto, "UDP");
        } else if(logs[i].protocol == IPPROTO_ICMP) {
            sprintf((storage+i)->proto, "ICMP");
        } else if(logs[i].protocol == IPPROTO_IP) {
            sprintf((storage+i)->proto, "IP");
        } else {
            sprintf((storage+i)->proto, "other");
        }
        // time
        timeinfo = localtime(&logs[i].tm);
        sprintf((storage+i)->time, "%4d-%02d-%02d %02d:%02d:%02d",
            1900 + timeinfo->tm_year, 1 + timeinfo->tm_mon, timeinfo->tm_mday, timeinfo->tm_hour, timeinfo->tm_min, timeinfo->tm_sec);
        (storage+i)->length=logs[i].len;
    }
    return 0;
}

int showConns(struct ConnLog *logs, int len, struct conlist *storage, int *storage_len) {
    int i;
    if(len == 0) {
        //printf("No connections now.\n");
        return 1;
    }
    *storage_len=len;
    for(i = 0; i < len; i++) {
        // ip
        IPint2IPstrWithPort(logs[i].saddr,logs[i].sport,(storage+i)->srcip);
        IPint2IPstrWithPort(logs[i].daddr,logs[i].dport,(storage+i)->dstip);
        // protocol
        if(logs[i].protocol == IPPROTO_TCP) {
            sprintf((storage+i)->proto, "TCP");
        } else if(logs[i].protocol == IPPROTO_UDP) {
            sprintf((storage+i)->proto, "UDP");
        } else if(logs[i].protocol == IPPROTO_ICMP) {
            sprintf((storage+i)->proto, "ICMP");
        } else if(logs[i].protocol == IPPROTO_IP) {
            sprintf((storage+i)->proto, "any");

        } else {
            sprintf((storage+i)->proto, "other");
        }
        sprintf((storage+i)->connect,"Established");
        if(logs[i].natType == NAT_TYPE_SRC) {
            IPint2IPstrWithPort(logs[i].nat.daddr, logs[i].nat.dport, (storage+i)->natsrcip);
            (storage+i)->ifnat=1;
        } else if(logs[i].natType == NAT_TYPE_DEST) {
            IPint2IPstrWithPort(logs[i].nat.daddr, logs[i].nat.dport, (storage+i)->natdstip);
            (storage+i)->ifnat=1;
        } else (storage+i)->ifnat=0;
    }
    return 0;
}


}


FirewallWidget::FirewallWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::FirewallWidget)
{
    ui->setupUi(this);
    setWindowFlags(Qt::FramelessWindowHint);   //去掉边框
    setAttribute(Qt::WA_StyledBackground);
    initwindow();
    option();
}

FirewallWidget::~FirewallWidget()
{
    delete ui;
}

void FirewallWidget::initwindow(){
    QStringList proto,action;
    proto<<"ICMP"<<"UDP"<<"TCP"<<"any";
    ui->proto->addItems(proto);
    ui->proto->setCurrentIndex(3);
    action<<"drop"<<"accept";
    ui->action->addItems(action);
    ui->action->setCurrentIndex(0);
    // ui->comboBox->currentText();获取当前内容
    //IP输入框正则
    QRegExp regExp("^((2[0-4]\\d|25[0-5]|[01]?\\d\\d?)\\.){3}(2[0-4]\\d|25[0-5]|[01]?\\d\\d?)$");
    QValidator *validator_src = new QRegExpValidator(regExp, ui->srcip);
    ui->srcip->setValidator(validator_src);
    QValidator *validator_dst = new QRegExpValidator(regExp, ui->dstip);
    ui->dstip->setValidator(validator_dst);
    ui->srcptl->setValidator(new QIntValidator(0,65536,ui->srcptl));
    ui->dstptl->setValidator(new QIntValidator(0,65536,ui->dstptl));
    ui->srcptr->setValidator(new QIntValidator(0,65536,ui->srcptr));
    ui->dstptr->setValidator(new QIntValidator(0,65536,ui->dstptr));
    ui->srcmk->setValidator(new QIntValidator(0,32,ui->srcmk));
    ui->dstmk->setValidator(new QIntValidator(0,32,ui->dstmk));
    ui->name->setMaxLength(9);
    ui->prior->setMaxLength(9);
}


void FirewallWidget::option(){

    connect(ui->open_firewall,&QPushButton::clicked,this,[=](){
        open_firewall();
    });
    connect(ui->close_firewall,&QPushButton::clicked,this,[=](){
        close_firewall();
    });
    connect(ui->default_drop,&QPushButton::clicked,this,[=](){
        if(flag){
        char response[100];
        default_drop(response);
        feedback(response);
        }
        else feedback("firewall doesn't start");
    });
    connect(ui->default_accept,&QPushButton::clicked,this,[=](){
        if(flag){
        char response[100];
        default_accept(response);
        feedback(response);
        }
        else feedback("firewall doesn't start");
    });
    connect(ui->add_rule,&QPushButton::clicked,this,[=](){
        if(flag){
        struct Common_Data trans;
        //格式转化数据项
        if(!ui->name->text().isEmpty()){
            tmp=ui->name->text().toLatin1();
            strcpy(trans.name,tmp.data());
        }else strcpy(trans.name,"\0");
        
        if(!ui->prior->text().isEmpty()){
            tmp=ui->prior->text().toLatin1();
            strcpy(trans.prior,tmp.data());
        }else strcpy(trans.prior,"\0");
        if(!ui->srcip->text().isEmpty()){
            tmp=ui->srcip->text().toLatin1();
            strcpy(trans.srcip,tmp.data());
        }
        if(!ui->dstip->text().isEmpty()){
            tmp=ui->dstip->text().toLatin1();
            strcpy(trans.dstip,tmp.data());
        }
        if((!ui->srcmk->text().isEmpty())&&(!ui->srcip->text().isEmpty())){
            tmp=ui->srcmk->text().toLatin1();
            strcat(trans.srcip,"/");
            strcat(trans.srcip,tmp.data());
        }
        if((!ui->dstmk->text().isEmpty())&&(!ui->dstip->text().isEmpty())){
            tmp=ui->dstmk->text().toLatin1();
            strcat(trans.dstip,"/");
            strcat(trans.dstip,tmp.data());
        }
        if(!ui->srcptl->text().isEmpty())trans.srcptl=ui->srcptl->text().toUInt();
        if(!ui->srcptr->text().isEmpty())trans.srcptr=ui->srcptr->text().toUInt();
        if(!ui->dstptl->text().isEmpty())trans.dstptl=ui->dstptl->text().toUInt();
        if(!ui->dstptr->text().isEmpty())trans.dstptr=ui->dstptr->text().toUInt();
        tmp=ui->proto->currentText().toLatin1();
        strcpy(trans.proto,tmp.data());
        if(ui->action->currentIndex()==0)trans.action=0;
        else trans.action=1;
        trans.srcany=ui->srcpt_any->isChecked();
        trans.dstany=ui->dstpt_any->isChecked();

        char response[100];
        add_rule(trans,response);
        feedback(response);
        }
        else feedback("firewall doesn't start");

    });

    connect(ui->del_rule,&QPushButton::clicked,this,[=](){
        if(flag){
        struct Common_Data trans;
        if(!ui->name->text().isEmpty()){
            tmp=ui->name->text().toLatin1();
            strcpy(trans.name,tmp.data());
        }
        char response[100];
        del_rule(trans.name,response);
        feedback(response);
        }
        else feedback("firewall doesn't start");
    });
    connect(ui->list_rule,&QPushButton::clicked,this,[=](){
        if(flag) list_rule();
        else feedback("firewall doesn't start");
    });
    connect(ui->add_nat,&QPushButton::clicked,this,[=](){
        if(flag){
        struct Common_Data trans;
        //格式转化数据项
        if(!ui->srcip->text().isEmpty()){
            tmp=ui->srcip->text().toLatin1();
            strcpy(trans.srcip,tmp.data());
        }
        if(!ui->dstip->text().isEmpty()){
            tmp=ui->dstip->text().toLatin1();
            strcpy(trans.dstip,tmp.data());
        }
        if((!ui->srcmk->text().isEmpty())&&(!ui->srcip->text().isEmpty())){
            tmp=ui->srcmk->text().toLatin1();
            strcat(trans.srcip,"/");
            strcat(trans.srcip,tmp.data());
        }
        if((!ui->dstmk->text().isEmpty())&&(!ui->dstip->text().isEmpty())){
            tmp=ui->dstmk->text().toLatin1();
            strcat(trans.dstip,"/");
            strcat(trans.dstip,tmp.data());
        }
        if(!ui->dstptl->text().isEmpty())trans.dstptl=ui->dstptl->text().toUInt();
        if(!ui->dstptr->text().isEmpty())trans.dstptr=ui->dstptr->text().toUInt();
        trans.dstany=ui->dstpt_any->isChecked();

        char response[100];
        add_nat(trans,response);
        feedback(response);

        }
        else feedback("firewall doesn't start");
    });
    connect(ui->list_nat,&QPushButton::clicked,this,[=](){
        if(flag){
        list_nat();
        }
        else feedback("firewall doesn't start");
    });
    connect(ui->del_nat,&QPushButton::clicked,this,[=](){
        if(flag){
        char response[100];
        del_nat(ui->name->text().toInt(),response);
        feedback(response);
        }
        else feedback("firewall doesn't start");
    });
    connect(ui->list_log,&QPushButton::clicked,this,[=](){
        if(flag){
        list_log(ui->name->text().toInt());
        }
        else feedback("firewall doesn't start");
    });
    connect(ui->list_conn,&QPushButton::clicked,this,[=](){
        if(flag){
        list_connect();
        }
        else feedback("firewall doesn't start");
    });
}

void FirewallWidget::open_firewall(){
    system("make");
    system("make install");
    feedback("start firewall!");
    flag=1;
}

void FirewallWidget::close_firewall(){
    system("rmmod firewall");
    system("make clean");
    feedback("close firewall!");
    flag=0;
}


void FirewallWidget::list_rule(){

    QStandardItemModel *rule_table = new QStandardItemModel();
    rule_table->setHorizontalHeaderItem(0, new QStandardItem(QObject::tr("Name")));
    rule_table->setHorizontalHeaderItem(1, new QStandardItem(QObject::tr("Src_ipaddr")));
    rule_table->setHorizontalHeaderItem(2, new QStandardItem(QObject::tr("Dst_ipaddr")));
    rule_table->setHorizontalHeaderItem(3, new QStandardItem(QObject::tr("Src_port")));
    rule_table->setHorizontalHeaderItem(4, new QStandardItem(QObject::tr("Dst_port")));
    rule_table->setHorizontalHeaderItem(5, new QStandardItem(QObject::tr("Protocol")));
    rule_table->setHorizontalHeaderItem(6, new QStandardItem(QObject::tr("Action")));
    //利用setModel()方法将数据模型与QTableView绑定
    ui->tableView->setModel(rule_table);

    for(int i=0;i<7;i++){ //固定列宽
        ui->tableView->horizontalHeader()->setSectionResizeMode(i, QHeaderView::Fixed);
    }
    //设置表格的各列的宽度值
    ui->tableView->setColumnWidth(0,120);
    ui->tableView->setColumnWidth(1,200);
    ui->tableView->setColumnWidth(2,200);
    ui->tableView->setColumnWidth(3,100);
    ui->tableView->setColumnWidth(4,100);
    ui->tableView->setColumnWidth(5,80);
    ui->tableView->setColumnWidth(6,80);
    //设置选中时为整行选中
    ui->tableView->setSelectionBehavior(QAbstractItemView::SelectRows);
    //设置表格的单元为只读属性，即不能编辑
    ui->tableView->setEditTriggers(QAbstractItemView::NoEditTriggers);

        
    //获取数据
    struct UserReq req;
    struct KerFunction rsp;
    struct rulelist list[100],*storage;
    storage=list;
    int storage_len=0;
    int re;
    
    req.tp = REQ_GETAllIPRules;
    rsp.code = ERROR_CODE_EXIT;
    rsp=exchangeMsgK(&req, sizeof(req));
    //rule=((struct IPRule*)rsp.body);
    re=showRules((struct IPRule*)rsp.body, rsp.header->arrayLen,storage,&storage_len);
    if(!re)feedback("No rule now");
    //显示内容
    if(storage_len){
        for(int i=0;i<storage_len;i++){
            rule_table->setItem(i,0,new QStandardItem(list[i].name));
            rule_table->setItem(i,1,new QStandardItem(list[i].srcip));
            rule_table->setItem(i,2,new QStandardItem(list[i].dstip));
            rule_table->setItem(i,3,new QStandardItem(list[i].srcpt));
            rule_table->setItem(i,4,new QStandardItem(list[i].dstpt));
            rule_table->setItem(i,5,new QStandardItem(list[i].proto));
            rule_table->setItem(i,6,new QStandardItem(list[i].action));
        }
    }

}

void FirewallWidget::list_nat(){

    QStandardItemModel *rule_table = new QStandardItemModel();
    rule_table->setHorizontalHeaderItem(0, new QStandardItem(QObject::tr("Seq")));
    rule_table->setHorizontalHeaderItem(1, new QStandardItem(QObject::tr("Src_ipaddr")));
    rule_table->setHorizontalHeaderItem(2, new QStandardItem(QObject::tr("NAT_ipaddr")));
    rule_table->setHorizontalHeaderItem(3, new QStandardItem(QObject::tr("NAT_port")));
    //利用setModel()方法将数据模型与QTableView绑定
    ui->tableView->setModel(rule_table);

    for(int i=0;i<4;i++){ //固定列宽
        ui->tableView->horizontalHeader()->setSectionResizeMode(i, QHeaderView::Fixed);
    }
    //设置表格的各列的宽度值
    ui->tableView->setColumnWidth(0,100);
    ui->tableView->setColumnWidth(1,200);
    ui->tableView->setColumnWidth(2,200);
    ui->tableView->setColumnWidth(3,150);
    //设置选中时为整行选中
    ui->tableView->setSelectionBehavior(QAbstractItemView::SelectRows);
    //设置表格的单元为只读属性，即不能编辑
    ui->tableView->setEditTriggers(QAbstractItemView::NoEditTriggers);


    //获取数据
    struct UserReq req;
    struct KerFunction rsp;
    struct natlist list[100], *storage;
    storage=list;
    int storage_len=0;
    int re;
    
    req.tp = REQ_GETNATRules;
    rsp.code = ERROR_CODE_EXIT;
    rsp=exchangeMsgK(&req, sizeof(req));
    //rule=((struct IPRule*)rsp.body);
    re=showNATRules((struct NATRule*)rsp.body, rsp.header->arrayLen,storage,&storage_len);
    if(re)feedback("No NAT rule now");
    
    QString str;
    //显示内容
    if(storage_len){
        for(int i=0;i<storage_len;i++){
            str=QString::number(list[i].natptl)+"~"+QString::number(list[i].natptr);
            rule_table->setItem(i,0,new QStandardItem(QString::number(i)));
            rule_table->setItem(i,1,new QStandardItem(list[i].srcip));
            rule_table->setItem(i,2,new QStandardItem(list[i].natip));
            rule_table->setItem(i,3,new QStandardItem(str));
        }
    }

}

void FirewallWidget::list_log(int num){

    QStandardItemModel *rule_table = new QStandardItemModel();
    rule_table->setHorizontalHeaderItem(0, new QStandardItem(QObject::tr("Time")));
    rule_table->setHorizontalHeaderItem(1, new QStandardItem(QObject::tr("Action")));
    rule_table->setHorizontalHeaderItem(2, new QStandardItem(QObject::tr("Src_ipaddr")));
    rule_table->setHorizontalHeaderItem(3, new QStandardItem(QObject::tr("Dst_ipaddr")));
    rule_table->setHorizontalHeaderItem(4, new QStandardItem(QObject::tr("Protocol")));
    rule_table->setHorizontalHeaderItem(5, new QStandardItem(QObject::tr("Length(Bytes)")));
    //利用setModel()方法将数据模型与QTableView绑定
    ui->tableView->setModel(rule_table);

    for(int i=0;i<6;i++){ //固定列宽
        ui->tableView->horizontalHeader()->setSectionResizeMode(i, QHeaderView::Fixed);
    }
    //设置表格的各列的宽度值
    ui->tableView->setColumnWidth(0,200);
    ui->tableView->setColumnWidth(1,60);
    ui->tableView->setColumnWidth(2,200);
    ui->tableView->setColumnWidth(3,200);
    ui->tableView->setColumnWidth(4,80);
    ui->tableView->setColumnWidth(5,80);
    //设置选中时为整行选中
    ui->tableView->setSelectionBehavior(QAbstractItemView::SelectRows);
    //设置表格的单元为只读属性，即不能编辑
    ui->tableView->setEditTriggers(QAbstractItemView::NoEditTriggers);


    //获取数据
    struct UserReq req;
    struct KerFunction rsp;
    struct loglist list[100], *storage;
    storage=list;
    int storage_len=0;
    int re;
    
    // exchange msg
    if(num<0)num=0;
    req.msg.num = num;
    req.tp = REQ_GETAllIPLogs;
    rsp.code = ERROR_CODE_EXIT;
    rsp=exchangeMsgK(&req, sizeof(req));
    re=showLogs((struct IPLog*)rsp.body, rsp.header->arrayLen,storage,&storage_len);
    if(re)feedback("No log now");
    //printf("trans complete!\n");
    //显示内容
    if(storage_len){
        for(int i=0;i<storage_len;i++){
            rule_table->setItem(i,0,new QStandardItem(list[i].time));
            rule_table->setItem(i,1,new QStandardItem(list[i].action));
            rule_table->setItem(i,2,new QStandardItem(list[i].srcip));
            rule_table->setItem(i,3,new QStandardItem(list[i].dstip));
            rule_table->setItem(i,4,new QStandardItem(list[i].proto));
            rule_table->setItem(i,5,new QStandardItem(list[i].length));
        }
    }

}

void FirewallWidget::list_connect(){ 

    QStandardItemModel *rule_table = new QStandardItemModel();
    rule_table->setHorizontalHeaderItem(0, new QStandardItem(QObject::tr("Protocol")));
    rule_table->setHorizontalHeaderItem(1, new QStandardItem(QObject::tr("Src_ipaddr")));
    rule_table->setHorizontalHeaderItem(2, new QStandardItem(QObject::tr("Dst_ipaddr")));
    rule_table->setHorizontalHeaderItem(3, new QStandardItem(QObject::tr("Connect")));
    rule_table->setHorizontalHeaderItem(4, new QStandardItem(QObject::tr("NAT")));
    rule_table->setHorizontalHeaderItem(5, new QStandardItem(QObject::tr("NAT_src_ipaddr")));
    rule_table->setHorizontalHeaderItem(6, new QStandardItem(QObject::tr("NAT_dst_ipaddr")));
    //利用setModel()方法将数据模型与QTableView绑定
    ui->tableView->setModel(rule_table);

    for(int i=0;i<7;i++){ //固定列宽
        ui->tableView->horizontalHeader()->setSectionResizeMode(i, QHeaderView::Fixed);
    }
    //设置表格的各列的宽度值
    ui->tableView->setColumnWidth(0,80);
    ui->tableView->setColumnWidth(1,200);
    ui->tableView->setColumnWidth(2,200);
    ui->tableView->setColumnWidth(3,80);
    ui->tableView->setColumnWidth(4,30);
    ui->tableView->setColumnWidth(5,200);
    ui->tableView->setColumnWidth(6,200);
    //设置选中时为整行选中
    ui->tableView->setSelectionBehavior(QAbstractItemView::SelectRows);
    //设置表格的单元为只读属性，即不能编辑
    ui->tableView->setEditTriggers(QAbstractItemView::NoEditTriggers);


    //获取数据
    struct UserReq req;
    struct KerFunction rsp;
    struct conlist list[100], *storage;
    storage=list;
    int storage_len=0;
    int re;

    rsp.code = ERROR_CODE_EXIT;
    req.tp = REQ_GETAllConns;
    rsp=exchangeMsgK(&req, sizeof(req));
    re=showConns((struct ConnLog*)rsp.body, rsp.header->arrayLen,storage, &storage_len);
    if(re)feedback("No connect now");
    //printf("trans complete!\n");
    //显示内容
    if(storage_len){
        for(int i=0;i<storage_len;i++){
            rule_table->setItem(i,0,new QStandardItem(list[i].proto));
            rule_table->setItem(i,1,new QStandardItem(list[i].srcip));
            rule_table->setItem(i,2,new QStandardItem(list[i].dstip));
            rule_table->setItem(i,3,new QStandardItem(list[i].connect));
            if(list[i].ifnat){
                rule_table->setItem(i,4,new QStandardItem("Yes"));
                rule_table->setItem(i,5,new QStandardItem(list[i].connect));
                rule_table->setItem(i,6,new QStandardItem(list[i].connect));
            }else rule_table->setItem(i,4,new QStandardItem("No"));
        }
    }

}

void FirewallWidget::feedback(const QString text){
    QString msg;
    msg.push_back(QString("time [%1] %2 %3").arg(DATETIME).arg(text).arg("\n"));
    ui->feedback->insertPlainText(msg);
}
