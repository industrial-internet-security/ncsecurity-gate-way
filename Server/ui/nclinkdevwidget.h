#ifndef NCLINKWIDGET_H
#define NCLINKWIDGET_H

#include <QWidget>

struct _root {
    char id[50];
    char name[50];
    char type[20];
    char description[50];
    char version[10];
    int configs_data[100];
    int configs_method[50];
    int device[50];
};

struct _device {
    char id[50];
    char name[50];
    char type[20];
    char description[50];
    char number[20];
    char guid[50];
    char version[10];
    int configs_data[100];
    int configs_channel[100];
    int dataitems[50];
    int components[50];
};

struct _components {
    char id[50];
    char name[50];
    char type[20];
    char description[50];
    char number[20];
    int configs[100];
    int dataitems[50];
    int components[50];
};

struct _data {
    char id[50];
    char name[50];
    char type[25];
    char description[50];
    char datatype[10];
    char value[20];
    char source[50];
    char units[25];
    bool setable;
};

struct _channel {
    char id[50];
    char name[50];
    char type[15];
    char description[50];
    int ids[100];
    int sampleinterval;
    int uploadinterval;
};

struct _method {
    char id[50];
    char name[50];
    char type[10];
    char description[50];
    char args_name[50][50];
    char args_text[50][50];
};

extern struct _root rootstorage[10];
extern struct _device devicestorage[50];
extern struct _components componentstorage[50];
extern struct _data datastorage[200];
extern struct _channel channelstorage[50];
extern struct _method methodstorage[50];

extern int _root_num;
extern int _device_num;
extern int _component_num;
extern int _data_num;
extern int _channel_num;
extern int _method_num;

extern int _root_config_to_data_num[10];
extern int _root_config_to_method_num[10];
extern int _root_device_num[10];
extern int _device_config_to_data_num[50];
extern int _device_config_to_channel_num[50];
extern int _device_dataitems_num[50];
extern int _device_components_num[50];
extern int _components_config_num[50];
extern int _components_dataitems_num[50];
extern int _components_components_num[50];
extern int _channel_ids_num[50];
extern int _method_args_num[50];

namespace Ui {
class nclinkdevwidget;
}

class nclinkdevwidget : public QWidget
{
    Q_OBJECT

public:
    explicit nclinkdevwidget(QWidget *parent = nullptr);
    ~nclinkdevwidget();
    void init_mod();
    void option_mod();
    void feedback(QString text);
    void root_option(int num);
    void device_option(int num);
    void component_option(int num);
    void data_option(int num);
    void channel_option(int num);
    void method_option(int num);

    void attach_item(int option,int src_num,int dst_num);
    void detach_item(int option,int src_num,int dst_num);
    void list_root();
    void list_device();
    void list_components();
    void list_data();
    void list_channel();
    void list_method();


private:
    Ui::nclinkdevwidget *ui;

    int seq;
};

#endif // NCLINKWIDGET_H
