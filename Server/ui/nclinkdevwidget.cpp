#include "nclinkdevwidget.h"
#include "ui_nclinkdevwidget.h"
#include <QJsonValue>
#include <QJsonArray>
#include <QJsonObject>
#include <QJsonDocument>
#include <QFile>
#include <QTime>
#include <QTextEdit>
#include <QStandardItemModel>
#include "stdlib.h"

#define DATETIME qPrintable (QDateTime::currentDateTime().toString("yyyy-MM-dd HH:mm:ss"))


struct _root rootstorage[10];
struct _device devicestorage[50];
struct _components componentstorage[50];
struct _data datastorage[200];
struct _channel channelstorage[50];
struct _method methodstorage[50];


int read_flag=0;
int write_flag=0;

int _root_num=0;
int _device_num=0;
int _component_num=0;
int _data_num=0;
int _channel_num=0;
int _method_num=0;

int _root_config_to_data_num[10]={0};
int _root_config_to_method_num[10]={0};
int _root_device_num[10]={0};
int _device_config_to_data_num[50]={0};
int _device_config_to_channel_num[50]={0};
int _device_dataitems_num[50]={0};
int _device_components_num[50]={0};
int _components_config_num[50]={0};
int _components_dataitems_num[50]={0};
int _components_components_num[50]={0};
int _channel_ids_num[50]={0};
int _method_args_num[50]={0};

nclinkdevwidget::nclinkdevwidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::nclinkdevwidget)
{
    ui->setupUi(this);
    setWindowFlags(Qt::FramelessWindowHint);   //去掉边框
    setAttribute(Qt::WA_StyledBackground);
    init_mod();
    option_mod();
}

nclinkdevwidget::~nclinkdevwidget()
{
    delete ui;
}

void nclinkdevwidget::init_mod(){
    memset(rootstorage,0,sizeof(rootstorage));
    memset(devicestorage,0,sizeof(devicestorage));
    memset(datastorage,0,sizeof(datastorage));
    memset(channelstorage,0,sizeof(channelstorage));
    memset(componentstorage,0,sizeof(componentstorage));
    memset(methodstorage,0,sizeof(methodstorage));

    QStringList device_type,components_type,data_type1,data_type2,data_datatype;
    device_type<<"AGV"<<"CLEAN"<<"MACHINE"<<"MEASURE"<<"PRODUCTLINE"<<"WAREHOUSE";
    ui->device_type->addItems(device_type);
    ui->device_type->setCurrentIndex(0);
    components_type<<"AUXILIARY"<<"AXIS"<<"COMPONENT"<<"CONTROLLER"<<"FIXTURE"
                  <<"MOTOR"<<"SCREW"<<"SENSOR"<<"SERVO_DRIVER"<<"TOOL_MAGAZINE";
    ui->components_type->addItems(components_type);
    ui->components_type->setCurrentIndex(0);
    data_type1<<"CATAGORY"<<"CREATE_TIME"<<"CREATOR"<<"IP"<<"MANUFACTURER"<<"NAME"<<"PARAMETER"<<"SERIAL_NUMBER"
            <<"STATUS"<<"UPPER_LIMIT"<<"USER_ROLE"<<"VARIABLE"<<"VERSION"<<"WARNING"<<"WORK_MODE"
           <<"ACCELERATION"<<"ANGLE"<<"ANGULAR_ACCELERATION"<<"ANGULAR_VELOCITY"<<"CONCENTRATION"<<"CONDUCTIVITY"
          <<"CURRENT"<<"DISPLACEMENT"<<"ENERGY"<<"FLOW"<<"FREQUENCY"<<"LENGTH"<<"MASS"<<"PERIOD"<<"POSITION"<<"POWER"
         <<"POWER_FACTOR"<<"PRESSURE"<<"RESISTANCE"<<"ROTATION_SPEED"<<"SPEED"<<"TEMPERATURE"<<"TORQUE"<<"VISCOSITY"
        <<"VOLT_AMPERE"
        <<"AUTO_MODE"<<"AXIS_BACKLASH"<<"AXIS_LOAD"<<"AXIS_TOREFPOINT"<<"BLOCK_SKIP_MODE"<<"CHIP_REMOVAL"<<"COMPACITY"
        <<"CONSOLE"<<"COOLANT_ON"<<"COORDINATE"<<"CYCLE_START";
    data_type2<<"DOOR_CLOSED"<<"EDIT_MODE"<<"EMG"<<"FEED_HOLD"<<"FEED_OVERRIDE"
        <<"FEED_SPEED"<<"FILE"<<"FIXTURE_MODE"<<"HANDLE_MODE"<<"INCREMENTAL_MODE"<<"JOG_MODE"<<"LIGHTING_ON"<<"LINE_NUMBER"
        <<"LUBRICANT_ON"<<"MACHINE_LOCKED"<<"MDI_MODE"<<"MST_LOCKED"<<"OPT_STOP_MODE"<<"PATH_LEFT_LENGTH"<<"PART"<<"PART_COUNT"
        <<"PERIOD"<<"PROGRAM"<<"PROGRAM_MODE"<<"PROGRAM_NUMBER"<<"RAPID_OVERRIDE"<<"RATE"<<"REF_MODE"<<"RESET_MODE"<<"RESIDUAL"
        <<"SHELF_UNIT"<<"SINGLE_BLOCK_MODE"<<"SITE"<<"SPINDLE_MODE"<<"SPINDLE_OVERRIDE"<<"SPINDLE_SPEED"<<"SUBPROGRAM"<<"TOOL"
        <<"TOOL_CHANGE"<<"TOOL_CHANGING"<<"TOOL_PARAM"<<"TOOL_NUMBER"<<"TOOL_POSITION";
    ui->data_type->addItems(data_type1);
    ui->data_type->addItems(data_type2);
    ui->data_type->setCurrentIndex(0);
    ui->data_type->setStyleSheet("QComboBox{combobox-popup:0;}");
    ui->data_type->setMaxVisibleItems(10);
    data_datatype<<"(default)"<<"list"<<"dict";
    ui->data_datatype->addItems(data_datatype);
    ui->data_datatype->setCurrentIndex(0);


    QRegExp vali_id("^[a-fA-F0-9]{8}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{12}$");
    QRegExpValidator *vali_root_id = new QRegExpValidator(vali_id,ui->root_id);
    ui->root_id->setValidator(vali_root_id);
    QRegExpValidator *vali_device_id = new QRegExpValidator(vali_id,ui->device_id);
    ui->device_id->setValidator(vali_device_id);

    QRegExp vali_ver("^[1]{1}\\.[0-9]{1,2}\\.[0-9]{1,2}$");
    QRegExpValidator *vali_root_ver = new QRegExpValidator(vali_ver,ui->root_version);
    ui->root_version->setValidator(vali_root_ver);
    QRegExpValidator *vali_device_ver = new QRegExpValidator(vali_ver,ui->device_version);
    ui->device_version->setValidator(vali_device_ver);

    ui->root_name->setMaxLength(50);
    ui->root_description->setMaxLength(50);
    ui->device_name->setMaxLength(50);
    ui->device_description->setMaxLength(50);
    ui->device_number->setMaxLength(20);
    ui->device_guid->setMaxLength(50);
    ui->components_name->setMaxLength(50);
    ui->components_id->setMaxLength(50);
    ui->components_description->setMaxLength(50);
    ui->components_number->setMaxLength(20);
    ui->data_id->setMaxLength(50);
    ui->data_name->setMaxLength(50);
    ui->data_description->setMaxLength(50);
    ui->data_value->setMaxLength(20);
    ui->data_source->setMaxLength(50);
    ui->data_units->setMaxLength(25);
    ui->channel_id->setMaxLength(50);
    ui->channel_name->setMaxLength(50);
    ui->channel_description->setMaxLength(50);
    ui->channel_upload->setValidator(new QIntValidator(0,0x7fffffff,ui->channel_upload));
    ui->channel_sample->setValidator(new QIntValidator(0,0x7fffffff,ui->channel_sample));
    ui->method_id->setMaxLength(50);
    ui->method_name->setMaxLength(50);
    ui->method_description->setMaxLength(50);
    ui->method_args_name->setMaxLength(50);
    ui->method_args_text->setMaxLength(50);

    ui->root_config_data->setRange(0,200);
    ui->root_config_device->setRange(0,50);
    ui->root_config_method->setRange(0,50);
    ui->components_config->setRange(0,200);
    ui->components_dataitem->setRange(0,200);
    ui->components_self->setRange(0,50);
    ui->components_device->setRange(0,50);
    ui->device_dataitem->setRange(0,200);
    ui->device_components->setRange(0,50);
    ui->device_config_data->setRange(0,200);
    ui->device_config_channel->setRange(0,50);
    ui->device_config_root->setRange(0,10);
    ui->method_root->setRange(0,10);
    ui->channel_device->setRange(0,50);
    ui->channel_ids->setRange(0,200);
    ui->data_root->setRange(0,10);
    ui->data_device_config->setRange(0,50);
    ui->data_device_dataitem->setRange(0,50);
    ui->data_components_config->setRange(0,50);
    ui->data_components_dataitem->setRange(0,50);

    //read_flag=0;
    //write_flag=1;
}

void nclinkdevwidget::option_mod(){
    
    //注册根对象
    connect(ui->root_add,&QPushButton::clicked,this,[=](){
        if(_root_num<=10){
        root_option(_root_num);
        _root_num++;
        feedback("成功新建root对象");
        }else feedback("root对象创建已达上限");
    });
    
    //注册设备对象
    connect(ui->device_add,&QPushButton::clicked,this,[=](){
        if(_device_num<=50){
        device_option(_device_num);
        _device_num++;
        feedback("成功新建device对象");
        }else feedback("device对象创建已达上限");
    });
    
    //注册组件对象
    connect(ui->components_add,&QPushButton::clicked,this,[=](){
        if(_component_num<=50){
        component_option(_component_num);
        _component_num++;
        feedback("成功新建component对象");
        }else feedback("component对象创建已达上限");
    });
    
    //注册数据对象
    connect(ui->data_add,&QPushButton::clicked,this,[=](){
        if(_data_num<=200){
        data_option(_data_num);
        _data_num++;
        feedback("成功新建data对象");
        }else feedback("data对象创建已达上限");
    });
    
    //注册数据通道
    connect(ui->channel_add,&QPushButton::clicked,this,[=](){
        if(_channel_num<=50){
        channel_option(_channel_num);
        _channel_num++;
        feedback("成功新建channel对象");
        }else feedback("channel对象创建已达上限");
    });
    
    //注册方法对象
    connect(ui->method_add,&QPushButton::clicked,this,[=](){
        if(_method_num<=50){
        method_option(_method_num);
        _method_num++;
        feedback("成功新建method对象");
        }else feedback("method对象创建已达上限");
    });

    //修改根对象
    connect(ui->root_change,&QPushButton::clicked,this,[=](){
        int tmp=((ui->root_seq->text()).toInt()-1);
        if(tmp>0){
        root_option(tmp);
        feedback("成功修改root对象");
        }else feedback("序号错误!");
    });

    //修改设备对象
    connect(ui->device_change,&QPushButton::clicked,this,[=](){
        int tmp=((ui->device_seq->text()).toInt()-1);
        if(tmp>0){
        device_option(tmp);
        feedback("成功修改device对象");
        }else feedback("序号错误!");
    });

    //修改组件对象
    connect(ui->components_change,&QPushButton::clicked,this,[=](){
        int tmp=((ui->components_seq->text()).toInt()-1);
        if(tmp>0){
        component_option(tmp);
        feedback("成功修改component对象");
        }else feedback("序号错误!");
    });

    //修改数据对象
    connect(ui->data_change,&QPushButton::clicked,this,[=](){
        int tmp=((ui->data_seq->text()).toInt()-1);
        if(tmp>=0){
        data_option(tmp);
        feedback("成功修改data对象");
        }else feedback("序号错误!");
    });

    //修改数据通道
    connect(ui->channel_change,&QPushButton::clicked,this,[=](){
        int tmp=((ui->channel_seq->text()).toInt()-1);
        if(tmp>=0){
        channel_option(tmp);
        feedback("成功修改channel对象");
        }else feedback("序号错误!");
    });

    //修改方法对象
    connect(ui->method_change,&QPushButton::clicked,this,[=](){
        int tmp=((ui->method_seq->text()).toInt()-1);
        if(tmp>=0){
        method_option(tmp);
        feedback("成功修改method对象");
        }else feedback("序号错误!");
    });

    //删除根对象
    connect(ui->root_del,&QPushButton::clicked,this,[=](){
        if(_root_num>0){
        _root_num--;

        memset(rootstorage[_root_num].id,0,sizeof(rootstorage[_root_num].id));
        memset(rootstorage[_root_num].name,0,sizeof(rootstorage[_root_num].name));
        memset(rootstorage[_root_num].version,0,sizeof(rootstorage[_root_num].version));
        memset(rootstorage[_root_num].description,0,sizeof(rootstorage[_root_num].description));
        memset(rootstorage[_root_num].type,0,sizeof(rootstorage[_root_num].type));
        memset(rootstorage[_root_num].configs_data,0,sizeof(rootstorage[_root_num].configs_data));
        memset(rootstorage[_root_num].configs_method,0,sizeof(rootstorage[_root_num].configs_method));
        memset(rootstorage[_root_num].device,0,sizeof(rootstorage[_root_num].device));
        feedback("成功删除root对象");
        }else feedback("已经清空root对象");
    });

    //删除设备对象
    connect(ui->device_del,&QPushButton::clicked,this,[=](){
        if(_device_num>0){
        _device_num--;
        memset(devicestorage[_device_num].id,0,sizeof(devicestorage[_device_num].id));
        memset(devicestorage[_device_num].name,0,sizeof(devicestorage[_device_num].name));
        memset(devicestorage[_device_num].version,0,sizeof(devicestorage[_device_num].version));
        memset(devicestorage[_device_num].description,0,sizeof(devicestorage[_device_num].description));
        memset(devicestorage[_device_num].type,0,sizeof(devicestorage[_device_num].type));
        memset(devicestorage[_device_num].number,0,sizeof(devicestorage[_device_num].number));
        memset(devicestorage[_device_num].guid,0,sizeof(devicestorage[_device_num].guid));
        memset(devicestorage[_device_num].configs_data,0,sizeof(devicestorage[_device_num].configs_data));
        memset(devicestorage[_device_num].configs_channel,0,sizeof(devicestorage[_device_num].configs_channel));
        memset(devicestorage[_device_num].dataitems,0,sizeof(devicestorage[_device_num].dataitems));
        memset(devicestorage[_device_num].components,0,sizeof(devicestorage[_device_num].components));      
        feedback("成功删除device对象");
        }else feedback("已经清空device对象");
    });

    //删除组件对象
    connect(ui->components_del,&QPushButton::clicked,this,[=](){
        if(_component_num>0){
        _component_num--;
        memset(componentstorage[_component_num].id,0,sizeof(componentstorage[_component_num].id));
        memset(componentstorage[_component_num].name,0,sizeof(componentstorage[_component_num].name));
        memset(componentstorage[_component_num].description,0,sizeof(componentstorage[_component_num].description));
        memset(componentstorage[_component_num].type,0,sizeof(componentstorage[_component_num].type));
        memset(componentstorage[_component_num].number,0,sizeof(componentstorage[_component_num].number));
        memset(componentstorage[_component_num].configs,0,sizeof(componentstorage[_component_num].configs));
        memset(componentstorage[_component_num].dataitems,0,sizeof(componentstorage[_component_num].dataitems));
        memset(componentstorage[_component_num].components,0,sizeof(componentstorage[_component_num].components));
        feedback("成功删除components对象");
        }else feedback("已经清空components对象");
    });

    //删除数据对象
    connect(ui->data_del,&QPushButton::clicked,this,[=](){
        if(_data_num>0){
        _data_num--;
        memset(datastorage[_data_num].id,0,sizeof(datastorage[_data_num].id));
        memset(datastorage[_data_num].name,0,sizeof(datastorage[_data_num].name));
        memset(datastorage[_data_num].type,0,sizeof(datastorage[_data_num].type));
        memset(datastorage[_data_num].description,0,sizeof(datastorage[_data_num].description));
        memset(datastorage[_data_num].datatype,0,sizeof(datastorage[_data_num].datatype));
        memset(datastorage[_data_num].value,0,sizeof(datastorage[_data_num].value));
        memset(datastorage[_data_num].source,0,sizeof(datastorage[_data_num].source));
        memset(datastorage[_data_num].units,0,sizeof(datastorage[_data_num].units));
        datastorage[_data_num].setable=false;
        feedback("成功删除data对象");
        }else feedback("已经清空data对象");
    });
    
    //删除采样通道
    connect(ui->channel_del,&QPushButton::clicked,this,[=](){
        if(_channel_num>0){
        _channel_num--;
        memset(channelstorage[_channel_num].id,0,sizeof(channelstorage[_channel_num].id));
        memset(channelstorage[_channel_num].name,0,sizeof(channelstorage[_channel_num].name));
        memset(channelstorage[_channel_num].type,0,sizeof(channelstorage[_channel_num].type));
        memset(channelstorage[_channel_num].description,0,sizeof(channelstorage[_channel_num].description));
        memset(channelstorage[_channel_num].ids,0,sizeof(channelstorage[_channel_num].ids));
        channelstorage[_channel_num].sampleinterval=0;
        channelstorage[_channel_num].uploadinterval=0;
        feedback("成功删除channel对象");
        }else feedback("已经清空channel对象");
    });

    //删除方法对象
    connect(ui->method_del,&QPushButton::clicked,this,[=](){
        if(_method_num>0){
        _method_num--;
        memset(methodstorage[_method_num].id,0,sizeof(methodstorage[_method_num].id));
        memset(methodstorage[_method_num].name,0,sizeof(methodstorage[_method_num].name));
        memset(methodstorage[_method_num].type,0,sizeof(methodstorage[_method_num].type));
        memset(methodstorage[_method_num].description,0,sizeof(methodstorage[_method_num].description));
        for(int i=0;i<50;i++){
            memset(methodstorage[_method_num].args_name[i],0,sizeof(methodstorage[_method_num].args_name[i]));
            memset(methodstorage[_method_num].args_text[i],0,sizeof(methodstorage[_method_num].args_text[i]));
        }
        feedback("成功删除method对象");
        }else feedback("已经清空method对象");
    });

    /*
     NC-Link的复合数据对象有以下11种情况：
     1. root.config--data
     2. root.config--method
     3. root.device--device
     4. device.config--data
     5. device.config--channel
     6. device.dataitem--data
     7. device.components--components
     8. components.config--data
     9. components.dataitems--data
     10. components--components
     11. channel--data
     */
    //挂载item
    connect(ui->root_config_data_add,&QPushButton::clicked,this,[=](){
        int src,dst;
        dst=(ui->root_seq->text()).toInt();  //绑定对象的序号
        src=(ui->root_config_data->text()).toInt(); //被绑定对象的序号
        if(dst>0){
            if(src>0){
                attach_item(1,src-1,dst-1);
            }else feedback("序号错误！");
        }else if(src>0){
            attach_item(1,src-1,_root_num-1);
        }else feedback("序号错误！");
    });

    connect(ui->root_config_method_add,&QPushButton::clicked,this,[=](){
        int src,dst;
        dst=(ui->root_seq->text()).toInt();  //绑定对象的序号
        src=(ui->root_config_method->text()).toInt(); //被绑定对象的序号
        if(dst>0){
            if(src>0){
                attach_item(2,src-1,dst-1);
            }else feedback("序号错误！");
        }else if(src>0){
            attach_item(2,src-1,_root_num-1);
        }else feedback("序号错误！");
    });

    connect(ui->root_config_device_add,&QPushButton::clicked,this,[=](){
        int src,dst;
        dst=(ui->root_seq->text()).toInt();  //绑定对象的序号
        src=(ui->root_config_device->text()).toInt(); //被绑定对象的序号
        if(dst>0){
            if(src>0){
                attach_item(3,src-1,dst-1);
            }else feedback("序号错误！");
        }else if(src>0){
            attach_item(3,src-1,_root_num-1);
        }else feedback("序号错误！");
    });

    connect(ui->device_config_data_add,&QPushButton::clicked,this,[=](){
        int src,dst;
        dst=(ui->device_seq->text()).toInt();  //绑定对象的序号
        src=(ui->device_config_data->text()).toInt(); //被绑定对象的序号
        if(dst>0){
            if(src>0){
                attach_item(4,src-1,dst-1);
            }else feedback("序号错误！");
        }else if(src>0){
            attach_item(4,src-1,_device_num-1);
        }else feedback("序号错误！");
    });

    connect(ui->device_config_channel_add,&QPushButton::clicked,this,[=](){
        int src,dst;
        dst=(ui->device_seq->text()).toInt();  //绑定对象的序号
        src=(ui->device_config_channel->text()).toInt(); //被绑定对象的序号
        if(dst>0){
            if(src>0){
                attach_item(5,src-1,dst-1);
            }else feedback("序号错误！");
        }else if(src>0){
            attach_item(5,src-1,_device_num-1);
        }else feedback("序号错误！");
    });

    connect(ui->device_dataitem_add,&QPushButton::clicked,this,[=](){
        int src,dst;
        dst=(ui->device_seq->text()).toInt();  //绑定对象的序号
        src=(ui->device_dataitem->text()).toInt(); //被绑定对象的序号
        if(dst>0){
            if(src>0){
                attach_item(6,src-1,dst-1);
            }else feedback("序号错误！");
        }else if(src>0){
            attach_item(6,src-1,_device_num-1);
        }else feedback("序号错误！");
    });

    connect(ui->device_components_add,&QPushButton::clicked,this,[=](){
        int src,dst;
        dst=(ui->device_seq->text()).toInt();  //绑定对象的序号
        src=(ui->device_components->text()).toInt(); //被绑定对象的序号
        if(dst>0){
            if(src>0){
                attach_item(7,src-1,dst-1);
            }else feedback("序号错误！");
        }else if(src>0){
            attach_item(7,src-1,_device_num-1);
        }else feedback("序号错误！");
    });

    connect(ui->components_config_add,&QPushButton::clicked,this,[=](){
        int src,dst;
        dst=(ui->components_seq->text()).toInt();  //绑定对象的序号
        src=(ui->components_config->text()).toInt(); //被绑定对象的序号
        if(dst>0){
            if(src>0){
                attach_item(8,src-1,dst-1);
            }else feedback("序号错误！");
        }else if(src>0){
            attach_item(8,src-1,_component_num-1);
        }else feedback("序号错误！");
    });

    connect(ui->components_config_add,&QPushButton::clicked,this,[=](){
        int src,dst;
        dst=(ui->components_seq->text()).toInt();  //绑定对象的序号
        src=(ui->components_config->text()).toInt(); //被绑定对象的序号
        if(dst>0){
            if(src>0){
                attach_item(8,src-1,dst-1);
            }else feedback("序号错误！");
        }else if(src>0){
            attach_item(8,src-1,_component_num-1);
        }else feedback("序号错误！");
    });

    connect(ui->components_dataitem_add,&QPushButton::clicked,this,[=](){
        int src,dst;
        dst=(ui->components_seq->text()).toInt();  //绑定对象的序号
        src=(ui->components_dataitem->text()).toInt(); //被绑定对象的序号
        if(dst>0){
            if(src>0){
                attach_item(9,src-1,dst-1);
            }else feedback("序号错误！");
        }else if(src>0){
            attach_item(9,src-1,_component_num-1);
        }else feedback("序号错误！");
    });

    connect(ui->components_self_add,&QPushButton::clicked,this,[=](){
        int src,dst;
        dst=(ui->components_seq->text()).toInt();  //绑定对象的序号
        src=(ui->components_self->text()).toInt(); //被绑定对象的序号
        if(dst>0){
            if(src>0){
                attach_item(10,src-1,dst-1);
            }else feedback("序号错误！");
        }else if(src>0){
            attach_item(10,src-1,_component_num-1);
        }else feedback("序号错误！");
    });

    connect(ui->channel_ids_add,&QPushButton::clicked,this,[=](){
        int src,dst;
        dst=(ui->channel_seq->text()).toInt();  //绑定对象的序号
        src=(ui->channel_ids->text()).toInt(); //被绑定对象的序号
        if(dst>0){
            if(src>0){
                attach_item(11,src-1,dst-1);
            }else feedback("序号错误！");
        }else if(src>0){
            attach_item(11,src-1,_channel_num-1);
        }else feedback("序号错误！");
    });

    //从这里开始是被attach的项目

    connect(ui->data_root_add,&QPushButton::clicked,this,[=](){
        int src,dst;
        src=(ui->data_seq->text()).toInt();  //绑定对象的序号
        dst=(ui->data_root->text()).toInt(); //被绑定对象的序号
        if(dst>0){
            if(src>0){
                attach_item(1,src-1,dst-1);
            }else feedback("序号错误！");
        }else if(src>0){
            attach_item(1,_data_num-1,dst-1);
        }else feedback("序号错误！");
    });

    connect(ui->method_root_add,&QPushButton::clicked,this,[=](){
        int src,dst;
        src=(ui->method_seq->text()).toInt();  //绑定对象的序号
        dst=(ui->method_root->text()).toInt(); //被绑定对象的序号
        if(dst>0){
            if(src>0){
                attach_item(2,src-1,dst-1);
            }else feedback("序号错误！");
        }else if(src>0){
            attach_item(2,_method_num-1,dst-1);
        }else feedback("序号错误！");
    });

    connect(ui->device_config_root_add,&QPushButton::clicked,this,[=](){
        int src,dst;
        src=(ui->device_seq->text()).toInt();  //绑定对象的序号
        dst=(ui->device_config_root->text()).toInt(); //被绑定对象的序号
        if(dst>0){
            if(src>0){
                attach_item(3,src-1,dst-1);
            }else feedback("序号错误！");
        }else if(src>0){
            attach_item(3,_device_num-1,dst-1);
        }else feedback("序号错误！");
    });

    connect(ui->data_device_config_add,&QPushButton::clicked,this,[=](){
        int src,dst;
        src=(ui->data_seq->text()).toInt();  //绑定对象的序号
        dst=(ui->data_device_config->text()).toInt(); //被绑定对象的序号
        if(dst>0){
            if(src>0){
                attach_item(4,src-1,dst-1);
            }else feedback("序号错误！");
        }else if(src>0){
            attach_item(4,_data_num-1,dst-1);
        }else feedback("序号错误！");
    });

    connect(ui->channel_device_add,&QPushButton::clicked,this,[=](){
        int src,dst;
        src=(ui->channel_seq->text()).toInt();  //绑定对象的序号
        dst=(ui->channel_device->text()).toInt(); //被绑定对象的序号
        if(dst>0){
            if(src>0){
                attach_item(5,src-1,dst-1);
            }else feedback("序号错误！");
        }else if(src>0){
            attach_item(5,_channel_num-1,dst-1);
        }else feedback("序号错误！");
    });

    connect(ui->data_device_dataitem_add,&QPushButton::clicked,this,[=](){
        int src,dst;
        src=(ui->data_seq->text()).toInt();  //绑定对象的序号
        dst=(ui->data_device_dataitem->text()).toInt(); //被绑定对象的序号
        if(dst>0){
            if(src>0){
                attach_item(6,src-1,dst-1);
            }else feedback("序号错误！");
        }else if(src>0){
            attach_item(6,_data_num-1,dst-1);
        }else feedback("序号错误！");
    });

    connect(ui->components_device_add,&QPushButton::clicked,this,[=](){
        int src,dst;
        src=(ui->components_seq->text()).toInt();  //绑定对象的序号
        dst=(ui->components_device->text()).toInt(); //被绑定对象的序号
        if(dst>0){
            if(src>0){
                attach_item(7,src-1,dst-1);
            }else feedback("序号错误！");
        }else if(src>0){
            attach_item(7,_component_num-1,dst-1);
        }else feedback("序号错误！");
    });

    connect(ui->data_components_config_add,&QPushButton::clicked,this,[=](){
        int src,dst;
        src=(ui->data_seq->text()).toInt();  //绑定对象的序号
        dst=(ui->data_components_config->text()).toInt(); //被绑定对象的序号
        if(dst>0){
            if(src>0){
                attach_item(8,src-1,dst-1);
            }else feedback("序号错误！");
        }else if(src>0){
            attach_item(8,_data_num-1,dst-1);
        }else feedback("序号错误！");
    });

    connect(ui->data_components_dataitem_add,&QPushButton::clicked,this,[=](){
        int src,dst;
        src=(ui->data_seq->text()).toInt();  //绑定对象的序号
        dst=(ui->data_components_dataitem->text()).toInt(); //被绑定对象的序号
        if(dst>0){
            if(src>0){
                attach_item(9,src-1,dst-1);
            }else feedback("序号错误！");
        }else if(src>0){
            attach_item(9,_data_num-1,dst-1);
        }else feedback("序号错误！");
    });

    connect(ui->data_channel_add,&QPushButton::clicked,this,[=](){
        int src,dst;
        src=(ui->data_seq->text()).toInt();  //绑定对象的序号
        dst=(ui->data_channel->text()).toInt(); //被绑定对象的序号
        if(dst>0){
            if(src>0){
                attach_item(11,src-1,dst-1);
            }else feedback("序号错误！");
        }else if(src>0){
            attach_item(11,_data_num-1,dst-1);
        }else feedback("序号错误！");
    });


    //移除操作，相当与挂载的逆操作
    connect(ui->root_config_data_del,&QPushButton::clicked,this,[=](){
        int src,dst;
        dst=(ui->root_seq->text()).toInt();  //绑定对象的序号
        src=(ui->root_config_data->text()).toInt(); //被绑定对象的序号
        if(dst>0){
            if(src>0){
                detach_item(1,src-1,dst-1);
            }else feedback("序号错误！");
        }else if(src>0){
            detach_item(1,src-1,_root_num-1);
        }else feedback("序号错误！");
    });

    connect(ui->root_config_method_del,&QPushButton::clicked,this,[=](){
        int src,dst;
        dst=(ui->root_seq->text()).toInt();  //绑定对象的序号
        src=(ui->root_config_method->text()).toInt(); //被绑定对象的序号
        if(dst>0){
            if(src>0){
                detach_item(2,src-1,dst-1);
            }else feedback("序号错误！");
        }else if(src>0){
            detach_item(2,src-1,_root_num-1);
        }else feedback("序号错误！");
    });

    connect(ui->root_config_device_del,&QPushButton::clicked,this,[=](){
        int src,dst;
        dst=(ui->root_seq->text()).toInt();  //绑定对象的序号
        src=(ui->root_config_device->text()).toInt(); //被绑定对象的序号
        if(dst>0){
            if(src>0){
                detach_item(3,src-1,dst-1);
            }else feedback("序号错误！");
        }else if(src>0){
            detach_item(3,src-1,_root_num-1);
        }else feedback("序号错误！");
    });

    connect(ui->device_config_data_del,&QPushButton::clicked,this,[=](){
        int src,dst;
        dst=(ui->device_seq->text()).toInt();  //绑定对象的序号
        src=(ui->device_config_data->text()).toInt(); //被绑定对象的序号
        if(dst>0){
            if(src>0){
                detach_item(4,src-1,dst-1);
            }else feedback("序号错误！");
        }else if(src>0){
            detach_item(4,src-1,_device_num-1);
        }else feedback("序号错误！");
    });

    connect(ui->device_config_channel_del,&QPushButton::clicked,this,[=](){
        int src,dst;
        dst=(ui->device_seq->text()).toInt();  //绑定对象的序号
        src=(ui->device_config_channel->text()).toInt(); //被绑定对象的序号
        if(dst>0){
            if(src>0){
                detach_item(5,src-1,dst-1);
            }else feedback("序号错误！");
        }else if(src>0){
            detach_item(5,src-1,_device_num-1);
        }else feedback("序号错误！");
    });

    connect(ui->device_dataitem_del,&QPushButton::clicked,this,[=](){
        int src,dst;
        dst=(ui->device_seq->text()).toInt();  //绑定对象的序号
        src=(ui->device_dataitem->text()).toInt(); //被绑定对象的序号
        if(dst>0){
            if(src>0){
                detach_item(6,src-1,dst-1);
            }else feedback("序号错误！");
        }else if(src>0){
            detach_item(6,src-1,_device_num-1);
        }else feedback("序号错误！");
    });

    connect(ui->device_components_del,&QPushButton::clicked,this,[=](){
        int src,dst;
        dst=(ui->device_seq->text()).toInt();  //绑定对象的序号
        src=(ui->device_components->text()).toInt(); //被绑定对象的序号
        if(dst>0){
            if(src>0){
                detach_item(7,src-1,dst-1);
            }else feedback("序号错误！");
        }else if(src>0){
            detach_item(7,src-1,_device_num-1);
        }else feedback("序号错误！");
    });

    connect(ui->components_config_del,&QPushButton::clicked,this,[=](){
        int src,dst;
        dst=(ui->components_seq->text()).toInt();  //绑定对象的序号
        src=(ui->components_config->text()).toInt(); //被绑定对象的序号
        if(dst>0){
            if(src>0){
                detach_item(8,src-1,dst-1);
            }else feedback("序号错误！");
        }else if(src>0){
            detach_item(8,src-1,_component_num-1);
        }else feedback("序号错误！");
    });

    connect(ui->components_config_del,&QPushButton::clicked,this,[=](){
        int src,dst;
        dst=(ui->components_seq->text()).toInt();  //绑定对象的序号
        src=(ui->components_config->text()).toInt(); //被绑定对象的序号
        if(dst>0){
            if(src>0){
                detach_item(8,src-1,dst-1);
            }else feedback("序号错误！");
        }else if(src>0){
            detach_item(8,src-1,_component_num-1);
        }else feedback("序号错误！");
    });

    connect(ui->components_dataitem_del,&QPushButton::clicked,this,[=](){
        int src,dst;
        dst=(ui->components_seq->text()).toInt();  //绑定对象的序号
        src=(ui->components_dataitem->text()).toInt(); //被绑定对象的序号
        if(dst>0){
            if(src>0){
                detach_item(9,src-1,dst-1);
            }else feedback("序号错误！");
        }else if(src>0){
            detach_item(9,src-1,_component_num-1);
        }else feedback("序号错误！");
    });

    connect(ui->components_self_del,&QPushButton::clicked,this,[=](){
        int src,dst;
        dst=(ui->components_seq->text()).toInt();  //绑定对象的序号
        src=(ui->components_self->text()).toInt(); //被绑定对象的序号
        if(dst>0){
            if(src>0){
                detach_item(10,src-1,dst-1);
            }else feedback("序号错误！");
        }else if(src>0){
            detach_item(10,src-1,_component_num-1);
        }else feedback("序号错误！");
    });

    connect(ui->channel_ids_del,&QPushButton::clicked,this,[=](){
        int src,dst;
        dst=(ui->channel_seq->text()).toInt();  //绑定对象的序号
        src=(ui->channel_ids->text()).toInt(); //被绑定对象的序号
        if(dst>0){
            if(src>0){
                detach_item(11,src-1,dst-1);
            }else feedback("序号错误！");
        }else if(src>0){
            detach_item(11,src-1,_channel_num-1);
        }else feedback("序号错误！");
    });

    //从这里开始是被detach的项目

    connect(ui->data_root_del,&QPushButton::clicked,this,[=](){
        int src,dst;
        src=(ui->data_seq->text()).toInt();  //绑定对象的序号
        dst=(ui->data_root->text()).toInt(); //被绑定对象的序号
        if(dst>0){
            if(src>0){
                detach_item(1,src-1,dst-1);
            }else feedback("序号错误！");
        }else if(src>0){
            detach_item(1,_data_num-1,dst-1);
        }else feedback("序号错误！");
    });

    connect(ui->method_root_del,&QPushButton::clicked,this,[=](){
        int src,dst;
        src=(ui->method_seq->text()).toInt();  //绑定对象的序号
        dst=(ui->method_root->text()).toInt(); //被绑定对象的序号
        if(dst>0){
            if(src>0){
                detach_item(2,src-1,dst-1);
            }else feedback("序号错误！");
        }else if(src>0){
            detach_item(2,_method_num-1,dst-1);
        }else feedback("序号错误！");
    });

    connect(ui->device_config_root_del,&QPushButton::clicked,this,[=](){
        int src,dst;
        src=(ui->device_seq->text()).toInt();  //绑定对象的序号
        dst=(ui->device_config_root->text()).toInt(); //被绑定对象的序号
        if(dst>0){
            if(src>0){
                detach_item(3,src-1,dst-1);
            }else feedback("序号错误！");
        }else if(src>0){
            detach_item(3,_device_num-1,dst-1);
        }else feedback("序号错误！");
    });

    connect(ui->data_device_config_del,&QPushButton::clicked,this,[=](){
        int src,dst;
        src=(ui->data_seq->text()).toInt();  //绑定对象的序号
        dst=(ui->data_device_config->text()).toInt(); //被绑定对象的序号
        if(dst>0){
            if(src>0){
                detach_item(4,src-1,dst-1);
            }else feedback("序号错误！");
        }else if(src>0){
            detach_item(4,_data_num-1,dst-1);
        }else feedback("序号错误！");
    });

    connect(ui->channel_device_del,&QPushButton::clicked,this,[=](){
        int src,dst;
        src=(ui->channel_seq->text()).toInt();  //绑定对象的序号
        dst=(ui->channel_device->text()).toInt(); //被绑定对象的序号
        if(dst>0){
            if(src>0){
                detach_item(5,src-1,dst-1);
            }else feedback("序号错误！");
        }else if(src>0){
            detach_item(5,_channel_num-1,dst-1);
        }else feedback("序号错误！");
    });

    connect(ui->data_device_dataitem_del,&QPushButton::clicked,this,[=](){
        int src,dst;
        src=(ui->data_seq->text()).toInt();  //绑定对象的序号
        dst=(ui->data_device_dataitem->text()).toInt(); //被绑定对象的序号
        if(dst>0){
            if(src>0){
                detach_item(6,src-1,dst-1);
            }else feedback("序号错误！");
        }else if(src>0){
            detach_item(6,_data_num-1,dst-1);
        }else feedback("序号错误！");
    });

    connect(ui->components_device_del,&QPushButton::clicked,this,[=](){
        int src,dst;
        src=(ui->components_seq->text()).toInt();  //绑定对象的序号
        dst=(ui->components_device->text()).toInt(); //被绑定对象的序号
        if(dst>0){
            if(src>0){
                detach_item(7,src-1,dst-1);
            }else feedback("序号错误！");
        }else if(src>0){
            detach_item(7,_component_num-1,dst-1);
        }else feedback("序号错误！");
    });

    connect(ui->data_components_config_del,&QPushButton::clicked,this,[=](){
        int src,dst;
        src=(ui->data_seq->text()).toInt();  //绑定对象的序号
        dst=(ui->data_components_config->text()).toInt(); //被绑定对象的序号
        if(dst>0){
            if(src>0){
                detach_item(8,src-1,dst-1);
            }else feedback("序号错误！");
        }else if(src>0){
            detach_item(8,_data_num-1,dst-1);
        }else feedback("序号错误！");
    });

    connect(ui->data_components_dataitem_del,&QPushButton::clicked,this,[=](){
        int src,dst;
        src=(ui->data_seq->text()).toInt();  //绑定对象的序号
        dst=(ui->data_components_dataitem->text()).toInt(); //被绑定对象的序号
        if(dst>0){
            if(src>0){
                detach_item(9,src-1,dst-1);
            }else feedback("序号错误！");
        }else if(src>0){
            detach_item(9,_data_num-1,dst-1);
        }else feedback("序号错误！");
    });

    connect(ui->data_channel_del,&QPushButton::clicked,this,[=](){
        int src,dst;
        src=(ui->data_seq->text()).toInt();  //绑定对象的序号
        dst=(ui->data_channel->text()).toInt(); //被绑定对象的序号
        if(dst>0){
            if(src>0){
                detach_item(11,src-1,dst-1);
            }else feedback("序号错误！");
        }else if(src>0){
            detach_item(11,_data_num-1,dst-1);
        }else feedback("序号错误！");
    });

    //新建method对象的args参数
    connect(ui->method_args_save,&QPushButton::clicked,this,[=](){

        QByteArray name_tmp,text_tmp;
        int seq;
        name_tmp=ui->method_args_name->text().toLatin1();
        text_tmp=ui->method_args_text->text().toLatin1();
        seq=(ui->method_seq->text()).toInt();
        if(seq>0){
             strcpy(methodstorage[seq-1].args_name[_method_args_num[seq-1]],name_tmp.data());
             strcpy(methodstorage[seq-1].args_text[_method_args_num[seq-1]],text_tmp.data());
             _method_args_num[seq-1]++;
        }else{
            strcpy(methodstorage[_method_num-1].args_name[_method_args_num[_method_num-1]],name_tmp.data());
            strcpy(methodstorage[_method_num-1].args_text[_method_args_num[_method_num-1]],text_tmp.data());
            _method_args_num[_method_num-1]++;
        }
        feedback("插入成功！");
    });

    //删除method对象的args参数
    connect(ui->method_args_del,&QPushButton::clicked,this,[=](){
        QByteArray name_tmp;
        int seq;
        name_tmp=ui->method_args_name->text().toLatin1();
        seq=(ui->method_seq->text()).toInt();
        if(seq>0){
           for(int i=0;i<_method_args_num[seq-1];i++){
                if(strcmp(methodstorage[seq-1].args_name[i],name_tmp.data())){
                    memset(methodstorage[seq-1].args_text[i],0,sizeof(methodstorage[seq-1].args_text[i]));
                    memset(methodstorage[seq-1].args_name[i],0,sizeof(methodstorage[seq-1].args_name[i]));
                    for(int j=i;j<_method_args_num[seq-1];j++){
                        strcpy(methodstorage[seq-1].args_text[j],methodstorage[seq-1].args_text[j+1]);
                        strcpy(methodstorage[seq-1].args_name[j],methodstorage[seq-1].args_name[j+1]);
                    }
                    _method_args_num[seq-1]--;
                    feedback("删除成功！");
                    return;
                }
           }
        }else{
            for(int i=0;i<_method_args_num[_method_num-1];i++){
                 if(strcmp(methodstorage[_method_num-1].args_name[i],name_tmp.data())){
                     memset(methodstorage[_method_num-1].args_text[i],0,sizeof(methodstorage[_method_num-1].args_text[i]));
                     memset(methodstorage[_method_num-1].args_name[i],0,sizeof(methodstorage[_method_num-1].args_name[i]));
                     for(int j=i;j<_method_args_num[_method_num-1];j++){
                         strcpy(methodstorage[_method_num-1].args_text[j],methodstorage[_method_num-1].args_text[j+1]);
                         strcpy(methodstorage[_method_num-1].args_name[j],methodstorage[_method_num-1].args_name[j+1]);
                     }
                     _method_args_num[_method_num-1]--;
                     feedback("删除成功！");
                     return;
                 }
            }
        }feedback("未查找到对应项目！");
    });

    //显示查询结果
    //root的查询结果
    connect(ui->root_find,&QPushButton::clicked,this,[=](){
        QStandardItemModel *root_table = new QStandardItemModel();
        root_table->setHorizontalHeaderItem(0, new QStandardItem(QObject::tr("No.")));
        root_table->setHorizontalHeaderItem(1, new QStandardItem(QObject::tr("id")));
        root_table->setHorizontalHeaderItem(2, new QStandardItem(QObject::tr("name")));
        root_table->setHorizontalHeaderItem(3, new QStandardItem(QObject::tr("type")));
        root_table->setHorizontalHeaderItem(4, new QStandardItem(QObject::tr("description")));
        root_table->setHorizontalHeaderItem(5, new QStandardItem(QObject::tr("version")));
        root_table->setHorizontalHeaderItem(6, new QStandardItem(QObject::tr("attached_configs_data_number")));
        root_table->setHorizontalHeaderItem(7, new QStandardItem(QObject::tr("attached_configs_method_number")));
        root_table->setHorizontalHeaderItem(8, new QStandardItem(QObject::tr("attached_device_number")));
        ui->mod_table->setModel(root_table);
        /*for(int i=0;i<9;i++){ //固定列宽
            ui->mod_table->horizontalHeader()->setSectionResizeMode(i, QHeaderView::Fixed);
        }*/
        ui->mod_table->setColumnWidth(0,30);
        ui->mod_table->setColumnWidth(1,250);
        ui->mod_table->setColumnWidth(2,250);
        ui->mod_table->setColumnWidth(3,200);
        ui->mod_table->setColumnWidth(4,250);
        ui->mod_table->setColumnWidth(5,50);
        ui->mod_table->setColumnWidth(6,300);
        ui->mod_table->setColumnWidth(7,300);
        ui->mod_table->setColumnWidth(8,300);
        ui->mod_table->setSelectionBehavior(QAbstractItemView::SelectRows);//设置整行选中
        ui->mod_table->setEditTriggers(QAbstractItemView::NoEditTriggers);//设置表格的单元为只读属性，即不能编辑

        QString configs_data_list,configs_method_list,device_list;
        for(int i=0;i<_root_num;i++){
            root_table->setItem(i,0,new QStandardItem(QString::number(i+1)));
            root_table->setItem(i,1,new QStandardItem(QObject::tr(rootstorage[i].id)));
            root_table->setItem(i,2,new QStandardItem(QObject::tr(rootstorage[i].name)));
            root_table->setItem(i,3,new QStandardItem(QObject::tr(rootstorage[i].type)));
            root_table->setItem(i,4,new QStandardItem(QObject::tr(rootstorage[i].description)));
            root_table->setItem(i,5,new QStandardItem(QObject::tr(rootstorage[i].version)));
            for(int j=0;j<_root_config_to_data_num[i];j++){
                configs_data_list=configs_data_list+QString::number(rootstorage[i].configs_data[j])+";";
            }
            root_table->setItem(i,6,new QStandardItem(configs_data_list));
            for(int j=0;j<_root_config_to_method_num[i];j++){
                configs_method_list=configs_method_list+QString::number(rootstorage[i].configs_method[j])+";";
            }
            root_table->setItem(i,7,new QStandardItem(configs_method_list));
            for(int j=0;j<_root_device_num[i];j++){
                device_list=device_list+QString::number(rootstorage[i].device[j])+";";
            }
            root_table->setItem(i,8,new QStandardItem(device_list));
        }
    });

    //device的查询结果
    connect(ui->device_find,&QPushButton::clicked,this,[=](){
        QStandardItemModel *device_table = new QStandardItemModel();
        device_table->setHorizontalHeaderItem(0, new QStandardItem(QObject::tr("No.")));
        device_table->setHorizontalHeaderItem(1, new QStandardItem(QObject::tr("id")));
        device_table->setHorizontalHeaderItem(2, new QStandardItem(QObject::tr("name")));
        device_table->setHorizontalHeaderItem(3, new QStandardItem(QObject::tr("type")));
        device_table->setHorizontalHeaderItem(4, new QStandardItem(QObject::tr("description")));
        device_table->setHorizontalHeaderItem(5, new QStandardItem(QObject::tr("number")));
        device_table->setHorizontalHeaderItem(6, new QStandardItem(QObject::tr("guid")));
        device_table->setHorizontalHeaderItem(7, new QStandardItem(QObject::tr("version")));
        device_table->setHorizontalHeaderItem(8, new QStandardItem(QObject::tr("attached_configs_data_number")));
        device_table->setHorizontalHeaderItem(9, new QStandardItem(QObject::tr("attached_configs_channel_number")));
        device_table->setHorizontalHeaderItem(10, new QStandardItem(QObject::tr("attached_dataitems_number")));
        device_table->setHorizontalHeaderItem(11, new QStandardItem(QObject::tr("attached_components_number")));

        ui->mod_table->setModel(device_table);
        /*for(int i=0;i<9;i++){ //固定列宽
            ui->mod_table->horizontalHeader()->setSectionResizeMode(i, QHeaderView::Fixed);
        }*/
        ui->mod_table->setColumnWidth(0,30);
        ui->mod_table->setColumnWidth(1,250);
        ui->mod_table->setColumnWidth(2,250);
        ui->mod_table->setColumnWidth(3,200);
        ui->mod_table->setColumnWidth(4,250);
        ui->mod_table->setColumnWidth(5,100);
        ui->mod_table->setColumnWidth(6,250);
        ui->mod_table->setColumnWidth(7,50);
        ui->mod_table->setColumnWidth(8,300);
        ui->mod_table->setColumnWidth(9,300);
        ui->mod_table->setColumnWidth(10,300);
        ui->mod_table->setColumnWidth(11,300);
        ui->mod_table->setSelectionBehavior(QAbstractItemView::SelectRows);//设置整行选中
        ui->mod_table->setEditTriggers(QAbstractItemView::NoEditTriggers);//设置表格的单元为只读属性，即不能编辑

        QString configs_data_list,configs_channel_list,dataitems_list,components_list;
        for(int i=0;i<_device_num;i++){
            device_table->setItem(i,0,new QStandardItem(QString::number(i+1)));
            device_table->setItem(i,1,new QStandardItem(QObject::tr(devicestorage[i].id)));
            device_table->setItem(i,2,new QStandardItem(QObject::tr(devicestorage[i].name)));
            device_table->setItem(i,3,new QStandardItem(QObject::tr(devicestorage[i].type)));
            device_table->setItem(i,4,new QStandardItem(QObject::tr(devicestorage[i].description)));
            device_table->setItem(i,5,new QStandardItem(QObject::tr(devicestorage[i].number)));
            device_table->setItem(i,6,new QStandardItem(QObject::tr(devicestorage[i].guid)));
            device_table->setItem(i,7,new QStandardItem(QObject::tr(devicestorage[i].version)));

            for(int j=0;j<_device_config_to_data_num[i];j++){
                configs_data_list=configs_data_list+QString::number(devicestorage[i].configs_data[j])+";";
            }
            device_table->setItem(i,8,new QStandardItem(configs_data_list));
            for(int j=0;j<_device_config_to_channel_num[i];j++){
                configs_channel_list=configs_channel_list+QString::number(devicestorage[i].configs_channel[j])+";";
            }
            device_table->setItem(i,9,new QStandardItem(configs_channel_list));
            for(int j=0;j<_device_dataitems_num[i];j++){
                dataitems_list=dataitems_list+QString::number(devicestorage[i].dataitems[j])+";";
            }
            device_table->setItem(i,10,new QStandardItem(dataitems_list));
            for(int j=0;j<_device_components_num[i];j++){
                components_list=components_list+QString::number(devicestorage[i].components[j])+";";
            }
            device_table->setItem(i,11,new QStandardItem(components_list));
        }
    });

    //components的查询结果
    connect(ui->components_find,&QPushButton::clicked,this,[=](){
        QStandardItemModel *components_table = new QStandardItemModel();
        components_table->setHorizontalHeaderItem(0, new QStandardItem(QObject::tr("No.")));
        components_table->setHorizontalHeaderItem(1, new QStandardItem(QObject::tr("id")));
        components_table->setHorizontalHeaderItem(2, new QStandardItem(QObject::tr("name")));
        components_table->setHorizontalHeaderItem(3, new QStandardItem(QObject::tr("type")));
        components_table->setHorizontalHeaderItem(4, new QStandardItem(QObject::tr("description")));
        components_table->setHorizontalHeaderItem(5, new QStandardItem(QObject::tr("number")));
        components_table->setHorizontalHeaderItem(6, new QStandardItem(QObject::tr("attached_config_number")));
        components_table->setHorizontalHeaderItem(7, new QStandardItem(QObject::tr("attached_dataitems_number")));
        components_table->setHorizontalHeaderItem(8, new QStandardItem(QObject::tr("attached_components_number")));

        ui->mod_table->setModel(components_table);
        /*for(int i=0;i<9;i++){ //固定列宽
            ui->mod_table->horizontalHeader()->setSectionResizeMode(i, QHeaderView::Fixed);
        }*/
        ui->mod_table->setColumnWidth(0,30);
        ui->mod_table->setColumnWidth(1,250);
        ui->mod_table->setColumnWidth(2,250);
        ui->mod_table->setColumnWidth(3,200);
        ui->mod_table->setColumnWidth(4,250);
        ui->mod_table->setColumnWidth(5,100);
        ui->mod_table->setColumnWidth(6,300);
        ui->mod_table->setColumnWidth(7,300);
        ui->mod_table->setColumnWidth(8,300);
        ui->mod_table->setSelectionBehavior(QAbstractItemView::SelectRows);//设置整行选中
        ui->mod_table->setEditTriggers(QAbstractItemView::NoEditTriggers);//设置表格的单元为只读属性，即不能编辑

        QString configs_list,datatiems_list,components_list;
        for(int i=0;i<_component_num;i++){
            components_table->setItem(i,0,new QStandardItem(QString::number(i+1)));
            components_table->setItem(i,1,new QStandardItem(QObject::tr(componentstorage[i].id)));
            components_table->setItem(i,2,new QStandardItem(QObject::tr(componentstorage[i].name)));
            components_table->setItem(i,3,new QStandardItem(QObject::tr(componentstorage[i].type)));
            components_table->setItem(i,4,new QStandardItem(QObject::tr(componentstorage[i].description)));
            components_table->setItem(i,5,new QStandardItem(QObject::tr(componentstorage[i].number)));

            for(int j=0;j<_components_config_num[i];j++){
                configs_list=configs_list+QString::number(componentstorage[i].configs[j])+";";
            }
            components_table->setItem(i,6,new QStandardItem(configs_list));
            for(int j=0;j<_components_dataitems_num[i];j++){
                datatiems_list=datatiems_list+QString::number(componentstorage[i].dataitems[j])+";";
            }
            components_table->setItem(i,7,new QStandardItem(datatiems_list));
            for(int j=0;j<_components_components_num[i];j++){
                components_list=components_list+QString::number(componentstorage[i].components[j])+";";
            }
            components_table->setItem(i,8,new QStandardItem(components_list));
        }
    });

    //data的查询结果
    connect(ui->data_find,&QPushButton::clicked,this,[=](){
        QStandardItemModel *data_table = new QStandardItemModel();
        data_table->setHorizontalHeaderItem(0, new QStandardItem(QObject::tr("No.")));
        data_table->setHorizontalHeaderItem(1, new QStandardItem(QObject::tr("id")));
        data_table->setHorizontalHeaderItem(2, new QStandardItem(QObject::tr("name")));
        data_table->setHorizontalHeaderItem(3, new QStandardItem(QObject::tr("type")));
        data_table->setHorizontalHeaderItem(4, new QStandardItem(QObject::tr("description")));
        data_table->setHorizontalHeaderItem(5, new QStandardItem(QObject::tr("datatype")));
        data_table->setHorizontalHeaderItem(6, new QStandardItem(QObject::tr("value")));
        data_table->setHorizontalHeaderItem(7, new QStandardItem(QObject::tr("source")));
        data_table->setHorizontalHeaderItem(8, new QStandardItem(QObject::tr("units")));
        data_table->setHorizontalHeaderItem(9, new QStandardItem(QObject::tr("setable")));

        ui->mod_table->setModel(data_table);
        /*for(int i=0;i<9;i++){ //固定列宽
            ui->mod_table->horizontalHeader()->setSectionResizeMode(i, QHeaderView::Fixed);
        }*/
        ui->mod_table->setColumnWidth(0,30);
        ui->mod_table->setColumnWidth(1,250);
        ui->mod_table->setColumnWidth(2,250);
        ui->mod_table->setColumnWidth(3,200);
        ui->mod_table->setColumnWidth(4,250);
        ui->mod_table->setColumnWidth(5,100);
        ui->mod_table->setColumnWidth(6,150);
        ui->mod_table->setColumnWidth(7,250);
        ui->mod_table->setColumnWidth(8,150);
        ui->mod_table->setColumnWidth(9,30);
        ui->mod_table->setSelectionBehavior(QAbstractItemView::SelectRows);//设置整行选中
        ui->mod_table->setEditTriggers(QAbstractItemView::NoEditTriggers);//设置表格的单元为只读属性，即不能编辑

        QString configs_list,datatiems_list,data_list;
        for(int i=0;i<_component_num;i++){
            data_table->setItem(i,0,new QStandardItem(QString::number(i+1)));
            data_table->setItem(i,1,new QStandardItem(QObject::tr(datastorage[i].id)));
            data_table->setItem(i,2,new QStandardItem(QObject::tr(datastorage[i].name)));
            data_table->setItem(i,3,new QStandardItem(QObject::tr(datastorage[i].type)));
            data_table->setItem(i,4,new QStandardItem(QObject::tr(datastorage[i].description)));
            data_table->setItem(i,5,new QStandardItem(QObject::tr(datastorage[i].datatype)));
            data_table->setItem(i,6,new QStandardItem(QObject::tr(datastorage[i].value)));
            data_table->setItem(i,7,new QStandardItem(QObject::tr(datastorage[i].source)));
            data_table->setItem(i,8,new QStandardItem(QObject::tr(datastorage[i].units)));
            if(datastorage[i].setable==true)
                data_table->setItem(i,9,new QStandardItem(QObject::tr("Yes")));
            else data_table->setItem(i,9,new QStandardItem(QObject::tr("No")));
        }
    });

    //channel的查询结果
    connect(ui->channel_find,&QPushButton::clicked,this,[=](){
        QStandardItemModel *channel_table = new QStandardItemModel();
        channel_table->setHorizontalHeaderItem(0, new QStandardItem(QObject::tr("No.")));
        channel_table->setHorizontalHeaderItem(1, new QStandardItem(QObject::tr("id")));
        channel_table->setHorizontalHeaderItem(2, new QStandardItem(QObject::tr("name")));
        channel_table->setHorizontalHeaderItem(3, new QStandardItem(QObject::tr("type")));
        channel_table->setHorizontalHeaderItem(4, new QStandardItem(QObject::tr("description")));
        channel_table->setHorizontalHeaderItem(5, new QStandardItem(QObject::tr("sampleinterval")));
        channel_table->setHorizontalHeaderItem(6, new QStandardItem(QObject::tr("uploadinterval")));
        channel_table->setHorizontalHeaderItem(7, new QStandardItem(QObject::tr("ids")));

        ui->mod_table->setModel(channel_table);
        /*for(int i=0;i<9;i++){ //固定列宽
            ui->mod_table->horizontalHeader()->setSectionResizeMode(i, QHeaderView::Fixed);
        }*/
        ui->mod_table->setColumnWidth(0,30);
        ui->mod_table->setColumnWidth(1,250);
        ui->mod_table->setColumnWidth(2,250);
        ui->mod_table->setColumnWidth(3,200);
        ui->mod_table->setColumnWidth(4,250);
        ui->mod_table->setColumnWidth(5,100);
        ui->mod_table->setColumnWidth(6,100);
        ui->mod_table->setColumnWidth(7,300);
        ui->mod_table->setSelectionBehavior(QAbstractItemView::SelectRows);//设置整行选中
        ui->mod_table->setEditTriggers(QAbstractItemView::NoEditTriggers);//设置表格的单元为只读属性，即不能编辑

        QString ids_list;
        for(int i=0;i<_component_num;i++){
            channel_table->setItem(i,0,new QStandardItem(QString::number(i+1)));
            channel_table->setItem(i,1,new QStandardItem(QObject::tr(channelstorage[i].id)));
            channel_table->setItem(i,2,new QStandardItem(QObject::tr(channelstorage[i].name)));
            channel_table->setItem(i,3,new QStandardItem(QObject::tr(channelstorage[i].type)));
            channel_table->setItem(i,4,new QStandardItem(QObject::tr(channelstorage[i].description)));
            channel_table->setItem(i,5,new QStandardItem(QString::number(channelstorage[i].sampleinterval)));
            channel_table->setItem(i,6,new QStandardItem(QString::number(channelstorage[i].uploadinterval)));

            for(int j=0;j<_channel_ids_num[i];j++){
                ids_list=ids_list+QString::number(channelstorage[i].ids[j])+";";
            }
            channel_table->setItem(i,7,new QStandardItem(ids_list));

        }
    });

    //method的查询结果
    connect(ui->method_find,&QPushButton::clicked,this,[=](){
        QStandardItemModel *method_table = new QStandardItemModel();
        method_table->setHorizontalHeaderItem(0, new QStandardItem(QObject::tr("No.")));
        method_table->setHorizontalHeaderItem(1, new QStandardItem(QObject::tr("id")));
        method_table->setHorizontalHeaderItem(2, new QStandardItem(QObject::tr("name")));
        method_table->setHorizontalHeaderItem(3, new QStandardItem(QObject::tr("type")));
        method_table->setHorizontalHeaderItem(4, new QStandardItem(QObject::tr("description")));
        method_table->setHorizontalHeaderItem(5, new QStandardItem(QObject::tr("args_name")));
        method_table->setHorizontalHeaderItem(6, new QStandardItem(QObject::tr("args_text")));


        ui->mod_table->setModel(method_table);
        /*for(int i=0;i<9;i++){ //固定列宽
            ui->mod_table->horizontalHeader()->setSectionResizeMode(i, QHeaderView::Fixed);
        }*/
        ui->mod_table->setColumnWidth(0,30);
        ui->mod_table->setColumnWidth(1,250);
        ui->mod_table->setColumnWidth(2,250);
        ui->mod_table->setColumnWidth(3,200);
        ui->mod_table->setColumnWidth(4,250);
        ui->mod_table->setColumnWidth(5,800);
        ui->mod_table->setColumnWidth(6,800);
        ui->mod_table->setSelectionBehavior(QAbstractItemView::SelectRows);//设置整行选中
        ui->mod_table->setEditTriggers(QAbstractItemView::NoEditTriggers);//设置表格的单元为只读属性，即不能编辑

        QString args_name_list,args_text_list;
        for(int i=0;i<_component_num;i++){
            method_table->setItem(i,0,new QStandardItem(QString::number(i+1)));
            method_table->setItem(i,1,new QStandardItem(QObject::tr(methodstorage[i].id)));
            method_table->setItem(i,2,new QStandardItem(QObject::tr(methodstorage[i].name)));
            method_table->setItem(i,3,new QStandardItem(QObject::tr(methodstorage[i].type)));
            method_table->setItem(i,4,new QStandardItem(QObject::tr(methodstorage[i].description)));

            for(int j=0;j<_method_args_num[i];j++){
                args_name_list=args_name_list+methodstorage[i].args_name[j]+";";
                args_text_list=args_text_list+methodstorage[i].args_text[j]+";";

            }
            method_table->setItem(i,5,new QStandardItem(args_name_list));
            method_table->setItem(i,6,new QStandardItem(args_text_list));
        }
    });

    //生成json文件
    connect(ui->save_all,&QPushButton::clicked,this,[=](){
        QString file_path;
        if(!ui->file_path->text().isEmpty()){
            file_path="..//json//";
            file_path=file_path+ui->file_path->text();
            file_path=file_path+".json";
        }else {
            feedback("文件路径错误！");
            return;
        }

        QJsonObject root_obj;
        for(int i=0;i<_root_num;i++){  //开始插入根对象
            root_obj.insert("ROOT",QString::number(i+1));//定位序号，方便解析时使用
            root_obj.insert("name",rootstorage[i].name);
            root_obj.insert("id",rootstorage[i].id);
            root_obj.insert("type",rootstorage[i].type);
            root_obj.insert("description",rootstorage[i].description);
            root_obj.insert("version",rootstorage[i].version);

            QJsonArray root_config_obj;
            for(int j=0;j<_root_config_to_data_num[i];j++){  //插入root对象中config中的data对象
                int root_config_data_seq;
                root_config_data_seq=rootstorage[i].configs_data[j];
                QJsonObject root_config_data_obj;
                //root_config_data_obj.insert("DATA",QString::number(root_config_data_seq));//定位序号，方便解析时使用
                root_config_data_obj.insert("name",datastorage[root_config_data_seq].name);
                root_config_data_obj.insert("id",datastorage[root_config_data_seq].id);
                root_config_data_obj.insert("type",datastorage[root_config_data_seq].type);
                root_config_data_obj.insert("description",datastorage[root_config_data_seq].description);
                root_config_data_obj.insert("datatype",datastorage[root_config_data_seq].datatype);
                root_config_data_obj.insert("value",datastorage[root_config_data_seq].value);
                root_config_data_obj.insert("source",datastorage[root_config_data_seq].source);
                root_config_data_obj.insert("units",datastorage[root_config_data_seq].units);
                root_config_data_obj.insert("setable",datastorage[root_config_data_seq].setable);
                root_config_obj.append(root_config_data_obj);
            }

            for(int j=0;j<_root_config_to_method_num[i];j++){ //插入root对象中config中的method对象
                int root_config_method_seq;
                root_config_method_seq=rootstorage[i].configs_method[j];
                QJsonObject root_config_method_obj;
                QJsonObject method_args;
                //root_config_method_obj.insert("METHOD",QString::number(root_config_method_seq));//定位序号，方便解析时使用
                root_config_method_obj.insert("name",methodstorage[root_config_method_seq].name);
                root_config_method_obj.insert("id",methodstorage[root_config_method_seq].id);
                root_config_method_obj.insert("type",methodstorage[root_config_method_seq].type);
                root_config_method_obj.insert("description",methodstorage[root_config_method_seq].description);

                for(int k=0;k<_method_args_num[root_config_method_seq];k++){
                    method_args.insert(methodstorage[root_config_method_seq].args_name[k],methodstorage[root_config_method_seq].args_text[k]);
                }
                root_config_method_obj.insert("args",method_args);
                root_config_obj.append(root_config_method_obj);
            }
            root_obj.insert("config",root_config_obj);

            QJsonArray root_devices_obj;
            for(int j=0;j<_root_device_num[i];j++){ //插入root对象中device对象
                 int root_device_seq;
                 root_device_seq=rootstorage[i].device[j];
                 QJsonObject root_device_obj;
                 //root_config_method_obj.insert("METHOD",QString::number(root_config_method_seq));//定位序号，方便解析时使用
                 root_device_obj.insert("name",devicestorage[root_device_seq].name);
                 root_device_obj.insert("id",devicestorage[root_device_seq].id);
                 root_device_obj.insert("type",devicestorage[root_device_seq].type);
                 root_device_obj.insert("description",devicestorage[root_device_seq].description);
                 root_device_obj.insert("number",devicestorage[root_device_seq].number);
                 root_device_obj.insert("guid",devicestorage[root_device_seq].guid);
                 root_device_obj.insert("version",devicestorage[root_device_seq].version);

                 QJsonArray device_config_obj;
                 for(int k=0;k<_device_config_to_data_num[root_device_seq];k++){  //插入device对象中config中的data对象
                     int device_config_to_data_seq;
                     device_config_to_data_seq=devicestorage[root_device_seq].configs_data[k];
                     QJsonObject device_config_to_data;
                     //device_config_to_data.insert("DATA",QString::number(device_config_to_data_seq));//定位序号，方便解析时使用
                     device_config_to_data.insert("name",datastorage[device_config_to_data_seq].name);
                     device_config_to_data.insert("id",datastorage[device_config_to_data_seq].id);
                     device_config_to_data.insert("type",datastorage[device_config_to_data_seq].type);
                     device_config_to_data.insert("description",datastorage[device_config_to_data_seq].description);
                     device_config_to_data.insert("datatype",datastorage[device_config_to_data_seq].datatype);
                     device_config_to_data.insert("value",datastorage[device_config_to_data_seq].value);
                     device_config_to_data.insert("source",datastorage[device_config_to_data_seq].source);
                     device_config_to_data.insert("units",datastorage[device_config_to_data_seq].units);
                     device_config_to_data.insert("setable",datastorage[device_config_to_data_seq].setable);
                     device_config_obj.append(device_config_to_data);
                 }

                 for(int k=0;k<_device_config_to_channel_num[root_device_seq];k++){  //插入device对象中config中的channel对象
                     int device_config_to_channel_seq;
                     device_config_to_channel_seq=devicestorage[root_device_seq].configs_channel[k];
                     QJsonObject device_config_to_channel;
                     QJsonObject channel_ids;
                     //device_config_to_channel.insert("CHANNEL",QString::number(device_config_to_channel_seq));//定位序号，方便解析时使用
                     device_config_to_channel.insert("name",channelstorage[device_config_to_channel_seq].name);
                     device_config_to_channel.insert("id",channelstorage[device_config_to_channel_seq].id);
                     device_config_to_channel.insert("type",channelstorage[device_config_to_channel_seq].type);
                     device_config_to_channel.insert("description",channelstorage[device_config_to_channel_seq].description);
                     for(int l=0;l<100&&channelstorage[device_config_to_channel_seq].ids[l]>0;l++){
                         int ids_seq=channelstorage[device_config_to_channel_seq].ids[l];
                         channel_ids.insert("id",datastorage[ids_seq].id);
                     }
                     device_config_to_channel.insert("ids",channel_ids);
                     device_config_to_channel.insert("sampleinterval",channelstorage[device_config_to_channel_seq].sampleinterval);
                     device_config_to_channel.insert("uploadinterval",channelstorage[device_config_to_channel_seq].uploadinterval);
                     device_config_obj.append(device_config_to_channel);
                 }
                 root_device_obj.insert("config",device_config_obj);

                 QJsonArray device_dataitems_obj;
                 for(int k=0;k<_device_dataitems_num[root_device_seq];k++){  //插入device对象中的dataitems对象
                     int device_dataitems_seq;
                     device_dataitems_seq=devicestorage[root_device_seq].dataitems[k];
                     QJsonObject device_dataitems;
                     //device_dataitems.insert("DATA",QString::number(device_dataitems_seq));//定位序号，方便解析时使用
                     device_dataitems.insert("name",datastorage[device_dataitems_seq].name);
                     device_dataitems.insert("id",datastorage[device_dataitems_seq].id);
                     device_dataitems.insert("type",datastorage[device_dataitems_seq].type);
                     device_dataitems.insert("description",datastorage[device_dataitems_seq].description);
                     device_dataitems.insert("datatype",datastorage[device_dataitems_seq].datatype);
                     device_dataitems.insert("value",datastorage[device_dataitems_seq].value);
                     device_dataitems.insert("source",datastorage[device_dataitems_seq].source);
                     device_dataitems.insert("units",datastorage[device_dataitems_seq].units);
                     device_dataitems.insert("setable",datastorage[device_dataitems_seq].setable);
                     device_dataitems_obj.append(device_dataitems);
                 }
                 root_device_obj.insert("dataitems",device_dataitems_obj);

                 QJsonArray device_component_obj;
                 for(int k=0;k<_device_components_num[root_device_seq];k++){  //插入device对象中的component对象
                     int device_component_seq;
                     device_component_seq=devicestorage[root_device_seq].components[k];
                     QJsonObject device_component;
                     //device_component.insert("component",QString::number(device_component_seq));//定位序号，方便解析时使用
                     device_component.insert("name",componentstorage[device_component_seq].name);
                     device_component.insert("id",componentstorage[device_component_seq].id);
                     device_component.insert("type",componentstorage[device_component_seq].type);
                     device_component.insert("description",componentstorage[device_component_seq].description);
                     device_component.insert("number",componentstorage[device_component_seq].number);

                     QJsonArray component_config_obj;
                     for(int l=0;l<_components_config_num[device_component_seq];l++){ //插入component对象中的config
                         int component_config_seq;
                         component_config_seq=componentstorage[device_component_seq].configs[l];
                         QJsonObject component_config;
                         //device_dataitems.insert("DATA",QString::number(device_dataitems_seq));//定位序号，方便解析时使用
                         component_config.insert("name",datastorage[device_component_seq].name);
                         component_config.insert("id",datastorage[device_component_seq].id);
                         component_config.insert("type",datastorage[device_component_seq].type);
                         component_config.insert("description",datastorage[device_component_seq].description);
                         component_config.insert("datatype",datastorage[device_component_seq].datatype);
                         component_config.insert("value",datastorage[device_component_seq].value);
                         component_config.insert("source",datastorage[device_component_seq].source);
                         component_config.insert("units",datastorage[device_component_seq].units);
                         component_config.insert("setable",datastorage[device_component_seq].setable);
                         component_config_obj.append(component_config);
                     }
                     device_component.insert("config",component_config_obj);

                     QJsonArray component_dataitems_obj;
                     for(int l=0;l<_components_dataitems_num[device_component_seq];l++){ //插入component对象中的dataitem
                         int component_dataitems_seq;
                         component_dataitems_seq=componentstorage[device_component_seq].dataitems[l];
                         QJsonObject component_dataitems;
                         //device_dataitems.insert("DATA",QString::number(device_dataitems_seq));//定位序号，方便解析时使用
                         component_dataitems.insert("name",datastorage[device_component_seq].name);
                         component_dataitems.insert("id",datastorage[device_component_seq].id);
                         component_dataitems.insert("type",datastorage[device_component_seq].type);
                         component_dataitems.insert("description",datastorage[device_component_seq].description);
                         component_dataitems.insert("datatype",datastorage[device_component_seq].datatype);
                         component_dataitems.insert("value",datastorage[device_component_seq].value);
                         component_dataitems.insert("source",datastorage[device_component_seq].source);
                         component_dataitems.insert("units",datastorage[device_component_seq].units);
                         component_dataitems.insert("setable",datastorage[device_component_seq].setable);
                         component_dataitems_obj.append(component_dataitems);
                     }
                     device_component.insert("dataitems",component_dataitems_obj);

                     QJsonArray component_component_obj;
                     for(int l=0;l<_components_components_num[device_component_seq];l++){  //插入device对象中的component对象
                         int component_component_seq;
                         component_component_seq=devicestorage[device_component_seq].components[l];
                         QJsonObject component_component;
                         //device_component.insert("COMPONENT",QString::number(device_component_seq));//定位序号，方便解析时使用
                         component_component.insert("name",componentstorage[device_component_seq].name);
                         component_component.insert("id",componentstorage[device_component_seq].id);
                         component_component.insert("type",componentstorage[device_component_seq].type);
                         component_component.insert("description",componentstorage[device_component_seq].description);
                         component_component.insert("number",componentstorage[device_component_seq].number);
                         component_component_obj.append(component_component);
                     }
                     device_component.insert("components",component_component_obj);
                 }
                 root_device_obj.insert("devices",root_devices_obj);
            }
            root_obj.insert("devices",root_devices_obj);
        }
        feedback("json文件封装完成！");

        QJsonDocument jsonDocu(root_obj);
        QByteArray jsonData = jsonDocu.toJson();

        QFile file(file_path);
        if(file.open(QIODevice::WriteOnly)){
            file.write(jsonData);
            file.close();
        }
        feedback("json文件写入完成");
    });


}

void nclinkdevwidget::root_option(int num){
    QByteArray temp;
    if(!ui->root_id->text().isEmpty()){
        temp=ui->root_id->text().toLatin1();
        strcpy(rootstorage[num].id,temp);
    }else strcpy(rootstorage[num].id,"NULL");

    if(!ui->root_name->text().isEmpty()){
        temp=ui->root_name->text().toLatin1();
        strcpy(rootstorage[num].name,temp);
    }else strcpy(rootstorage[num].name,"NULL");

    if(!ui->root_version->text().isEmpty()){
        temp=ui->root_version->text().toLatin1();
        strcpy(rootstorage[num].version,temp.data());
    }else strcpy(rootstorage[num].version,"NULL");

    if(!ui->root_description->text().isEmpty()){
        temp=ui->root_description->text().toLatin1();
        strcpy(rootstorage[num].description,temp.data());
    }else strcpy(rootstorage[num].description,"NULL");
    strcpy(rootstorage[num].type,"NC_LINK_ROOT");
}

void nclinkdevwidget::device_option(int num){
    QByteArray temp;
    if(!ui->device_id->text().isEmpty()){
        temp=ui->device_id->text().toLatin1();
        strcpy(devicestorage[num].id,temp.data());
    }else strcpy(devicestorage[num].id,"NULL");

    if(!ui->device_name->text().isEmpty()){
        temp=ui->device_name->text().toLatin1();
        strcpy(devicestorage[num].name,temp.data());
    }else strcpy(devicestorage[num].name,"NULL");

    if(!ui->device_version->text().isEmpty()){
        temp=ui->device_version->text().toLatin1();
        strcpy(devicestorage[num].version,temp.data());
    }else strcpy(devicestorage[num].version,"NULL");

    if(!ui->device_description->text().isEmpty()){
        temp=ui->device_description->text().toLatin1();
        strcpy(devicestorage[num].description,temp.data());
    }else strcpy(devicestorage[num].description,"NULL");

    if(!ui->device_number->text().isEmpty()){
         temp=ui->device_number->text().toLatin1();
         strcpy(devicestorage[num].number,temp.data());
    }else strcpy(devicestorage[num].number,"NULL");

    if(!ui->device_guid->text().isEmpty()){
        temp=ui->device_guid->text().toLatin1();
        strcpy(devicestorage[num].guid,temp.data());
    }else strcpy(devicestorage[num].guid,"NULL");
    temp=ui->device_type->currentText().toLatin1();
    strcpy(devicestorage[num].type,temp.data());
}

void nclinkdevwidget::component_option(int num){
    QByteArray temp;
    if(!ui->components_id->text().isEmpty()){
        temp=ui->components_id->text().toLatin1();
        strcpy(componentstorage[num].id,temp.data());
    }else strcpy(componentstorage[num].id,"NULL");

    if(!ui->components_name->text().isEmpty()){
        temp=ui->components_name->text().toLatin1();
        strcpy(componentstorage[num].name,temp.data());
    }else strcpy(componentstorage[num].name,"NULL");

    if(!ui->components_description->text().isEmpty()){
        temp=ui->components_description->text().toLatin1();
        strcpy(componentstorage[num].description,temp.data());
    }else strcpy(componentstorage[num].description,"NULL");

    if(!ui->components_number->text().isEmpty()){
        temp=ui->components_number->text().toLatin1();
        strcpy(componentstorage[num].number,temp.data());
    }else strcpy(componentstorage[num].number,"NULL");
    temp=ui->components_type->currentText().toLatin1();
    strcpy(componentstorage[num].type,temp.data());
}

void nclinkdevwidget::data_option(int num){
    QByteArray temp;
    if(!ui->data_id->text().isEmpty()){
        temp=ui->data_id->text().toLatin1();
        strcpy(datastorage[num].id,temp.data());
    }else strcpy(datastorage[num].id,"NULL");

    if(!ui->data_name->text().isEmpty()){
        temp=ui->data_name->text().toLatin1();
        strcpy(datastorage[num].name,temp.data());
    }else strcpy(datastorage[num].name,"NULL");

    if(!ui->data_description->text().isEmpty()){
        temp=ui->data_description->text().toLatin1();
        strcpy(datastorage[num].description,temp.data());
    }else strcpy(datastorage[num].description,"NULL");

    if(!ui->data_value->text().isEmpty()){
        temp=ui->data_value->text().toLatin1();
        strcpy(datastorage[num].value,temp.data());
    }else strcpy(datastorage[num].value,"NULL");

    if(!ui->data_source->text().isEmpty()){
        temp=ui->data_source->text().toLatin1();
        strcpy(datastorage[num].source,temp.data());
    }else strcpy(datastorage[num].source,"NULL");

    if(!ui->data_units->text().isEmpty()){
        temp=ui->data_units->text().toLatin1();
        strcpy(datastorage[num].units,temp.data());
    }else strcpy(datastorage[num].units,"NULL");
    temp=ui->data_type->currentText().toLatin1();
    strcpy(datastorage[num].type,temp.data());
    temp=ui->data_datatype->currentText().toLatin1();
    strcpy(datastorage[num].datatype,temp.data());
    datastorage[num].setable=ui->data_setable->isChecked();
}

void nclinkdevwidget::channel_option(int num){
    QByteArray temp;
    if(!ui->channel_id->text().isEmpty()){
        temp=ui->channel_id->text().toLatin1();
        strcpy(channelstorage[num].id,temp.data());
    }else strcpy(channelstorage[num].id,"NULL");

    if(!ui->channel_name->text().isEmpty()){
        temp=ui->channel_name->text().toLatin1();
        strcpy(channelstorage[num].name,temp.data());
    }else strcpy(channelstorage[num].name,"NULL");

    if(!ui->channel_description->text().isEmpty()){
        temp=ui->channel_description->text().toLatin1();
        strcpy(channelstorage[num].description,temp.data());
    }else strcpy(channelstorage[num].description,"NULL");

    if(!ui->channel_upload->text().isEmpty()){
        channelstorage[num].uploadinterval=(ui->channel_upload->text()).toInt();
    }else channelstorage[num].uploadinterval=0;

    if(!ui->channel_sample->text().isEmpty()){
        channelstorage[num].sampleinterval=(ui->channel_sample->text()).toInt();
    }else channelstorage[num].sampleinterval=0;
    strcpy(channelstorage[num].type,"SAMPLE_CHANNEL");
}

void nclinkdevwidget::method_option(int num){
    QByteArray temp;
    if(!ui->method_id->text().isEmpty()){
        temp=ui->method_id->text().toLatin1();
        strcpy(methodstorage[num].id,temp.data());
    }else strcpy(methodstorage[num].id,"NULL");

    if(!ui->method_name->text().isEmpty()){
        temp=ui->method_name->text().toLatin1();
        strcpy(methodstorage[num].name,temp.data());
    }else strcpy(methodstorage[num].name,"NULL");

    if(!ui->method_description->text().isEmpty()){
        temp=ui->method_description->text().toLatin1();
        strcpy(methodstorage[num].description,temp.data());
    }else strcpy(methodstorage[num].description,"NULL");
    strcpy(methodstorage[num].type,"METHOD");
}

/*
 NC-Link的复合数据对象有以下11种情况：
 1. root.config--data
 2. root.config--method
 3. root.device--device
 4. device.config--data
 5. device.config--channel
 6. device.dataitem--data
 7. device.components--components
 8. components.config--data
 9. components.dataitems--data
 10. components--components
 11. channel--data
 */
void nclinkdevwidget::attach_item(int option,int src_num,int dst_num){
    if(option<0||src_num<0||dst_num<0){
        feedback("参数错误！");
        return;
    }
    switch (option){
        case 1:
        rootstorage[dst_num].configs_data[_root_config_to_data_num[dst_num]]=src_num;
        _root_config_to_data_num[dst_num]++;
        feedback("挂载成功！");
        break;

        case 2:
        rootstorage[dst_num].configs_method[_root_config_to_method_num[dst_num]]=src_num;
        _root_config_to_method_num[dst_num]++;
        feedback("挂载成功！");
        break;

        case 3:
        rootstorage[dst_num].device[_root_device_num[dst_num]]=src_num;
        _root_device_num[dst_num]++;
        feedback("挂载成功！");
        break;

        case 4:
        devicestorage[dst_num].configs_data[_device_config_to_data_num[dst_num]]=src_num;
        _device_config_to_data_num[dst_num]++;
        feedback("挂载成功！");
        break;

        case 5:
        devicestorage[dst_num].configs_channel[_device_config_to_channel_num[dst_num]]=src_num;
        _device_config_to_channel_num[dst_num]++;
        feedback("挂载成功！");
        break;

        case 6:
        devicestorage[dst_num].dataitems[_device_dataitems_num[dst_num]]=src_num;
        _device_dataitems_num[dst_num]++;
        feedback("挂载成功！");
        break;

        case 7:
        devicestorage[dst_num].components[_device_components_num[dst_num]]=src_num;
        _device_components_num[dst_num]++;
        feedback("挂载成功！");
        break;

        case 8:
        componentstorage[dst_num].configs[_components_config_num[dst_num]]=src_num;
        _components_config_num[dst_num]++;
        feedback("挂载成功！");
        break;

        case 9:
        componentstorage[dst_num].dataitems[_components_dataitems_num[dst_num]]=src_num;
        _components_dataitems_num[dst_num]++;
        feedback("挂载成功！");
        break;

        case 10:
        componentstorage[dst_num].components[_components_components_num[dst_num]]=src_num;
        _components_components_num[dst_num]++;
        feedback("挂载成功！");
        break;

        case 11:
        channelstorage[dst_num].ids[_channel_ids_num[dst_num]]=src_num;
        _channel_ids_num[dst_num]++;
        feedback("挂载成功！");
        break;

        default:
        feedback("参数错误！");
        break;
    }
    return;
}

int detach_move(int num[],int src_num,int dst_num){
    for(int i=0;i<dst_num;i++){
        if(num[i]==src_num){
            num[i]=0;
            for(int j=i;j<i-1;j++){
                num[j]=num[j+1]; //删除后移位操作
            }
            return 1;
        }
    }
    return 0;
}

//相当于attach的逆操作
void nclinkdevwidget::detach_item(int option,int src_num,int dst_num){
    if(option<0||src_num<0||dst_num<0){
        feedback("参数错误！");
        return;
    }
    int flag=0;
    switch (option){
        case 1:
        /*
        for(i=0;i<_root_config_to_data_num[dst_num];i++){
            if(rootstorage[dst_num].configs_data[i]==src_num){
                rootstorage[dst_num].configs_data[i]=0;
                for(j=i;j<_root_config_to_data_num[dst_num]-1;j++){
                    rootstorage[dst_num].configs_data[j]=rootstorage[dst_num].configs_data[j+1];
                }
            }
        }
        */
        flag=detach_move(rootstorage[dst_num].configs_data,src_num,_root_config_to_data_num[dst_num]);
        if(flag){
            _root_config_to_data_num[dst_num]--;
            feedback("移除成功！");
        }
        else feedback("未找到移除项！");
        break;

        case 2:
        flag=detach_move(rootstorage[dst_num].configs_method,src_num,_root_config_to_method_num[dst_num]);
        if(flag){
            _root_config_to_method_num[dst_num]--;
            feedback("移除成功！");
        } else feedback("未找到移除项！");
        break;


        case 3:
        flag=detach_move(rootstorage[dst_num].device,src_num,_root_device_num[dst_num]);
        if(flag){
            _root_device_num[dst_num]--;
            feedback("移除成功！");
        } else feedback("未找到移除项！");

        case 4:
        flag=detach_move(devicestorage[dst_num].configs_data,src_num,_device_config_to_data_num[dst_num]);
        if(flag){
            _device_config_to_data_num[dst_num]--;
            feedback("移除成功！");
        } else feedback("未找到移除项！");

        case 5:
        flag=detach_move(devicestorage[dst_num].configs_channel,src_num,_device_config_to_channel_num[dst_num]);
        if(flag){
            _device_config_to_channel_num[dst_num]--;
            feedback("移除成功！");
        } else feedback("未找到移除项！");
        break;

        case 6:
        flag=detach_move(devicestorage[dst_num].dataitems,src_num,_device_dataitems_num[dst_num]);
        if(flag){
            _device_dataitems_num[dst_num]--;
            feedback("移除成功！");
        } else feedback("未找到移除项！");
        break;

        case 7:
        flag=detach_move(devicestorage[dst_num].components,src_num,_device_components_num[dst_num]);
        if(flag){
            _device_components_num[dst_num]--;
            feedback("移除成功！");
        } else feedback("未找到移除项！");
        break;

        case 8:
        flag=detach_move(componentstorage[dst_num].configs,src_num,_components_config_num[dst_num]);
        if(flag){
            _components_config_num[dst_num]--;
            feedback("移除成功！");
        } else feedback("未找到移除项！");
        break;

        case 9:
        flag=detach_move(componentstorage[dst_num].dataitems,src_num,_components_dataitems_num[dst_num]);
        if(flag){
            _components_dataitems_num[dst_num]--;
            feedback("移除成功！");
        } else feedback("未找到移除项！");
        break;

        case 10:
        flag=detach_move(componentstorage[dst_num].components,src_num,_components_components_num[dst_num]);
        if(flag){
            _components_components_num[dst_num]--;
            feedback("移除成功！");
        } else feedback("未找到移除项！");
        break;

        case 11:
        flag=detach_move(channelstorage[dst_num].ids,src_num,_channel_ids_num[dst_num]);
        if(flag){
            _channel_ids_num[dst_num]--;
            feedback("移除成功！");
        } else feedback("未找到移除项！");
        break;

        default:
        feedback("参数错误！");
        break;
    }
    return;
}



void nclinkdevwidget::feedback(QString text){
    QString str;
    str.push_back(QString("time [%1] %2 %3").arg(DATETIME).arg(text).arg("\n"));
    ui->feedback->insertPlainText(str);
}
