#include "managewidget.h"
#include "ui_managewidget.h"
#include<QStandardItemModel>

ManageWidget::ManageWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ManageWidget)
{
    ui->setupUi(this);
    setWindowFlags(Qt::FramelessWindowHint);   //去掉边框
    setAttribute(Qt::WA_StyledBackground);
}

ManageWidget::~ManageWidget(){
    delete ui;
}
