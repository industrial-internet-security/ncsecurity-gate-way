#include "nclinkmsgwidget.h"
#include "ui_nclinkmsgwidget.h"
#include "nclinkdevwidget.h"
#include "serverthread.h"

/*
struct _root rootstorage[10];
struct _device devicestorage[50];
struct _components componentstorage[50];
struct _data datastorage[200];
struct _channel channelstorage[50];
struct _method methodstorage[50];
*/

//extern struct _connect_data connectstorage[100];

nclinkmsgwidget::nclinkmsgwidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::nclinkmsgwidget)
{
    ui->setupUi(this);
    //创建实例化TCP通信对象
    tcp_server=new QTcpServer(this);
    tcp_socket=new QTcpSocket(this);
    ui->choose_port->setValidator(new QIntValidator(0, 65536,ui->choose_port));
    //ui->choose_port->setText("6000");

    connect(tcp_server,&QTcpServer::newConnection,this,[=](){
        tcp_socket=tcp_server->nextPendingConnection();

        connect(tcp_socket,&QTcpSocket::connected,this,[=](){
            ui->msg_table->append ("客户端链接成功！");
        });
        //接收数据
        connect(tcp_socket,&QTcpSocket::readyRead,this,[=](){
            QByteArray data = tcp_socket->readAll();
            ui->msg_table->append ("客户端say: " + data);
        });

        connect(tcp_socket,&QTcpSocket::disconnected,this,[=](){
            tcp_socket->close();
            tcp_socket->deleteLater();
            ui->msg_table->append ("客户端断开链接！");
        });


    });
}

nclinkmsgwidget::~nclinkmsgwidget()
{
    delete ui;
}


void nclinkmsgwidget::on_start_server_clicked()
{
    unsigned short port;
    if(!ui->choose_port->text().isEmpty())
        port=ui->choose_port->text().toUShort();
    else port=7000;
    if(!strcmp(connectstorage[0].briaddr,"\0")){
        return;
    }
    ui->msg_table->append("服务器启动！");
    tcp_server->listen(QHostAddress(connectstorage[0].briaddr),port);
    ui->start_server->setDisabled(true);
}

void nclinkmsgwidget::on_send_msg_clicked()
{
    QString msg=ui->send_msg_text->toPlainText();
    tcp_socket->write(msg.toUtf8());
    ui->msg_table->append ("服务器say: " + msg);
}
