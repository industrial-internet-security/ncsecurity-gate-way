#ifndef MANAGEWIDGET_H
#define MANAGEWIDGET_H

#include <QWidget>
#include <QTableWidget>
#include <QVector>
#include<QStandardItemModel>
//extern

namespace Ui {
class ManageWidget;
}

class ManageWidget : public QWidget
{
    Q_OBJECT

public:
    explicit ManageWidget(QWidget *parent = nullptr);
    ~ManageWidget();
    //void save_mana(int seq,int net_fd,int tap_id,char *srcaddr, unsigned short port, unsigned short family, char *briaddr);
    //void close_net(int net_fd);

private slots:
    //void init_table(QStandardItemModel *rule_table);
    //void flash_table(QStandardItemModel *rule_table);
    //void flash_table(QTableWidget *tab,int max_num,struct conn_info * conns);


private:
    Ui::ManageWidget *ui;
};

#endif // MANAGEWIDGET_H
