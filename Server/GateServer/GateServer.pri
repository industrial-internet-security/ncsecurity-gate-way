DISTFILES += \
    $$PWD/cacert.pem \
    $$PWD/cakey.pem \
    $$PWD/compile.sh \
    $$PWD/double_certs.pem \
    $$PWD/enccert.pem \
    $$PWD/enckey.pem \
    $$PWD/rootcacert.pem \
    $$PWD/rootcakey.pem \
    $$PWD/server \
    $$PWD/signcert.pem \
    $$PWD/signkey.pem

HEADERS += \
    $$PWD/bridgeAPI.h \
    $$PWD/gatestart.h \
    $$PWD/ifManage.h \
    $$PWD/tuntapAPI.h

SOURCES += \
    $$PWD/bridgeAPI.c \
    $$PWD/ifManage.c \
    $$PWD/tuntapAPI.c
