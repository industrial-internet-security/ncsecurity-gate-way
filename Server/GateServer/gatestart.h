#include <pthread.h>
#include <gmssl/mem.h>
#include <gmssl/sm2.h>
#include <gmssl/tls.h>
#include <gmssl/error.h>

typedef struct tapandip
{
    int tap;
    unsigned char ip[4];
    struct tapandip* next;
} tapandip;

typedef struct 
{
    pthread_mutex_t mutex;
    tapandip *volatile front;
    tapandip *volatile rear;
}tpqueue;

typedef struct pargs
{
    int tap_id;
    // int net_fd;
    TLS_CONNECT *conn;
    unsigned char ip[4];
}pargs;

extern tpqueue tpq;
