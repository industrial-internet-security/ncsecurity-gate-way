#include <net/if.h>
#include<unistd.h>
#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include <errno.h>
#include"bridgeAPI.h"
#include"ifManage.h"

extern char br_name[];

static void usage(void)
{
    fprintf(stderr, "Usage:\n");
    fprintf(stderr, "there is no help messsage yet\n");
    exit(1);
}

int do_clean(int argc, char **argv)
{
    int option;
    int ret=0;
    char br_to_del[IFNAMSIZ];
    strcpy(br_to_del,br_name);
    while ((option = getopt(argc, argv, "b:h")) > 0)
    {
        switch (option)
        {
        case 'h':
            usage();
            break;
        case 'b':
            strncpy(br_to_del, optarg, IFNAMSIZ - 1);
            break;
        default:
            printf("Unknown option %c\n", option);
            usage();
        }
    }
    int del_res;
    del_res=br_del_bridge(br_to_del);
    switch (del_res)
    {
        case 0:
            printf("Successfully delete bridge %s\n", br_to_del);
            break;

        case ENXIO:
            fprintf(stderr, "bridge %s doesn't exist; can't delete it\n",
                br_to_del);
            ret=1;
            break;

        case EBUSY:
            printf("%s is still up, try to down it...\n",br_to_del);
            if(net_eth_state(br_to_del,"down")!=0)
            {
                perror("down br failed!");
                ret=1;
                break;
            }
            printf("%s is down now\n",br_to_del);
            if(br_del_bridge(br_to_del)==0)
            {
                printf("Successfully delete bridge %s\n", br_to_del);
                break;
            }
            fprintf(stderr, "can't delete bridge %s: %s\n",
                br_to_del, strerror(del_res));
            ret=1;
            break;

        default:
            fprintf(stderr, "can't delete bridge %s: %s\n",
                br_to_del, strerror(del_res));
            break;
    }
    // if(my_del_veth(veth_to_del)!=0)
    // {
    //     ret=1;
    //     printf("failed to del %s!\n",veth_to_del);
    // }
    // else
    //     printf("successfully delete veth pair:%s\n",veth_to_del);
    return ret;
}