#ifndef _BRIDGEAPI_H
#define _BRIDGEAPI_H 1
extern int br_add_bridge(const char *brname);
extern int br_add_interface(const char *bridge, const char *dev);
extern int br_del_interface(const char *bridge, const char *dev);
extern int br_del_bridge(const char *brname);
#endif /* bridgeAPI.h */