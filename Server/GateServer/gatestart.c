
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <net/if.h>
#include <linux/if_tun.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <arpa/inet.h>
#include <sys/time.h>
#include <errno.h>
#include <stdarg.h>
#include"tuntapAPI.h"
#include"bridgeAPI.h"
#include"ifManage.h"
#include"gatestart.h"
#include"process.h"

#define PORT 6000
#define MAXNUM 15000
char *progname;
tpqueue tpq;
int debug=0;
int stop_flag=0;


char br_name[IFNAMSIZ] = "gatebr0";
// char veth_br[IFNAMSIZ] = "vethbr0";
// char veth_nat[IFNAMSIZ] = "vethnat0";
unsigned char br_ip[4] = {10,222,0,1};
unsigned char br_mask[4]={255,255,0,0};
/**************************************************************************
 * usage: prints usage and exits.                                         *
 **************************************************************************/
static void usage(void)
{
    fprintf(stderr, "Usage:\n");
    fprintf(stderr, "there is no help messsage yet\n");
    exit(1);
}

static void gate_init()
{   
    //创建bridge
    if(br_add_bridge(br_name)!=0)
    {
        perror("br_add_bridge()");
        exit(1);
    }
    printf("Successfully add a bridge %s\n", br_name);

    //启动bridge
    int br_state;
    if((br_state=net_eth_state_is_up(br_name))==0)
    {
        if(net_eth_state(br_name,"up")!=0)
        {
            fprintf(stderr, "can't up bridge %s: %s\n",
                br_name, strerror(errno));
            exit(1);
        }
    }
    else if(br_state==-1)
    {
        perror("net_eth_state_is_up()");
        exit(1);
    }
    printf("%s is up now\n", br_name);


    //设置bridge的ip和掩码
    if(net_eth_set_ipv4_addr(br_name,br_ip,br_mask)!=0)
    {
        perror("set br ip failed");
        exit(1);
    }
    printf("Successfully set %s's ip,netmask\n", br_name);

}

int do_start(int argc, char **argv)
{

    int  option;
    struct sockaddr_in local, remote;
    char remote_ip[16] = ""; /* dotted quad IP string */
    unsigned short int port = PORT;
    int sock_fd,  optval = 1;
    socklen_t remotelen;
    progname = argv[0];

    tpq.front=NULL;
    tpq.rear=NULL;
    pthread_mutex_init(&(tpq.mutex), NULL);

    while ((option = getopt(argc, argv, "b:p:hd")) > 0)
    {
        switch (option)
        {
        case 'd':
            debug=1;
            break;
        case 'h':
            usage();
            break;
        case 'b':
            strncpy(br_name, optarg, IFNAMSIZ - 1);
            break;
        case 'p':
            port = atoi(optarg);
            break;
        default:
            printf("Unknown option %c\n", option);
            usage();
        }
    }

    argv += optind;
    argc -= optind;

    if (argc > 0)
    {
        printf("Too many options!\n");
        usage();
    }

    if (*br_name == '\0')
    {
        printf("Must specify bridge interface name!\n");
        usage();
    }


    gate_init();

    char certfile[128] = "../GateServer/double_certs.pem";
    char signkeyfile[128] = "../GateServer/signkey.pem";
	char signpass[128] = "1234";
    char enckeyfile[128] = "../GateServer/enckey.pem";
	char encpass[128] = "1234";
    char cacertfile[128] = "../GateServer/cacert.pem";


    int server_ciphers[] = { TLS_cipher_ecc_sm4_cbc_sm3, };
    TLS_CTX ctx;
    memset(&ctx, 0, sizeof(ctx));
    if (tls_ctx_init(&ctx, TLS_protocol_tlcp, TLS_server_mode) != 1
        || tls_ctx_set_cipher_suites(&ctx, server_ciphers, sizeof(server_ciphers)/sizeof(int)) != 1
        || tls_ctx_set_tlcp_server_certificate_and_keys(&ctx, certfile, signkeyfile, signpass, enckeyfile, encpass) != 1) {
        error_print();
        return -1;
    }
    if (cacertfile) {
        if (tls_ctx_set_ca_certificates(&ctx, cacertfile, TLS_DEFAULT_VERIFY_DEPTH) != 1) {
            error_print();
            return -1;
        }
    }

    if ((sock_fd = socket(AF_INET, SOCK_STREAM, 0)) < 0)
    {
        perror("socket()");
        exit(1);
    }

    /* avoid EADDRINUSE error on bind() */
    if (setsockopt(sock_fd, SOL_SOCKET, SO_REUSEADDR, (char *)&optval, sizeof(optval)) < 0)
    {
        perror("setsockopt()");
        exit(1);
    }

    memset(&local, 0, sizeof(local));
    local.sin_family = AF_INET;
    local.sin_addr.s_addr = htonl(INADDR_ANY);
    local.sin_port = htons(port);
    if (bind(sock_fd, (struct sockaddr *)&local, sizeof(local)) < 0)
    {
        perror("bind()");
        exit(1);
    }

    if (listen(sock_fd, 5) < 0)
    {
        perror("listen()");
        exit(1);
    }

    /* wait for connection request */
    remotelen = sizeof(remote);
    memset(&remote, 0, remotelen);

    int tap_id,net_fd;
    pthread_t new_thread;
    tapandip* tptmp=NULL;
    int currentnum=2;
    pargs *ags=NULL;
    while(1)
    {
        if ((net_fd = accept(sock_fd, (struct sockaddr *)&remote, &remotelen)) < 0)
        {
            perror("accept()");
            exit(1);
        }
        printf("SERVER: Client connected from %s\n", inet_ntoa(remote.sin_addr));

        TLS_CONNECT *conn = (TLS_CONNECT *)malloc(sizeof(TLS_CONNECT));
        memset(conn, 0, sizeof(TLS_CONNECT));
        if (tls_init(conn, &ctx) != 1
            || tls_set_socket(conn, net_fd) != 1) {
            error_print();
            return -1;
        }
        if (tls_do_handshake(conn) != 1) {
		error_print();
		return -1;
	    }
        
        ags=(pargs*)malloc(sizeof(pargs));

        if(ags==NULL)
        {
            perror("malloc (pargs)");
            exit(1);
        }
        ags->conn=conn;
        ags->ip[0]=br_ip[0];
        ags->ip[1]=br_ip[1];
        pthread_mutex_lock(&(tpq.mutex));
        if(tpq.front==NULL)
        {   
            printf("queue is empty.\n");
            if(currentnum>MAXNUM)
            {
                printf("The number of connections exceeded the limit!\n");
                close(net_fd);
                continue;
            }
            ags->ip[2]=(unsigned char)(currentnum/256);
            ags->ip[3]=(unsigned char)(currentnum%256);
            tap_id=currentnum;
            currentnum++;
            ags->tap_id=tap_id;
        }
        else
        {
            printf("queue is not empty.\n");
            tptmp=tpq.front;
            tpq.front=tpq.front->next;
            if(tpq.front==NULL) tpq.rear=NULL;
            ags->tap_id=tptmp->tap;
            ags->ip[0]=tptmp->ip[0];
            ags->ip[1]=tptmp->ip[1];
            ags->ip[2]=tptmp->ip[2];
            ags->ip[3]=tptmp->ip[3];
            free(tptmp);
        }
        pthread_mutex_unlock(&(tpq.mutex));
        pthread_create(&new_thread,NULL,process,ags);
        pthread_detach(new_thread);

        if(stop_flag=1){
            exit(1);
        }
    }
}
