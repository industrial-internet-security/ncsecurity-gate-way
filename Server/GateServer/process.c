
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <net/if.h>
#include <linux/if_tun.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
// #include <sys/select.h>
#include <sys/time.h>
#include <errno.h>
#include <stdarg.h>
#include <sys/epoll.h>
#include"tuntapAPI.h"
#include"bridgeAPI.h"
#include"ifManage.h"
#include "gatestart.h"

/* buffer for reading from tun/tap interface, must be >= 1500 */
#define BUFSIZE 2000
extern int debug;
extern char br_name[IFNAMSIZ];

static void my_debug(char *msg, ...)
{
    
    if(debug) 
    {
        va_list argp;
        va_start(argp, msg);
        vfprintf(stderr, msg, argp);
        va_end(argp);
    }
}

static size_t tls_read_n(TLS_CONNECT *conn, char *buf, size_t n)
{
    size_t nread=0, left = n;

    while (left > 0)
    {
        int rv;
        if ((rv = tls_recv(conn, (uint8_t *)buf, left, &nread)) != 1) 
        {
			if (rv < 0) 
                return rv;
			else 
                return 0;
		}
        else
        {
            if(nread==0)
                return 0;
            left -= nread;
            buf += nread;
        }
    }
    return n;
}

/**************************************************************************
 * cread: read routine that checks for errors and exits if an error is    *
 *        returned.                                                       *
 **************************************************************************/
static int cread(int fd, char *buf, int n)
{

    int nread;

    if ((nread = read(fd, buf, n)) < 0)
        perror("Reading data");
    return nread;
}

/**************************************************************************
 * cwrite: write routine that checks for errors and exits if an error is  *
 *         returned.                                                      *
 **************************************************************************/
static int cwrite(int fd, char *buf, int n)
{

    int nwrite;

    if ((nwrite = write(fd, buf, n)) < 0){
        perror("Writing data");
        return -1;
    }
    return nwrite;
}

/**************************************************************************
 * read_n: ensures we read exactly n bytes, and puts them into "buf".     *
 *         (unless EOF, of course)                                        *
 **************************************************************************/
static int read_n(int fd, char *buf, int n)
{

    int nread, left = n;

    while (left > 0)
    {
        if ((nread = cread(fd, buf, left)) <= 0)
        {
            return 0;
        }
        else if(nread<0)
            return nread;
        else
        {
            left -= nread;
            buf += nread;
        }
    }
    return n;
}

static int tap_init(char *tap_name)
{
    int tap_state;
    /* initialize tap interface */
    int tap_fd;
    if ((tap_fd = tun_alloc(tap_name, IFF_TAP | IFF_NO_PI)) < 0)
    {
        printf("Error connecting to tap interface %s!\n", tap_name);
        return -1;
    }

    //启动虚拟网卡
    if((tap_state=net_eth_state_is_up(tap_name))==0)
    {
        if(net_eth_state(tap_name,"up")!=0)
        {
            perror("up tap failed");
            return -1;
        }
    }
    else if(tap_state==-1)
    {
        perror("net_eth_state_is_up()");
        return -1;
    }
    

    //虚拟网卡加到桥上
    if(br_add_interface(br_name,tap_name)!=0)
    {
        perror("br_add_interface(tap)");
        return -1;
    }
    printf("init %s and add it to %s\n", tap_name,br_name);
    return tap_fd;
}

static void process_quit(TLS_CONNECT* conn,int tap_fd,int tap_id,unsigned char *ipmsg)
{
    if(tap_fd>=0) close(tap_fd);
    close(conn->sock);
    tls_cleanup(conn);
    pthread_mutex_lock(&(tpq.mutex));
    if(tpq.rear==NULL)
    {
        while((tpq.rear=(tapandip*)malloc(sizeof(tapandip)))==NULL);
        tpq.rear->next=NULL;
        tpq.rear->tap=tap_id;
        tpq.rear->ip[0]=ipmsg[0];
        tpq.rear->ip[1]=ipmsg[1];
        tpq.rear->ip[2]=ipmsg[2];
        tpq.rear->ip[3]=ipmsg[3];
        tpq.front=tpq.rear;
    }
    else
    {
        while((tpq.rear->next=(tapandip*)malloc(sizeof(tapandip)))==NULL);
        tpq.rear=tpq.rear->next;
        tpq.rear->next=NULL;
        tpq.rear->tap=tap_id;
        tpq.rear->ip[0]=ipmsg[0];
        tpq.rear->ip[1]=ipmsg[1];
        tpq.rear->ip[2]=ipmsg[2];
        tpq.rear->ip[3]=ipmsg[3];
    }
    pthread_mutex_unlock(&(tpq.mutex));
    printf("thread:%lu quit...\n",pthread_self());
    pthread_exit(NULL);
}
void* process(void* args)
{
    printf("thread:%lu start...\n",pthread_self());
    pargs* a= (pargs*)args;
    TLS_CONNECT conn = *(a->conn);
    free(a->conn);
    int tap_id=a->tap_id;
    unsigned char ipmsg[4];
    ipmsg[0]=a->ip[0];
    ipmsg[1]=a->ip[1];
    ipmsg[2]=a->ip[2];
    ipmsg[3]=a->ip[3];
    free(a);

    char tap_name[IFNAMSIZ] = "";
    sprintf(tap_name,"gtap%d",tap_id);

    int tap_fd;
    if((tap_fd=tap_init(tap_name))<0)
    {
        process_quit(&conn,tap_fd,tap_id,ipmsg);
    }

    size_t sentlen;
    if(tls_send(&conn, ipmsg, 4, &sentlen) != 1)
    {
        fprintf(stderr,"send ip:%d.%d.%d.%d failed\n",ipmsg[0],ipmsg[1],ipmsg[2],ipmsg[3]);
        process_quit(&conn,tap_fd,tap_id,ipmsg);
    }
    else if(sentlen!=4)
    {
        fprintf(stderr,"send ip:%d.%d.%d.%d failed\n",ipmsg[0],ipmsg[1],ipmsg[2],ipmsg[3]);
        process_quit(&conn,tap_fd,tap_id,ipmsg);
    }


    // int maxfd = (tap_fd > net_fd) ? tap_fd : net_fd;
    uint16_t nread, nwrite, plength;
    char buffer[BUFSIZE];
    unsigned long int tap2net = 0, net2tap = 0;

    //epoll
    int epfd=epoll_create(2);
    struct epoll_event ev, events[2];

    ev.data.fd=tap_fd;
    ev.events=EPOLLIN;
    epoll_ctl(epfd,EPOLL_CTL_ADD,tap_fd,&ev);

    ev.data.fd=conn.sock;
    ev.events=EPOLLIN;
    epoll_ctl(epfd,EPOLL_CTL_ADD,conn.sock,&ev);

    while (1)
    {
        int nfds=epoll_wait(epfd,events,2,-1);
        if (nfds < 0 && errno == EINTR)
        {
            continue;
        }

        if (nfds < 0)
        {
            perror("epoll_wait()");
            process_quit(&conn,tap_fd,tap_id,ipmsg);
        }
        for(int i=0;i<nfds;i++)
        {
            if(events[i].data.fd==conn.sock)
            {
                 /* data from the network: read it, and write it to the tun/tap interface.
                * We need to read the length first, and then the packet */

                /* Read length */

                if((nread = tls_read_n(&conn, (char *)&plength, sizeof(plength)))<0)
                    process_quit(&conn,tap_fd,tap_id,ipmsg);
                if (nread == 0)
                {
                    /* ctrl-c at the other end */
                    // break;
                    process_quit(&conn,tap_fd,tap_id,ipmsg);
                }
                my_debug("ntohs(plength)=%d\n", ntohs(plength));

                net2tap++;

                /* read packet */
                if((nread = tls_read_n(&conn, buffer, ntohs(plength)))<0)
                    process_quit(&conn,tap_fd,tap_id,ipmsg);
                my_debug("NET2TAP %lu: Read %d bytes from the network\n", net2tap, nread);

                /* now buffer[] contains a full packet or frame, write it into the tun/tap interface */
                if((nwrite = cwrite(tap_fd, buffer, nread))<0)
                    process_quit(&conn,tap_fd,tap_id,ipmsg);
                my_debug("NET2TAP %lu: Written %d bytes to the tap interface\n", net2tap, nwrite);
            }
            else if (events[i].data.fd==tap_fd)
            {
                /* data from tun/tap: just read it and write it to the network */

                if((nread = cread(tap_fd, buffer, BUFSIZE))<0)
                    process_quit(&conn,tap_fd,tap_id,ipmsg);

                tap2net++;
                my_debug("TAP2NET %lu: Read %d bytes from the tap interface\n", tap2net, nread);

                /* write length + packet */
                plength = htons(nread);
                // if((nwrite = cwrite(net_fd, (char *)&plength, sizeof(plength)))<0)
                //     process_quit(net_fd,tap_fd,tap_id,ipmsg);
                // if((nwrite = cwrite(net_fd, buffer, nread))<0)
                //     process_quit(net_fd,tap_fd,tap_id,ipmsg);
                
                if(tls_send(&conn, (char *)&plength, sizeof(plength), &sentlen) != 1)
                    process_quit(&conn,tap_fd,tap_id,ipmsg);
                if(tls_send(&conn, buffer, nread, &sentlen) != 1)
                    process_quit(&conn,tap_fd,tap_id,ipmsg);

                my_debug("TAP2NET %lu: Written %d bytes to the network\n", tap2net, nwrite);
            }

            
        }

    }
    process_quit(&conn,tap_fd,tap_id,ipmsg);
    
    
}
