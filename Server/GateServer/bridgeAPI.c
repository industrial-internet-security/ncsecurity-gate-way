#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <sys/fcntl.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <sys/socket.h>
#include <linux/sockios.h>
#include <linux/if_bridge.h>
#include <net/if.h>

static int br_socket_fd = -1;

static int br_init(void)
{
	if ((br_socket_fd = socket(AF_LOCAL, SOCK_STREAM, 0)) < 0)
		return errno;
	return 0;
}

static void br_shutdown(void)
{
	if (br_socket_fd >= 0) {
		close(br_socket_fd);
		br_socket_fd = -1;
	}
}

int br_add_bridge(const char *brname)
{
	if (br_init()) 
		return errno;
	

	int ret;

#ifdef SIOCBRADDBR
	ret = ioctl(br_socket_fd, SIOCBRADDBR, brname);
	if (ret < 0)
#endif
	{
		char _br[IFNAMSIZ];
		unsigned long arg[3] = {BRCTL_ADD_BRIDGE, (unsigned long)_br};

		strncpy(_br, brname, IFNAMSIZ);
		ret = ioctl(br_socket_fd, SIOCSIFBR, arg);
	}
	br_shutdown();
	return ret < 0 ? errno : 0;
}

int br_del_bridge(const char *brname)
{
	if (br_init()) 
		return errno;
	int ret;

#ifdef SIOCBRDELBR
	ret = ioctl(br_socket_fd, SIOCBRDELBR, brname);
	if (ret < 0)
#endif
	{
		char _br[IFNAMSIZ];
		unsigned long arg[3] = {BRCTL_DEL_BRIDGE, (unsigned long)_br};

		strncpy(_br, brname, IFNAMSIZ);
		ret = ioctl(br_socket_fd, SIOCSIFBR, arg);
	}
	br_shutdown();
	return ret < 0 ? errno : 0;
}

int br_add_interface(const char *bridge, const char *dev)
{
	if (br_init()) 
		return errno;	
	struct ifreq ifr;
	int err;
	int ifindex = if_nametoindex(dev);

	if (ifindex == 0)
		return ENODEV;

	strncpy(ifr.ifr_name, bridge, IFNAMSIZ);
#ifdef SIOCBRADDIF
	ifr.ifr_ifindex = ifindex;
	err = ioctl(br_socket_fd, SIOCBRADDIF, &ifr);
	if (err < 0)
#endif
	{
		unsigned long args[4] = {BRCTL_ADD_IF, ifindex, 0, 0};

		ifr.ifr_data = (char *)args;
		err = ioctl(br_socket_fd, SIOCDEVPRIVATE, &ifr);
	}
	br_shutdown();
	return err < 0 ? errno : 0;
}

int br_del_interface(const char *bridge, const char *dev)
{
	if (br_init()) 
		return errno;	
	struct ifreq ifr;
	int err;
	int ifindex = if_nametoindex(dev);

	if (ifindex == 0)
		return ENODEV;

	strncpy(ifr.ifr_name, bridge, IFNAMSIZ);
#ifdef SIOCBRDELIF
	ifr.ifr_ifindex = ifindex;
	err = ioctl(br_socket_fd, SIOCBRDELIF, &ifr);
	if (err < 0)
#endif
	{
		unsigned long args[4] = {BRCTL_DEL_IF, ifindex, 0, 0};

		ifr.ifr_data = (char *)args;
		err = ioctl(br_socket_fd, SIOCDEVPRIVATE, &ifr);
	}
	br_shutdown();
	return err < 0 ? errno : 0;
}

