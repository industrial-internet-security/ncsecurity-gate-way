
#include<stdlib.h>
#include<stdio.h>
#include<string.h>
#include"gatecmds.h"


static void usage(void)
{
    fprintf(stderr, "Usage:\n");
    fprintf(stderr, "there is no help messsage yet\n");
    exit(1);
}

static const struct cmd {
	const char *cmd;
	int (*func)(int argc, char **argv);
} cmds[] = {
	{ "clean", 	do_clean },
	{ "start",	do_start },
	{ 0 }
};

static int do_cmd(const char *argv0, int argc, char **argv)
{
	const struct cmd *c;

	for (c = cmds; c->cmd; ++c) {
		if (strcmp(argv0, c->cmd) == 0) {
			return -(c->func(argc, argv));
		}
	}

	fprintf(stderr, "unknown command:%s\n", argv0);
	usage();
	return 1;
}


int main_start(int argc, char **argv)
{
    if (argc > 1)
		return do_cmd(argv[1], argc-1, argv+1);
    usage();
}
