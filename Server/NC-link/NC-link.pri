HEADERS += \
    $$PWD/config.h \
    $$PWD/default_sample_driver.h \
    $$PWD/driver.h \
    $$PWD/interface.h \
    $$PWD/model.h \
    $$PWD/mqtt.h \
    $$PWD/nclink.h \
    $$PWD/nclinkdef.h \
    $$PWD/request_output.h \
    $$PWD/sample_manager.h
