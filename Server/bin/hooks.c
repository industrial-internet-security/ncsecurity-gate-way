#include <linux/time.h>
#include <linux/timer.h>
#include <linux/jiffies.h>
#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/module.h>
#include <linux/version.h>
#include <linux/skbuff.h>
#include <linux/netfilter.h>
#include <linux/netfilter_ipv4.h>
#include <linux/ip.h>
#include <linux/tcp.h>
#include <linux/udp.h>
#include <linux/icmp.h>
#include <linux/spinlock.h>
#include <linux/netlink.h>
#include <linux/rbtree.h>

#define REQ_GETAllIPRules 1 //功能标志
#define REQ_ADDIPRule 2
#define REQ_DELIPRule 3
#define REQ_SETAction 4 
#define REQ_GETAllIPLogs 5
#define REQ_GETAllConns 6
#define REQ_ADDNATRule 7
#define REQ_DELNATRule 8
#define REQ_GETNATRules 9
#define RSP_Only_Head 10
#define RSP_MSG 11
#define RSP_IPRules 12  
#define RSP_IPLogs 13   
#define RSP_NATRules 14 
#define RSP_ConnLogs 15 
#define NAT_TYPE_NO 0
#define NAT_TYPE_SRC 1
#define NAT_TYPE_DEST 2
#define NETLINK_MYFW 17

#define MAX_LOG_LEN 1000
#define CONN_NEEDLOG 0x10
#define CONN_MAX_SYM_NUM 3
#define CONN_EXPIRES 7 
#define CONN_NAT_TIMES 10 
#define CONN_ROLL_INTERVAL 5 

struct IPRule {
    char name[10];
    unsigned int saddr;
    unsigned int smask;
    unsigned int daddr;
    unsigned int dmask;
    unsigned int sport; 
    unsigned int dport; 
    u_int8_t protocol;
    unsigned int action;
    unsigned int log;
    struct IPRule* next;
};

struct IPLog {
    long tm;
    unsigned int saddr;
    unsigned int daddr;
    unsigned short sport;
    unsigned short dport;
    u_int8_t protocol;
    unsigned int len;
    unsigned int action;
    struct IPLog* next;
};

struct NATRule { 
    unsigned int saddr; 
    unsigned int smask; 
    unsigned int daddr; 

    unsigned short sport; 
    unsigned short dport; 
    unsigned short nowPort; 
    struct NATRule* next;
};

struct ConnLog {
    unsigned int saddr;
    unsigned int daddr;
    unsigned short sport;
    unsigned short dport;
    u_int8_t protocol;
    int natType;
    struct NATRule nat; 
};

struct UserReq {
    unsigned int tp;
    char ruleName[10];
    union {
        struct IPRule ipRule;
        struct NATRule natRule;
        unsigned int defaultAction;
        unsigned int num;
    } msg;
};

struct KerFunctionHeader {
    unsigned int bodyTp;
    unsigned int arrayLen;
};

typedef unsigned int conn_key_t[CONN_MAX_SYM_NUM]; 
typedef struct connNode {
    struct rb_node node;
    conn_key_t key; 
    unsigned long expires;
    u_int8_t protocol; 
    u_int8_t needLog; 

    struct NATRule nat; 
    int natType;           
}connNode;

struct sock *netlink_init(void); //初始化netlink
void netlink_release(void);  //关闭netlink
int netlinkSend(unsigned int pid, void *data, unsigned int len);  //netlink发送
int dealAppMessage(unsigned int pid, void *msg, unsigned int len); //处理用户模块netlink
void* formAllIPRules(unsigned int *len);  //执行功能
struct IPRule * addIPRuleToKernel(char after[], struct IPRule rule);
int delIPRuleFromKernel(char name[]);
void* formAllIPLogs(unsigned int num, unsigned int *len);
void* formAllConns(unsigned int *len);
struct NATRule * addNATRuleToKernel(struct NATRule rule);
int delNATRuleFromKernel(int num);
void* formAllNATRules(unsigned int *len);
struct IPRule matchIPRules(struct sk_buff *skb, int *isMatch);
int addLog(struct IPLog log);
int addLogBySKB(unsigned int action, struct sk_buff *skb);

void conn_init(void);  //初始化连接表
void conn_exit(void);  //关闭连接表
struct connNode *hasConn(unsigned int sip, unsigned int dip, unsigned short sport, unsigned short dport);
struct connNode *addConn(unsigned int sip, unsigned int dip, unsigned short sport, unsigned short dport, u_int8_t proto, u_int8_t log);
bool matchOneRule(struct IPRule *rule, unsigned int sip, unsigned int dip, unsigned short sport, unsigned int dport, u_int8_t proto);
int eraseConnRelated(struct IPRule rule);
void addConnExpires(struct connNode *node, unsigned int plus);
int setConnNAT(struct connNode *node, struct NATRule record, int natType);
struct NATRule *matchNATRule(unsigned int sip, unsigned int dip, int *isMatch);
unsigned short getNewNATPort(struct NATRule rule);
struct NATRule genNATRule(unsigned int preIP, unsigned int afterIP, unsigned short prePort, unsigned short afterPort);
unsigned int hook_main(void *priv,struct sk_buff *skb,const struct nf_hook_state *state);
unsigned int hook_nat_in(void *priv,struct sk_buff *skb,const struct nf_hook_state *state);
unsigned int hook_nat_out(void *priv,struct sk_buff *skb,const struct nf_hook_state *state);
void getPort(struct sk_buff *skb, struct iphdr *hdr, unsigned short *src_port, unsigned short *dst_port);
bool isIPMatch(unsigned int ipl, unsigned int ipr, unsigned int mask);

extern unsigned int DEFAULT_ACTION;

int sendMsgToApp(unsigned int pid, const char *msg) {
    void* mem;
    unsigned int rspLen;
    struct KerFunctionHeader *rspH;
    rspLen = sizeof(struct KerFunctionHeader) + strlen(msg) + 1;
    mem = kzalloc(rspLen, GFP_ATOMIC);
    if(mem == NULL) {
        printk(KERN_WARNING "[firewall k2app] sendMsgToApp kzalloc fail.\n");
        return 0;
    }
    rspH = (struct KerFunctionHeader *)mem;
    rspH->bodyTp = RSP_MSG;
    rspH->arrayLen = strlen(msg);
    memcpy(mem+sizeof(struct KerFunctionHeader), msg, strlen(msg));
    netlinkSend(pid, mem, rspLen);
    kfree(mem);
    return rspLen;
}

void dealWithSetAction(unsigned int action) {
    if(action != NF_ACCEPT) {
        struct IPRule rule = {
            .smask = 0,
            .dmask = 0,
            .sport = -1,
            .dport = -1
        }; // 清除全部连接
        eraseConnRelated(rule);
    }
}

int dealAppMessage(unsigned int pid, void *msg, unsigned int len) {
    struct UserReq *req;
    struct KerFunctionHeader *rspH;
    void* mem;
    unsigned int rspLen = 0;
    req = (struct UserReq *) msg;
    switch (req->tp)
    {
    case REQ_GETAllIPLogs:
        mem = formAllIPLogs(req->msg.num, &rspLen);
        if(mem == NULL) {
            printk(KERN_WARNING "[firewall k2app] formAllIPLogs fail.\n");
            sendMsgToApp(pid, "form all logs fail.");
            break;
        }
        netlinkSend(pid, mem, rspLen);
        kfree(mem);
        break;
    case REQ_GETAllConns:
        mem = formAllConns(&rspLen);
        if(mem == NULL) {
            printk(KERN_WARNING "[firewall k2app] formAllConns fail.\n");
            sendMsgToApp(pid, "form all conns fail.");
            break;
        }
        netlinkSend(pid, mem, rspLen);
        kfree(mem);
        break;
    case REQ_GETAllIPRules:
        mem = formAllIPRules(&rspLen);
        if(mem == NULL) {
            printk(KERN_WARNING "[firewall k2app] formAllIPRules fail.\n");
            sendMsgToApp(pid, "form all rules fail.");
            break;
        }
        netlinkSend(pid, mem, rspLen);
        kfree(mem);
        break;
    case REQ_ADDIPRule:
        if(addIPRuleToKernel(req->ruleName, req->msg.ipRule)==NULL) {
            rspLen = sendMsgToApp(pid, "Fail: no such rule or retry it.");
            printk("[firewall k2app] add rule fail.\n");
        } else {
            rspLen = sendMsgToApp(pid, "Success.");
            printk("[firewall k2app] add one rule success: %s.\n", req->msg.ipRule.name);
        }
        break;
    case REQ_DELIPRule:
        rspLen = sizeof(struct KerFunctionHeader);
        rspH = (struct KerFunctionHeader *)kzalloc(rspLen, GFP_KERNEL);
        if(rspH == NULL) {
            printk(KERN_WARNING "[firewall k2app] kzalloc fail.\n");
            sendMsgToApp(pid, "form rsp fail but del maybe success.");
            break;
        }
        rspH->bodyTp = RSP_Only_Head;
        rspH->arrayLen = delIPRuleFromKernel(req->ruleName);
        printk("[firewall k2app] success del %d rules.\n", rspH->arrayLen);
        netlinkSend(pid, rspH, rspLen);
        kfree(rspH);
        break;
    case REQ_GETNATRules:
        mem = formAllNATRules(&rspLen);
        if(mem == NULL) {
            printk(KERN_WARNING "[firewall k2app] formAllNATRules fail.\n");
            sendMsgToApp(pid, "form all NAT rules fail.");
            break;
        }
        netlinkSend(pid, mem, rspLen);
        kfree(mem);
        break;
    case REQ_ADDNATRule:
        if(addNATRuleToKernel(req->msg.natRule)==NULL) {
            rspLen = sendMsgToApp(pid, "Fail: please retry it.");
            printk("[firewall k2app] add NAT rule fail.\n");
        } else {
            rspLen = sendMsgToApp(pid, "Success.");
            printk("[firewall k2app] add one NAT rule success.\n");
        }
        break;
    case REQ_DELNATRule:
        rspLen = sizeof(struct KerFunctionHeader);
        rspH = (struct KerFunctionHeader *)kzalloc(rspLen, GFP_KERNEL);
        if(rspH == NULL) {
            printk(KERN_WARNING "[firewall k2app] kzalloc fail.\n");
            sendMsgToApp(pid, "form rsp fail but del maybe success.");
            break;
        }
        rspH->bodyTp = RSP_Only_Head;
        rspH->arrayLen = delNATRuleFromKernel(req->msg.num);
        printk("[firewall k2app] success del %d NAT rules.\n", rspH->arrayLen);
        netlinkSend(pid, rspH, rspLen);
        kfree(rspH);
        break;
    case REQ_SETAction:
        if(req->msg.defaultAction == NF_ACCEPT) {
            DEFAULT_ACTION = NF_ACCEPT;
            rspLen = sendMsgToApp(pid, "Set default action to ACCEPT.");
            printk("[firewall k2app] Set default action to NF_ACCEPT.\n");
        } else {
            DEFAULT_ACTION = NF_DROP;
            rspLen = sendMsgToApp(pid, "Set default action to DROP.");
            printk("[firewall k2app] Set default action to NF_DROP.\n");
        }
        dealWithSetAction(DEFAULT_ACTION);
        break;
    default:
        rspLen = sendMsgToApp(pid, "No such req.");
        break;
    }
    return rspLen;
}

static struct IPLog *logHead = NULL,*logTail = NULL;
static unsigned int logNum = 0;
static DEFINE_RWLOCK(logLock);

// 释放首部多余的日志节点 整理链表
int rollLog(void) {
    struct IPLog *tmp;
    unsigned int count = 0;
    printk("[firewall logs] roll log chain.\n");
    write_lock(&logLock);
    while(logNum > MAX_LOG_LEN) {
        if(logHead == NULL) { // 链表头指针丢失
            logHead = logTail;
            logNum = logTail==NULL ? 0 : 1;
            write_unlock(&logLock);
            return count;
        }
        tmp = logHead;
        logHead = logHead->next;
        logNum--;
        count++;
        if(logTail == tmp) { // 链表尾指针丢失
            logTail = logHead;
            logNum = logTail==NULL ? 0 : 1;
        }
        kfree(tmp);
    }
    write_unlock(&logLock);
    return count;
}

// 新增日志记录
int addLog(struct IPLog log) {
    struct IPLog *newLog;
    newLog = (struct IPLog *) kzalloc(sizeof(struct IPLog), GFP_KERNEL);
    if(newLog == NULL) {
        printk(KERN_WARNING "[firewall logs] kzalloc fail.\n");
        return 0;
    }
    memcpy(newLog, &log, sizeof(struct IPLog));
    newLog->next = NULL;
    // 新增日志至日志链表
    write_lock(&logLock);
    if(logTail == NULL) { // 日志链表为空
        logTail = newLog;
        logHead = logTail;
        logNum = 1;
        write_unlock(&logLock);
        return 1;
    }
    logTail->next = newLog;
    logTail = newLog;
    logNum++;
    write_unlock(&logLock);
    if(logNum > MAX_LOG_LEN) {
        rollLog();
    }
    return 1;
}

int addLogBySKB(unsigned int action, struct sk_buff *skb) {
    struct IPLog log;
    unsigned short sport,dport;
	struct iphdr *header;
	//struct timeval now;
    /*struct timeval now = {
        .tv_sec = 0,
        .tv_usec = 0
    };
    do_gettimeofday(&now);
    log.tm = now.tv_sec;*/
    struct timespec64 ts64;
    ktime_get_real_ts64(&ts64);
    log.tm=ts64.tv_sec;
    
    header = ip_hdr(skb);
	getPort(skb,header,&sport,&dport);
    log.saddr = ntohl(header->saddr);
    log.daddr = ntohl(header->daddr);
    log.sport = sport;
    log.dport = dport;
    log.len = header->tot_len - (header->ihl * 4);
    log.protocol = header->protocol;
    log.action = action;
    log.next = NULL;
    return addLog(log);
}

// 将所有过滤日志形成Netlink回包
void* formAllIPLogs(unsigned int num, unsigned int *len) {
    struct KerFunctionHeader *head;
    struct IPLog *now;
    void *mem,*p;
    unsigned int count;
    read_lock(&logLock);
    for(now=logHead,count=0;now!=NULL;now=now->next,count++); // 计算日志总量
    printk("[firewall logs] form logs count=%d, need num=%d.\n", count, num);
    if(num == 0 || num > count)
        num = count;
    *len = sizeof(struct KerFunctionHeader) + sizeof(struct IPLog) * num; // 申请回包空间
    mem = kzalloc(*len, GFP_ATOMIC);
    if(mem == NULL) {
        printk(KERN_WARNING "[firewall logs] formAllIPLogs kzalloc fail.\n");
        read_unlock(&logLock);
        return NULL;
    }
    // 构建回包
    head = (struct KerFunctionHeader *)mem;
    head->bodyTp = RSP_IPLogs;
    head->arrayLen = num;
    p=(mem + sizeof(struct KerFunctionHeader));
    for(now=logHead;now!=NULL;now=now->next) {
        if(count > num) { // 只取最后num个日志
            count--;
            continue;
        }
        memcpy(p, now, sizeof(struct IPLog));
        p=p+sizeof(struct IPLog);
    }
    read_unlock(&logLock);
    return mem;
}

static struct NATRule *natRuleHead = NULL;
static DEFINE_RWLOCK(natRuleLock);

// 首部新增一条NAT规则
struct NATRule * addNATRuleToKernel(struct NATRule rule) {
    struct NATRule *newRule;
    newRule = (struct NATRule *) kzalloc(sizeof(struct NATRule), GFP_KERNEL);
    if(newRule == NULL) {
        printk(KERN_WARNING "[firewall nat] kzalloc fail.\n");
        return NULL;
    }
    memcpy(newRule, &rule, sizeof(struct NATRule));
    // 新增规则至规则链表
    write_lock(&natRuleLock);
    if(natRuleHead == NULL) {
        natRuleHead = newRule;
        natRuleHead->next = NULL;
        write_unlock(&natRuleLock);
        return newRule;
    }
    newRule->next = natRuleHead;
    natRuleHead = newRule;
    write_unlock(&natRuleLock);
    return newRule;
}

// 删除序号为num的NAT规则
int delNATRuleFromKernel(int num) {
    struct NATRule *now,*tmp;
    struct IPRule iprule;
    int count = 0;
    write_lock(&natRuleLock);
    if(num == 0) {
        tmp = natRuleHead;
        natRuleHead = natRuleHead->next;
        kfree(tmp);
        write_unlock(&natRuleLock);
        return 1;
    }
    for(now=natRuleHead,count=1;now!=NULL && now->next!=NULL;now=now->next,count++) {
        if(count == num) { // 删除规则
            tmp = now->next;
            now->next = now->next->next;
            iprule.saddr = tmp->saddr; // 消除连接池影响
            iprule.smask = tmp->smask;
            iprule.dmask = 0;
            iprule.sport = 0xFFFFu;
            iprule.dport = 0xFFFFu;
            eraseConnRelated(iprule);
            kfree(tmp);
            write_unlock(&natRuleLock);
            return 1;
        }
    }
    write_unlock(&natRuleLock);
    return 0;
}

// 将所有NAT规则形成Netlink回包
void* formAllNATRules(unsigned int *len) {
    struct KerFunctionHeader *head;
    struct NATRule *now;
    void *mem,*p;
    unsigned int count;
    read_lock(&natRuleLock);
    for(now=natRuleHead,count=0;now!=NULL;now=now->next,count++);
    *len = sizeof(struct KerFunctionHeader) + sizeof(struct NATRule)*count;
    mem = kzalloc(*len, GFP_ATOMIC);
    if(mem == NULL) {
        printk(KERN_WARNING "[firewall nat] kzalloc fail.\n");
        read_unlock(&natRuleLock);
        return NULL;
    }
    head = (struct KerFunctionHeader *)mem;
    head->bodyTp = RSP_NATRules;
    head->arrayLen = count;
    for(now=natRuleHead,p=(mem + sizeof(struct KerFunctionHeader));now!=NULL;now=now->next,p=p+sizeof(struct NATRule))
        memcpy(p, now, sizeof(struct NATRule));
    read_unlock(&natRuleLock);
    return mem;
}

struct NATRule *matchNATRule(unsigned int sip, unsigned int dip, int *isMatch) {
    struct NATRule *now;
    *isMatch = 0;
    read_lock(&natRuleLock);
	for(now=natRuleHead;now!=NULL;now=now->next) {
		if(isIPMatch(sip, now->saddr, now->smask) &&
           !isIPMatch(dip, now->saddr, now->smask) &&
           dip != now->daddr) {
            read_unlock(&natRuleLock);
            *isMatch = 1;
			return now;
		}
	}
	read_unlock(&natRuleLock);
    return NULL;
}

struct NATRule genNATRule(unsigned int preIP, unsigned int afterIP, unsigned short prePort, unsigned short afterPort) {
    struct NATRule record;
    record.saddr = preIP;
    record.sport = prePort;
    record.daddr = afterIP;
    record.dport = afterPort;
    return record;
}

static struct rb_root connRoot = RB_ROOT;
static DEFINE_RWLOCK(connLock);

// 比较连接标识符
int connKeyCmp(conn_key_t l, conn_key_t r) {
	register int i;
	for(i=0;i<CONN_MAX_SYM_NUM;i++) {
		if(l[i] != r[i]) {
			return (l[i] < r[i]) ? -1 : 1;
		}
	}
	return 0;
}

// 按标识符查找节点，无该节点返回则NULL
struct connNode *searchNode(struct rb_root *root, conn_key_t key) {
	int result;
	struct rb_node *node;
	read_lock(&connLock);
	node = root->rb_node;
	while (node) {
		struct connNode *data = rb_entry(node, struct connNode, node);
		result = connKeyCmp(key, data->key);
		if (result < 0)
			node = node->rb_left;
		else if (result > 0)
			node = node->rb_right;
		else { // 找到节点
			read_unlock(&connLock);
			return data;
		}
	}
	read_unlock(&connLock);
	return NULL;
}

// 插入新节点，返回节点
struct connNode *insertNode(struct rb_root *root, struct connNode *data) {
	struct rb_node **new, *parent;
	if(data == NULL) {
		return NULL;
	}
	parent = NULL;
	read_lock(&connLock);
	new = &(root->rb_node);
	while (*new) {
		struct connNode *this = rb_entry(*new, struct connNode, node);
		int result = connKeyCmp(data->key, this->key);
		parent = *new;
		if (result < 0)
			new = &((*new)->rb_left);
		else if (result > 0)
			new = &((*new)->rb_right);
		else { //已存在
			read_unlock(&connLock);
			return this;
		}
	}
	read_unlock(&connLock);
	write_lock(&connLock);
	rb_link_node(&data->node, parent, new);
	rb_insert_color(&data->node, root);
	write_unlock(&connLock);
	return data; // 插入成功
}

// 删除节点
void eraseNode(struct rb_root *root, struct connNode *node) {
	if(node != NULL) {
		write_lock(&connLock);
		rb_erase(&(node->node), root);
		write_unlock(&connLock);
		kfree(node);
	}
}

// 是否超时
int isTimeout(unsigned long expires) {
	return (jiffies >= expires)? 1 : 0; // 当前时间 >= 超时时间?
}

void addConnExpires(struct connNode *node, unsigned int plus) {
	if(node == NULL)
		return ;
	write_lock(&connLock);
	node->expires = (jiffies + ((plus) * HZ));
	write_unlock(&connLock);
}

// 检查是否存在指定连接
struct connNode *hasConn(unsigned int sip, unsigned int dip, unsigned short sport, unsigned short dport) {
	conn_key_t key;
	struct connNode *node = NULL;
	// 构建标识符
	key[0] = sip;
	key[1] = dip;
	key[2] = ((((unsigned int)sport) << 16) | ((unsigned int)dport));
	// 查找节点
	node = searchNode(&connRoot, key);
	addConnExpires(node, CONN_EXPIRES); // 重新设置超时时间
	return node;
}

// 新建连接
struct connNode *addConn(unsigned int sip, unsigned int dip, unsigned short sport, unsigned short dport, u_int8_t proto, u_int8_t log) {
	// 初始化
	struct connNode *node = (struct connNode *)kzalloc(sizeof(connNode), GFP_ATOMIC);
	if(node == NULL) {
		printk(KERN_WARNING "[firewall conns] kzalloc fail.\n");
		return 0;
	}
	node->needLog = log;
	node->protocol = proto;
	node->expires = /*timeFromNow*/(CONN_EXPIRES); // 设置超时时间
	node->natType = NAT_TYPE_NO;
	// 构建标识符
	node->key[0] = sip;
	node->key[1] = dip;
	node->key[2] = ((((unsigned int)sport) << 16) | ((unsigned int)dport));
	// 插入节点
	return insertNode(&connRoot, node);
}

// 设置连接的NAT
int setConnNAT(struct connNode *node, struct NATRule record, int natType) {
	if(node==NULL)
		return 0;
	write_lock(&connLock);
	node->natType = natType;
	node->nat = record;
	write_unlock(&connLock);
	return 1;
}

// 获取新的可用NAT端口
unsigned short getNewNATPort(struct NATRule rule) {
	struct rb_node *node;
	struct connNode *now;
	unsigned short port, inUse;
	// 遍历
	if(rule.nowPort > rule.dport || rule.nowPort < rule.sport)
		rule.nowPort = rule.dport;
	for(port = rule.nowPort + 1; port != rule.nowPort; port++) {
		if(port > rule.dport || port < rule.sport)
			port = rule.sport;
		read_lock(&connLock);
		for(node = rb_first(&connRoot), inUse = 0; node; node=rb_next(node)) {
			now = rb_entry(node, struct connNode, node);
			if(now->natType != NAT_TYPE_SRC)
				continue;
			if(now->nat.daddr != rule.daddr)
				continue;
			if(port == now->nat.dport) {
				inUse = 1;
				break;
			}
		}
		read_unlock(&connLock);
		if(!inUse) {
			return port;
		}
	}
	return 0;
}

// 将所有已有连接形成Netlink回包
void* formAllConns(unsigned int *len) {
    struct KerFunctionHeader *head;
    struct rb_node *node;
	struct connNode *now;
	struct ConnLog log;
    void *mem,*p;
    unsigned int count;
    read_lock(&connLock);
	// 计算总量
    for (node=rb_first(&connRoot),count=0;node;node=rb_next(node),count++); 
	// 申请回包空间
	*len = sizeof(struct KerFunctionHeader) + sizeof(struct ConnLog) * count;
	mem = kzalloc(*len, GFP_ATOMIC);
    if(mem == NULL) {
        printk(KERN_WARNING "[firewall conns] formAllConns kzalloc fail.\n");
        read_unlock(&connLock);
        return NULL;
    }
    // 构建回包
    head = (struct KerFunctionHeader *)mem;
    head->bodyTp = RSP_ConnLogs;
    head->arrayLen = count;
    p=(mem + sizeof(struct KerFunctionHeader));
    for (node = rb_first(&connRoot); node; node=rb_next(node),p=p+sizeof(struct ConnLog)) {
		now = rb_entry(node, struct connNode, node);
		log.saddr = now->key[0];
		log.daddr = now->key[1];
		log.sport = (unsigned short)(now->key[2] >> 16);
		log.dport = (unsigned short)(now->key[2] & 0xFFFFu);
		log.protocol = now->protocol;
		log.natType = now->natType;
		log.nat = now->nat;
		memcpy(p, &log, sizeof(struct ConnLog));
	}
    read_unlock(&connLock);
    return mem;
}

// 依据过滤规则，删除相关连接
int eraseConnRelated(struct IPRule rule) {
	struct rb_node *node;
	unsigned short sport,dport;
	struct connNode *needDel = NULL;
	unsigned int count = 0;
	int hasChange = 1; // 连接池是否有更改（删除节点）
	// 初始化
	rule.protocol = IPPROTO_IP;
	// 删除相关节点
	while(hasChange) { // 有更改时，持续遍历，防止漏下节点
		hasChange = 0;
		read_lock(&connLock);
		for (node = rb_first(&connRoot); node; node = rb_next(node)) {
			needDel = rb_entry(node, struct connNode, node);
			sport = (unsigned short)(needDel->key[2] >> 16);
			dport = (unsigned short)(needDel->key[2] & 0xFFFFu);
			if(matchOneRule(&rule, needDel->key[0], needDel->key[1], sport, dport, needDel->protocol)) { // 相关规则
				hasChange = 1;
				break;
			}
		}
		read_unlock(&connLock);
		if(hasChange) { //开始删除节点
			eraseNode(&connRoot, needDel);
			count++;
		}
	}
	printk("[firewall conns] erase all related conn finish.\n");
	return count;
}

// 刷新连接池定时器所用，删除超时连接
int rollConn(void) {
	struct rb_node *node;
	struct connNode *needDel = NULL;
	int hasChange = 1; // 连接池是否有更改（删除节点）
	while(hasChange) { // 有更改时，持续遍历，防止漏下节点
		hasChange = 0;
		read_lock(&connLock);
		for (node = rb_first(&connRoot); node; node = rb_next(node)) {
			needDel = rb_entry(node, struct connNode, node);
			if(isTimeout(needDel->expires)) { // 超时确认删除
				hasChange = 1;
				break;
			}
		}
		read_unlock(&connLock);
		if(hasChange) { // 开始删除节点
			eraseNode(&connRoot, needDel);
		}
	}
	return 0;
}

// --- 定时器相关 ---
static struct timer_list conn_timer;//定义计时器

// 计时器回调函数
#if LINUX_VERSION_CODE < KERNEL_VERSION(4,14,0)
void conn_timer_callback(unsigned long arg) {
#else
void conn_timer_callback(struct timer_list *t) {
#endif
    rollConn();
	mod_timer(&conn_timer, /*timeFromNow*/(CONN_ROLL_INTERVAL)); //重新激活定时器
}

// 初始化连接池相关内容（包括定时器）
void conn_init(void) {
#if LINUX_VERSION_CODE < KERNEL_VERSION(4,14,0)
    init_timer(&conn_timer);
    conn_timer.function = &conn_timer_callback;//设置定时器回调方法
    conn_timer.data = ((unsigned long)0);
#else
    timer_setup(&conn_timer, conn_timer_callback, 0);
#endif
	conn_timer.expires = /*timeFromNow*/(CONN_ROLL_INTERVAL);//超时时间设置为CONN_ROLL_INTERVAL秒后
	add_timer(&conn_timer);//激活定时器
}

// 关闭连接池
void conn_exit(void) {
	del_timer(&conn_timer);
}

static struct IPRule *ipRuleHead = NULL;
static DEFINE_RWLOCK(ipRuleLock);

// 在名称为after的规则后新增一条规则，after为空时则在首部新增一条规则
struct IPRule * addIPRuleToKernel(char after[], struct IPRule rule) {
    struct IPRule *newRule,*now;
    newRule = (struct IPRule *) kzalloc(sizeof(struct IPRule), GFP_KERNEL);
    if(newRule == NULL) {
        printk(KERN_WARNING "[firewall rules] kzalloc fail.\n");
        return NULL;
    }
    memcpy(newRule, &rule, sizeof(struct IPRule));
    // 新增规则至规则链表
    write_lock(&ipRuleLock);
    if(rule.action != NF_ACCEPT) 
        eraseConnRelated(rule); // 消除新增规则的影响
    if(ipRuleHead == NULL) {
        ipRuleHead = newRule;
        ipRuleHead->next = NULL;
        write_unlock(&ipRuleLock);
        return newRule;
    }
    if(strlen(after)==0) {
        newRule->next = ipRuleHead;
        ipRuleHead = newRule;
        write_unlock(&ipRuleLock);
        return newRule;
    }
    for(now=ipRuleHead;now!=NULL;now=now->next) {
        if(strcmp(now->name, after)==0) {
            newRule->next = now->next;
            now->next = newRule;
            write_unlock(&ipRuleLock);
            return newRule;
        }
    }
    // 添加失败
    write_unlock(&ipRuleLock);
    kfree(newRule);
    return NULL;
}

// 删除所有名称为name的规则
int delIPRuleFromKernel(char name[]) {
    struct IPRule *now,*tmp;
    int count = 0;
    write_lock(&ipRuleLock);
    while(ipRuleHead!=NULL && strcmp(ipRuleHead->name,name)==0) {
        tmp = ipRuleHead;
        ipRuleHead = ipRuleHead->next;
        eraseConnRelated(*tmp); // 消除删除规则的影响
        kfree(tmp);
        count++;
    }
    for(now=ipRuleHead;now!=NULL && now->next!=NULL;) {
        if(strcmp(now->next->name,name)==0) { // 删除下条规则
            tmp = now->next;
            now->next = now->next->next;
            eraseConnRelated(*tmp); // 消除删除规则的影响
            kfree(tmp);
            count++;
        } else {
            now = now->next;
        }
    }
    write_unlock(&ipRuleLock);
    return count;
}

// 将所有规则形成Netlink回包
void* formAllIPRules(unsigned int *len) {
    struct KerFunctionHeader *head;
    struct IPRule *now;
    void *mem,*p;
    unsigned int count;
    read_lock(&ipRuleLock);
    for(now=ipRuleHead,count=0;now!=NULL;now=now->next,count++);
    *len = sizeof(struct KerFunctionHeader) + sizeof(struct IPRule)*count;
    mem = kzalloc(*len, GFP_ATOMIC);
    if(mem == NULL) {
        printk(KERN_WARNING "[firewall rules] kzalloc fail.\n");
        read_unlock(&ipRuleLock);
        return NULL;
    }
    head = (struct KerFunctionHeader *)mem;
    head->bodyTp = RSP_IPRules;
    head->arrayLen = count;
    for(now=ipRuleHead,p=(mem + sizeof(struct KerFunctionHeader));now!=NULL;now=now->next,p=p+sizeof(struct IPRule))
        memcpy(p, now, sizeof(struct IPRule));
    read_unlock(&ipRuleLock);
    return mem;
}

bool matchOneRule(struct IPRule *rule,
 unsigned int sip, unsigned int dip, unsigned short sport, unsigned int dport, u_int8_t proto) {
    return (isIPMatch(sip,rule->saddr,rule->smask) &&
			isIPMatch(dip,rule->daddr,rule->dmask) &&
			(sport >= ((unsigned short)(rule->sport >> 16)) && sport <= ((unsigned short)(rule->sport & 0xFFFFu))) &&
			(dport >= ((unsigned short)(rule->dport >> 16)) && dport <= ((unsigned short)(rule->dport & 0xFFFFu))) &&
			(rule->protocol == IPPROTO_IP || rule->protocol == proto));
}

// 进行过滤规则匹配，isMatch存储是否匹配到规则
struct IPRule matchIPRules(struct sk_buff *skb, int *isMatch) {
    struct IPRule *now,ret;
	unsigned short sport,dport;
	struct iphdr *header = ip_hdr(skb);
	*isMatch = 0;
	getPort(skb,header,&sport,&dport);
	read_lock(&ipRuleLock);
	for(now=ipRuleHead;now!=NULL;now=now->next) {
		if(matchOneRule(now,ntohl(header->saddr),ntohl(header->daddr),sport,dport,header->protocol)) {
				ret = *now;
				*isMatch = 1;
				break;
		}
	}
	read_unlock(&ipRuleLock);
	return ret;
}

static struct sock *nlsk = NULL;

int netlinkSend(unsigned int pid, void *data, unsigned int len) {
	int retval;
	struct nlmsghdr *nlh;
	struct sk_buff *skb;
	// init sk_buff
	skb = nlmsg_new(len, GFP_ATOMIC);
	if (skb == NULL) {
		printk(KERN_WARNING "[firewall netlink] alloc reply nlmsg skb failed!\n");
		return -1;
	}
	nlh = nlmsg_put(skb, 0, 0, 0, NLMSG_SPACE(len) - NLMSG_HDRLEN, 0);
	// send data
	memcpy(NLMSG_DATA(nlh), data, len);
	NETLINK_CB(skb).dst_group = 0;
	retval = netlink_unicast(nlsk, skb, pid, MSG_DONTWAIT);
	printk("[firewall netlink] send to user pid=%d,len=%d,ret=%d\n", pid, nlh->nlmsg_len - NLMSG_SPACE(0), retval);
	return retval;
}

void netlinkRecv(struct sk_buff *skb) {
	void *data;
	struct nlmsghdr *nlh = NULL;
	unsigned int pid,len;
    // check skb
    nlh = nlmsg_hdr(skb);
	if ((nlh->nlmsg_len < NLMSG_HDRLEN) || (skb->len < nlh->nlmsg_len)) {
		printk(KERN_WARNING "[firewall netlink] Illegal netlink packet!\n");
		return;
	}
    // deal data
	data = NLMSG_DATA(nlh);
    pid = nlh->nlmsg_pid;
    len = nlh->nlmsg_len - NLMSG_SPACE(0);
	if(len<sizeof(struct UserReq)) {
		printk(KERN_WARNING "[firewall netlink] packet size < UserReq!\n");
		return;
	}
	printk("[firewall netlink] data receive from user: user_pid=%d, len=%d\n", pid, len);
	dealAppMessage(pid, data, len);
}

struct netlink_kernel_cfg nltest_cfg = {
	.groups = 0,
	.flags = 0,
	.input = netlinkRecv,
	.cb_mutex = NULL,
	.bind = NULL,
	.unbind = NULL,
	.compare = NULL,
};

struct sock *netlink_init() {
    nlsk = netlink_kernel_create(&init_net, NETLINK_MYFW, &nltest_cfg);
	if (!nlsk) {
		printk(KERN_WARNING "[firewall netlink] can not create a netlink socket\n");
		return NULL;
	}
	printk("[firewall netlink] netlink_kernel_create() success, nlsk = %p\n", nlsk);
    return nlsk;
}

void netlink_release() {
    netlink_kernel_release(nlsk);
}

unsigned int DEFAULT_ACTION = NF_ACCEPT;

unsigned int hook_main(void *priv,struct sk_buff *skb,const struct nf_hook_state *state) {
    struct IPRule rule;
    struct connNode *conn;
    unsigned short sport, dport;
    unsigned int sip, dip, action = DEFAULT_ACTION;
    int isMatch = 0, isLog = 0;
    // 初始化
	struct iphdr *header = ip_hdr(skb);
	getPort(skb,header,&sport,&dport);
    sip = ntohl(header->saddr);
    dip = ntohl(header->daddr);
    // 查询是否有已有连接
    conn = hasConn(sip, dip, sport, dport);
    if(conn != NULL) {
        if(conn->needLog) // 记录日志
            addLogBySKB(action, skb);
        return NF_ACCEPT;
    }
    // 匹配规则
    rule = matchIPRules(skb, &isMatch);
    if(isMatch) { // 匹配到了一条规则
        printk(KERN_DEBUG "[firewall netfilter] patch rule %s.\n", rule.name);
        action = (rule.action==NF_ACCEPT) ? NF_ACCEPT : NF_DROP;
        if(rule.log) { // 记录日志
            isLog = 1;
            addLogBySKB(action, skb);
        }
    }
    // 更新连接池
    if(action == NF_ACCEPT) {
        addConn(sip,dip,sport,dport,header->protocol,isLog);
    }
    return action;
}

unsigned int hook_nat_in(void *priv,struct sk_buff *skb,const struct nf_hook_state *state) {
    struct connNode *conn;
    struct NATRule record;
    unsigned short sport, dport;
    unsigned int sip, dip;
    u_int8_t proto;
    struct tcphdr *tcpHeader;
    struct udphdr *udpHeader;
    int hdr_len, tot_len;
    // 初始化
    struct iphdr *header = ip_hdr(skb);
    getPort(skb,header,&sport,&dport);
    sip = ntohl(header->saddr);
    dip = ntohl(header->daddr);
    proto = header->protocol;
    // 查连接池 NAT_TYPE_DEST
    conn = hasConn(sip, dip, sport, dport);
    if(conn == NULL) { // 不应出现连接表中不存在的情况
        printk(KERN_WARNING "[firewall nat] (in)get a connection that is not in the connection pool!\n");
        return NF_ACCEPT;
    }
    // 无记录->返回
    if(conn->natType != NAT_TYPE_DEST) {
        return NF_ACCEPT;
    }
    // 转换目的地址+端口
    record = conn->nat;
    header->daddr = htonl(record.daddr);
    hdr_len = header->ihl * 4;
    tot_len = ntohs(header->tot_len);
    header->check = 0;
    header->check = ip_fast_csum(header, header->ihl);
    switch(proto) {
        case IPPROTO_TCP:
            tcpHeader = (struct tcphdr *)(skb->data + (header->ihl * 4));
            tcpHeader->dest = htons(record.dport);
            tcpHeader->check = 0;
            skb->csum = csum_partial((unsigned char *)tcpHeader, tot_len - hdr_len, 0);
            tcpHeader->check = csum_tcpudp_magic(header->saddr, header->daddr,
                                        tot_len - hdr_len, header->protocol, skb->csum);
            break;
        case IPPROTO_UDP:
            udpHeader = (struct udphdr *)(skb->data + (header->ihl * 4));
            udpHeader->dest = htons(record.dport);
            udpHeader->check = 0;
            skb->csum = csum_partial((unsigned char *)udpHeader, tot_len - hdr_len, 0);
            udpHeader->check = csum_tcpudp_magic(header->saddr, header->daddr,
                                        tot_len - hdr_len, header->protocol, skb->csum);
            break;
        case IPPROTO_ICMP:
        default:
            break;
    }
    return NF_ACCEPT;
}

unsigned int hook_nat_out(void *priv,struct sk_buff *skb,const struct nf_hook_state *state) {
    struct connNode *conn,*reverseConn;
    struct NATRule record;
    int isMatch, hdr_len, tot_len;
    struct tcphdr *tcpHeader;
    struct udphdr *udpHeader;
    u_int8_t proto;
    unsigned int sip, dip;
    unsigned short sport, dport;
    // 初始化
    struct iphdr *header = ip_hdr(skb);
    getPort(skb,header,&sport,&dport);
    sip = ntohl(header->saddr);
    dip = ntohl(header->daddr);
    proto = header->protocol;
    // 查连接池 NAT_TYPE_SRC
    conn = hasConn(sip, dip, sport, dport);
    if(conn == NULL) { // 不应出现连接表中不存在的情况
        printk(KERN_WARNING "[firewall nat] (out)get a connection that is not in the connection pool!\n");
        return NF_ACCEPT;
    }
    // 确定NAT记录
    if(conn->natType == NAT_TYPE_SRC) { // 已有
        record = conn->nat;
    } else {
        unsigned short newPort = 0;
        struct NATRule *rule = matchNATRule(sip, dip, &isMatch);
        if(!isMatch || rule == NULL) { // 不符合NAT规则，无需NAT
            return NF_ACCEPT;
        }
        // 新建NAT记录
        if(sport != 0) {
            newPort = getNewNATPort(*rule);
            if(newPort == 0) { // 获取新端口失败，放弃NAT
                printk(KERN_WARNING "[firewall nat] get new port failed!\n");
                return NF_ACCEPT;
            }
        }
        record = genNATRule(sip, rule->daddr, sport, newPort);
        // 记录在原连接中
        setConnNAT(conn, record, NAT_TYPE_SRC);
        rule->nowPort = newPort;
    }
    // 寻找反向连接
    reverseConn = hasConn(dip, record.daddr, dport, record.dport);
    if(reverseConn == NULL) { // 新建反向连接入连接池
        reverseConn = addConn(dip, record.daddr, dport, record.dport, proto, 0);
        if(reverseConn == NULL) { // 创建反向连接失败，放弃NAT
            printk(KERN_WARNING "[firewall nat] add reverse connection failed!\n");
            return NF_ACCEPT;
        }
        setConnNAT(reverseConn, genNATRule(record.daddr, sip, record.dport, sport), NAT_TYPE_DEST);
    }
    addConnExpires(reverseConn, CONN_EXPIRES * CONN_NAT_TIMES); // 更新超时时间
    addConnExpires(conn, CONN_EXPIRES * CONN_NAT_TIMES); // 更新超时时间
    // 转换源地址+端口
    header->saddr = htonl(record.daddr);
    hdr_len = header->ihl * 4;
    tot_len = ntohs(header->tot_len);
    header->check = 0;
    header->check = ip_fast_csum(header, header->ihl);
    switch(proto) {
        case IPPROTO_TCP:
            tcpHeader = (struct tcphdr *)(skb->data + (header->ihl * 4));
            tcpHeader->source = htons(record.dport);
            tcpHeader->check = 0;
            skb->csum = csum_partial((unsigned char *)tcpHeader, tot_len - hdr_len, 0);
            tcpHeader->check = csum_tcpudp_magic(header->saddr, header->daddr,
                                        tot_len - hdr_len, header->protocol, skb->csum);
            break;
        case IPPROTO_UDP:
            udpHeader = (struct udphdr *)(skb->data + (header->ihl * 4));
            udpHeader->source = htons(record.dport);
            udpHeader->check = 0;
            skb->csum = csum_partial((unsigned char *)udpHeader, tot_len - hdr_len, 0);
            udpHeader->check = csum_tcpudp_magic(header->saddr, header->daddr,
                                        tot_len - hdr_len, header->protocol, skb->csum);
            break;
        case IPPROTO_ICMP:
        default:
            break;
    }
    return NF_ACCEPT;
}

void getPort(struct sk_buff *skb, struct iphdr *hdr, unsigned short *src_port, unsigned short *dst_port){
	struct tcphdr *tcpHeader;
	struct udphdr *udpHeader;
	switch(hdr->protocol){
		case IPPROTO_TCP:
			tcpHeader = (struct tcphdr *)(skb->data + (hdr->ihl * 4));
			*src_port = ntohs(tcpHeader->source);
			*dst_port = ntohs(tcpHeader->dest);
			break;
		case IPPROTO_UDP:
			udpHeader = (struct udphdr *)(skb->data + (hdr->ihl * 4));
			*src_port = ntohs(udpHeader->source);
			*dst_port = ntohs(udpHeader->dest);
			break;
		case IPPROTO_ICMP:
		default:
			*src_port = 0;
			*dst_port = 0;
			break;
	}
}

bool isIPMatch(unsigned int ipl, unsigned int ipr, unsigned int mask) {
	return (ipl & mask) == (ipr & mask);
}
