/********************************************************************************
** Form generated from reading UI file 'FloatWidget.ui'
**
** Created by: Qt User Interface Compiler version 5.12.8
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_FLOATWIDGET_H
#define UI_FLOATWIDGET_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_FloatWidget
{
public:
    QGridLayout *gridLayout;
    QLineEdit *HexLine;
    QPushButton *convFlaot;
    QPushButton *convHex;
    QLabel *label_2;
    QLabel *label;
    QLineEdit *FloatLine;

    void setupUi(QWidget *FloatWidget)
    {
        if (FloatWidget->objectName().isEmpty())
            FloatWidget->setObjectName(QString::fromUtf8("FloatWidget"));
        FloatWidget->resize(400, 300);
        gridLayout = new QGridLayout(FloatWidget);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        HexLine = new QLineEdit(FloatWidget);
        HexLine->setObjectName(QString::fromUtf8("HexLine"));

        gridLayout->addWidget(HexLine, 0, 1, 1, 1);

        convFlaot = new QPushButton(FloatWidget);
        convFlaot->setObjectName(QString::fromUtf8("convFlaot"));

        gridLayout->addWidget(convFlaot, 0, 2, 1, 1);

        convHex = new QPushButton(FloatWidget);
        convHex->setObjectName(QString::fromUtf8("convHex"));

        gridLayout->addWidget(convHex, 1, 2, 1, 1);

        label_2 = new QLabel(FloatWidget);
        label_2->setObjectName(QString::fromUtf8("label_2"));

        gridLayout->addWidget(label_2, 0, 0, 1, 1);

        label = new QLabel(FloatWidget);
        label->setObjectName(QString::fromUtf8("label"));

        gridLayout->addWidget(label, 1, 0, 1, 1);

        FloatLine = new QLineEdit(FloatWidget);
        FloatLine->setObjectName(QString::fromUtf8("FloatLine"));

        gridLayout->addWidget(FloatLine, 1, 1, 1, 1);


        retranslateUi(FloatWidget);

        QMetaObject::connectSlotsByName(FloatWidget);
    } // setupUi

    void retranslateUi(QWidget *FloatWidget)
    {
        FloatWidget->setWindowTitle(QApplication::translate("FloatWidget", "Form", nullptr));
        convFlaot->setText(QApplication::translate("FloatWidget", "\350\275\254\345\214\226\344\270\272\345\215\225\347\262\276\345\272\246\346\265\256\347\202\271\346\225\260", nullptr));
        convHex->setText(QApplication::translate("FloatWidget", "\350\275\254\345\214\226\344\270\27216\350\277\233\345\210\266\346\225\260", nullptr));
        label_2->setText(QApplication::translate("FloatWidget", "Hex:", nullptr));
        label->setText(QApplication::translate("FloatWidget", "Float:", nullptr));
    } // retranslateUi

};

namespace Ui {
    class FloatWidget: public Ui_FloatWidget {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_FLOATWIDGET_H
