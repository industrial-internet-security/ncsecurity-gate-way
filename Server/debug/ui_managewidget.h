/********************************************************************************
** Form generated from reading UI file 'managewidget.ui'
**
** Created by: Qt User Interface Compiler version 5.12.8
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MANAGEWIDGET_H
#define UI_MANAGEWIDGET_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QTableView>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_ManageWidget
{
public:
    QStatusBar *statusBar;
    QGridLayout *gridLayout_4;
    QGridLayout *gridLayout_3;
    QGroupBox *groupBox;
    QVBoxLayout *verticalLayout;
    QTableView *tableView;
    QGroupBox *groupBox_2;
    QGridLayout *gridLayout;
    QPushButton *flash;
    QLabel *label;
    QPushButton *save;
    QPushButton *disconnect;
    QSpinBox *fd_num;

    void setupUi(QWidget *ManageWidget)
    {
        if (ManageWidget->objectName().isEmpty())
            ManageWidget->setObjectName(QString::fromUtf8("ManageWidget"));
        ManageWidget->resize(662, 693);
        ManageWidget->setStyleSheet(QString::fromUtf8(""));
        statusBar = new QStatusBar(ManageWidget);
        statusBar->setObjectName(QString::fromUtf8("statusBar"));
        statusBar->setGeometry(QRect(0, 0, 3, 18));
        gridLayout_4 = new QGridLayout(ManageWidget);
        gridLayout_4->setSpacing(6);
        gridLayout_4->setContentsMargins(11, 11, 11, 11);
        gridLayout_4->setObjectName(QString::fromUtf8("gridLayout_4"));
        gridLayout_3 = new QGridLayout();
        gridLayout_3->setSpacing(6);
        gridLayout_3->setObjectName(QString::fromUtf8("gridLayout_3"));
        groupBox = new QGroupBox(ManageWidget);
        groupBox->setObjectName(QString::fromUtf8("groupBox"));
        groupBox->setStyleSheet(QString::fromUtf8(""));
        verticalLayout = new QVBoxLayout(groupBox);
        verticalLayout->setSpacing(6);
        verticalLayout->setContentsMargins(11, 11, 11, 11);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        tableView = new QTableView(groupBox);
        tableView->setObjectName(QString::fromUtf8("tableView"));

        verticalLayout->addWidget(tableView);

        groupBox_2 = new QGroupBox(groupBox);
        groupBox_2->setObjectName(QString::fromUtf8("groupBox_2"));
        gridLayout = new QGridLayout(groupBox_2);
        gridLayout->setSpacing(6);
        gridLayout->setContentsMargins(11, 11, 11, 11);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        flash = new QPushButton(groupBox_2);
        flash->setObjectName(QString::fromUtf8("flash"));

        gridLayout->addWidget(flash, 0, 1, 1, 1);

        label = new QLabel(groupBox_2);
        label->setObjectName(QString::fromUtf8("label"));

        gridLayout->addWidget(label, 1, 0, 1, 1);

        save = new QPushButton(groupBox_2);
        save->setObjectName(QString::fromUtf8("save"));

        gridLayout->addWidget(save, 0, 3, 1, 1);

        disconnect = new QPushButton(groupBox_2);
        disconnect->setObjectName(QString::fromUtf8("disconnect"));

        gridLayout->addWidget(disconnect, 0, 0, 1, 1);

        fd_num = new QSpinBox(groupBox_2);
        fd_num->setObjectName(QString::fromUtf8("fd_num"));

        gridLayout->addWidget(fd_num, 1, 1, 1, 1);


        verticalLayout->addWidget(groupBox_2);


        gridLayout_3->addWidget(groupBox, 0, 0, 1, 1);


        gridLayout_4->addLayout(gridLayout_3, 1, 0, 1, 1);


        retranslateUi(ManageWidget);

        QMetaObject::connectSlotsByName(ManageWidget);
    } // setupUi

    void retranslateUi(QWidget *ManageWidget)
    {
        ManageWidget->setWindowTitle(QApplication::translate("ManageWidget", "tcp", nullptr));
        groupBox->setTitle(QApplication::translate("ManageWidget", "\351\223\276\346\216\245\347\256\241\347\220\206", nullptr));
        groupBox_2->setTitle(QApplication::translate("ManageWidget", "\346\223\215\344\275\234\346\214\211\351\222\256", nullptr));
        flash->setText(QApplication::translate("ManageWidget", "\345\210\267\346\226\260\351\223\276\346\216\245\346\225\260\346\215\256", nullptr));
        label->setText(QApplication::translate("ManageWidget", "\350\257\267\350\276\223\345\205\245\351\234\200\350\246\201\346\226\255\345\274\200\347\232\204net_fd\357\274\232", nullptr));
        save->setText(QApplication::translate("ManageWidget", "\344\277\235\345\255\230\351\223\276\346\216\245\346\225\260\346\215\256\357\274\210\346\234\252\345\256\236\347\216\260\357\274\211", nullptr));
        disconnect->setText(QApplication::translate("ManageWidget", "\346\226\255\345\274\200\351\223\276\346\216\245\357\274\210\346\234\252\345\256\236\347\216\260\357\274\211", nullptr));
    } // retranslateUi

};

namespace Ui {
    class ManageWidget: public Ui_ManageWidget {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MANAGEWIDGET_H
