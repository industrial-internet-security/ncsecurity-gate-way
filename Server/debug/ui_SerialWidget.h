/********************************************************************************
** Form generated from reading UI file 'SerialWidget.ui'
**
** Created by: Qt User Interface Compiler version 5.12.8
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_SERIALWIDGET_H
#define UI_SERIALWIDGET_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QTextBrowser>
#include <QtWidgets/QTextEdit>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_SerialWidget
{
public:
    QStatusBar *statusBar;
    QGridLayout *gridLayout_3;
    QGridLayout *gridLayout_2;
    QGroupBox *groupBox_2;
    QHBoxLayout *horizontalLayout;
    QTextEdit *senddata;
    QPushButton *sendbtn;
    QGroupBox *groupBox;
    QGridLayout *gridLayout;
    QTextBrowser *display;
    QGroupBox *groupBox_3;
    QVBoxLayout *verticalLayout;
    QPushButton *checkport;
    QHBoxLayout *horizontalLayout_2;
    QLabel *label;
    QComboBox *portname;
    QHBoxLayout *horizontalLayout_3;
    QLabel *label_2;
    QComboBox *baudrate;
    QHBoxLayout *horizontalLayout_4;
    QLabel *label_3;
    QComboBox *stopbit;
    QHBoxLayout *horizontalLayout_5;
    QLabel *label_4;
    QComboBox *databit;
    QHBoxLayout *horizontalLayout_6;
    QLabel *label_5;
    QComboBox *checkbit;
    QPushButton *openbtn;
    QCheckBox *hexsend;
    QCheckBox *hexrecv;
    QCheckBox *autosend;
    QComboBox *autosendtime;
    QPushButton *sendcountbtn;
    QPushButton *recvcountbtn;
    QPushButton *savedatabtn;
    QPushButton *cleardatabtn;

    void setupUi(QWidget *SerialWidget)
    {
        if (SerialWidget->objectName().isEmpty())
            SerialWidget->setObjectName(QString::fromUtf8("SerialWidget"));
        SerialWidget->resize(516, 470);
        SerialWidget->setStyleSheet(QString::fromUtf8(""));
        statusBar = new QStatusBar(SerialWidget);
        statusBar->setObjectName(QString::fromUtf8("statusBar"));
        statusBar->setGeometry(QRect(0, 0, 3, 18));
        gridLayout_3 = new QGridLayout(SerialWidget);
        gridLayout_3->setSpacing(6);
        gridLayout_3->setContentsMargins(11, 11, 11, 11);
        gridLayout_3->setObjectName(QString::fromUtf8("gridLayout_3"));
        gridLayout_2 = new QGridLayout();
        gridLayout_2->setSpacing(6);
        gridLayout_2->setObjectName(QString::fromUtf8("gridLayout_2"));
        groupBox_2 = new QGroupBox(SerialWidget);
        groupBox_2->setObjectName(QString::fromUtf8("groupBox_2"));
        groupBox_2->setMaximumSize(QSize(16777215, 112));
        horizontalLayout = new QHBoxLayout(groupBox_2);
        horizontalLayout->setSpacing(6);
        horizontalLayout->setContentsMargins(11, 11, 11, 11);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        senddata = new QTextEdit(groupBox_2);
        senddata->setObjectName(QString::fromUtf8("senddata"));
        senddata->setMinimumSize(QSize(0, 80));
        senddata->setMaximumSize(QSize(16777215, 80));

        horizontalLayout->addWidget(senddata);

        sendbtn = new QPushButton(groupBox_2);
        sendbtn->setObjectName(QString::fromUtf8("sendbtn"));
        sendbtn->setMinimumSize(QSize(80, 80));
        sendbtn->setMaximumSize(QSize(16777215, 80));

        horizontalLayout->addWidget(sendbtn);


        gridLayout_2->addWidget(groupBox_2, 1, 0, 1, 1);

        groupBox = new QGroupBox(SerialWidget);
        groupBox->setObjectName(QString::fromUtf8("groupBox"));
        gridLayout = new QGridLayout(groupBox);
        gridLayout->setSpacing(6);
        gridLayout->setContentsMargins(11, 11, 11, 11);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        display = new QTextBrowser(groupBox);
        display->setObjectName(QString::fromUtf8("display"));

        gridLayout->addWidget(display, 0, 0, 1, 1);


        gridLayout_2->addWidget(groupBox, 0, 0, 1, 1);

        groupBox_3 = new QGroupBox(SerialWidget);
        groupBox_3->setObjectName(QString::fromUtf8("groupBox_3"));
        verticalLayout = new QVBoxLayout(groupBox_3);
        verticalLayout->setSpacing(6);
        verticalLayout->setContentsMargins(11, 11, 11, 11);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        checkport = new QPushButton(groupBox_3);
        checkport->setObjectName(QString::fromUtf8("checkport"));

        verticalLayout->addWidget(checkport);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setSpacing(6);
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        label = new QLabel(groupBox_3);
        label->setObjectName(QString::fromUtf8("label"));

        horizontalLayout_2->addWidget(label);

        portname = new QComboBox(groupBox_3);
        portname->setObjectName(QString::fromUtf8("portname"));

        horizontalLayout_2->addWidget(portname);


        verticalLayout->addLayout(horizontalLayout_2);

        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setSpacing(6);
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        label_2 = new QLabel(groupBox_3);
        label_2->setObjectName(QString::fromUtf8("label_2"));

        horizontalLayout_3->addWidget(label_2);

        baudrate = new QComboBox(groupBox_3);
        baudrate->setObjectName(QString::fromUtf8("baudrate"));

        horizontalLayout_3->addWidget(baudrate);


        verticalLayout->addLayout(horizontalLayout_3);

        horizontalLayout_4 = new QHBoxLayout();
        horizontalLayout_4->setSpacing(6);
        horizontalLayout_4->setObjectName(QString::fromUtf8("horizontalLayout_4"));
        label_3 = new QLabel(groupBox_3);
        label_3->setObjectName(QString::fromUtf8("label_3"));

        horizontalLayout_4->addWidget(label_3);

        stopbit = new QComboBox(groupBox_3);
        stopbit->setObjectName(QString::fromUtf8("stopbit"));

        horizontalLayout_4->addWidget(stopbit);


        verticalLayout->addLayout(horizontalLayout_4);

        horizontalLayout_5 = new QHBoxLayout();
        horizontalLayout_5->setSpacing(6);
        horizontalLayout_5->setObjectName(QString::fromUtf8("horizontalLayout_5"));
        label_4 = new QLabel(groupBox_3);
        label_4->setObjectName(QString::fromUtf8("label_4"));

        horizontalLayout_5->addWidget(label_4);

        databit = new QComboBox(groupBox_3);
        databit->setObjectName(QString::fromUtf8("databit"));

        horizontalLayout_5->addWidget(databit);


        verticalLayout->addLayout(horizontalLayout_5);

        horizontalLayout_6 = new QHBoxLayout();
        horizontalLayout_6->setSpacing(6);
        horizontalLayout_6->setObjectName(QString::fromUtf8("horizontalLayout_6"));
        label_5 = new QLabel(groupBox_3);
        label_5->setObjectName(QString::fromUtf8("label_5"));

        horizontalLayout_6->addWidget(label_5);

        checkbit = new QComboBox(groupBox_3);
        checkbit->setObjectName(QString::fromUtf8("checkbit"));

        horizontalLayout_6->addWidget(checkbit);


        verticalLayout->addLayout(horizontalLayout_6);

        openbtn = new QPushButton(groupBox_3);
        openbtn->setObjectName(QString::fromUtf8("openbtn"));

        verticalLayout->addWidget(openbtn);

        hexsend = new QCheckBox(groupBox_3);
        hexsend->setObjectName(QString::fromUtf8("hexsend"));

        verticalLayout->addWidget(hexsend);

        hexrecv = new QCheckBox(groupBox_3);
        hexrecv->setObjectName(QString::fromUtf8("hexrecv"));

        verticalLayout->addWidget(hexrecv);

        autosend = new QCheckBox(groupBox_3);
        autosend->setObjectName(QString::fromUtf8("autosend"));

        verticalLayout->addWidget(autosend);

        autosendtime = new QComboBox(groupBox_3);
        autosendtime->setObjectName(QString::fromUtf8("autosendtime"));

        verticalLayout->addWidget(autosendtime);

        sendcountbtn = new QPushButton(groupBox_3);
        sendcountbtn->setObjectName(QString::fromUtf8("sendcountbtn"));

        verticalLayout->addWidget(sendcountbtn);

        recvcountbtn = new QPushButton(groupBox_3);
        recvcountbtn->setObjectName(QString::fromUtf8("recvcountbtn"));

        verticalLayout->addWidget(recvcountbtn);

        savedatabtn = new QPushButton(groupBox_3);
        savedatabtn->setObjectName(QString::fromUtf8("savedatabtn"));

        verticalLayout->addWidget(savedatabtn);

        cleardatabtn = new QPushButton(groupBox_3);
        cleardatabtn->setObjectName(QString::fromUtf8("cleardatabtn"));

        verticalLayout->addWidget(cleardatabtn);


        gridLayout_2->addWidget(groupBox_3, 0, 1, 2, 1);


        gridLayout_3->addLayout(gridLayout_2, 0, 0, 1, 1);


        retranslateUi(SerialWidget);

        QMetaObject::connectSlotsByName(SerialWidget);
    } // setupUi

    void retranslateUi(QWidget *SerialWidget)
    {
        SerialWidget->setWindowTitle(QApplication::translate("SerialWidget", "serial", nullptr));
        groupBox_2->setTitle(QApplication::translate("SerialWidget", "\346\225\260\346\215\256\345\217\221\351\200\201\347\252\227\345\217\243", nullptr));
        sendbtn->setText(QApplication::translate("SerialWidget", "\345\217\221\351\200\201", nullptr));
        groupBox->setTitle(QApplication::translate("SerialWidget", "\346\225\260\346\215\256\346\230\276\347\244\272\347\252\227\345\217\243", nullptr));
        groupBox_3->setTitle(QApplication::translate("SerialWidget", "\344\270\262\345\217\243\350\256\276\347\275\256\347\252\227\345\217\243", nullptr));
        checkport->setText(QApplication::translate("SerialWidget", "\346\243\200\346\265\213\345\275\223\345\211\215\345\217\257\347\224\250\344\270\262\345\217\243", nullptr));
        label->setText(QApplication::translate("SerialWidget", "\344\270\262\345\217\243\351\200\211\346\213\251\357\274\232", nullptr));
        label_2->setText(QApplication::translate("SerialWidget", "\346\263\242\347\211\271\347\216\207\357\274\232", nullptr));
        label_3->setText(QApplication::translate("SerialWidget", "\345\201\234\346\255\242\344\275\215\357\274\232", nullptr));
        label_4->setText(QApplication::translate("SerialWidget", "\346\225\260\346\215\256\344\275\215\357\274\232", nullptr));
        label_5->setText(QApplication::translate("SerialWidget", "\346\240\241\351\252\214\344\275\215\357\274\232", nullptr));
        openbtn->setText(QApplication::translate("SerialWidget", "\346\211\223\345\274\200\344\270\262\345\217\243", nullptr));
        hexsend->setText(QApplication::translate("SerialWidget", "HEX\345\217\221\351\200\201", nullptr));
        hexrecv->setText(QApplication::translate("SerialWidget", "HEX\346\216\245\346\224\266", nullptr));
        autosend->setText(QApplication::translate("SerialWidget", "\350\207\252\345\212\250\345\217\221\351\200\201\357\274\210\346\257\253\347\247\222\357\274\211", nullptr));
        sendcountbtn->setText(QApplication::translate("SerialWidget", "\345\217\221\351\200\201\357\274\2320\345\255\227\350\212\202", nullptr));
        recvcountbtn->setText(QApplication::translate("SerialWidget", "\346\216\245\346\224\266\357\274\2320\345\255\227\350\212\202", nullptr));
        savedatabtn->setText(QApplication::translate("SerialWidget", "\344\277\235\345\255\230\346\225\260\346\215\256", nullptr));
        cleardatabtn->setText(QApplication::translate("SerialWidget", "\346\270\205\347\251\272\346\225\260\346\215\256", nullptr));
    } // retranslateUi

};

namespace Ui {
    class SerialWidget: public Ui_SerialWidget {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_SERIALWIDGET_H
