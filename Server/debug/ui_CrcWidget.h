/********************************************************************************
** Form generated from reading UI file 'CrcWidget.ui'
**
** Created by: Qt User Interface Compiler version 5.12.8
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_CRCWIDGET_H
#define UI_CRCWIDGET_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QTextBrowser>
#include <QtWidgets/QTextEdit>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_CrcWidget
{
public:
    QGridLayout *gridLayout_2;
    QTextBrowser *display;
    QGroupBox *groupBox;
    QGridLayout *gridLayout;
    QLabel *label;
    QComboBox *modelcbx;
    QTextEdit *data;
    QPushButton *calculate;

    void setupUi(QWidget *CrcWidget)
    {
        if (CrcWidget->objectName().isEmpty())
            CrcWidget->setObjectName(QString::fromUtf8("CrcWidget"));
        CrcWidget->resize(622, 521);
        gridLayout_2 = new QGridLayout(CrcWidget);
        gridLayout_2->setObjectName(QString::fromUtf8("gridLayout_2"));
        display = new QTextBrowser(CrcWidget);
        display->setObjectName(QString::fromUtf8("display"));

        gridLayout_2->addWidget(display, 0, 0, 1, 1);

        groupBox = new QGroupBox(CrcWidget);
        groupBox->setObjectName(QString::fromUtf8("groupBox"));
        gridLayout = new QGridLayout(groupBox);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        label = new QLabel(groupBox);
        label->setObjectName(QString::fromUtf8("label"));
        label->setMinimumSize(QSize(75, 0));
        label->setStyleSheet(QString::fromUtf8(""));

        gridLayout->addWidget(label, 0, 0, 1, 1);


        gridLayout_2->addWidget(groupBox, 0, 1, 1, 1);

        modelcbx = new QComboBox(CrcWidget);
        modelcbx->setObjectName(QString::fromUtf8("modelcbx"));

        gridLayout_2->addWidget(modelcbx, 1, 0, 1, 1);

        data = new QTextEdit(CrcWidget);
        data->setObjectName(QString::fromUtf8("data"));

        gridLayout_2->addWidget(data, 2, 0, 1, 1);

        calculate = new QPushButton(CrcWidget);
        calculate->setObjectName(QString::fromUtf8("calculate"));
        calculate->setMinimumSize(QSize(75, 60));

        gridLayout_2->addWidget(calculate, 2, 1, 1, 1);


        retranslateUi(CrcWidget);

        QMetaObject::connectSlotsByName(CrcWidget);
    } // setupUi

    void retranslateUi(QWidget *CrcWidget)
    {
        CrcWidget->setWindowTitle(QApplication::translate("CrcWidget", "Form", nullptr));
        groupBox->setTitle(QApplication::translate("CrcWidget", "Instruction", nullptr));
        label->setText(QApplication::translate("CrcWidget", "infomessage", nullptr));
        calculate->setText(QApplication::translate("CrcWidget", "\350\256\241\347\256\227", nullptr));
    } // retranslateUi

};

namespace Ui {
    class CrcWidget: public Ui_CrcWidget {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_CRCWIDGET_H
