/********************************************************************************
** Form generated from reading UI file 'nclinkdevwidget.ui'
**
** Created by: Qt User Interface Compiler version 5.12.8
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_NCLINKDEVWIDGET_H
#define UI_NCLINKDEVWIDGET_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QTableView>
#include <QtWidgets/QTextBrowser>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_nclinkdevwidget
{
public:
    QGridLayout *gridLayout_11;
    QGridLayout *gridLayout_10;
    QGroupBox *groupBox_4;
    QGridLayout *gridLayout_3;
    QLabel *label_14;
    QLineEdit *components_name;
    QLabel *label_19;
    QSpinBox *components_config;
    QPushButton *components_config_add;
    QPushButton *components_config_del;
    QLabel *label_15;
    QLineEdit *components_id;
    QLabel *label_21;
    QSpinBox *components_dataitem;
    QPushButton *components_dataitem_add;
    QPushButton *components_dataitem_del;
    QLabel *label_17;
    QComboBox *components_type;
    QLabel *label_20;
    QSpinBox *components_self;
    QPushButton *components_self_add;
    QPushButton *components_self_del;
    QLabel *label_16;
    QLineEdit *components_description;
    QLabel *label_26;
    QSpinBox *components_device;
    QPushButton *components_device_add;
    QPushButton *components_device_del;
    QLabel *label_18;
    QLineEdit *components_number;
    QPushButton *components_add;
    QPushButton *components_find;
    QPushButton *components_del;
    QPushButton *components_change;
    QLabel *label_61;
    QSpinBox *components_seq;
    QGroupBox *groupBox_3;
    QGridLayout *gridLayout_4;
    QLabel *label_6;
    QLineEdit *device_name;
    QLabel *label_23;
    QLineEdit *device_guid;
    QLabel *label_13;
    QSpinBox *device_dataitem;
    QPushButton *device_dataitem_add;
    QPushButton *device_dataitem_del;
    QLabel *label_7;
    QLineEdit *device_id;
    QLabel *label_24;
    QLineEdit *device_version;
    QLabel *label_12;
    QSpinBox *device_components;
    QPushButton *device_components_add;
    QPushButton *device_components_del;
    QLabel *label_9;
    QComboBox *device_type;
    QPushButton *device_add;
    QPushButton *device_find;
    QLabel *label_11;
    QSpinBox *device_config_data;
    QPushButton *device_config_data_add;
    QPushButton *device_config_data_del;
    QLabel *label_8;
    QLineEdit *device_description;
    QPushButton *device_del;
    QPushButton *device_change;
    QLabel *label_47;
    QSpinBox *device_config_channel;
    QPushButton *device_config_channel_add;
    QPushButton *device_config_channel_del;
    QLabel *label_10;
    QLineEdit *device_number;
    QLabel *label_63;
    QSpinBox *device_seq;
    QLabel *label_25;
    QSpinBox *device_config_root;
    QPushButton *device_config_root_add;
    QPushButton *device_config_root_del;
    QGroupBox *groupBox_9;
    QGridLayout *gridLayout_8;
    QTextBrowser *feedback;
    QGroupBox *groupBox_2;
    QGridLayout *gridLayout_2;
    QLabel *label;
    QLineEdit *root_name;
    QLabel *label_4;
    QSpinBox *root_config_data;
    QPushButton *root_config_data_add;
    QPushButton *root_config_data_del;
    QLabel *label_2;
    QLineEdit *root_id;
    QLabel *label_48;
    QSpinBox *root_config_method;
    QPushButton *root_config_method_add;
    QPushButton *root_config_method_del;
    QLabel *label_22;
    QLineEdit *root_version;
    QLabel *label_5;
    QSpinBox *root_config_device;
    QPushButton *root_config_device_add;
    QPushButton *root_config_device_del;
    QLabel *label_3;
    QLineEdit *root_description;
    QPushButton *root_add;
    QPushButton *root_find;
    QPushButton *root_del;
    QPushButton *root_change;
    QLabel *label_59;
    QSpinBox *root_seq;
    QGroupBox *groupBox;
    QGridLayout *gridLayout;
    QTableView *mod_table;
    QGroupBox *groupBox_8;
    QGridLayout *gridLayout_9;
    QLabel *label_56;
    QLabel *label_51;
    QLabel *label_54;
    QLineEdit *file_path;
    QLabel *label_55;
    QPushButton *exit;
    QPushButton *read_all;
    QPushButton *save_all;
    QLabel *label_27;
    QGroupBox *groupBox_7;
    QGridLayout *gridLayout_7;
    QLabel *label_43;
    QLineEdit *method_name;
    QLabel *label_57;
    QLineEdit *method_args_name;
    QPushButton *method_args_save;
    QPushButton *method_add;
    QPushButton *method_find;
    QPushButton *method_del;
    QPushButton *method_change;
    QLabel *label_50;
    QLineEdit *method_id;
    QLabel *label_58;
    QLineEdit *method_args_text;
    QPushButton *method_args_del;
    QLabel *label_52;
    QLineEdit *method_description;
    QLabel *label_53;
    QSpinBox *method_root;
    QPushButton *method_root_add;
    QPushButton *method_root_del;
    QLabel *label_62;
    QSpinBox *method_seq;
    QGroupBox *groupBox_6;
    QGridLayout *gridLayout_6;
    QLabel *label_30;
    QLineEdit *channel_name;
    QLabel *label_39;
    QLineEdit *channel_upload;
    QLabel *label_33;
    QLineEdit *channel_sample;
    QPushButton *channel_add;
    QPushButton *channel_find;
    QLabel *label_38;
    QLineEdit *channel_id;
    QLabel *label_40;
    QPushButton *channel_ids_add;
    QPushButton *channel_ids_del;
    QPushButton *channel_del;
    QPushButton *channel_change;
    QLabel *label_42;
    QLineEdit *channel_description;
    QLabel *label_41;
    QPushButton *channel_device_add;
    QPushButton *channel_device_del;
    QLabel *label_64;
    QSpinBox *channel_seq;
    QSpinBox *channel_ids;
    QSpinBox *channel_device;
    QGroupBox *groupBox_5;
    QGridLayout *gridLayout_5;
    QPushButton *data_device_dataitem_add;
    QSpinBox *data_components_dataitem;
    QLineEdit *data_units;
    QPushButton *data_device_dataitem_del;
    QLineEdit *data_name;
    QPushButton *data_device_config_add;
    QLabel *label_46;
    QLabel *label_49;
    QPushButton *data_channel_del;
    QLabel *label_45;
    QLineEdit *data_id;
    QLabel *label_37;
    QSpinBox *data_device_dataitem;
    QLabel *label_34;
    QPushButton *data_components_config_add;
    QPushButton *data_channel_add;
    QSpinBox *data_channel;
    QLabel *label_31;
    QLineEdit *data_source;
    QComboBox *data_type;
    QPushButton *data_components_config_del;
    QPushButton *data_device_config_del;
    QLabel *label_28;
    QLineEdit *data_value;
    QSpinBox *data_device_config;
    QSpinBox *data_components_config;
    QLabel *label_66;
    QPushButton *data_components_dataitem_add;
    QPushButton *data_components_dataitem_del;
    QLabel *label_35;
    QLabel *label_65;
    QLabel *label_36;
    QLineEdit *data_description;
    QLabel *label_29;
    QLabel *label_32;
    QPushButton *data_add;
    QPushButton *data_find;
    QPushButton *data_del;
    QPushButton *data_change;
    QLabel *label_60;
    QSpinBox *data_seq;
    QCheckBox *data_setable;
    QLabel *label_44;
    QPushButton *data_root_del;
    QPushButton *data_root_add;
    QSpinBox *data_root;
    QComboBox *data_datatype;

    void setupUi(QWidget *nclinkdevwidget)
    {
        if (nclinkdevwidget->objectName().isEmpty())
            nclinkdevwidget->setObjectName(QString::fromUtf8("nclinkdevwidget"));
        nclinkdevwidget->resize(1919, 1217);
        gridLayout_11 = new QGridLayout(nclinkdevwidget);
        gridLayout_11->setObjectName(QString::fromUtf8("gridLayout_11"));
        gridLayout_10 = new QGridLayout();
        gridLayout_10->setObjectName(QString::fromUtf8("gridLayout_10"));
        groupBox_4 = new QGroupBox(nclinkdevwidget);
        groupBox_4->setObjectName(QString::fromUtf8("groupBox_4"));
        gridLayout_3 = new QGridLayout(groupBox_4);
        gridLayout_3->setObjectName(QString::fromUtf8("gridLayout_3"));
        label_14 = new QLabel(groupBox_4);
        label_14->setObjectName(QString::fromUtf8("label_14"));

        gridLayout_3->addWidget(label_14, 0, 0, 1, 1);

        components_name = new QLineEdit(groupBox_4);
        components_name->setObjectName(QString::fromUtf8("components_name"));

        gridLayout_3->addWidget(components_name, 0, 1, 1, 1);

        label_19 = new QLabel(groupBox_4);
        label_19->setObjectName(QString::fromUtf8("label_19"));

        gridLayout_3->addWidget(label_19, 0, 2, 1, 2);

        components_config = new QSpinBox(groupBox_4);
        components_config->setObjectName(QString::fromUtf8("components_config"));

        gridLayout_3->addWidget(components_config, 0, 5, 1, 2);

        components_config_add = new QPushButton(groupBox_4);
        components_config_add->setObjectName(QString::fromUtf8("components_config_add"));

        gridLayout_3->addWidget(components_config_add, 0, 7, 1, 1);

        components_config_del = new QPushButton(groupBox_4);
        components_config_del->setObjectName(QString::fromUtf8("components_config_del"));

        gridLayout_3->addWidget(components_config_del, 0, 8, 1, 1);

        label_15 = new QLabel(groupBox_4);
        label_15->setObjectName(QString::fromUtf8("label_15"));

        gridLayout_3->addWidget(label_15, 1, 0, 1, 1);

        components_id = new QLineEdit(groupBox_4);
        components_id->setObjectName(QString::fromUtf8("components_id"));

        gridLayout_3->addWidget(components_id, 1, 1, 1, 1);

        label_21 = new QLabel(groupBox_4);
        label_21->setObjectName(QString::fromUtf8("label_21"));

        gridLayout_3->addWidget(label_21, 1, 2, 1, 3);

        components_dataitem = new QSpinBox(groupBox_4);
        components_dataitem->setObjectName(QString::fromUtf8("components_dataitem"));

        gridLayout_3->addWidget(components_dataitem, 1, 5, 1, 2);

        components_dataitem_add = new QPushButton(groupBox_4);
        components_dataitem_add->setObjectName(QString::fromUtf8("components_dataitem_add"));

        gridLayout_3->addWidget(components_dataitem_add, 1, 7, 1, 1);

        components_dataitem_del = new QPushButton(groupBox_4);
        components_dataitem_del->setObjectName(QString::fromUtf8("components_dataitem_del"));

        gridLayout_3->addWidget(components_dataitem_del, 1, 8, 1, 1);

        label_17 = new QLabel(groupBox_4);
        label_17->setObjectName(QString::fromUtf8("label_17"));

        gridLayout_3->addWidget(label_17, 2, 0, 1, 1);

        components_type = new QComboBox(groupBox_4);
        components_type->setObjectName(QString::fromUtf8("components_type"));

        gridLayout_3->addWidget(components_type, 2, 1, 1, 1);

        label_20 = new QLabel(groupBox_4);
        label_20->setObjectName(QString::fromUtf8("label_20"));

        gridLayout_3->addWidget(label_20, 2, 2, 1, 3);

        components_self = new QSpinBox(groupBox_4);
        components_self->setObjectName(QString::fromUtf8("components_self"));

        gridLayout_3->addWidget(components_self, 2, 5, 1, 2);

        components_self_add = new QPushButton(groupBox_4);
        components_self_add->setObjectName(QString::fromUtf8("components_self_add"));

        gridLayout_3->addWidget(components_self_add, 2, 7, 1, 1);

        components_self_del = new QPushButton(groupBox_4);
        components_self_del->setObjectName(QString::fromUtf8("components_self_del"));

        gridLayout_3->addWidget(components_self_del, 2, 8, 1, 1);

        label_16 = new QLabel(groupBox_4);
        label_16->setObjectName(QString::fromUtf8("label_16"));

        gridLayout_3->addWidget(label_16, 3, 0, 1, 1);

        components_description = new QLineEdit(groupBox_4);
        components_description->setObjectName(QString::fromUtf8("components_description"));

        gridLayout_3->addWidget(components_description, 3, 1, 1, 1);

        label_26 = new QLabel(groupBox_4);
        label_26->setObjectName(QString::fromUtf8("label_26"));

        gridLayout_3->addWidget(label_26, 3, 2, 1, 3);

        components_device = new QSpinBox(groupBox_4);
        components_device->setObjectName(QString::fromUtf8("components_device"));

        gridLayout_3->addWidget(components_device, 3, 5, 1, 2);

        components_device_add = new QPushButton(groupBox_4);
        components_device_add->setObjectName(QString::fromUtf8("components_device_add"));

        gridLayout_3->addWidget(components_device_add, 3, 7, 1, 1);

        components_device_del = new QPushButton(groupBox_4);
        components_device_del->setObjectName(QString::fromUtf8("components_device_del"));

        gridLayout_3->addWidget(components_device_del, 3, 8, 1, 1);

        label_18 = new QLabel(groupBox_4);
        label_18->setObjectName(QString::fromUtf8("label_18"));

        gridLayout_3->addWidget(label_18, 4, 0, 1, 1);

        components_number = new QLineEdit(groupBox_4);
        components_number->setObjectName(QString::fromUtf8("components_number"));

        gridLayout_3->addWidget(components_number, 4, 1, 1, 1);

        components_add = new QPushButton(groupBox_4);
        components_add->setObjectName(QString::fromUtf8("components_add"));

        gridLayout_3->addWidget(components_add, 4, 2, 1, 1);

        components_find = new QPushButton(groupBox_4);
        components_find->setObjectName(QString::fromUtf8("components_find"));

        gridLayout_3->addWidget(components_find, 4, 3, 1, 1);

        components_del = new QPushButton(groupBox_4);
        components_del->setObjectName(QString::fromUtf8("components_del"));

        gridLayout_3->addWidget(components_del, 4, 4, 1, 2);

        components_change = new QPushButton(groupBox_4);
        components_change->setObjectName(QString::fromUtf8("components_change"));

        gridLayout_3->addWidget(components_change, 4, 6, 1, 1);

        label_61 = new QLabel(groupBox_4);
        label_61->setObjectName(QString::fromUtf8("label_61"));

        gridLayout_3->addWidget(label_61, 4, 7, 1, 1);

        components_seq = new QSpinBox(groupBox_4);
        components_seq->setObjectName(QString::fromUtf8("components_seq"));

        gridLayout_3->addWidget(components_seq, 4, 8, 1, 1);


        gridLayout_10->addWidget(groupBox_4, 2, 0, 1, 1);

        groupBox_3 = new QGroupBox(nclinkdevwidget);
        groupBox_3->setObjectName(QString::fromUtf8("groupBox_3"));
        gridLayout_4 = new QGridLayout(groupBox_3);
        gridLayout_4->setObjectName(QString::fromUtf8("gridLayout_4"));
        label_6 = new QLabel(groupBox_3);
        label_6->setObjectName(QString::fromUtf8("label_6"));

        gridLayout_4->addWidget(label_6, 0, 0, 1, 1);

        device_name = new QLineEdit(groupBox_3);
        device_name->setObjectName(QString::fromUtf8("device_name"));

        gridLayout_4->addWidget(device_name, 0, 1, 1, 1);

        label_23 = new QLabel(groupBox_3);
        label_23->setObjectName(QString::fromUtf8("label_23"));

        gridLayout_4->addWidget(label_23, 0, 2, 1, 1);

        device_guid = new QLineEdit(groupBox_3);
        device_guid->setObjectName(QString::fromUtf8("device_guid"));

        gridLayout_4->addWidget(device_guid, 0, 3, 1, 2);

        label_13 = new QLabel(groupBox_3);
        label_13->setObjectName(QString::fromUtf8("label_13"));

        gridLayout_4->addWidget(label_13, 0, 5, 1, 1);

        device_dataitem = new QSpinBox(groupBox_3);
        device_dataitem->setObjectName(QString::fromUtf8("device_dataitem"));

        gridLayout_4->addWidget(device_dataitem, 0, 6, 1, 1);

        device_dataitem_add = new QPushButton(groupBox_3);
        device_dataitem_add->setObjectName(QString::fromUtf8("device_dataitem_add"));

        gridLayout_4->addWidget(device_dataitem_add, 0, 7, 1, 1);

        device_dataitem_del = new QPushButton(groupBox_3);
        device_dataitem_del->setObjectName(QString::fromUtf8("device_dataitem_del"));

        gridLayout_4->addWidget(device_dataitem_del, 0, 8, 1, 1);

        label_7 = new QLabel(groupBox_3);
        label_7->setObjectName(QString::fromUtf8("label_7"));

        gridLayout_4->addWidget(label_7, 1, 0, 1, 1);

        device_id = new QLineEdit(groupBox_3);
        device_id->setObjectName(QString::fromUtf8("device_id"));

        gridLayout_4->addWidget(device_id, 1, 1, 1, 1);

        label_24 = new QLabel(groupBox_3);
        label_24->setObjectName(QString::fromUtf8("label_24"));

        gridLayout_4->addWidget(label_24, 1, 2, 1, 1);

        device_version = new QLineEdit(groupBox_3);
        device_version->setObjectName(QString::fromUtf8("device_version"));

        gridLayout_4->addWidget(device_version, 1, 3, 1, 2);

        label_12 = new QLabel(groupBox_3);
        label_12->setObjectName(QString::fromUtf8("label_12"));

        gridLayout_4->addWidget(label_12, 1, 5, 1, 1);

        device_components = new QSpinBox(groupBox_3);
        device_components->setObjectName(QString::fromUtf8("device_components"));

        gridLayout_4->addWidget(device_components, 1, 6, 1, 1);

        device_components_add = new QPushButton(groupBox_3);
        device_components_add->setObjectName(QString::fromUtf8("device_components_add"));

        gridLayout_4->addWidget(device_components_add, 1, 7, 1, 1);

        device_components_del = new QPushButton(groupBox_3);
        device_components_del->setObjectName(QString::fromUtf8("device_components_del"));

        gridLayout_4->addWidget(device_components_del, 1, 8, 1, 1);

        label_9 = new QLabel(groupBox_3);
        label_9->setObjectName(QString::fromUtf8("label_9"));

        gridLayout_4->addWidget(label_9, 2, 0, 1, 1);

        device_type = new QComboBox(groupBox_3);
        device_type->setObjectName(QString::fromUtf8("device_type"));

        gridLayout_4->addWidget(device_type, 2, 1, 1, 1);

        device_add = new QPushButton(groupBox_3);
        device_add->setObjectName(QString::fromUtf8("device_add"));

        gridLayout_4->addWidget(device_add, 2, 3, 1, 1);

        device_find = new QPushButton(groupBox_3);
        device_find->setObjectName(QString::fromUtf8("device_find"));

        gridLayout_4->addWidget(device_find, 2, 4, 1, 1);

        label_11 = new QLabel(groupBox_3);
        label_11->setObjectName(QString::fromUtf8("label_11"));

        gridLayout_4->addWidget(label_11, 2, 5, 1, 1);

        device_config_data = new QSpinBox(groupBox_3);
        device_config_data->setObjectName(QString::fromUtf8("device_config_data"));

        gridLayout_4->addWidget(device_config_data, 2, 6, 1, 1);

        device_config_data_add = new QPushButton(groupBox_3);
        device_config_data_add->setObjectName(QString::fromUtf8("device_config_data_add"));

        gridLayout_4->addWidget(device_config_data_add, 2, 7, 1, 1);

        device_config_data_del = new QPushButton(groupBox_3);
        device_config_data_del->setObjectName(QString::fromUtf8("device_config_data_del"));

        gridLayout_4->addWidget(device_config_data_del, 2, 8, 1, 1);

        label_8 = new QLabel(groupBox_3);
        label_8->setObjectName(QString::fromUtf8("label_8"));

        gridLayout_4->addWidget(label_8, 3, 0, 1, 1);

        device_description = new QLineEdit(groupBox_3);
        device_description->setObjectName(QString::fromUtf8("device_description"));

        gridLayout_4->addWidget(device_description, 3, 1, 1, 1);

        device_del = new QPushButton(groupBox_3);
        device_del->setObjectName(QString::fromUtf8("device_del"));

        gridLayout_4->addWidget(device_del, 3, 3, 1, 1);

        device_change = new QPushButton(groupBox_3);
        device_change->setObjectName(QString::fromUtf8("device_change"));

        gridLayout_4->addWidget(device_change, 3, 4, 1, 1);

        label_47 = new QLabel(groupBox_3);
        label_47->setObjectName(QString::fromUtf8("label_47"));

        gridLayout_4->addWidget(label_47, 3, 5, 1, 1);

        device_config_channel = new QSpinBox(groupBox_3);
        device_config_channel->setObjectName(QString::fromUtf8("device_config_channel"));

        gridLayout_4->addWidget(device_config_channel, 3, 6, 1, 1);

        device_config_channel_add = new QPushButton(groupBox_3);
        device_config_channel_add->setObjectName(QString::fromUtf8("device_config_channel_add"));

        gridLayout_4->addWidget(device_config_channel_add, 3, 7, 1, 1);

        device_config_channel_del = new QPushButton(groupBox_3);
        device_config_channel_del->setObjectName(QString::fromUtf8("device_config_channel_del"));

        gridLayout_4->addWidget(device_config_channel_del, 3, 8, 1, 1);

        label_10 = new QLabel(groupBox_3);
        label_10->setObjectName(QString::fromUtf8("label_10"));

        gridLayout_4->addWidget(label_10, 4, 0, 1, 1);

        device_number = new QLineEdit(groupBox_3);
        device_number->setObjectName(QString::fromUtf8("device_number"));

        gridLayout_4->addWidget(device_number, 4, 1, 1, 1);

        label_63 = new QLabel(groupBox_3);
        label_63->setObjectName(QString::fromUtf8("label_63"));

        gridLayout_4->addWidget(label_63, 4, 3, 1, 1);

        device_seq = new QSpinBox(groupBox_3);
        device_seq->setObjectName(QString::fromUtf8("device_seq"));

        gridLayout_4->addWidget(device_seq, 4, 4, 1, 1);

        label_25 = new QLabel(groupBox_3);
        label_25->setObjectName(QString::fromUtf8("label_25"));

        gridLayout_4->addWidget(label_25, 4, 5, 1, 1);

        device_config_root = new QSpinBox(groupBox_3);
        device_config_root->setObjectName(QString::fromUtf8("device_config_root"));

        gridLayout_4->addWidget(device_config_root, 4, 6, 1, 1);

        device_config_root_add = new QPushButton(groupBox_3);
        device_config_root_add->setObjectName(QString::fromUtf8("device_config_root_add"));

        gridLayout_4->addWidget(device_config_root_add, 4, 7, 1, 1);

        device_config_root_del = new QPushButton(groupBox_3);
        device_config_root_del->setObjectName(QString::fromUtf8("device_config_root_del"));

        gridLayout_4->addWidget(device_config_root_del, 4, 8, 1, 1);


        gridLayout_10->addWidget(groupBox_3, 2, 1, 1, 1);

        groupBox_9 = new QGroupBox(nclinkdevwidget);
        groupBox_9->setObjectName(QString::fromUtf8("groupBox_9"));
        gridLayout_8 = new QGridLayout(groupBox_9);
        gridLayout_8->setObjectName(QString::fromUtf8("gridLayout_8"));
        feedback = new QTextBrowser(groupBox_9);
        feedback->setObjectName(QString::fromUtf8("feedback"));

        gridLayout_8->addWidget(feedback, 0, 0, 1, 1);


        gridLayout_10->addWidget(groupBox_9, 4, 1, 1, 1);

        groupBox_2 = new QGroupBox(nclinkdevwidget);
        groupBox_2->setObjectName(QString::fromUtf8("groupBox_2"));
        gridLayout_2 = new QGridLayout(groupBox_2);
        gridLayout_2->setObjectName(QString::fromUtf8("gridLayout_2"));
        label = new QLabel(groupBox_2);
        label->setObjectName(QString::fromUtf8("label"));

        gridLayout_2->addWidget(label, 0, 0, 1, 1);

        root_name = new QLineEdit(groupBox_2);
        root_name->setObjectName(QString::fromUtf8("root_name"));

        gridLayout_2->addWidget(root_name, 0, 1, 1, 1);

        label_4 = new QLabel(groupBox_2);
        label_4->setObjectName(QString::fromUtf8("label_4"));

        gridLayout_2->addWidget(label_4, 0, 2, 1, 2);

        root_config_data = new QSpinBox(groupBox_2);
        root_config_data->setObjectName(QString::fromUtf8("root_config_data"));

        gridLayout_2->addWidget(root_config_data, 0, 4, 1, 2);

        root_config_data_add = new QPushButton(groupBox_2);
        root_config_data_add->setObjectName(QString::fromUtf8("root_config_data_add"));

        gridLayout_2->addWidget(root_config_data_add, 0, 6, 1, 1);

        root_config_data_del = new QPushButton(groupBox_2);
        root_config_data_del->setObjectName(QString::fromUtf8("root_config_data_del"));

        gridLayout_2->addWidget(root_config_data_del, 0, 7, 1, 1);

        label_2 = new QLabel(groupBox_2);
        label_2->setObjectName(QString::fromUtf8("label_2"));

        gridLayout_2->addWidget(label_2, 1, 0, 1, 1);

        root_id = new QLineEdit(groupBox_2);
        root_id->setObjectName(QString::fromUtf8("root_id"));

        gridLayout_2->addWidget(root_id, 1, 1, 1, 1);

        label_48 = new QLabel(groupBox_2);
        label_48->setObjectName(QString::fromUtf8("label_48"));

        gridLayout_2->addWidget(label_48, 1, 2, 1, 2);

        root_config_method = new QSpinBox(groupBox_2);
        root_config_method->setObjectName(QString::fromUtf8("root_config_method"));

        gridLayout_2->addWidget(root_config_method, 1, 4, 1, 2);

        root_config_method_add = new QPushButton(groupBox_2);
        root_config_method_add->setObjectName(QString::fromUtf8("root_config_method_add"));

        gridLayout_2->addWidget(root_config_method_add, 1, 6, 1, 1);

        root_config_method_del = new QPushButton(groupBox_2);
        root_config_method_del->setObjectName(QString::fromUtf8("root_config_method_del"));

        gridLayout_2->addWidget(root_config_method_del, 1, 7, 1, 1);

        label_22 = new QLabel(groupBox_2);
        label_22->setObjectName(QString::fromUtf8("label_22"));

        gridLayout_2->addWidget(label_22, 2, 0, 1, 1);

        root_version = new QLineEdit(groupBox_2);
        root_version->setObjectName(QString::fromUtf8("root_version"));

        gridLayout_2->addWidget(root_version, 2, 1, 1, 1);

        label_5 = new QLabel(groupBox_2);
        label_5->setObjectName(QString::fromUtf8("label_5"));

        gridLayout_2->addWidget(label_5, 2, 2, 1, 2);

        root_config_device = new QSpinBox(groupBox_2);
        root_config_device->setObjectName(QString::fromUtf8("root_config_device"));

        gridLayout_2->addWidget(root_config_device, 2, 4, 1, 2);

        root_config_device_add = new QPushButton(groupBox_2);
        root_config_device_add->setObjectName(QString::fromUtf8("root_config_device_add"));

        gridLayout_2->addWidget(root_config_device_add, 2, 6, 1, 1);

        root_config_device_del = new QPushButton(groupBox_2);
        root_config_device_del->setObjectName(QString::fromUtf8("root_config_device_del"));

        gridLayout_2->addWidget(root_config_device_del, 2, 7, 1, 1);

        label_3 = new QLabel(groupBox_2);
        label_3->setObjectName(QString::fromUtf8("label_3"));

        gridLayout_2->addWidget(label_3, 3, 0, 1, 1);

        root_description = new QLineEdit(groupBox_2);
        root_description->setObjectName(QString::fromUtf8("root_description"));

        gridLayout_2->addWidget(root_description, 3, 1, 1, 1);

        root_add = new QPushButton(groupBox_2);
        root_add->setObjectName(QString::fromUtf8("root_add"));

        gridLayout_2->addWidget(root_add, 3, 2, 1, 1);

        root_find = new QPushButton(groupBox_2);
        root_find->setObjectName(QString::fromUtf8("root_find"));

        gridLayout_2->addWidget(root_find, 3, 3, 1, 1);

        root_del = new QPushButton(groupBox_2);
        root_del->setObjectName(QString::fromUtf8("root_del"));

        gridLayout_2->addWidget(root_del, 3, 4, 1, 1);

        root_change = new QPushButton(groupBox_2);
        root_change->setObjectName(QString::fromUtf8("root_change"));

        gridLayout_2->addWidget(root_change, 3, 5, 1, 1);

        label_59 = new QLabel(groupBox_2);
        label_59->setObjectName(QString::fromUtf8("label_59"));

        gridLayout_2->addWidget(label_59, 3, 6, 1, 1);

        root_seq = new QSpinBox(groupBox_2);
        root_seq->setObjectName(QString::fromUtf8("root_seq"));

        gridLayout_2->addWidget(root_seq, 3, 7, 1, 1);


        gridLayout_10->addWidget(groupBox_2, 1, 0, 1, 1);

        groupBox = new QGroupBox(nclinkdevwidget);
        groupBox->setObjectName(QString::fromUtf8("groupBox"));
        gridLayout = new QGridLayout(groupBox);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        mod_table = new QTableView(groupBox);
        mod_table->setObjectName(QString::fromUtf8("mod_table"));

        gridLayout->addWidget(mod_table, 0, 0, 1, 1);


        gridLayout_10->addWidget(groupBox, 0, 0, 1, 2);

        groupBox_8 = new QGroupBox(nclinkdevwidget);
        groupBox_8->setObjectName(QString::fromUtf8("groupBox_8"));
        gridLayout_9 = new QGridLayout(groupBox_8);
        gridLayout_9->setObjectName(QString::fromUtf8("gridLayout_9"));
        label_56 = new QLabel(groupBox_8);
        label_56->setObjectName(QString::fromUtf8("label_56"));

        gridLayout_9->addWidget(label_56, 5, 0, 1, 3);

        label_51 = new QLabel(groupBox_8);
        label_51->setObjectName(QString::fromUtf8("label_51"));

        gridLayout_9->addWidget(label_51, 1, 0, 1, 3);

        label_54 = new QLabel(groupBox_8);
        label_54->setObjectName(QString::fromUtf8("label_54"));

        gridLayout_9->addWidget(label_54, 2, 0, 1, 2);

        file_path = new QLineEdit(groupBox_8);
        file_path->setObjectName(QString::fromUtf8("file_path"));

        gridLayout_9->addWidget(file_path, 3, 1, 1, 2);

        label_55 = new QLabel(groupBox_8);
        label_55->setObjectName(QString::fromUtf8("label_55"));

        gridLayout_9->addWidget(label_55, 3, 0, 1, 1);

        exit = new QPushButton(groupBox_8);
        exit->setObjectName(QString::fromUtf8("exit"));

        gridLayout_9->addWidget(exit, 4, 2, 1, 1);

        read_all = new QPushButton(groupBox_8);
        read_all->setObjectName(QString::fromUtf8("read_all"));

        gridLayout_9->addWidget(read_all, 4, 1, 1, 1);

        save_all = new QPushButton(groupBox_8);
        save_all->setObjectName(QString::fromUtf8("save_all"));

        gridLayout_9->addWidget(save_all, 4, 0, 1, 1);

        label_27 = new QLabel(groupBox_8);
        label_27->setObjectName(QString::fromUtf8("label_27"));

        gridLayout_9->addWidget(label_27, 0, 0, 1, 3);


        gridLayout_10->addWidget(groupBox_8, 4, 0, 1, 1);

        groupBox_7 = new QGroupBox(nclinkdevwidget);
        groupBox_7->setObjectName(QString::fromUtf8("groupBox_7"));
        gridLayout_7 = new QGridLayout(groupBox_7);
        gridLayout_7->setObjectName(QString::fromUtf8("gridLayout_7"));
        label_43 = new QLabel(groupBox_7);
        label_43->setObjectName(QString::fromUtf8("label_43"));

        gridLayout_7->addWidget(label_43, 0, 0, 1, 1);

        method_name = new QLineEdit(groupBox_7);
        method_name->setObjectName(QString::fromUtf8("method_name"));

        gridLayout_7->addWidget(method_name, 0, 1, 1, 1);

        label_57 = new QLabel(groupBox_7);
        label_57->setObjectName(QString::fromUtf8("label_57"));

        gridLayout_7->addWidget(label_57, 0, 2, 1, 1);

        method_args_name = new QLineEdit(groupBox_7);
        method_args_name->setObjectName(QString::fromUtf8("method_args_name"));

        gridLayout_7->addWidget(method_args_name, 0, 3, 1, 1);

        method_args_save = new QPushButton(groupBox_7);
        method_args_save->setObjectName(QString::fromUtf8("method_args_save"));

        gridLayout_7->addWidget(method_args_save, 0, 4, 2, 2);

        method_add = new QPushButton(groupBox_7);
        method_add->setObjectName(QString::fromUtf8("method_add"));

        gridLayout_7->addWidget(method_add, 0, 6, 1, 2);

        method_find = new QPushButton(groupBox_7);
        method_find->setObjectName(QString::fromUtf8("method_find"));

        gridLayout_7->addWidget(method_find, 0, 8, 1, 1);

        method_del = new QPushButton(groupBox_7);
        method_del->setObjectName(QString::fromUtf8("method_del"));

        gridLayout_7->addWidget(method_del, 1, 6, 2, 2);

        method_change = new QPushButton(groupBox_7);
        method_change->setObjectName(QString::fromUtf8("method_change"));

        gridLayout_7->addWidget(method_change, 1, 8, 2, 1);

        label_50 = new QLabel(groupBox_7);
        label_50->setObjectName(QString::fromUtf8("label_50"));

        gridLayout_7->addWidget(label_50, 2, 0, 1, 1);

        method_id = new QLineEdit(groupBox_7);
        method_id->setObjectName(QString::fromUtf8("method_id"));

        gridLayout_7->addWidget(method_id, 2, 1, 1, 1);

        label_58 = new QLabel(groupBox_7);
        label_58->setObjectName(QString::fromUtf8("label_58"));

        gridLayout_7->addWidget(label_58, 2, 2, 1, 1);

        method_args_text = new QLineEdit(groupBox_7);
        method_args_text->setObjectName(QString::fromUtf8("method_args_text"));

        gridLayout_7->addWidget(method_args_text, 2, 3, 1, 1);

        method_args_del = new QPushButton(groupBox_7);
        method_args_del->setObjectName(QString::fromUtf8("method_args_del"));

        gridLayout_7->addWidget(method_args_del, 2, 4, 1, 2);

        label_52 = new QLabel(groupBox_7);
        label_52->setObjectName(QString::fromUtf8("label_52"));

        gridLayout_7->addWidget(label_52, 3, 0, 1, 1);

        method_description = new QLineEdit(groupBox_7);
        method_description->setObjectName(QString::fromUtf8("method_description"));

        gridLayout_7->addWidget(method_description, 3, 1, 1, 1);

        label_53 = new QLabel(groupBox_7);
        label_53->setObjectName(QString::fromUtf8("label_53"));

        gridLayout_7->addWidget(label_53, 3, 2, 1, 1);

        method_root = new QSpinBox(groupBox_7);
        method_root->setObjectName(QString::fromUtf8("method_root"));

        gridLayout_7->addWidget(method_root, 3, 3, 1, 1);

        method_root_add = new QPushButton(groupBox_7);
        method_root_add->setObjectName(QString::fromUtf8("method_root_add"));

        gridLayout_7->addWidget(method_root_add, 3, 4, 1, 1);

        method_root_del = new QPushButton(groupBox_7);
        method_root_del->setObjectName(QString::fromUtf8("method_root_del"));

        gridLayout_7->addWidget(method_root_del, 3, 5, 1, 2);

        label_62 = new QLabel(groupBox_7);
        label_62->setObjectName(QString::fromUtf8("label_62"));

        gridLayout_7->addWidget(label_62, 3, 7, 1, 1);

        method_seq = new QSpinBox(groupBox_7);
        method_seq->setObjectName(QString::fromUtf8("method_seq"));

        gridLayout_7->addWidget(method_seq, 3, 8, 1, 1);


        gridLayout_10->addWidget(groupBox_7, 3, 0, 1, 1);

        groupBox_6 = new QGroupBox(nclinkdevwidget);
        groupBox_6->setObjectName(QString::fromUtf8("groupBox_6"));
        gridLayout_6 = new QGridLayout(groupBox_6);
        gridLayout_6->setObjectName(QString::fromUtf8("gridLayout_6"));
        label_30 = new QLabel(groupBox_6);
        label_30->setObjectName(QString::fromUtf8("label_30"));

        gridLayout_6->addWidget(label_30, 0, 0, 1, 1);

        channel_name = new QLineEdit(groupBox_6);
        channel_name->setObjectName(QString::fromUtf8("channel_name"));

        gridLayout_6->addWidget(channel_name, 0, 1, 1, 1);

        label_39 = new QLabel(groupBox_6);
        label_39->setObjectName(QString::fromUtf8("label_39"));

        gridLayout_6->addWidget(label_39, 0, 2, 1, 1);

        channel_upload = new QLineEdit(groupBox_6);
        channel_upload->setObjectName(QString::fromUtf8("channel_upload"));

        gridLayout_6->addWidget(channel_upload, 0, 3, 1, 2);

        label_33 = new QLabel(groupBox_6);
        label_33->setObjectName(QString::fromUtf8("label_33"));

        gridLayout_6->addWidget(label_33, 0, 5, 1, 1);

        channel_sample = new QLineEdit(groupBox_6);
        channel_sample->setObjectName(QString::fromUtf8("channel_sample"));

        gridLayout_6->addWidget(channel_sample, 0, 6, 1, 2);

        channel_add = new QPushButton(groupBox_6);
        channel_add->setObjectName(QString::fromUtf8("channel_add"));

        gridLayout_6->addWidget(channel_add, 0, 8, 1, 1);

        channel_find = new QPushButton(groupBox_6);
        channel_find->setObjectName(QString::fromUtf8("channel_find"));

        gridLayout_6->addWidget(channel_find, 0, 9, 1, 1);

        label_38 = new QLabel(groupBox_6);
        label_38->setObjectName(QString::fromUtf8("label_38"));

        gridLayout_6->addWidget(label_38, 1, 0, 1, 1);

        channel_id = new QLineEdit(groupBox_6);
        channel_id->setObjectName(QString::fromUtf8("channel_id"));

        gridLayout_6->addWidget(channel_id, 1, 1, 1, 1);

        label_40 = new QLabel(groupBox_6);
        label_40->setObjectName(QString::fromUtf8("label_40"));

        gridLayout_6->addWidget(label_40, 1, 2, 1, 2);

        channel_ids_add = new QPushButton(groupBox_6);
        channel_ids_add->setObjectName(QString::fromUtf8("channel_ids_add"));

        gridLayout_6->addWidget(channel_ids_add, 1, 6, 1, 1);

        channel_ids_del = new QPushButton(groupBox_6);
        channel_ids_del->setObjectName(QString::fromUtf8("channel_ids_del"));

        gridLayout_6->addWidget(channel_ids_del, 1, 7, 1, 1);

        channel_del = new QPushButton(groupBox_6);
        channel_del->setObjectName(QString::fromUtf8("channel_del"));

        gridLayout_6->addWidget(channel_del, 1, 8, 1, 1);

        channel_change = new QPushButton(groupBox_6);
        channel_change->setObjectName(QString::fromUtf8("channel_change"));

        gridLayout_6->addWidget(channel_change, 1, 9, 1, 1);

        label_42 = new QLabel(groupBox_6);
        label_42->setObjectName(QString::fromUtf8("label_42"));

        gridLayout_6->addWidget(label_42, 2, 0, 1, 1);

        channel_description = new QLineEdit(groupBox_6);
        channel_description->setObjectName(QString::fromUtf8("channel_description"));

        gridLayout_6->addWidget(channel_description, 2, 1, 1, 1);

        label_41 = new QLabel(groupBox_6);
        label_41->setObjectName(QString::fromUtf8("label_41"));

        gridLayout_6->addWidget(label_41, 2, 2, 1, 2);

        channel_device_add = new QPushButton(groupBox_6);
        channel_device_add->setObjectName(QString::fromUtf8("channel_device_add"));

        gridLayout_6->addWidget(channel_device_add, 2, 6, 1, 1);

        channel_device_del = new QPushButton(groupBox_6);
        channel_device_del->setObjectName(QString::fromUtf8("channel_device_del"));

        gridLayout_6->addWidget(channel_device_del, 2, 7, 1, 1);

        label_64 = new QLabel(groupBox_6);
        label_64->setObjectName(QString::fromUtf8("label_64"));

        gridLayout_6->addWidget(label_64, 2, 8, 1, 1);

        channel_seq = new QSpinBox(groupBox_6);
        channel_seq->setObjectName(QString::fromUtf8("channel_seq"));

        gridLayout_6->addWidget(channel_seq, 2, 9, 1, 1);

        channel_ids = new QSpinBox(groupBox_6);
        channel_ids->setObjectName(QString::fromUtf8("channel_ids"));

        gridLayout_6->addWidget(channel_ids, 1, 5, 1, 1);

        channel_device = new QSpinBox(groupBox_6);
        channel_device->setObjectName(QString::fromUtf8("channel_device"));

        gridLayout_6->addWidget(channel_device, 2, 5, 1, 1);


        gridLayout_10->addWidget(groupBox_6, 3, 1, 1, 1);

        groupBox_5 = new QGroupBox(nclinkdevwidget);
        groupBox_5->setObjectName(QString::fromUtf8("groupBox_5"));
        gridLayout_5 = new QGridLayout(groupBox_5);
        gridLayout_5->setObjectName(QString::fromUtf8("gridLayout_5"));
        data_device_dataitem_add = new QPushButton(groupBox_5);
        data_device_dataitem_add->setObjectName(QString::fromUtf8("data_device_dataitem_add"));

        gridLayout_5->addWidget(data_device_dataitem_add, 0, 14, 1, 1);

        data_components_dataitem = new QSpinBox(groupBox_5);
        data_components_dataitem->setObjectName(QString::fromUtf8("data_components_dataitem"));

        gridLayout_5->addWidget(data_components_dataitem, 5, 13, 1, 1);

        data_units = new QLineEdit(groupBox_5);
        data_units->setObjectName(QString::fromUtf8("data_units"));

        gridLayout_5->addWidget(data_units, 2, 5, 1, 2);

        data_device_dataitem_del = new QPushButton(groupBox_5);
        data_device_dataitem_del->setObjectName(QString::fromUtf8("data_device_dataitem_del"));

        gridLayout_5->addWidget(data_device_dataitem_del, 0, 15, 1, 1);

        data_name = new QLineEdit(groupBox_5);
        data_name->setObjectName(QString::fromUtf8("data_name"));

        gridLayout_5->addWidget(data_name, 0, 2, 1, 1);

        data_device_config_add = new QPushButton(groupBox_5);
        data_device_config_add->setObjectName(QString::fromUtf8("data_device_config_add"));

        gridLayout_5->addWidget(data_device_config_add, 1, 14, 1, 1);

        label_46 = new QLabel(groupBox_5);
        label_46->setObjectName(QString::fromUtf8("label_46"));

        gridLayout_5->addWidget(label_46, 2, 12, 1, 1);

        label_49 = new QLabel(groupBox_5);
        label_49->setObjectName(QString::fromUtf8("label_49"));

        gridLayout_5->addWidget(label_49, 4, 12, 1, 1);

        data_channel_del = new QPushButton(groupBox_5);
        data_channel_del->setObjectName(QString::fromUtf8("data_channel_del"));

        gridLayout_5->addWidget(data_channel_del, 4, 15, 1, 1);

        label_45 = new QLabel(groupBox_5);
        label_45->setObjectName(QString::fromUtf8("label_45"));

        gridLayout_5->addWidget(label_45, 1, 12, 1, 1);

        data_id = new QLineEdit(groupBox_5);
        data_id->setObjectName(QString::fromUtf8("data_id"));

        gridLayout_5->addWidget(data_id, 1, 2, 1, 1);

        label_37 = new QLabel(groupBox_5);
        label_37->setObjectName(QString::fromUtf8("label_37"));

        gridLayout_5->addWidget(label_37, 2, 3, 1, 2);

        data_device_dataitem = new QSpinBox(groupBox_5);
        data_device_dataitem->setObjectName(QString::fromUtf8("data_device_dataitem"));

        gridLayout_5->addWidget(data_device_dataitem, 0, 13, 1, 1);

        label_34 = new QLabel(groupBox_5);
        label_34->setObjectName(QString::fromUtf8("label_34"));

        gridLayout_5->addWidget(label_34, 2, 0, 1, 2);

        data_components_config_add = new QPushButton(groupBox_5);
        data_components_config_add->setObjectName(QString::fromUtf8("data_components_config_add"));

        gridLayout_5->addWidget(data_components_config_add, 2, 14, 1, 1);

        data_channel_add = new QPushButton(groupBox_5);
        data_channel_add->setObjectName(QString::fromUtf8("data_channel_add"));

        gridLayout_5->addWidget(data_channel_add, 4, 14, 1, 1);

        data_channel = new QSpinBox(groupBox_5);
        data_channel->setObjectName(QString::fromUtf8("data_channel"));

        gridLayout_5->addWidget(data_channel, 4, 13, 1, 1);

        label_31 = new QLabel(groupBox_5);
        label_31->setObjectName(QString::fromUtf8("label_31"));

        gridLayout_5->addWidget(label_31, 1, 0, 1, 2);

        data_source = new QLineEdit(groupBox_5);
        data_source->setObjectName(QString::fromUtf8("data_source"));

        gridLayout_5->addWidget(data_source, 1, 5, 1, 2);

        data_type = new QComboBox(groupBox_5);
        data_type->setObjectName(QString::fromUtf8("data_type"));

        gridLayout_5->addWidget(data_type, 2, 2, 1, 1);

        data_components_config_del = new QPushButton(groupBox_5);
        data_components_config_del->setObjectName(QString::fromUtf8("data_components_config_del"));

        gridLayout_5->addWidget(data_components_config_del, 2, 15, 1, 1);

        data_device_config_del = new QPushButton(groupBox_5);
        data_device_config_del->setObjectName(QString::fromUtf8("data_device_config_del"));

        gridLayout_5->addWidget(data_device_config_del, 1, 15, 1, 1);

        label_28 = new QLabel(groupBox_5);
        label_28->setObjectName(QString::fromUtf8("label_28"));

        gridLayout_5->addWidget(label_28, 0, 0, 1, 2);

        data_value = new QLineEdit(groupBox_5);
        data_value->setObjectName(QString::fromUtf8("data_value"));

        gridLayout_5->addWidget(data_value, 0, 5, 1, 2);

        data_device_config = new QSpinBox(groupBox_5);
        data_device_config->setObjectName(QString::fromUtf8("data_device_config"));

        gridLayout_5->addWidget(data_device_config, 1, 13, 1, 1);

        data_components_config = new QSpinBox(groupBox_5);
        data_components_config->setObjectName(QString::fromUtf8("data_components_config"));

        gridLayout_5->addWidget(data_components_config, 2, 13, 1, 1);

        label_66 = new QLabel(groupBox_5);
        label_66->setObjectName(QString::fromUtf8("label_66"));

        gridLayout_5->addWidget(label_66, 0, 12, 1, 1);

        data_components_dataitem_add = new QPushButton(groupBox_5);
        data_components_dataitem_add->setObjectName(QString::fromUtf8("data_components_dataitem_add"));

        gridLayout_5->addWidget(data_components_dataitem_add, 5, 14, 1, 1);

        data_components_dataitem_del = new QPushButton(groupBox_5);
        data_components_dataitem_del->setObjectName(QString::fromUtf8("data_components_dataitem_del"));

        gridLayout_5->addWidget(data_components_dataitem_del, 5, 15, 1, 1);

        label_35 = new QLabel(groupBox_5);
        label_35->setObjectName(QString::fromUtf8("label_35"));

        gridLayout_5->addWidget(label_35, 1, 3, 1, 2);

        label_65 = new QLabel(groupBox_5);
        label_65->setObjectName(QString::fromUtf8("label_65"));

        gridLayout_5->addWidget(label_65, 5, 12, 1, 1);

        label_36 = new QLabel(groupBox_5);
        label_36->setObjectName(QString::fromUtf8("label_36"));

        gridLayout_5->addWidget(label_36, 4, 0, 1, 1);

        data_description = new QLineEdit(groupBox_5);
        data_description->setObjectName(QString::fromUtf8("data_description"));

        gridLayout_5->addWidget(data_description, 4, 2, 1, 1);

        label_29 = new QLabel(groupBox_5);
        label_29->setObjectName(QString::fromUtf8("label_29"));

        gridLayout_5->addWidget(label_29, 5, 0, 1, 1);

        label_32 = new QLabel(groupBox_5);
        label_32->setObjectName(QString::fromUtf8("label_32"));

        gridLayout_5->addWidget(label_32, 0, 3, 1, 1);

        data_add = new QPushButton(groupBox_5);
        data_add->setObjectName(QString::fromUtf8("data_add"));

        gridLayout_5->addWidget(data_add, 0, 9, 1, 1);

        data_find = new QPushButton(groupBox_5);
        data_find->setObjectName(QString::fromUtf8("data_find"));

        gridLayout_5->addWidget(data_find, 0, 10, 1, 1);

        data_del = new QPushButton(groupBox_5);
        data_del->setObjectName(QString::fromUtf8("data_del"));

        gridLayout_5->addWidget(data_del, 1, 9, 1, 1);

        data_change = new QPushButton(groupBox_5);
        data_change->setObjectName(QString::fromUtf8("data_change"));

        gridLayout_5->addWidget(data_change, 1, 10, 1, 1);

        label_60 = new QLabel(groupBox_5);
        label_60->setObjectName(QString::fromUtf8("label_60"));

        gridLayout_5->addWidget(label_60, 2, 9, 1, 1);

        data_seq = new QSpinBox(groupBox_5);
        data_seq->setObjectName(QString::fromUtf8("data_seq"));

        gridLayout_5->addWidget(data_seq, 2, 10, 1, 1);

        data_setable = new QCheckBox(groupBox_5);
        data_setable->setObjectName(QString::fromUtf8("data_setable"));

        gridLayout_5->addWidget(data_setable, 4, 3, 1, 1);

        label_44 = new QLabel(groupBox_5);
        label_44->setObjectName(QString::fromUtf8("label_44"));

        gridLayout_5->addWidget(label_44, 5, 3, 1, 1);

        data_root_del = new QPushButton(groupBox_5);
        data_root_del->setObjectName(QString::fromUtf8("data_root_del"));

        gridLayout_5->addWidget(data_root_del, 5, 10, 1, 1);

        data_root_add = new QPushButton(groupBox_5);
        data_root_add->setObjectName(QString::fromUtf8("data_root_add"));

        gridLayout_5->addWidget(data_root_add, 5, 9, 1, 1);

        data_root = new QSpinBox(groupBox_5);
        data_root->setObjectName(QString::fromUtf8("data_root"));

        gridLayout_5->addWidget(data_root, 5, 5, 1, 2);

        data_datatype = new QComboBox(groupBox_5);
        data_datatype->setObjectName(QString::fromUtf8("data_datatype"));

        gridLayout_5->addWidget(data_datatype, 5, 2, 1, 1);


        gridLayout_10->addWidget(groupBox_5, 1, 1, 1, 1);


        gridLayout_11->addLayout(gridLayout_10, 0, 0, 1, 1);


        retranslateUi(nclinkdevwidget);

        QMetaObject::connectSlotsByName(nclinkdevwidget);
    } // setupUi

    void retranslateUi(QWidget *nclinkdevwidget)
    {
        nclinkdevwidget->setWindowTitle(QApplication::translate("nclinkdevwidget", "Form", nullptr));
        groupBox_4->setTitle(QApplication::translate("nclinkdevwidget", "\347\273\204\344\273\266\345\257\271\350\261\241\344\277\241\346\201\257(conponents)", nullptr));
        label_14->setText(QApplication::translate("nclinkdevwidget", "name\357\274\232", nullptr));
        label_19->setText(QApplication::translate("nclinkdevwidget", "config(\346\214\202\350\275\275\346\225\260\346\215\256\345\257\271\350\261\241seq) \357\274\232", nullptr));
        components_config_add->setText(QApplication::translate("nclinkdevwidget", "\346\214\202\350\275\275", nullptr));
        components_config_del->setText(QApplication::translate("nclinkdevwidget", "\347\247\273\351\231\244", nullptr));
        label_15->setText(QApplication::translate("nclinkdevwidget", "id\357\274\232", nullptr));
        label_21->setText(QApplication::translate("nclinkdevwidget", "dataItems(\346\214\202\350\275\275\347\232\204\346\225\260\346\215\256\345\257\271\350\261\241seq) \357\274\232", nullptr));
        components_dataitem_add->setText(QApplication::translate("nclinkdevwidget", "\346\214\202\350\275\275", nullptr));
        components_dataitem_del->setText(QApplication::translate("nclinkdevwidget", "\347\247\273\351\231\244", nullptr));
        label_17->setText(QApplication::translate("nclinkdevwidget", "type\357\274\232", nullptr));
        label_20->setText(QApplication::translate("nclinkdevwidget", "components(\346\214\202\350\275\275\347\273\204\344\273\266\345\257\271\350\261\241seq) \357\274\232", nullptr));
        components_self_add->setText(QApplication::translate("nclinkdevwidget", "\346\214\202\350\275\275", nullptr));
        components_self_del->setText(QApplication::translate("nclinkdevwidget", "\347\247\273\351\231\244", nullptr));
        label_16->setText(QApplication::translate("nclinkdevwidget", "description\357\274\232", nullptr));
        label_26->setText(QApplication::translate("nclinkdevwidget", "\350\242\253\346\214\202\350\275\275\347\232\204\350\256\276\345\244\207\345\257\271\350\261\241(device) seq\357\274\232", nullptr));
        components_device_add->setText(QApplication::translate("nclinkdevwidget", "\346\214\202\350\275\275", nullptr));
        components_device_del->setText(QApplication::translate("nclinkdevwidget", "\347\247\273\351\231\244", nullptr));
        label_18->setText(QApplication::translate("nclinkdevwidget", "number\357\274\232", nullptr));
        components_add->setText(QApplication::translate("nclinkdevwidget", "\345\210\233\345\273\272", nullptr));
        components_find->setText(QApplication::translate("nclinkdevwidget", "\346\237\245\350\257\242", nullptr));
        components_del->setText(QApplication::translate("nclinkdevwidget", "\345\210\240\351\231\244", nullptr));
        components_change->setText(QApplication::translate("nclinkdevwidget", "\344\277\256\346\224\271", nullptr));
        label_61->setText(QApplication::translate("nclinkdevwidget", "\345\272\217\345\217\267\357\274\232", nullptr));
        groupBox_3->setTitle(QApplication::translate("nclinkdevwidget", "\350\256\276\345\244\207\345\257\271\350\261\241\344\277\241\346\201\257(devices)", nullptr));
        label_6->setText(QApplication::translate("nclinkdevwidget", "name\357\274\232", nullptr));
        label_23->setText(QApplication::translate("nclinkdevwidget", "guid\357\274\232", nullptr));
        label_13->setText(QApplication::translate("nclinkdevwidget", "dataItems(\346\225\260\346\215\256\345\257\271\350\261\241id) \357\274\232", nullptr));
        device_dataitem_add->setText(QApplication::translate("nclinkdevwidget", "\346\214\202\350\275\275", nullptr));
        device_dataitem_del->setText(QApplication::translate("nclinkdevwidget", "\347\247\273\351\231\244", nullptr));
        label_7->setText(QApplication::translate("nclinkdevwidget", "id\357\274\232", nullptr));
        label_24->setText(QApplication::translate("nclinkdevwidget", "version\357\274\232", nullptr));
        label_12->setText(QApplication::translate("nclinkdevwidget", "components(\347\273\204\344\273\266\345\257\271\350\261\241id) \357\274\232", nullptr));
        device_components_add->setText(QApplication::translate("nclinkdevwidget", "\346\214\202\350\275\275", nullptr));
        device_components_del->setText(QApplication::translate("nclinkdevwidget", "\347\247\273\351\231\244", nullptr));
        label_9->setText(QApplication::translate("nclinkdevwidget", "type\357\274\232", nullptr));
        device_add->setText(QApplication::translate("nclinkdevwidget", "\345\210\233\345\273\272", nullptr));
        device_find->setText(QApplication::translate("nclinkdevwidget", "\346\237\245\350\257\242", nullptr));
        label_11->setText(QApplication::translate("nclinkdevwidget", "config(\346\225\260\346\215\256\345\257\271\350\261\241id) \357\274\232", nullptr));
        device_config_data_add->setText(QApplication::translate("nclinkdevwidget", "\346\214\202\350\275\275", nullptr));
        device_config_data_del->setText(QApplication::translate("nclinkdevwidget", "\347\247\273\351\231\244", nullptr));
        label_8->setText(QApplication::translate("nclinkdevwidget", "description\357\274\232", nullptr));
        device_del->setText(QApplication::translate("nclinkdevwidget", "\345\210\240\351\231\244", nullptr));
        device_change->setText(QApplication::translate("nclinkdevwidget", "\344\277\256\346\224\271", nullptr));
        label_47->setText(QApplication::translate("nclinkdevwidget", "config(\351\207\207\346\240\267\351\200\232\351\201\223\345\257\271\350\261\241id) \357\274\232", nullptr));
        device_config_channel_add->setText(QApplication::translate("nclinkdevwidget", "\346\214\202\350\275\275", nullptr));
        device_config_channel_del->setText(QApplication::translate("nclinkdevwidget", "\347\247\273\351\231\244", nullptr));
        label_10->setText(QApplication::translate("nclinkdevwidget", "number\357\274\232", nullptr));
        label_63->setText(QApplication::translate("nclinkdevwidget", "\345\272\217\345\217\267\357\274\232", nullptr));
        label_25->setText(QApplication::translate("nclinkdevwidget", "\350\242\253\346\214\202\350\275\275\347\232\204\346\240\271\345\257\271\350\261\241(root) id\357\274\232", nullptr));
        device_config_root_add->setText(QApplication::translate("nclinkdevwidget", "\346\214\202\350\275\275", nullptr));
        device_config_root_del->setText(QApplication::translate("nclinkdevwidget", "\347\247\273\351\231\244", nullptr));
        groupBox_9->setTitle(QApplication::translate("nclinkdevwidget", "\346\223\215\344\275\234\345\217\215\351\246\210\344\277\241\346\201\257", nullptr));
        groupBox_2->setTitle(QApplication::translate("nclinkdevwidget", "\346\240\271\345\257\271\350\261\241\344\277\241\346\201\257(root)", nullptr));
        label->setText(QApplication::translate("nclinkdevwidget", "name\357\274\232", nullptr));
        label_4->setText(QApplication::translate("nclinkdevwidget", "config(\346\214\202\350\275\275\346\225\260\346\215\256\345\257\271\350\261\241seq) \357\274\232", nullptr));
        root_config_data_add->setText(QApplication::translate("nclinkdevwidget", "\346\214\202\350\275\275", nullptr));
        root_config_data_del->setText(QApplication::translate("nclinkdevwidget", "\347\247\273\351\231\244", nullptr));
        label_2->setText(QApplication::translate("nclinkdevwidget", "id\357\274\232", nullptr));
        label_48->setText(QApplication::translate("nclinkdevwidget", "config(\346\214\202\350\275\275\346\226\271\346\263\225\345\257\271\350\261\241seq) \357\274\232", nullptr));
        root_config_method_add->setText(QApplication::translate("nclinkdevwidget", "\346\214\202\350\275\275", nullptr));
        root_config_method_del->setText(QApplication::translate("nclinkdevwidget", "\347\247\273\351\231\244", nullptr));
        label_22->setText(QApplication::translate("nclinkdevwidget", "version\357\274\232", nullptr));
        label_5->setText(QApplication::translate("nclinkdevwidget", "device(\346\214\202\350\275\275\350\256\276\345\244\207\345\257\271\350\261\241seq) \357\274\232", nullptr));
        root_config_device_add->setText(QApplication::translate("nclinkdevwidget", "\346\214\202\350\275\275", nullptr));
        root_config_device_del->setText(QApplication::translate("nclinkdevwidget", "\347\247\273\351\231\244", nullptr));
        label_3->setText(QApplication::translate("nclinkdevwidget", "description\357\274\232", nullptr));
        root_add->setText(QApplication::translate("nclinkdevwidget", "\345\210\233\345\273\272", nullptr));
        root_find->setText(QApplication::translate("nclinkdevwidget", "\346\237\245\350\257\242", nullptr));
        root_del->setText(QApplication::translate("nclinkdevwidget", "\345\210\240\351\231\244", nullptr));
        root_change->setText(QApplication::translate("nclinkdevwidget", "\344\277\256\346\224\271", nullptr));
        label_59->setText(QApplication::translate("nclinkdevwidget", "\345\272\217\345\217\267\357\274\232", nullptr));
        groupBox->setTitle(QApplication::translate("nclinkdevwidget", "\346\225\260\346\216\247\346\234\272\345\272\212\346\250\241\345\236\213\344\277\241\346\201\257\350\241\250\346\240\274", nullptr));
        groupBox_8->setTitle(QApplication::translate("nclinkdevwidget", "\346\200\273\344\275\223\346\223\215\344\275\234", nullptr));
        label_56->setText(QApplication::translate("nclinkdevwidget", "\344\270\212\350\277\260\346\225\260\346\216\247\346\234\272\345\272\212\346\250\241\345\236\213\345\217\202\346\225\260\345\217\202\350\200\203\346\240\207\345\207\206\344\270\272\357\274\232\346\231\272\350\203\275\345\267\245\345\216\202\346\225\260\346\216\247\346\234\272\345\272\212\344\272\222\350\201\224\346\216\245\345\217\243\350\247\204\350\214\203-\345\233\275\345\256\266\346\240\207\345\207\206\345\276\201\346\261\202\346\204\217\350\247\201\347\250\2771.1", nullptr));
        label_51->setText(QApplication::translate("nclinkdevwidget", "\350\257\273\345\217\226\357\274\232\344\273\216json\346\226\207\344\273\266\344\270\255\350\257\273\345\217\226\345\205\250\351\203\250\345\206\205\345\256\271\357\274\210\351\234\200\350\246\201\350\276\223\345\205\245\344\270\215\345\270\246\345\220\216\347\274\200\347\232\204\346\226\207\344\273\266\345\220\215\357\274\211", nullptr));
        label_54->setText(QApplication::translate("nclinkdevwidget", "\351\200\200\345\207\272\357\274\232\344\270\215\344\277\235\345\255\230\345\275\223\345\211\215\345\206\205\345\256\271\351\200\200\345\207\272/\345\205\263\351\227\255", nullptr));
        label_55->setText(QApplication::translate("nclinkdevwidget", "\351\273\230\350\256\244\350\267\257\345\276\204\344\270\272\357\274\232../json/", nullptr));
        exit->setText(QApplication::translate("nclinkdevwidget", "\351\200\200\345\207\272", nullptr));
        read_all->setText(QApplication::translate("nclinkdevwidget", "\350\257\273\345\217\226", nullptr));
        save_all->setText(QApplication::translate("nclinkdevwidget", "\344\277\235\345\255\230", nullptr));
        label_27->setText(QApplication::translate("nclinkdevwidget", "\344\277\235\345\255\230\357\274\232\344\277\235\345\255\230\345\205\250\351\203\250\345\206\205\345\256\271\345\210\260json\346\226\207\344\273\266\344\270\255\357\274\210\351\234\200\350\246\201\350\276\223\345\205\245\344\270\215\345\270\246\345\220\216\347\274\200\347\232\204\346\226\207\344\273\266\345\220\215\357\274\211", nullptr));
        groupBox_7->setTitle(QApplication::translate("nclinkdevwidget", "\346\226\271\346\263\225\345\257\271\350\261\241\344\277\241\346\201\257(method)", nullptr));
        label_43->setText(QApplication::translate("nclinkdevwidget", "name\357\274\232", nullptr));
        label_57->setText(QApplication::translate("nclinkdevwidget", "args_name\357\274\232", nullptr));
        method_args_save->setText(QApplication::translate("nclinkdevwidget", "\344\277\235\345\255\230\345\275\223\345\211\215arg", nullptr));
        method_add->setText(QApplication::translate("nclinkdevwidget", "\345\210\233\345\273\272", nullptr));
        method_find->setText(QApplication::translate("nclinkdevwidget", "\346\237\245\350\257\242", nullptr));
        method_del->setText(QApplication::translate("nclinkdevwidget", "\345\210\240\351\231\244", nullptr));
        method_change->setText(QApplication::translate("nclinkdevwidget", "\344\277\256\346\224\271", nullptr));
        label_50->setText(QApplication::translate("nclinkdevwidget", "id\357\274\232", nullptr));
        label_58->setText(QApplication::translate("nclinkdevwidget", "args_text\357\274\232", nullptr));
        method_args_del->setText(QApplication::translate("nclinkdevwidget", "\345\210\240\351\231\244\345\275\223\345\211\215arg", nullptr));
        label_52->setText(QApplication::translate("nclinkdevwidget", "description\357\274\232", nullptr));
        label_53->setText(QApplication::translate("nclinkdevwidget", "\350\242\253\346\214\202\350\275\275\347\232\204(root)id\357\274\232", nullptr));
        method_root_add->setText(QApplication::translate("nclinkdevwidget", "\346\214\202\350\275\275", nullptr));
        method_root_del->setText(QApplication::translate("nclinkdevwidget", "\347\247\273\351\231\244", nullptr));
        label_62->setText(QApplication::translate("nclinkdevwidget", "\345\272\217\345\217\267\357\274\232", nullptr));
        groupBox_6->setTitle(QApplication::translate("nclinkdevwidget", "\351\207\207\346\240\267\351\200\232\351\201\223\345\257\271\350\261\241\344\277\241\346\201\257(channels)", nullptr));
        label_30->setText(QApplication::translate("nclinkdevwidget", "name\357\274\232", nullptr));
        label_39->setText(QApplication::translate("nclinkdevwidget", "uploadInterval:", nullptr));
        label_33->setText(QApplication::translate("nclinkdevwidget", "sampleInterval\357\274\232", nullptr));
        channel_add->setText(QApplication::translate("nclinkdevwidget", "\345\210\233\345\273\272", nullptr));
        channel_find->setText(QApplication::translate("nclinkdevwidget", "\346\237\245\350\257\242", nullptr));
        label_38->setText(QApplication::translate("nclinkdevwidget", "id\357\274\232", nullptr));
        label_40->setText(QApplication::translate("nclinkdevwidget", "ids(\346\214\202\350\275\275\346\225\260\346\215\256\345\257\271\350\261\241id)\357\274\232", nullptr));
        channel_ids_add->setText(QApplication::translate("nclinkdevwidget", "\346\214\202\350\275\275", nullptr));
        channel_ids_del->setText(QApplication::translate("nclinkdevwidget", "\347\247\273\351\231\244", nullptr));
        channel_del->setText(QApplication::translate("nclinkdevwidget", "\345\210\240\351\231\244", nullptr));
        channel_change->setText(QApplication::translate("nclinkdevwidget", "\344\277\256\346\224\271", nullptr));
        label_42->setText(QApplication::translate("nclinkdevwidget", "description\357\274\232", nullptr));
        label_41->setText(QApplication::translate("nclinkdevwidget", "\350\242\253\346\214\202\350\275\275\350\256\276\345\244\207\345\257\271\350\261\241(device)id\357\274\232", nullptr));
        channel_device_add->setText(QApplication::translate("nclinkdevwidget", "\346\214\202\350\275\275", nullptr));
        channel_device_del->setText(QApplication::translate("nclinkdevwidget", "\347\247\273\351\231\244", nullptr));
        label_64->setText(QApplication::translate("nclinkdevwidget", "\345\272\217\345\217\267\357\274\232", nullptr));
        groupBox_5->setTitle(QApplication::translate("nclinkdevwidget", "\346\225\260\346\215\256\345\257\271\350\261\241\344\277\241\346\201\257(data)", nullptr));
        data_device_dataitem_add->setText(QApplication::translate("nclinkdevwidget", "\346\214\202\350\275\275", nullptr));
        data_device_dataitem_del->setText(QApplication::translate("nclinkdevwidget", "\347\247\273\351\231\244", nullptr));
        data_device_config_add->setText(QApplication::translate("nclinkdevwidget", "\346\214\202\350\275\275", nullptr));
        label_46->setText(QApplication::translate("nclinkdevwidget", "\350\242\253\346\214\202\350\275\275comp.config seq\357\274\232", nullptr));
        label_49->setText(QApplication::translate("nclinkdevwidget", "\350\242\253\346\214\202\350\275\275channel seq\357\274\232", nullptr));
        data_channel_del->setText(QApplication::translate("nclinkdevwidget", "\347\247\273\351\231\244", nullptr));
        label_45->setText(QApplication::translate("nclinkdevwidget", "\350\242\253\346\214\202\350\275\275device.config seq\357\274\232", nullptr));
        label_37->setText(QApplication::translate("nclinkdevwidget", "units\357\274\232", nullptr));
        label_34->setText(QApplication::translate("nclinkdevwidget", "type\357\274\232", nullptr));
        data_components_config_add->setText(QApplication::translate("nclinkdevwidget", "\346\214\202\350\275\275", nullptr));
        data_channel_add->setText(QApplication::translate("nclinkdevwidget", "\346\214\202\350\275\275", nullptr));
        label_31->setText(QApplication::translate("nclinkdevwidget", "id\357\274\232", nullptr));
        data_components_config_del->setText(QApplication::translate("nclinkdevwidget", "\347\247\273\351\231\244", nullptr));
        data_device_config_del->setText(QApplication::translate("nclinkdevwidget", "\347\247\273\351\231\244", nullptr));
        label_28->setText(QApplication::translate("nclinkdevwidget", "name\357\274\232", nullptr));
        label_66->setText(QApplication::translate("nclinkdevwidget", "\350\242\253\346\214\202\350\275\275device.dataitem seq\357\274\232", nullptr));
        data_components_dataitem_add->setText(QApplication::translate("nclinkdevwidget", "\346\214\202\350\275\275", nullptr));
        data_components_dataitem_del->setText(QApplication::translate("nclinkdevwidget", "\347\247\273\351\231\244", nullptr));
        label_35->setText(QApplication::translate("nclinkdevwidget", "source\357\274\232", nullptr));
        label_65->setText(QApplication::translate("nclinkdevwidget", "\350\242\253\346\214\202\350\275\275comp.dataitem seq\357\274\232", nullptr));
        label_36->setText(QApplication::translate("nclinkdevwidget", "description\357\274\232", nullptr));
        label_29->setText(QApplication::translate("nclinkdevwidget", "datatype\357\274\232", nullptr));
        label_32->setText(QApplication::translate("nclinkdevwidget", "value\357\274\232", nullptr));
        data_add->setText(QApplication::translate("nclinkdevwidget", "\345\210\233\345\273\272", nullptr));
        data_find->setText(QApplication::translate("nclinkdevwidget", "\346\237\245\350\257\242", nullptr));
        data_del->setText(QApplication::translate("nclinkdevwidget", "\345\210\240\351\231\244", nullptr));
        data_change->setText(QApplication::translate("nclinkdevwidget", "\344\277\256\346\224\271", nullptr));
        label_60->setText(QApplication::translate("nclinkdevwidget", "\345\272\217\345\217\267\357\274\232", nullptr));
        data_setable->setText(QApplication::translate("nclinkdevwidget", "setable", nullptr));
        label_44->setText(QApplication::translate("nclinkdevwidget", "\350\242\253\346\214\202\350\275\275root seq\357\274\232", nullptr));
        data_root_del->setText(QApplication::translate("nclinkdevwidget", "\347\247\273\351\231\244", nullptr));
        data_root_add->setText(QApplication::translate("nclinkdevwidget", "\346\214\202\350\275\275", nullptr));
    } // retranslateUi

};

namespace Ui {
    class nclinkdevwidget: public Ui_nclinkdevwidget {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_NCLINKDEVWIDGET_H
