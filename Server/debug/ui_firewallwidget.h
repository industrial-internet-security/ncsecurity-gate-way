/********************************************************************************
** Form generated from reading UI file 'firewallwidget.ui'
**
** Created by: Qt User Interface Compiler version 5.12.8
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_FIREWALLWIDGET_H
#define UI_FIREWALLWIDGET_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QTableView>
#include <QtWidgets/QTextBrowser>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_FirewallWidget
{
public:
    QGridLayout *gridLayout_4;
    QGridLayout *gridLayout;
    QGroupBox *groupBox;
    QGridLayout *gridLayout_2;
    QTableView *tableView;
    QGroupBox *groupBox_2;
    QGridLayout *gridLayout_3;
    QPushButton *del_nat;
    QPushButton *list_conn;
    QPushButton *list_log;
    QPushButton *add_nat;
    QPushButton *list_rule;
    QPushButton *add_rule;
    QPushButton *default_accept;
    QPushButton *list_nat;
    QPushButton *del_rule;
    QPushButton *default_drop;
    QPushButton *close_firewall;
    QPushButton *open_firewall;
    QGroupBox *groupBox_3;
    QGridLayout *gridLayout_5;
    QComboBox *action;
    QLabel *proto_label;
    QLineEdit *prior;
    QLabel *dstip_label;
    QLineEdit *name;
    QLabel *dstmk_label;
    QComboBox *proto;
    QLabel *srcmk_lable;
    QLabel *srcptr_label;
    QLabel *srcip_label;
    QLineEdit *dstip;
    QLabel *prior_label;
    QLineEdit *dstmk;
    QLineEdit *dstptl;
    QLineEdit *srcmk;
    QLineEdit *srcptl;
    QLineEdit *srcip;
    QLabel *srcptl_label;
    QLineEdit *dstptr;
    QLabel *dstptl_label;
    QLabel *action_label;
    QCheckBox *dstpt_any;
    QLabel *dstptr_label;
    QLineEdit *srcptr;
    QCheckBox *srcpt_any;
    QLabel *name_label;
    QLabel *label;
    QTextBrowser *feedback;
    QGroupBox *groupBox_4;
    QGridLayout *gridLayout_6;
    QLabel *text1;
    QLabel *text2;
    QLabel *label_2;

    void setupUi(QWidget *FirewallWidget)
    {
        if (FirewallWidget->objectName().isEmpty())
            FirewallWidget->setObjectName(QString::fromUtf8("FirewallWidget"));
        FirewallWidget->resize(1020, 955);
        gridLayout_4 = new QGridLayout(FirewallWidget);
        gridLayout_4->setObjectName(QString::fromUtf8("gridLayout_4"));
        gridLayout = new QGridLayout();
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        groupBox = new QGroupBox(FirewallWidget);
        groupBox->setObjectName(QString::fromUtf8("groupBox"));
        gridLayout_2 = new QGridLayout(groupBox);
        gridLayout_2->setObjectName(QString::fromUtf8("gridLayout_2"));
        tableView = new QTableView(groupBox);
        tableView->setObjectName(QString::fromUtf8("tableView"));

        gridLayout_2->addWidget(tableView, 0, 0, 1, 1);


        gridLayout->addWidget(groupBox, 0, 0, 1, 2);

        groupBox_2 = new QGroupBox(FirewallWidget);
        groupBox_2->setObjectName(QString::fromUtf8("groupBox_2"));
        gridLayout_3 = new QGridLayout(groupBox_2);
        gridLayout_3->setObjectName(QString::fromUtf8("gridLayout_3"));
        del_nat = new QPushButton(groupBox_2);
        del_nat->setObjectName(QString::fromUtf8("del_nat"));

        gridLayout_3->addWidget(del_nat, 3, 1, 1, 1);

        list_conn = new QPushButton(groupBox_2);
        list_conn->setObjectName(QString::fromUtf8("list_conn"));

        gridLayout_3->addWidget(list_conn, 5, 1, 1, 1);

        list_log = new QPushButton(groupBox_2);
        list_log->setObjectName(QString::fromUtf8("list_log"));

        gridLayout_3->addWidget(list_log, 5, 0, 1, 1);

        add_nat = new QPushButton(groupBox_2);
        add_nat->setObjectName(QString::fromUtf8("add_nat"));

        gridLayout_3->addWidget(add_nat, 2, 1, 1, 1);

        list_rule = new QPushButton(groupBox_2);
        list_rule->setObjectName(QString::fromUtf8("list_rule"));

        gridLayout_3->addWidget(list_rule, 1, 0, 1, 1);

        add_rule = new QPushButton(groupBox_2);
        add_rule->setObjectName(QString::fromUtf8("add_rule"));

        gridLayout_3->addWidget(add_rule, 2, 0, 1, 1);

        default_accept = new QPushButton(groupBox_2);
        default_accept->setObjectName(QString::fromUtf8("default_accept"));

        gridLayout_3->addWidget(default_accept, 4, 1, 1, 1);

        list_nat = new QPushButton(groupBox_2);
        list_nat->setObjectName(QString::fromUtf8("list_nat"));

        gridLayout_3->addWidget(list_nat, 1, 1, 1, 1);

        del_rule = new QPushButton(groupBox_2);
        del_rule->setObjectName(QString::fromUtf8("del_rule"));

        gridLayout_3->addWidget(del_rule, 3, 0, 1, 1);

        default_drop = new QPushButton(groupBox_2);
        default_drop->setObjectName(QString::fromUtf8("default_drop"));

        gridLayout_3->addWidget(default_drop, 4, 0, 1, 1);

        close_firewall = new QPushButton(groupBox_2);
        close_firewall->setObjectName(QString::fromUtf8("close_firewall"));

        gridLayout_3->addWidget(close_firewall, 6, 0, 1, 2);

        open_firewall = new QPushButton(groupBox_2);
        open_firewall->setObjectName(QString::fromUtf8("open_firewall"));

        gridLayout_3->addWidget(open_firewall, 0, 0, 1, 2);


        gridLayout->addWidget(groupBox_2, 1, 0, 1, 1);

        groupBox_3 = new QGroupBox(FirewallWidget);
        groupBox_3->setObjectName(QString::fromUtf8("groupBox_3"));
        gridLayout_5 = new QGridLayout(groupBox_3);
        gridLayout_5->setObjectName(QString::fromUtf8("gridLayout_5"));
        action = new QComboBox(groupBox_3);
        action->setObjectName(QString::fromUtf8("action"));

        gridLayout_5->addWidget(action, 8, 4, 1, 1);

        proto_label = new QLabel(groupBox_3);
        proto_label->setObjectName(QString::fromUtf8("proto_label"));

        gridLayout_5->addWidget(proto_label, 8, 1, 1, 1);

        prior = new QLineEdit(groupBox_3);
        prior->setObjectName(QString::fromUtf8("prior"));

        gridLayout_5->addWidget(prior, 0, 4, 1, 1, Qt::AlignHCenter);

        dstip_label = new QLabel(groupBox_3);
        dstip_label->setObjectName(QString::fromUtf8("dstip_label"));

        gridLayout_5->addWidget(dstip_label, 1, 3, 1, 1);

        name = new QLineEdit(groupBox_3);
        name->setObjectName(QString::fromUtf8("name"));

        gridLayout_5->addWidget(name, 0, 2, 1, 1, Qt::AlignHCenter);

        dstmk_label = new QLabel(groupBox_3);
        dstmk_label->setObjectName(QString::fromUtf8("dstmk_label"));

        gridLayout_5->addWidget(dstmk_label, 2, 3, 1, 1);

        proto = new QComboBox(groupBox_3);
        proto->setObjectName(QString::fromUtf8("proto"));

        gridLayout_5->addWidget(proto, 8, 2, 1, 1);

        srcmk_lable = new QLabel(groupBox_3);
        srcmk_lable->setObjectName(QString::fromUtf8("srcmk_lable"));

        gridLayout_5->addWidget(srcmk_lable, 2, 1, 1, 1);

        srcptr_label = new QLabel(groupBox_3);
        srcptr_label->setObjectName(QString::fromUtf8("srcptr_label"));

        gridLayout_5->addWidget(srcptr_label, 4, 1, 1, 1);

        srcip_label = new QLabel(groupBox_3);
        srcip_label->setObjectName(QString::fromUtf8("srcip_label"));

        gridLayout_5->addWidget(srcip_label, 1, 1, 1, 1);

        dstip = new QLineEdit(groupBox_3);
        dstip->setObjectName(QString::fromUtf8("dstip"));

        gridLayout_5->addWidget(dstip, 1, 4, 1, 1, Qt::AlignHCenter);

        prior_label = new QLabel(groupBox_3);
        prior_label->setObjectName(QString::fromUtf8("prior_label"));

        gridLayout_5->addWidget(prior_label, 0, 3, 1, 1);

        dstmk = new QLineEdit(groupBox_3);
        dstmk->setObjectName(QString::fromUtf8("dstmk"));

        gridLayout_5->addWidget(dstmk, 2, 4, 1, 1, Qt::AlignHCenter);

        dstptl = new QLineEdit(groupBox_3);
        dstptl->setObjectName(QString::fromUtf8("dstptl"));

        gridLayout_5->addWidget(dstptl, 3, 4, 1, 1, Qt::AlignHCenter);

        srcmk = new QLineEdit(groupBox_3);
        srcmk->setObjectName(QString::fromUtf8("srcmk"));

        gridLayout_5->addWidget(srcmk, 2, 2, 1, 1, Qt::AlignHCenter);

        srcptl = new QLineEdit(groupBox_3);
        srcptl->setObjectName(QString::fromUtf8("srcptl"));

        gridLayout_5->addWidget(srcptl, 3, 2, 1, 1, Qt::AlignHCenter);

        srcip = new QLineEdit(groupBox_3);
        srcip->setObjectName(QString::fromUtf8("srcip"));

        gridLayout_5->addWidget(srcip, 1, 2, 1, 1, Qt::AlignHCenter);

        srcptl_label = new QLabel(groupBox_3);
        srcptl_label->setObjectName(QString::fromUtf8("srcptl_label"));

        gridLayout_5->addWidget(srcptl_label, 3, 1, 1, 1);

        dstptr = new QLineEdit(groupBox_3);
        dstptr->setObjectName(QString::fromUtf8("dstptr"));

        gridLayout_5->addWidget(dstptr, 4, 4, 1, 1, Qt::AlignHCenter);

        dstptl_label = new QLabel(groupBox_3);
        dstptl_label->setObjectName(QString::fromUtf8("dstptl_label"));

        gridLayout_5->addWidget(dstptl_label, 3, 3, 1, 1);

        action_label = new QLabel(groupBox_3);
        action_label->setObjectName(QString::fromUtf8("action_label"));

        gridLayout_5->addWidget(action_label, 8, 3, 1, 1);

        dstpt_any = new QCheckBox(groupBox_3);
        dstpt_any->setObjectName(QString::fromUtf8("dstpt_any"));

        gridLayout_5->addWidget(dstpt_any, 6, 3, 1, 2);

        dstptr_label = new QLabel(groupBox_3);
        dstptr_label->setObjectName(QString::fromUtf8("dstptr_label"));

        gridLayout_5->addWidget(dstptr_label, 4, 3, 1, 1);

        srcptr = new QLineEdit(groupBox_3);
        srcptr->setObjectName(QString::fromUtf8("srcptr"));

        gridLayout_5->addWidget(srcptr, 4, 2, 1, 1, Qt::AlignHCenter);

        srcpt_any = new QCheckBox(groupBox_3);
        srcpt_any->setObjectName(QString::fromUtf8("srcpt_any"));

        gridLayout_5->addWidget(srcpt_any, 6, 1, 1, 2);

        name_label = new QLabel(groupBox_3);
        name_label->setObjectName(QString::fromUtf8("name_label"));

        gridLayout_5->addWidget(name_label, 0, 1, 1, 1);

        label = new QLabel(groupBox_3);
        label->setObjectName(QString::fromUtf8("label"));

        gridLayout_5->addWidget(label, 9, 1, 1, 1);

        feedback = new QTextBrowser(groupBox_3);
        feedback->setObjectName(QString::fromUtf8("feedback"));

        gridLayout_5->addWidget(feedback, 9, 2, 1, 3);


        gridLayout->addWidget(groupBox_3, 1, 1, 1, 1);

        groupBox_4 = new QGroupBox(FirewallWidget);
        groupBox_4->setObjectName(QString::fromUtf8("groupBox_4"));
        gridLayout_6 = new QGridLayout(groupBox_4);
        gridLayout_6->setObjectName(QString::fromUtf8("gridLayout_6"));
        text1 = new QLabel(groupBox_4);
        text1->setObjectName(QString::fromUtf8("text1"));

        gridLayout_6->addWidget(text1, 0, 0, 1, 1);

        text2 = new QLabel(groupBox_4);
        text2->setObjectName(QString::fromUtf8("text2"));

        gridLayout_6->addWidget(text2, 1, 0, 1, 1);

        label_2 = new QLabel(groupBox_4);
        label_2->setObjectName(QString::fromUtf8("label_2"));

        gridLayout_6->addWidget(label_2, 2, 0, 1, 1);


        gridLayout->addWidget(groupBox_4, 2, 0, 1, 2);


        gridLayout_4->addLayout(gridLayout, 0, 0, 1, 1);


        retranslateUi(FirewallWidget);

        QMetaObject::connectSlotsByName(FirewallWidget);
    } // setupUi

    void retranslateUi(QWidget *FirewallWidget)
    {
        FirewallWidget->setWindowTitle(QApplication::translate("FirewallWidget", "Form", nullptr));
        groupBox->setTitle(QApplication::translate("FirewallWidget", "\346\230\276\347\244\272\345\267\262\347\273\217\351\223\276\346\216\245\347\232\204\350\247\204\345\210\231\346\210\226\350\200\205\346\227\245\345\277\227", nullptr));
        groupBox_2->setTitle(QApplication::translate("FirewallWidget", "\346\223\215\344\275\234\346\240\217", nullptr));
        del_nat->setText(QApplication::translate("FirewallWidget", "\345\210\240\351\231\244nat\350\247\204\345\210\231", nullptr));
        list_conn->setText(QApplication::translate("FirewallWidget", "\346\237\245\347\234\213\345\275\223\345\211\215\351\223\276\346\216\245", nullptr));
        list_log->setText(QApplication::translate("FirewallWidget", "\346\237\245\347\234\213\346\227\245\345\277\227", nullptr));
        add_nat->setText(QApplication::translate("FirewallWidget", "\346\267\273\345\212\240nat\350\247\204\345\210\231", nullptr));
        list_rule->setText(QApplication::translate("FirewallWidget", "\346\237\245\347\234\213\345\270\270\350\247\204\350\247\204\345\210\231", nullptr));
        add_rule->setText(QApplication::translate("FirewallWidget", "\346\267\273\345\212\240\345\270\270\350\247\204\350\247\204\345\210\231", nullptr));
        default_accept->setText(QApplication::translate("FirewallWidget", "\351\273\230\350\256\244\345\205\201\350\256\270\346\211\200\346\234\211", nullptr));
        list_nat->setText(QApplication::translate("FirewallWidget", "\346\237\245\347\234\213nat\350\247\204\345\210\231", nullptr));
        del_rule->setText(QApplication::translate("FirewallWidget", "\345\210\240\351\231\244\345\270\270\350\247\204\350\247\204\345\210\231", nullptr));
        default_drop->setText(QApplication::translate("FirewallWidget", "\351\273\230\350\256\244\346\213\222\347\273\235\346\211\200\346\234\211", nullptr));
        close_firewall->setText(QApplication::translate("FirewallWidget", "\347\273\210\346\255\242\351\230\262\347\201\253\345\242\231", nullptr));
        open_firewall->setText(QApplication::translate("FirewallWidget", "\345\220\257\345\212\250\351\230\262\347\201\253\345\242\231", nullptr));
        groupBox_3->setTitle(QApplication::translate("FirewallWidget", "\346\225\260\346\215\256\346\240\217", nullptr));
        proto_label->setText(QApplication::translate("FirewallWidget", "\350\277\207\346\273\244\345\215\217\350\256\256\351\200\211\346\213\251\357\274\232", nullptr));
        dstip_label->setText(QApplication::translate("FirewallWidget", "\347\233\256\347\232\204/nat IP\345\234\260\345\235\200\357\274\232", nullptr));
        dstmk_label->setText(QApplication::translate("FirewallWidget", "\347\233\256\347\232\204/nat mask\344\275\215\346\225\260\357\274\232", nullptr));
        srcmk_lable->setText(QApplication::translate("FirewallWidget", "\346\272\220mask\344\275\215\346\225\260\357\274\232", nullptr));
        srcptr_label->setText(QApplication::translate("FirewallWidget", "\346\272\220\347\253\257\345\217\243\346\234\200\345\244\247\345\200\274\357\274\232", nullptr));
        srcip_label->setText(QApplication::translate("FirewallWidget", "\346\272\220IP\345\234\260\345\235\200\357\274\232", nullptr));
        prior_label->setText(QApplication::translate("FirewallWidget", "\344\275\215\344\272\216\350\247\204\345\210\231\344\271\213\345\220\216\357\274\232", nullptr));
        srcmk->setText(QString());
        srcptl->setText(QString());
        srcptl_label->setText(QApplication::translate("FirewallWidget", "\346\272\220\347\253\257\345\217\243\346\234\200\345\260\217\345\200\274\357\274\232", nullptr));
        dstptl_label->setText(QApplication::translate("FirewallWidget", "\347\233\256\347\232\204/nat \347\253\257\345\217\243\346\234\200\345\260\217\345\200\274\357\274\232", nullptr));
        action_label->setText(QApplication::translate("FirewallWidget", "\345\205\201\350\256\270/\346\213\222\347\273\235\357\274\237", nullptr));
        dstpt_any->setText(QApplication::translate("FirewallWidget", "\350\277\207\346\273\244\346\211\200\346\234\211\347\232\204\347\233\256\347\232\204/nat\347\253\257\345\217\243", nullptr));
        dstptr_label->setText(QApplication::translate("FirewallWidget", "\347\233\256\347\232\204/nat \347\253\257\345\217\243\346\234\200\345\244\247\345\200\274\357\274\232", nullptr));
        srcpt_any->setText(QApplication::translate("FirewallWidget", "\350\277\207\346\273\244\346\211\200\346\234\211\347\232\204\346\272\220\347\253\257\345\217\243", nullptr));
        name_label->setText(QApplication::translate("FirewallWidget", "\350\247\204\345\210\231\345\220\215\347\247\260/\345\272\217\345\217\267\357\274\232", nullptr));
        label->setText(QApplication::translate("FirewallWidget", "\346\223\215\344\275\234\344\277\241\346\201\257\345\217\215\351\246\210", nullptr));
        groupBox_4->setTitle(QApplication::translate("FirewallWidget", "\350\257\264\346\230\216", nullptr));
        text1->setText(QApplication::translate("FirewallWidget", "\346\267\273\345\212\240/\345\210\240\351\231\244\350\247\204\345\210\231\346\227\266\357\274\214\351\234\200\350\246\201\345\234\250\346\225\260\346\215\256\346\240\217\344\270\212\350\276\223\345\205\245\347\233\270\345\272\224\347\232\204\345\206\205\345\256\271\343\200\202\344\275\215\344\272\216\350\247\204\345\210\231\344\271\213\345\220\216\347\232\204\346\225\210\346\236\234\346\230\257\344\275\277\350\257\245\350\247\204\345\210\231\344\275\215\344\272\216\350\276\223\345\205\245\347\232\204\345\220\215\347\247\260\350\247\204\345\210\231\344\274\230\345\205\210\347\272\247\344\271\213\345\220\216\357\274\214\344\270\215\350\276\223\345\205\245\345\210\231\350\257\245\350\247\204\345\210\231\346\234\200\344\274\230\345\205\210\343\200\202", nullptr));
        text2->setText(QApplication::translate("FirewallWidget", "ip\345\234\260\345\235\200\350\276\223\345\205\245\346\240\274\345\274\217\344\270\272\345\233\233\346\256\265\345\274\217\357\274\214mask\344\275\215\350\241\250\347\244\272\344\270\200\344\270\252\347\275\221\346\256\265\347\232\204\346\223\215\344\275\234\357\274\214\345\217\257\344\270\215\345\206\231\343\200\202\350\277\207\346\273\244\346\211\200\346\234\211\347\253\257\345\217\243\346\227\266\345\217\257\344\270\215\345\206\231\346\234\200\345\244\247\346\234\200\345\260\217\345\200\274\343\200\202 nat\346\267\273\345\212\240\351\234\200\350\246\201\345\212\240\344\270\212\346\272\220ip,\345\210\240\351\231\244\351\234\200\350\246\201\345\272\217\345\217\267\343\200\202", nullptr));
        label_2->setText(QApplication::translate("FirewallWidget", "\346\227\245\345\277\227\346\237\245\350\257\242\346\227\266\357\274\214\345\217\257\344\273\245\345\234\250\345\272\217\345\217\267\346\240\217\350\276\223\345\205\245\346\225\260\345\255\227\344\273\243\350\241\250\346\234\200\350\277\221\346\225\260\347\233\256\347\232\204\346\227\245\345\277\227\343\200\202", nullptr));
    } // retranslateUi

};

namespace Ui {
    class FirewallWidget: public Ui_FirewallWidget {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_FIREWALLWIDGET_H
