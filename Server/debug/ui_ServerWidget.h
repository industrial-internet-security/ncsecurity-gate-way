/********************************************************************************
** Form generated from reading UI file 'ServerWidget.ui'
**
** Created by: Qt User Interface Compiler version 5.12.8
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_SERVERWIDGET_H
#define UI_SERVERWIDGET_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QTableView>
#include <QtWidgets/QTextBrowser>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_ServerWidget
{
public:
    QGridLayout *gridLayout_6;
    QStatusBar *statusBar;
    QGridLayout *gridLayout_3;
    QGroupBox *groupBox;
    QGridLayout *gridLayout;
    QTextBrowser *display;
    QGroupBox *groupBox_3;
    QGridLayout *gridLayout_5;
    QLabel *label;
    QLabel *iplabel;
    QLineEdit *ip;
    QLabel *portlabel;
    QLineEdit *port;
    QLabel *gatelable;
    QLineEdit *gatename;
    QPushButton *startbridge;
    QPushButton *cleanbridge;
    QPushButton *stopconnect;
    QCheckBox *debugmode;
    QGroupBox *groupBox_4;
    QGridLayout *gridLayout_4;
    QTableView *tableView;
    QGroupBox *groupBox_5;
    QHBoxLayout *horizontalLayout;
    QPushButton *flash;
    QPushButton *save;
    QLabel *label_5;
    QLineEdit *save_path;
    QGroupBox *groupBox_2;
    QGridLayout *gridLayout_2;
    QLabel *label_4;
    QLabel *label_2;
    QLabel *label_3;

    void setupUi(QWidget *ServerWidget)
    {
        if (ServerWidget->objectName().isEmpty())
            ServerWidget->setObjectName(QString::fromUtf8("ServerWidget"));
        ServerWidget->resize(1199, 801);
        ServerWidget->setStyleSheet(QString::fromUtf8(""));
        gridLayout_6 = new QGridLayout(ServerWidget);
        gridLayout_6->setSpacing(6);
        gridLayout_6->setContentsMargins(11, 11, 11, 11);
        gridLayout_6->setObjectName(QString::fromUtf8("gridLayout_6"));
        statusBar = new QStatusBar(ServerWidget);
        statusBar->setObjectName(QString::fromUtf8("statusBar"));

        gridLayout_6->addWidget(statusBar, 0, 0, 2, 2);

        gridLayout_3 = new QGridLayout();
        gridLayout_3->setSpacing(6);
        gridLayout_3->setObjectName(QString::fromUtf8("gridLayout_3"));
        groupBox = new QGroupBox(ServerWidget);
        groupBox->setObjectName(QString::fromUtf8("groupBox"));
        QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
        sizePolicy.setHorizontalStretch(5);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(groupBox->sizePolicy().hasHeightForWidth());
        groupBox->setSizePolicy(sizePolicy);
        groupBox->setStyleSheet(QString::fromUtf8(""));
        gridLayout = new QGridLayout(groupBox);
        gridLayout->setSpacing(6);
        gridLayout->setContentsMargins(11, 11, 11, 11);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        display = new QTextBrowser(groupBox);
        display->setObjectName(QString::fromUtf8("display"));

        gridLayout->addWidget(display, 0, 0, 1, 1);


        gridLayout_3->addWidget(groupBox, 0, 0, 1, 1);

        groupBox_3 = new QGroupBox(ServerWidget);
        groupBox_3->setObjectName(QString::fromUtf8("groupBox_3"));
        QSizePolicy sizePolicy1(QSizePolicy::Preferred, QSizePolicy::Preferred);
        sizePolicy1.setHorizontalStretch(1);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(groupBox_3->sizePolicy().hasHeightForWidth());
        groupBox_3->setSizePolicy(sizePolicy1);
        gridLayout_5 = new QGridLayout(groupBox_3);
        gridLayout_5->setSpacing(6);
        gridLayout_5->setContentsMargins(11, 11, 11, 11);
        gridLayout_5->setObjectName(QString::fromUtf8("gridLayout_5"));
        label = new QLabel(groupBox_3);
        label->setObjectName(QString::fromUtf8("label"));

        gridLayout_5->addWidget(label, 0, 0, 1, 1);

        iplabel = new QLabel(groupBox_3);
        iplabel->setObjectName(QString::fromUtf8("iplabel"));

        gridLayout_5->addWidget(iplabel, 1, 0, 1, 1);

        ip = new QLineEdit(groupBox_3);
        ip->setObjectName(QString::fromUtf8("ip"));

        gridLayout_5->addWidget(ip, 2, 0, 1, 1);

        portlabel = new QLabel(groupBox_3);
        portlabel->setObjectName(QString::fromUtf8("portlabel"));

        gridLayout_5->addWidget(portlabel, 3, 0, 1, 1);

        port = new QLineEdit(groupBox_3);
        port->setObjectName(QString::fromUtf8("port"));

        gridLayout_5->addWidget(port, 4, 0, 1, 1);

        gatelable = new QLabel(groupBox_3);
        gatelable->setObjectName(QString::fromUtf8("gatelable"));

        gridLayout_5->addWidget(gatelable, 5, 0, 1, 1);

        gatename = new QLineEdit(groupBox_3);
        gatename->setObjectName(QString::fromUtf8("gatename"));

        gridLayout_5->addWidget(gatename, 6, 0, 1, 1);

        startbridge = new QPushButton(groupBox_3);
        startbridge->setObjectName(QString::fromUtf8("startbridge"));

        gridLayout_5->addWidget(startbridge, 7, 0, 1, 1);

        cleanbridge = new QPushButton(groupBox_3);
        cleanbridge->setObjectName(QString::fromUtf8("cleanbridge"));

        gridLayout_5->addWidget(cleanbridge, 8, 0, 1, 1);

        stopconnect = new QPushButton(groupBox_3);
        stopconnect->setObjectName(QString::fromUtf8("stopconnect"));

        gridLayout_5->addWidget(stopconnect, 9, 0, 1, 1);

        debugmode = new QCheckBox(groupBox_3);
        debugmode->setObjectName(QString::fromUtf8("debugmode"));

        gridLayout_5->addWidget(debugmode, 10, 0, 1, 1);


        gridLayout_3->addWidget(groupBox_3, 0, 1, 1, 1);

        groupBox_4 = new QGroupBox(ServerWidget);
        groupBox_4->setObjectName(QString::fromUtf8("groupBox_4"));
        groupBox_4->setStyleSheet(QString::fromUtf8(""));
        gridLayout_4 = new QGridLayout(groupBox_4);
        gridLayout_4->setSpacing(6);
        gridLayout_4->setContentsMargins(11, 11, 11, 11);
        gridLayout_4->setObjectName(QString::fromUtf8("gridLayout_4"));
        tableView = new QTableView(groupBox_4);
        tableView->setObjectName(QString::fromUtf8("tableView"));

        gridLayout_4->addWidget(tableView, 0, 0, 1, 1);


        gridLayout_3->addWidget(groupBox_4, 1, 0, 1, 2);

        groupBox_5 = new QGroupBox(ServerWidget);
        groupBox_5->setObjectName(QString::fromUtf8("groupBox_5"));
        horizontalLayout = new QHBoxLayout(groupBox_5);
        horizontalLayout->setSpacing(6);
        horizontalLayout->setContentsMargins(11, 11, 11, 11);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        flash = new QPushButton(groupBox_5);
        flash->setObjectName(QString::fromUtf8("flash"));

        horizontalLayout->addWidget(flash);

        save = new QPushButton(groupBox_5);
        save->setObjectName(QString::fromUtf8("save"));

        horizontalLayout->addWidget(save);

        label_5 = new QLabel(groupBox_5);
        label_5->setObjectName(QString::fromUtf8("label_5"));

        horizontalLayout->addWidget(label_5);

        save_path = new QLineEdit(groupBox_5);
        save_path->setObjectName(QString::fromUtf8("save_path"));

        horizontalLayout->addWidget(save_path);


        gridLayout_3->addWidget(groupBox_5, 2, 0, 1, 2);

        groupBox_2 = new QGroupBox(ServerWidget);
        groupBox_2->setObjectName(QString::fromUtf8("groupBox_2"));
        groupBox_2->setMaximumSize(QSize(16777215, 112));
        gridLayout_2 = new QGridLayout(groupBox_2);
        gridLayout_2->setSpacing(6);
        gridLayout_2->setContentsMargins(11, 11, 11, 11);
        gridLayout_2->setObjectName(QString::fromUtf8("gridLayout_2"));
        label_4 = new QLabel(groupBox_2);
        label_4->setObjectName(QString::fromUtf8("label_4"));

        gridLayout_2->addWidget(label_4, 3, 0, 1, 1);

        label_2 = new QLabel(groupBox_2);
        label_2->setObjectName(QString::fromUtf8("label_2"));

        gridLayout_2->addWidget(label_2, 1, 0, 1, 1);

        label_3 = new QLabel(groupBox_2);
        label_3->setObjectName(QString::fromUtf8("label_3"));

        gridLayout_2->addWidget(label_3, 2, 0, 1, 1);


        gridLayout_3->addWidget(groupBox_2, 3, 0, 1, 2);


        gridLayout_6->addLayout(gridLayout_3, 1, 1, 1, 1);


        retranslateUi(ServerWidget);

        QMetaObject::connectSlotsByName(ServerWidget);
    } // setupUi

    void retranslateUi(QWidget *ServerWidget)
    {
        ServerWidget->setWindowTitle(QApplication::translate("ServerWidget", "tcp", nullptr));
        groupBox->setTitle(QApplication::translate("ServerWidget", "\344\272\244\344\272\222\346\225\260\346\215\256\346\230\276\347\244\272\347\252\227\345\217\243", nullptr));
        groupBox_3->setTitle(QApplication::translate("ServerWidget", "\347\275\221\346\241\245\346\223\215\344\275\234\346\214\211\351\222\256", nullptr));
        label->setText(QApplication::translate("ServerWidget", "\351\200\232\350\256\257\346\250\241\345\274\217\351\200\211\346\213\251\357\274\232Server", nullptr));
        iplabel->setText(QApplication::translate("ServerWidget", "\346\236\204\345\273\272\347\275\221\346\241\245IP\357\274\232", nullptr));
        portlabel->setText(QApplication::translate("ServerWidget", "\346\236\204\345\273\272\347\275\221\346\241\245\347\253\257\345\217\243\357\274\232", nullptr));
        gatelable->setText(QApplication::translate("ServerWidget", "\347\275\221\346\241\245\345\220\215\347\247\260\357\274\232", nullptr));
        startbridge->setText(QApplication::translate("ServerWidget", "\346\236\204\345\273\272\347\275\221\346\241\245", nullptr));
        cleanbridge->setText(QApplication::translate("ServerWidget", "\346\270\205\347\220\206\347\275\221\346\241\245", nullptr));
        stopconnect->setText(QApplication::translate("ServerWidget", "\346\226\255\345\274\200\351\223\276\346\216\245", nullptr));
        debugmode->setText(QApplication::translate("ServerWidget", "debug\346\250\241\345\274\217", nullptr));
        groupBox_4->setTitle(QApplication::translate("ServerWidget", "\351\223\276\346\216\245\350\257\246\347\273\206\344\277\241\346\201\257", nullptr));
        groupBox_5->setTitle(QApplication::translate("ServerWidget", "\351\223\276\346\216\245\346\223\215\344\275\234\346\214\211\351\222\256", nullptr));
        flash->setText(QApplication::translate("ServerWidget", "\345\210\267\346\226\260\345\275\223\345\211\215\351\223\276\346\216\245\346\225\260\346\215\256", nullptr));
        save->setText(QApplication::translate("ServerWidget", "\344\277\235\345\255\230\351\223\276\346\216\245\346\225\260\346\215\256", nullptr));
        label_5->setText(QApplication::translate("ServerWidget", "\344\277\235\345\255\230\351\223\276\346\216\245\346\225\260\346\215\256\350\267\257\345\276\204\357\274\210\347\233\270\345\257\271\357\274\211\357\274\232", nullptr));
        groupBox_2->setTitle(QApplication::translate("ServerWidget", "\344\275\277\347\224\250\350\257\264\346\230\216", nullptr));
        label_4->setText(QApplication::translate("ServerWidget", "\351\273\230\350\256\244\347\275\221\346\241\245ip\345\234\260\345\235\200\347\253\257\345\217\243\344\270\27210.222.0.1:6000 \351\273\230\350\256\244\347\275\221\346\241\245\345\220\215\347\247\260\344\270\272gatebr0", nullptr));
        label_2->setText(QApplication::translate("ServerWidget", "\347\254\254\344\270\200\346\254\241\344\275\277\347\224\250\346\227\266\357\274\214\351\234\200\350\246\201\346\236\204\345\273\272\347\275\221\346\241\245\357\274\214\344\275\277\347\224\250\347\273\223\346\235\237\345\220\216\357\274\214\347\202\271\345\207\273\346\226\255\345\274\200\351\223\276\346\216\245\343\200\202", nullptr));
        label_3->setText(QApplication::translate("ServerWidget", "\346\226\255\345\274\200\351\223\276\346\216\245\345\220\216\357\274\214\351\234\200\350\246\201\346\270\205\347\220\206\347\275\221\346\241\245\343\200\202\347\233\264\345\210\260\345\206\215\346\254\241\344\275\277\347\224\250\346\227\266\351\207\215\346\226\260\346\236\204\345\273\272\347\275\221\346\241\245\343\200\202", nullptr));
    } // retranslateUi

};

namespace Ui {
    class ServerWidget: public Ui_ServerWidget {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_SERVERWIDGET_H
