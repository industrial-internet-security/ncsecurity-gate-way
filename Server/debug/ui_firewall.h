/********************************************************************************
** Form generated from reading UI file 'firewall.ui'
**
** Created by: Qt User Interface Compiler version 5.12.8
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_FIREWALL_H
#define UI_FIREWALL_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QTableView>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_Firewall
{
public:
    QGridLayout *gridLayout_4;
    QGridLayout *gridLayout;
    QGroupBox *groupBox_2;
    QGridLayout *gridLayout_3;
    QPushButton *pushButton_6;
    QPushButton *pushButton_9;
    QPushButton *pushButton_4;
    QPushButton *pushButton_10;
    QPushButton *pushButton_11;
    QPushButton *pushButton_12;
    QPushButton *pushButton_5;
    QPushButton *pushButton_7;
    QPushButton *pushButton_8;
    QPushButton *pushButton_13;
    QGroupBox *groupBox_3;
    QLineEdit *lineEdit;
    QLabel *label;
    QLabel *label_2;
    QGroupBox *groupBox;
    QGridLayout *gridLayout_2;
    QTableView *tableView;

    void setupUi(QWidget *Firewall)
    {
        if (Firewall->objectName().isEmpty())
            Firewall->setObjectName(QString::fromUtf8("Firewall"));
        Firewall->resize(693, 757);
        gridLayout_4 = new QGridLayout(Firewall);
        gridLayout_4->setObjectName(QString::fromUtf8("gridLayout_4"));
        gridLayout = new QGridLayout();
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        groupBox_2 = new QGroupBox(Firewall);
        groupBox_2->setObjectName(QString::fromUtf8("groupBox_2"));
        gridLayout_3 = new QGridLayout(groupBox_2);
        gridLayout_3->setObjectName(QString::fromUtf8("gridLayout_3"));
        pushButton_6 = new QPushButton(groupBox_2);
        pushButton_6->setObjectName(QString::fromUtf8("pushButton_6"));

        gridLayout_3->addWidget(pushButton_6, 0, 1, 1, 1);

        pushButton_9 = new QPushButton(groupBox_2);
        pushButton_9->setObjectName(QString::fromUtf8("pushButton_9"));

        gridLayout_3->addWidget(pushButton_9, 1, 0, 1, 1);

        pushButton_4 = new QPushButton(groupBox_2);
        pushButton_4->setObjectName(QString::fromUtf8("pushButton_4"));

        gridLayout_3->addWidget(pushButton_4, 3, 0, 1, 1);

        pushButton_10 = new QPushButton(groupBox_2);
        pushButton_10->setObjectName(QString::fromUtf8("pushButton_10"));

        gridLayout_3->addWidget(pushButton_10, 0, 0, 1, 1);

        pushButton_11 = new QPushButton(groupBox_2);
        pushButton_11->setObjectName(QString::fromUtf8("pushButton_11"));

        gridLayout_3->addWidget(pushButton_11, 2, 1, 1, 1);

        pushButton_12 = new QPushButton(groupBox_2);
        pushButton_12->setObjectName(QString::fromUtf8("pushButton_12"));

        gridLayout_3->addWidget(pushButton_12, 3, 1, 1, 1);

        pushButton_5 = new QPushButton(groupBox_2);
        pushButton_5->setObjectName(QString::fromUtf8("pushButton_5"));

        gridLayout_3->addWidget(pushButton_5, 4, 0, 1, 1);

        pushButton_7 = new QPushButton(groupBox_2);
        pushButton_7->setObjectName(QString::fromUtf8("pushButton_7"));

        gridLayout_3->addWidget(pushButton_7, 1, 1, 1, 1);

        pushButton_8 = new QPushButton(groupBox_2);
        pushButton_8->setObjectName(QString::fromUtf8("pushButton_8"));

        gridLayout_3->addWidget(pushButton_8, 2, 0, 1, 1);

        pushButton_13 = new QPushButton(groupBox_2);
        pushButton_13->setObjectName(QString::fromUtf8("pushButton_13"));

        gridLayout_3->addWidget(pushButton_13, 4, 1, 1, 1);


        gridLayout->addWidget(groupBox_2, 1, 0, 1, 1);

        groupBox_3 = new QGroupBox(Firewall);
        groupBox_3->setObjectName(QString::fromUtf8("groupBox_3"));
        lineEdit = new QLineEdit(groupBox_3);
        lineEdit->setObjectName(QString::fromUtf8("lineEdit"));
        lineEdit->setGeometry(QRect(100, 40, 113, 25));
        label = new QLabel(groupBox_3);
        label->setObjectName(QString::fromUtf8("label"));
        label->setGeometry(QRect(10, 40, 67, 17));
        label_2 = new QLabel(groupBox_3);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        label_2->setGeometry(QRect(10, 80, 311, 17));

        gridLayout->addWidget(groupBox_3, 1, 1, 1, 1);

        groupBox = new QGroupBox(Firewall);
        groupBox->setObjectName(QString::fromUtf8("groupBox"));
        gridLayout_2 = new QGridLayout(groupBox);
        gridLayout_2->setObjectName(QString::fromUtf8("gridLayout_2"));
        tableView = new QTableView(groupBox);
        tableView->setObjectName(QString::fromUtf8("tableView"));

        gridLayout_2->addWidget(tableView, 0, 0, 1, 1);


        gridLayout->addWidget(groupBox, 0, 0, 1, 2);


        gridLayout_4->addLayout(gridLayout, 0, 0, 1, 1);


        retranslateUi(Firewall);

        QMetaObject::connectSlotsByName(Firewall);
    } // setupUi

    void retranslateUi(QWidget *Firewall)
    {
        Firewall->setWindowTitle(QApplication::translate("Firewall", "Form", nullptr));
        groupBox_2->setTitle(QApplication::translate("Firewall", "\346\223\215\344\275\234\346\240\217", nullptr));
        pushButton_6->setText(QApplication::translate("Firewall", "\346\237\245\347\234\213\345\275\223\345\211\215\351\223\276\346\216\245", nullptr));
        pushButton_9->setText(QApplication::translate("Firewall", "\345\210\240\351\231\244\345\270\270\350\247\204\350\247\204\345\210\231", nullptr));
        pushButton_4->setText(QApplication::translate("Firewall", "\351\273\230\350\256\244\346\213\222\347\273\235", nullptr));
        pushButton_10->setText(QApplication::translate("Firewall", "\346\237\245\347\234\213\345\270\270\350\247\204\350\247\204\345\210\231", nullptr));
        pushButton_11->setText(QApplication::translate("Firewall", "\346\267\273\345\212\240nat\350\247\204\345\210\231", nullptr));
        pushButton_12->setText(QApplication::translate("Firewall", "\345\210\240\351\231\244nat\350\247\204\345\210\231", nullptr));
        pushButton_5->setText(QApplication::translate("Firewall", "\346\237\245\347\234\213\346\227\245\345\277\227", nullptr));
        pushButton_7->setText(QApplication::translate("Firewall", "\346\237\245\347\234\213nat\350\247\204\345\210\231", nullptr));
        pushButton_8->setText(QApplication::translate("Firewall", "\346\267\273\345\212\240\345\270\270\350\247\204\350\247\204\345\210\231", nullptr));
        pushButton_13->setText(QApplication::translate("Firewall", "\351\273\230\350\256\244\345\205\201\350\256\270", nullptr));
        groupBox_3->setTitle(QApplication::translate("Firewall", "\346\225\260\346\215\256\346\240\217", nullptr));
        label->setText(QApplication::translate("Firewall", "\350\247\204\345\210\231\345\220\215\347\247\260\357\274\232", nullptr));
        label_2->setText(QApplication::translate("Firewall", "\350\247\204\345\210\231\351\241\272\345\272\217\357\274\210\345\234\250\350\276\223\345\205\245\350\247\204\345\210\231\345\220\215\347\247\260\344\271\213\345\220\216\357\274\214\351\273\230\350\256\244\344\270\272\346\234\200\345\205\210\357\274\211\357\274\232", nullptr));
        groupBox->setTitle(QApplication::translate("Firewall", "\346\230\276\347\244\272\345\267\262\347\273\217\351\223\276\346\216\245\347\232\204\350\247\204\345\210\231\346\210\226\350\200\205\346\227\245\345\277\227", nullptr));
    } // retranslateUi

};

namespace Ui {
    class Firewall: public Ui_Firewall {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_FIREWALL_H
