/********************************************************************************
** Form generated from reading UI file 'TcpWidget.ui'
**
** Created by: Qt User Interface Compiler version 5.12.8
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_TCPWIDGET_H
#define UI_TCPWIDGET_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QTextBrowser>
#include <QtWidgets/QTextEdit>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_TcpWidget
{
public:
    QStatusBar *statusBar;
    QGridLayout *gridLayout_4;
    QGridLayout *gridLayout_3;
    QGroupBox *groupBox;
    QGridLayout *gridLayout;
    QTextBrowser *display;
    QGroupBox *groupBox_3;
    QVBoxLayout *verticalLayout;
    QLabel *label;
    QComboBox *model;
    QLabel *iplabel;
    QComboBox *iplist;
    QLineEdit *ip;
    QLabel *portlabel;
    QLineEdit *port;
    QPushButton *startconnect;
    QPushButton *stopconnect;
    QCheckBox *debugmode;
    QCheckBox *hexsend;
    QCheckBox *autosend;
    QCheckBox *autoconnect;
    QComboBox *autosendtime;
    QComboBox *autoconnecttime;
    QComboBox *allconnect;
    QSpacerItem *verticalSpacer;
    QPushButton *sendcount;
    QPushButton *recvcount;
    QPushButton *savedata;
    QGroupBox *groupBox_2;
    QGridLayout *gridLayout_2;
    QPushButton *sendbtn;
    QTextEdit *senddata;

    void setupUi(QWidget *TcpWidget)
    {
        if (TcpWidget->objectName().isEmpty())
            TcpWidget->setObjectName(QString::fromUtf8("TcpWidget"));
        TcpWidget->resize(662, 617);
        TcpWidget->setStyleSheet(QString::fromUtf8(""));
        statusBar = new QStatusBar(TcpWidget);
        statusBar->setObjectName(QString::fromUtf8("statusBar"));
        statusBar->setGeometry(QRect(0, 0, 3, 18));
        gridLayout_4 = new QGridLayout(TcpWidget);
        gridLayout_4->setSpacing(6);
        gridLayout_4->setContentsMargins(11, 11, 11, 11);
        gridLayout_4->setObjectName(QString::fromUtf8("gridLayout_4"));
        gridLayout_3 = new QGridLayout();
        gridLayout_3->setSpacing(6);
        gridLayout_3->setObjectName(QString::fromUtf8("gridLayout_3"));
        groupBox = new QGroupBox(TcpWidget);
        groupBox->setObjectName(QString::fromUtf8("groupBox"));
        groupBox->setStyleSheet(QString::fromUtf8(""));
        gridLayout = new QGridLayout(groupBox);
        gridLayout->setSpacing(6);
        gridLayout->setContentsMargins(11, 11, 11, 11);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        display = new QTextBrowser(groupBox);
        display->setObjectName(QString::fromUtf8("display"));

        gridLayout->addWidget(display, 0, 0, 1, 1);


        gridLayout_3->addWidget(groupBox, 0, 0, 1, 1);

        groupBox_3 = new QGroupBox(TcpWidget);
        groupBox_3->setObjectName(QString::fromUtf8("groupBox_3"));
        groupBox_3->setMaximumSize(QSize(200, 16777215));
        verticalLayout = new QVBoxLayout(groupBox_3);
        verticalLayout->setSpacing(6);
        verticalLayout->setContentsMargins(11, 11, 11, 11);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        label = new QLabel(groupBox_3);
        label->setObjectName(QString::fromUtf8("label"));

        verticalLayout->addWidget(label);

        model = new QComboBox(groupBox_3);
        model->setObjectName(QString::fromUtf8("model"));

        verticalLayout->addWidget(model);

        iplabel = new QLabel(groupBox_3);
        iplabel->setObjectName(QString::fromUtf8("iplabel"));

        verticalLayout->addWidget(iplabel);

        iplist = new QComboBox(groupBox_3);
        iplist->setObjectName(QString::fromUtf8("iplist"));

        verticalLayout->addWidget(iplist);

        ip = new QLineEdit(groupBox_3);
        ip->setObjectName(QString::fromUtf8("ip"));

        verticalLayout->addWidget(ip);

        portlabel = new QLabel(groupBox_3);
        portlabel->setObjectName(QString::fromUtf8("portlabel"));

        verticalLayout->addWidget(portlabel);

        port = new QLineEdit(groupBox_3);
        port->setObjectName(QString::fromUtf8("port"));

        verticalLayout->addWidget(port);

        startconnect = new QPushButton(groupBox_3);
        startconnect->setObjectName(QString::fromUtf8("startconnect"));

        verticalLayout->addWidget(startconnect);

        stopconnect = new QPushButton(groupBox_3);
        stopconnect->setObjectName(QString::fromUtf8("stopconnect"));

        verticalLayout->addWidget(stopconnect);

        debugmode = new QCheckBox(groupBox_3);
        debugmode->setObjectName(QString::fromUtf8("debugmode"));

        verticalLayout->addWidget(debugmode);

        hexsend = new QCheckBox(groupBox_3);
        hexsend->setObjectName(QString::fromUtf8("hexsend"));

        verticalLayout->addWidget(hexsend);

        autosend = new QCheckBox(groupBox_3);
        autosend->setObjectName(QString::fromUtf8("autosend"));

        verticalLayout->addWidget(autosend);

        autoconnect = new QCheckBox(groupBox_3);
        autoconnect->setObjectName(QString::fromUtf8("autoconnect"));

        verticalLayout->addWidget(autoconnect);

        autosendtime = new QComboBox(groupBox_3);
        autosendtime->setObjectName(QString::fromUtf8("autosendtime"));

        verticalLayout->addWidget(autosendtime);

        autoconnecttime = new QComboBox(groupBox_3);
        autoconnecttime->setObjectName(QString::fromUtf8("autoconnecttime"));

        verticalLayout->addWidget(autoconnecttime);

        allconnect = new QComboBox(groupBox_3);
        allconnect->setObjectName(QString::fromUtf8("allconnect"));

        verticalLayout->addWidget(allconnect);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout->addItem(verticalSpacer);

        sendcount = new QPushButton(groupBox_3);
        sendcount->setObjectName(QString::fromUtf8("sendcount"));

        verticalLayout->addWidget(sendcount);

        recvcount = new QPushButton(groupBox_3);
        recvcount->setObjectName(QString::fromUtf8("recvcount"));

        verticalLayout->addWidget(recvcount);

        savedata = new QPushButton(groupBox_3);
        savedata->setObjectName(QString::fromUtf8("savedata"));

        verticalLayout->addWidget(savedata);


        gridLayout_3->addWidget(groupBox_3, 0, 1, 2, 1);

        groupBox_2 = new QGroupBox(TcpWidget);
        groupBox_2->setObjectName(QString::fromUtf8("groupBox_2"));
        groupBox_2->setMaximumSize(QSize(16777215, 112));
        gridLayout_2 = new QGridLayout(groupBox_2);
        gridLayout_2->setSpacing(6);
        gridLayout_2->setContentsMargins(11, 11, 11, 11);
        gridLayout_2->setObjectName(QString::fromUtf8("gridLayout_2"));
        sendbtn = new QPushButton(groupBox_2);
        sendbtn->setObjectName(QString::fromUtf8("sendbtn"));
        sendbtn->setMinimumSize(QSize(80, 80));
        sendbtn->setMaximumSize(QSize(16777215, 80));

        gridLayout_2->addWidget(sendbtn, 0, 1, 1, 1);

        senddata = new QTextEdit(groupBox_2);
        senddata->setObjectName(QString::fromUtf8("senddata"));
        senddata->setMinimumSize(QSize(0, 80));
        senddata->setMaximumSize(QSize(16777215, 80));

        gridLayout_2->addWidget(senddata, 0, 0, 1, 1);


        gridLayout_3->addWidget(groupBox_2, 1, 0, 1, 1);


        gridLayout_4->addLayout(gridLayout_3, 0, 0, 1, 1);


        retranslateUi(TcpWidget);

        QMetaObject::connectSlotsByName(TcpWidget);
    } // setupUi

    void retranslateUi(QWidget *TcpWidget)
    {
        TcpWidget->setWindowTitle(QApplication::translate("TcpWidget", "tcp", nullptr));
        groupBox->setTitle(QApplication::translate("TcpWidget", "\346\225\260\346\215\256\346\230\276\347\244\272\347\252\227\345\217\243", nullptr));
        groupBox_3->setTitle(QApplication::translate("TcpWidget", "\347\275\221\347\273\234\350\256\276\347\275\256\347\252\227\345\217\243", nullptr));
        label->setText(QApplication::translate("TcpWidget", "\351\200\232\350\256\257\346\250\241\345\274\217\351\200\211\346\213\251\357\274\232", nullptr));
        iplabel->setText(QApplication::translate("TcpWidget", "\344\270\273\346\234\272IP\357\274\232", nullptr));
        portlabel->setText(QApplication::translate("TcpWidget", "\346\234\254\345\234\260\347\253\257\345\217\243\357\274\232", nullptr));
        startconnect->setText(QApplication::translate("TcpWidget", "\345\274\200\345\247\213\350\277\236\346\216\245", nullptr));
        stopconnect->setText(QApplication::translate("TcpWidget", "\346\226\255\345\274\200\350\277\236\346\216\245", nullptr));
        debugmode->setText(QApplication::translate("TcpWidget", "debug\346\250\241\345\274\217", nullptr));
        hexsend->setText(QApplication::translate("TcpWidget", "HEX\345\217\221\351\200\201", nullptr));
        autosend->setText(QApplication::translate("TcpWidget", "\350\207\252\345\212\250\345\217\221\351\200\201\357\274\210\346\257\253\347\247\222\357\274\211", nullptr));
        autoconnect->setText(QApplication::translate("TcpWidget", "\350\207\252\345\212\250\351\207\215\350\277\236\357\274\210\346\257\253\347\247\222\357\274\211", nullptr));
        sendcount->setText(QApplication::translate("TcpWidget", "\345\217\221\351\200\201\357\274\2320\345\255\227\350\212\202", nullptr));
        recvcount->setText(QApplication::translate("TcpWidget", "\346\216\245\346\224\266\357\274\2320\345\255\227\350\212\202", nullptr));
        savedata->setText(QApplication::translate("TcpWidget", "\344\277\235\345\255\230\346\225\260\346\215\256", nullptr));
        groupBox_2->setTitle(QApplication::translate("TcpWidget", "\346\225\260\346\215\256\345\217\221\351\200\201\347\252\227\345\217\243", nullptr));
        sendbtn->setText(QApplication::translate("TcpWidget", "\345\217\221\351\200\201", nullptr));
    } // retranslateUi

};

namespace Ui {
    class TcpWidget: public Ui_TcpWidget {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_TCPWIDGET_H
