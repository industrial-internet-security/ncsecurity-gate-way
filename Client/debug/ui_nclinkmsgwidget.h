/********************************************************************************
** Form generated from reading UI file 'nclinkmsgwidget.ui'
**
** Created by: Qt User Interface Compiler version 5.12.8
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_NCLINKMSGWIDGET_H
#define UI_NCLINKMSGWIDGET_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QTableView>
#include <QtWidgets/QTextBrowser>
#include <QtWidgets/QTextEdit>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_nclinkmsgwidget
{
public:
    QWidget *layoutWidget;
    QGridLayout *gridLayout_4;
    QGroupBox *groupBox;
    QGridLayout *gridLayout;
    QTextBrowser *msg_table;
    QGroupBox *groupBox_2;
    QGridLayout *gridLayout_2;
    QTableView *list_connect;
    QGroupBox *groupBox_3;
    QGridLayout *gridLayout_3;
    QLabel *label_3;
    QLineEdit *choose_ip;
    QPushButton *start_client;
    QLabel *label;
    QLineEdit *choose_port;
    QPushButton *stop_client;
    QLabel *label_2;
    QPushButton *send_msg;
    QTextEdit *send_msg_text;

    void setupUi(QWidget *nclinkmsgwidget)
    {
        if (nclinkmsgwidget->objectName().isEmpty())
            nclinkmsgwidget->setObjectName(QString::fromUtf8("nclinkmsgwidget"));
        nclinkmsgwidget->resize(1448, 902);
        layoutWidget = new QWidget(nclinkmsgwidget);
        layoutWidget->setObjectName(QString::fromUtf8("layoutWidget"));
        layoutWidget->setGeometry(QRect(10, 10, 681, 691));
        gridLayout_4 = new QGridLayout(layoutWidget);
        gridLayout_4->setObjectName(QString::fromUtf8("gridLayout_4"));
        gridLayout_4->setContentsMargins(0, 0, 0, 0);
        groupBox = new QGroupBox(layoutWidget);
        groupBox->setObjectName(QString::fromUtf8("groupBox"));
        gridLayout = new QGridLayout(groupBox);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        msg_table = new QTextBrowser(groupBox);
        msg_table->setObjectName(QString::fromUtf8("msg_table"));

        gridLayout->addWidget(msg_table, 0, 0, 1, 1);


        gridLayout_4->addWidget(groupBox, 0, 0, 1, 1);

        groupBox_2 = new QGroupBox(layoutWidget);
        groupBox_2->setObjectName(QString::fromUtf8("groupBox_2"));
        gridLayout_2 = new QGridLayout(groupBox_2);
        gridLayout_2->setObjectName(QString::fromUtf8("gridLayout_2"));
        list_connect = new QTableView(groupBox_2);
        list_connect->setObjectName(QString::fromUtf8("list_connect"));

        gridLayout_2->addWidget(list_connect, 0, 0, 1, 1);


        gridLayout_4->addWidget(groupBox_2, 0, 1, 1, 1);

        groupBox_3 = new QGroupBox(layoutWidget);
        groupBox_3->setObjectName(QString::fromUtf8("groupBox_3"));
        gridLayout_3 = new QGridLayout(groupBox_3);
        gridLayout_3->setObjectName(QString::fromUtf8("gridLayout_3"));
        label_3 = new QLabel(groupBox_3);
        label_3->setObjectName(QString::fromUtf8("label_3"));

        gridLayout_3->addWidget(label_3, 0, 0, 1, 2);

        choose_ip = new QLineEdit(groupBox_3);
        choose_ip->setObjectName(QString::fromUtf8("choose_ip"));
        QSizePolicy sizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(choose_ip->sizePolicy().hasHeightForWidth());
        choose_ip->setSizePolicy(sizePolicy);

        gridLayout_3->addWidget(choose_ip, 0, 2, 1, 1);

        start_client = new QPushButton(groupBox_3);
        start_client->setObjectName(QString::fromUtf8("start_client"));
        QSizePolicy sizePolicy1(QSizePolicy::Minimum, QSizePolicy::Fixed);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(start_client->sizePolicy().hasHeightForWidth());
        start_client->setSizePolicy(sizePolicy1);

        gridLayout_3->addWidget(start_client, 0, 3, 1, 1);

        label = new QLabel(groupBox_3);
        label->setObjectName(QString::fromUtf8("label"));

        gridLayout_3->addWidget(label, 1, 0, 1, 2);

        choose_port = new QLineEdit(groupBox_3);
        choose_port->setObjectName(QString::fromUtf8("choose_port"));
        sizePolicy.setHeightForWidth(choose_port->sizePolicy().hasHeightForWidth());
        choose_port->setSizePolicy(sizePolicy);

        gridLayout_3->addWidget(choose_port, 1, 2, 1, 1);

        stop_client = new QPushButton(groupBox_3);
        stop_client->setObjectName(QString::fromUtf8("stop_client"));
        sizePolicy1.setHeightForWidth(stop_client->sizePolicy().hasHeightForWidth());
        stop_client->setSizePolicy(sizePolicy1);

        gridLayout_3->addWidget(stop_client, 1, 3, 1, 1);

        label_2 = new QLabel(groupBox_3);
        label_2->setObjectName(QString::fromUtf8("label_2"));

        gridLayout_3->addWidget(label_2, 2, 0, 1, 1);

        send_msg = new QPushButton(groupBox_3);
        send_msg->setObjectName(QString::fromUtf8("send_msg"));
        sizePolicy1.setHeightForWidth(send_msg->sizePolicy().hasHeightForWidth());
        send_msg->setSizePolicy(sizePolicy1);

        gridLayout_3->addWidget(send_msg, 3, 3, 1, 1);

        send_msg_text = new QTextEdit(groupBox_3);
        send_msg_text->setObjectName(QString::fromUtf8("send_msg_text"));
        QSizePolicy sizePolicy2(QSizePolicy::Expanding, QSizePolicy::Expanding);
        sizePolicy2.setHorizontalStretch(1);
        sizePolicy2.setVerticalStretch(0);
        sizePolicy2.setHeightForWidth(send_msg_text->sizePolicy().hasHeightForWidth());
        send_msg_text->setSizePolicy(sizePolicy2);

        gridLayout_3->addWidget(send_msg_text, 2, 1, 2, 2);


        gridLayout_4->addWidget(groupBox_3, 1, 0, 1, 2);


        retranslateUi(nclinkmsgwidget);

        QMetaObject::connectSlotsByName(nclinkmsgwidget);
    } // setupUi

    void retranslateUi(QWidget *nclinkmsgwidget)
    {
        nclinkmsgwidget->setWindowTitle(QApplication::translate("nclinkmsgwidget", "Form", nullptr));
        groupBox->setTitle(QApplication::translate("nclinkmsgwidget", "\344\277\241\346\201\257\344\272\244\344\272\222\347\252\227\345\217\243", nullptr));
        groupBox_2->setTitle(QApplication::translate("nclinkmsgwidget", "\351\200\232\344\277\241\345\257\271\350\261\241\345\210\227\350\241\250", nullptr));
        groupBox_3->setTitle(QApplication::translate("nclinkmsgwidget", "\346\200\273\346\216\247\345\210\266\351\203\250\345\210\206", nullptr));
        label_3->setText(QApplication::translate("nclinkmsgwidget", "\351\200\211\346\213\251ip\357\274\210\351\273\230\350\256\244\344\270\272\347\275\221\346\241\245\347\232\204ip\357\274\211\357\274\232", nullptr));
        start_client->setText(QApplication::translate("nclinkmsgwidget", "\345\220\257\345\212\250\345\256\242\346\210\267\347\253\257", nullptr));
        label->setText(QApplication::translate("nclinkmsgwidget", "\351\200\211\346\213\251\347\253\257\345\217\243\357\274\210\346\263\250\346\204\217\344\270\215\350\246\201\344\270\216\347\275\221\346\241\245\347\253\257\345\217\243\347\233\270\345\220\214\357\274\211\357\274\232", nullptr));
        stop_client->setText(QApplication::translate("nclinkmsgwidget", "\345\205\263\351\227\255\345\256\242\346\210\267\347\253\257", nullptr));
        label_2->setText(QApplication::translate("nclinkmsgwidget", "\345\217\221\351\200\201\346\266\210\346\201\257\357\274\232", nullptr));
        send_msg->setText(QApplication::translate("nclinkmsgwidget", "\345\217\221\351\200\201\346\266\210\346\201\257", nullptr));
    } // retranslateUi

};

namespace Ui {
    class nclinkmsgwidget: public Ui_nclinkmsgwidget {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_NCLINKMSGWIDGET_H
