/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.12.8
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QListWidget>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QStackedWidget>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralwidget;
    QGridLayout *gridLayout;
    QWidget *titlewidget;
    QHBoxLayout *horizontalLayout;
    QLabel *labelicon;
    QLabel *label;
    QSpacerItem *horizontalSpacer_2;
    QPushButton *hidebtn;
    QPushButton *maxbtn;
    QPushButton *minbtn;
    QPushButton *closebtn;
    QListWidget *listWidget;
    QStackedWidget *stackedWidget;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QString::fromUtf8("MainWindow"));
        MainWindow->resize(690, 598);
        centralwidget = new QWidget(MainWindow);
        centralwidget->setObjectName(QString::fromUtf8("centralwidget"));
        gridLayout = new QGridLayout(centralwidget);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        titlewidget = new QWidget(centralwidget);
        titlewidget->setObjectName(QString::fromUtf8("titlewidget"));
        horizontalLayout = new QHBoxLayout(titlewidget);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        labelicon = new QLabel(titlewidget);
        labelicon->setObjectName(QString::fromUtf8("labelicon"));

        horizontalLayout->addWidget(labelicon);

        label = new QLabel(titlewidget);
        label->setObjectName(QString::fromUtf8("label"));

        horizontalLayout->addWidget(label);

        horizontalSpacer_2 = new QSpacerItem(277, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer_2);

        hidebtn = new QPushButton(titlewidget);
        hidebtn->setObjectName(QString::fromUtf8("hidebtn"));

        horizontalLayout->addWidget(hidebtn);

        maxbtn = new QPushButton(titlewidget);
        maxbtn->setObjectName(QString::fromUtf8("maxbtn"));

        horizontalLayout->addWidget(maxbtn);

        minbtn = new QPushButton(titlewidget);
        minbtn->setObjectName(QString::fromUtf8("minbtn"));

        horizontalLayout->addWidget(minbtn);

        closebtn = new QPushButton(titlewidget);
        closebtn->setObjectName(QString::fromUtf8("closebtn"));

        horizontalLayout->addWidget(closebtn);


        gridLayout->addWidget(titlewidget, 0, 0, 1, 2);

        listWidget = new QListWidget(centralwidget);
        listWidget->setObjectName(QString::fromUtf8("listWidget"));
        listWidget->setMaximumSize(QSize(230, 16777215));

        gridLayout->addWidget(listWidget, 1, 0, 2, 1);

        stackedWidget = new QStackedWidget(centralwidget);
        stackedWidget->setObjectName(QString::fromUtf8("stackedWidget"));

        gridLayout->addWidget(stackedWidget, 1, 1, 1, 1);

        MainWindow->setCentralWidget(centralwidget);

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "MainWindow", nullptr));
        labelicon->setText(QString());
        label->setText(QApplication::translate("MainWindow", "<html><head/><body><p align=\"center\"><br/></p></body></html>", nullptr));
        hidebtn->setText(QString());
        maxbtn->setText(QString());
        minbtn->setText(QString());
        closebtn->setText(QString());
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
