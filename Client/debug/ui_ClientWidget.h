/********************************************************************************
** Form generated from reading UI file 'ClientWidget.ui'
**
** Created by: Qt User Interface Compiler version 5.12.8
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_CLIENTWIDGET_H
#define UI_CLIENTWIDGET_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QTextBrowser>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_ClientWidget
{
public:
    QStatusBar *statusBar;
    QGridLayout *gridLayout_4;
    QGridLayout *gridLayout_3;
    QGroupBox *groupBox;
    QGridLayout *gridLayout;
    QTextBrowser *display;
    QGroupBox *groupBox_3;
    QVBoxLayout *verticalLayout;
    QLabel *label;
    QLabel *iplabel;
    QLineEdit *ip;
    QLabel *portlabel;
    QLineEdit *port;
    QPushButton *startconnect;
    QPushButton *stopconnect;
    QPushButton *resetconnect;
    QCheckBox *debugmode;
    QGroupBox *groupBox_2;
    QGridLayout *gridLayout_2;
    QLabel *label_3;
    QLabel *label_4;
    QLabel *label_2;

    void setupUi(QWidget *ClientWidget)
    {
        if (ClientWidget->objectName().isEmpty())
            ClientWidget->setObjectName(QString::fromUtf8("ClientWidget"));
        ClientWidget->resize(667, 693);
        ClientWidget->setStyleSheet(QString::fromUtf8(""));
        statusBar = new QStatusBar(ClientWidget);
        statusBar->setObjectName(QString::fromUtf8("statusBar"));
        statusBar->setGeometry(QRect(0, 0, 3, 18));
        gridLayout_4 = new QGridLayout(ClientWidget);
        gridLayout_4->setSpacing(6);
        gridLayout_4->setContentsMargins(11, 11, 11, 11);
        gridLayout_4->setObjectName(QString::fromUtf8("gridLayout_4"));
        gridLayout_3 = new QGridLayout();
        gridLayout_3->setSpacing(6);
        gridLayout_3->setObjectName(QString::fromUtf8("gridLayout_3"));
        groupBox = new QGroupBox(ClientWidget);
        groupBox->setObjectName(QString::fromUtf8("groupBox"));
        groupBox->setStyleSheet(QString::fromUtf8(""));
        gridLayout = new QGridLayout(groupBox);
        gridLayout->setSpacing(6);
        gridLayout->setContentsMargins(11, 11, 11, 11);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        display = new QTextBrowser(groupBox);
        display->setObjectName(QString::fromUtf8("display"));

        gridLayout->addWidget(display, 0, 0, 1, 1);


        gridLayout_3->addWidget(groupBox, 0, 0, 1, 1);

        groupBox_3 = new QGroupBox(ClientWidget);
        groupBox_3->setObjectName(QString::fromUtf8("groupBox_3"));
        groupBox_3->setMaximumSize(QSize(200, 16777215));
        verticalLayout = new QVBoxLayout(groupBox_3);
        verticalLayout->setSpacing(6);
        verticalLayout->setContentsMargins(11, 11, 11, 11);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        label = new QLabel(groupBox_3);
        label->setObjectName(QString::fromUtf8("label"));

        verticalLayout->addWidget(label);

        iplabel = new QLabel(groupBox_3);
        iplabel->setObjectName(QString::fromUtf8("iplabel"));

        verticalLayout->addWidget(iplabel);

        ip = new QLineEdit(groupBox_3);
        ip->setObjectName(QString::fromUtf8("ip"));

        verticalLayout->addWidget(ip);

        portlabel = new QLabel(groupBox_3);
        portlabel->setObjectName(QString::fromUtf8("portlabel"));

        verticalLayout->addWidget(portlabel);

        port = new QLineEdit(groupBox_3);
        port->setObjectName(QString::fromUtf8("port"));

        verticalLayout->addWidget(port);

        startconnect = new QPushButton(groupBox_3);
        startconnect->setObjectName(QString::fromUtf8("startconnect"));

        verticalLayout->addWidget(startconnect);

        stopconnect = new QPushButton(groupBox_3);
        stopconnect->setObjectName(QString::fromUtf8("stopconnect"));

        verticalLayout->addWidget(stopconnect);

        resetconnect = new QPushButton(groupBox_3);
        resetconnect->setObjectName(QString::fromUtf8("resetconnect"));

        verticalLayout->addWidget(resetconnect);

        debugmode = new QCheckBox(groupBox_3);
        debugmode->setObjectName(QString::fromUtf8("debugmode"));

        verticalLayout->addWidget(debugmode);


        gridLayout_3->addWidget(groupBox_3, 0, 1, 2, 1);

        groupBox_2 = new QGroupBox(ClientWidget);
        groupBox_2->setObjectName(QString::fromUtf8("groupBox_2"));
        groupBox_2->setMaximumSize(QSize(16777215, 112));
        gridLayout_2 = new QGridLayout(groupBox_2);
        gridLayout_2->setSpacing(6);
        gridLayout_2->setContentsMargins(11, 11, 11, 11);
        gridLayout_2->setObjectName(QString::fromUtf8("gridLayout_2"));
        label_3 = new QLabel(groupBox_2);
        label_3->setObjectName(QString::fromUtf8("label_3"));

        gridLayout_2->addWidget(label_3, 1, 0, 1, 1);

        label_4 = new QLabel(groupBox_2);
        label_4->setObjectName(QString::fromUtf8("label_4"));

        gridLayout_2->addWidget(label_4, 2, 0, 1, 1);

        label_2 = new QLabel(groupBox_2);
        label_2->setObjectName(QString::fromUtf8("label_2"));

        gridLayout_2->addWidget(label_2, 0, 0, 1, 1);


        gridLayout_3->addWidget(groupBox_2, 1, 0, 1, 1);


        gridLayout_4->addLayout(gridLayout_3, 0, 0, 1, 1);


        retranslateUi(ClientWidget);

        QMetaObject::connectSlotsByName(ClientWidget);
    } // setupUi

    void retranslateUi(QWidget *ClientWidget)
    {
        ClientWidget->setWindowTitle(QApplication::translate("ClientWidget", "tcp", nullptr));
        groupBox->setTitle(QApplication::translate("ClientWidget", "\346\225\260\346\215\256\346\230\276\347\244\272\347\252\227\345\217\243", nullptr));
        groupBox_3->setTitle(QApplication::translate("ClientWidget", "\347\275\221\347\273\234\350\256\276\347\275\256\347\252\227\345\217\243", nullptr));
        label->setText(QApplication::translate("ClientWidget", "\351\200\232\350\256\257\346\250\241\345\274\217\351\200\211\346\213\251\357\274\232clinet", nullptr));
        iplabel->setText(QApplication::translate("ClientWidget", "\347\233\256\347\232\204IP\357\274\232", nullptr));
        portlabel->setText(QApplication::translate("ClientWidget", "\347\233\256\347\232\204\347\253\257\345\217\243\357\274\232", nullptr));
        startconnect->setText(QApplication::translate("ClientWidget", "\345\274\200\345\247\213\350\277\236\346\216\245", nullptr));
        stopconnect->setText(QApplication::translate("ClientWidget", "\346\226\255\345\274\200\350\277\236\346\216\245", nullptr));
        resetconnect->setText(QApplication::translate("ClientWidget", "\351\207\215\347\275\256\350\277\236\346\216\245", nullptr));
        debugmode->setText(QApplication::translate("ClientWidget", "debug\346\250\241\345\274\217", nullptr));
        groupBox_2->setTitle(QApplication::translate("ClientWidget", "\344\275\277\347\224\250\350\257\264\346\230\216", nullptr));
        label_3->setText(QApplication::translate("ClientWidget", "\347\202\271\345\207\273\346\226\255\345\274\200\350\277\236\346\216\245\345\220\216\351\200\200\345\207\272\344\270\216\346\234\215\345\212\241\345\231\250\351\200\232\344\277\241\357\274\214\344\270\213\346\254\241\346\227\240\346\263\225\350\277\233\345\205\245\346\227\266\345\217\257\347\202\271\345\207\273\351\207\215\347\275\256\350\277\236\346\216\245", nullptr));
        label_4->setText(QApplication::translate("ClientWidget", "\351\273\230\350\256\244\347\233\256\347\232\204ip\357\274\232192.168.159.172  \351\273\230\350\256\244\347\233\256\347\232\204\347\253\257\345\217\243\357\274\2326000", nullptr));
        label_2->setText(QApplication::translate("ClientWidget", "\350\276\223\345\205\245\347\233\256\347\232\204IP\345\222\214\347\233\256\347\232\204\347\253\257\345\217\243\345\220\216\347\202\271\345\207\273\345\274\200\345\247\213\350\277\236\346\216\245\345\215\263\345\217\257\344\270\216\346\234\215\345\212\241\345\231\250\351\200\232\344\277\241", nullptr));
    } // retranslateUi

};

namespace Ui {
    class ClientWidget: public Ui_ClientWidget {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_CLIENTWIDGET_H
