/********************************************************************************
** Form generated from reading UI file 'LoginWidget.ui'
**
** Created by: Qt User Interface Compiler version 5.12.8
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_LOGINWIDGET_H
#define UI_LOGINWIDGET_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_LoginWidget
{
public:
    QGridLayout *gridLayout;
    QWidget *titlewidget;
    QHBoxLayout *horizontalLayout;
    QSpacerItem *horizontalSpacer_3;
    QPushButton *hidebtn;
    QPushButton *closebtn;
    QSpacerItem *horizontalSpacer;
    QLabel *picture;
    QSpacerItem *horizontalSpacer_2;
    QPushButton *loginbtn;
    QLineEdit *jusername;
    QLineEdit *kpassword;

    void setupUi(QWidget *LoginWidget)
    {
        if (LoginWidget->objectName().isEmpty())
            LoginWidget->setObjectName(QString::fromUtf8("LoginWidget"));
        LoginWidget->resize(234, 169);
        gridLayout = new QGridLayout(LoginWidget);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        titlewidget = new QWidget(LoginWidget);
        titlewidget->setObjectName(QString::fromUtf8("titlewidget"));
        horizontalLayout = new QHBoxLayout(titlewidget);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        horizontalSpacer_3 = new QSpacerItem(60, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer_3);

        hidebtn = new QPushButton(titlewidget);
        hidebtn->setObjectName(QString::fromUtf8("hidebtn"));

        horizontalLayout->addWidget(hidebtn);

        closebtn = new QPushButton(titlewidget);
        closebtn->setObjectName(QString::fromUtf8("closebtn"));

        horizontalLayout->addWidget(closebtn);


        gridLayout->addWidget(titlewidget, 0, 0, 1, 3);

        horizontalSpacer = new QSpacerItem(63, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout->addItem(horizontalSpacer, 1, 0, 1, 1);

        picture = new QLabel(LoginWidget);
        picture->setObjectName(QString::fromUtf8("picture"));

        gridLayout->addWidget(picture, 1, 1, 1, 1);

        horizontalSpacer_2 = new QSpacerItem(60, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout->addItem(horizontalSpacer_2, 1, 2, 1, 1);

        loginbtn = new QPushButton(LoginWidget);
        loginbtn->setObjectName(QString::fromUtf8("loginbtn"));

        gridLayout->addWidget(loginbtn, 4, 1, 1, 1);

        jusername = new QLineEdit(LoginWidget);
        jusername->setObjectName(QString::fromUtf8("jusername"));

        gridLayout->addWidget(jusername, 2, 0, 1, 3);

        kpassword = new QLineEdit(LoginWidget);
        kpassword->setObjectName(QString::fromUtf8("kpassword"));

        gridLayout->addWidget(kpassword, 3, 0, 1, 3);

        picture->raise();
        titlewidget->raise();
        jusername->raise();
        kpassword->raise();
        loginbtn->raise();

        retranslateUi(LoginWidget);

        QMetaObject::connectSlotsByName(LoginWidget);
    } // setupUi

    void retranslateUi(QWidget *LoginWidget)
    {
        LoginWidget->setWindowTitle(QApplication::translate("LoginWidget", "Form", nullptr));
        hidebtn->setText(QString());
        closebtn->setText(QString());
        picture->setText(QApplication::translate("LoginWidget", "<html><head/><body><p align=\"center\">picture</p></body></html>", nullptr));
        loginbtn->setText(QApplication::translate("LoginWidget", "\347\231\273\345\275\225", nullptr));
    } // retranslateUi

};

namespace Ui {
    class LoginWidget: public Ui_LoginWidget {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_LOGINWIDGET_H
