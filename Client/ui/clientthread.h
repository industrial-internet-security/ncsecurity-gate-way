#ifndef CLIENTTHREAD_H
#define CLIENTTHREAD_H

#include <QVector>
#include <QObject>
#include <gmssl/tls.h>

extern char bridge_ip[25];

class ClientThread : public QObject
{
    Q_OBJECT
public:
    explicit ClientThread(QObject *parent = nullptr);
    void startconnect(QString ip, QString port, bool debug);
    void output(quint8 type, QString exp);
    //void recv_option(QString ip,QString port,bool debug);
    void stopconnect();
    void resetconnect();
    
protected:

signals:
    void send_msg(QVector<QString>msg);

    
private:
   QString ip;
   QString port;
   int debug;
   int stop_flag;
   int net_fd;
   int tap_fd;
   TLS_CTX ctx;
   TLS_CONNECT conn;
    
};

#endif // CLIENTTHREAD_H
