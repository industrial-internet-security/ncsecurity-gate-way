#include "nclinkmsgwidget.h"
#include "ui_nclinkmsgwidget.h"
//#include "nclinkdevwidget.h"
#include"clientthread.h"


/*
struct _root rootstorage[10];
struct _device devicestorage[50];
struct _components componentstorage[50];
struct _data datastorage[200];
struct _channel channelstorage[50];
struct _method methodstorage[50];
*/

//extern struct _connect_data connectstorage[100];

nclinkmsgwidget::nclinkmsgwidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::nclinkmsgwidget)
{
    ui->setupUi(this);
    //创建实例化TCP通信对象
    tcp_socket=new QTcpSocket(this);
    QRegExp regExp("^((2[0-4]\\d|25[0-5]|[01]?\\d\\d?)\\.){3}(2[0-4]\\d|25[0-5]|[01]?\\d\\d?)$");
    QValidator *validator = new QRegExpValidator(regExp, ui->choose_ip);
    ui->choose_ip->setValidator(validator);
    ui->choose_port->setValidator(new QIntValidator(0, 65536,ui->choose_port));
    //ui->choose_port->setText("6000");

    connect(tcp_socket,&QTcpSocket::readyRead,this,[=](){
        //接收数据
        QByteArray data = tcp_socket->readAll();
        ui->msg_table->append ("服务器say: " + data);
    });

    connect(tcp_socket,&QTcpSocket::disconnected,this,[=](){
            tcp_socket->close();
            tcp_socket->deleteLater();
            ui->msg_table->append ("服务器断开链接！");
    });
    /*
    connect(tcp_socket,&QTcpSocket::connected,this,[=](){
            ui->msg_table->append ("服务器链接成功！");
    });*/

}

nclinkmsgwidget::~nclinkmsgwidget()
{
    delete ui;
}


void nclinkmsgwidget::on_send_msg_clicked()
{
    QString msg=ui->send_msg_text->toPlainText();
    tcp_socket->write(msg.toUtf8());
    ui->msg_table->append ("客户端say: " + msg);
}


void nclinkmsgwidget::on_start_client_clicked()
{
    unsigned short port;
    QString ip;
    if(!ui->choose_port->text().isEmpty())
        port=ui->choose_port->text().toUShort();
    else port=7000;
    if(ui->choose_ip->text().isEmpty()){
        ui->choose_ip->setText(bridge_ip);
    }
    ip=ui->choose_ip->text();
    ui->msg_table->append("客户端启动！");
    tcp_socket->connectToHost(ip,port);
}
