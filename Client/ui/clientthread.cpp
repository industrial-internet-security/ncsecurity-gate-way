
extern "C"{
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <net/if.h>
#include <linux/if_tun.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <arpa/inet.h>
#include <sys/select.h>
#include <sys/time.h>
#include <errno.h>
#include <stdarg.h>
#include <gmssl/tls.h>
#include <gmssl/error.h>
#include "../GateClient/ifManage.h"
#include "../GateClient/tuntapAPI.h"
}

#include "clientthread.h"

#include <QTime>
#include <QVector>
#include <QObject>

#define PORT 6000
#define MAXNUM 15000
#define BUFSIZE 2000
#define DATETIME qPrintable (QDateTime::currentDateTime().toString("yyyy-MM-dd HH:mm:ss"))


using namespace std;

#pragma execution_character_set("utf-8")

/* buffer for reading from tun/tap interface, must be >= 1500 */

QVector<QString> msg(100);
QString str;

char *progname;
int debug=0;
int stop_flag=0;
char remote_ip[16]="192.168.159.170";/* dotted quad IP string */
unsigned short int remote_port=6000;

char bridge_ip[25];

static void my_debug(char *msg, ...)
{
  if(debug)
  {
    va_list argp;
    va_start(argp, msg);
    vfprintf(stderr, msg, argp);
    va_end(argp);
  }
}

static size_t tls_read_n(TLS_CONNECT *conn, char *buf, size_t n)
{
    size_t nread=0, left = n;

    while (left > 0)
    {
        int rv;
        rv = tls_recv(conn, (uint8_t *)buf, left, &nread);
        if (rv != 1)
        {
            if (rv < 0)
                return rv;
            else
                return 0;
        }
        else
        {
            if(nread==0)
                return 0;
            left -= nread;
            buf += nread;
        }
    }
    return n;
}

/**************************************************************************
 * cread: read routine that checks for errors and exits if an error is    *
 *        returned.                                                       *
 **************************************************************************/
int cread(int fd, char *buf, int n)
{

    int nread;

    if ((nread = read(fd, buf, n)) < 0)
    {
        perror("Reading data");
        return -1;
    }
    return nread;
}

/**************************************************************************
 * cwrite: write routine that checks for errors and exits if an error is  *
 *         returned.                                                      *
 **************************************************************************/
int cwrite(int fd, char *buf, int n)
{

    int nwrite;

    if ((nwrite = write(fd, buf, n)) < 0)
    {
        perror("Writing data");
        return -1;
    }
    return nwrite;
}

/**************************************************************************
 * read_n: ensures we read exactly n bytes, and puts them into "buf".     *
 *         (unless EOF, of course)                                        *
 **************************************************************************/
int read_n(int fd, char *buf, int n)
{

    int nread, left = n;

    while (left > 0)
    {
        if ((nread = cread(fd, buf, left)) == 0)
        {
            return 0;
        }
        else if(nread<0)
            return nread;
        else
        {
            left -= nread;
            buf += nread;
        }
    }
    return n;
}



/**************************************************************************
 * usage: prints usage and exits.                                         *
 **************************************************************************/
static void usage(void)
{
    fprintf(stderr, "Usage:\n");
    fprintf(stderr, "there is no help messsage yet\n");
    exit(1);
}

//avoid function name conflict
extern void close_fd(int net_fd, TLS_CTX* ctx, TLS_CONNECT* conn){
    close(net_fd);
    tls_ctx_cleanup(ctx);
    tls_cleanup(conn);
    return;
}
extern void close_tap(int net_fd){
    close(net_fd);
    return;
}

extern int connect_fd(int __fd, __CONST_SOCKADDR_ARG __addr, socklen_t __len){
    return connect(__fd,__addr,__len);
}

void ClientThread::output(quint8 type, QString exp){
    QString temp;

    if (type == 1) {
        temp = "[error] :";

    }else if (type == 2) {
        temp = "[info] :";

    }else if (type == 3) {
        temp = "[trans] :";

    }
    msg.push_back(QString("time [%1] %2 %3").arg(DATETIME).arg(temp).arg(exp));
    emit send_msg(msg);
    msg.clear();
}


void ClientThread::startconnect(QString ip, QString port, bool debug)
{
    stop_flag=0;
    //int tap_fd;
    int option;
    char tap_name[IFNAMSIZ] = "gatetap0";
    int maxfd;
    uint16_t nread, nwrite, plength,temp;
    char buffer[BUFSIZE];
    struct sockaddr_in local, remote;

    int sock_fd, optval = 1;
    socklen_t remotelen;
    unsigned long int tap2net = 0, net2tap = 0;
    output(2,"Client start!");
    //progname = argv[0];

    // unsigned char eth_mac[6] = {0};
    unsigned char eth_mask[4]={255,255,0,0};

    if(!ip.isEmpty()){
        QByteArray ip_name=ip.toLatin1();
        strcpy(remote_ip,ip_name.data());
    }else strcpy(remote_ip,"192.168.159.172");


    if(!port.isEmpty()){
        remote_port=port.toUShort();
    }else remote_port=6000;

    if (debug==true) {
        debug=1;
    }
    else debug=0;

    str="set ip=";
    str=str+remote_ip+" and set port="+QString::number(remote_port);
    output(2,str);
    if(debug) output(2,"using debug mode");
    else output(2,"not using debug mode");


    if (*remote_ip == '\0')
    {
        output(1,"Must specify gateserver IP!");
        printf("Must specify gateserver IP!\n");
        usage();
    }
    if (*tap_name == '\0')
    {
        output(1,"Must specify tap interface name!");
        printf("Must specify tap interface name!\n");
        usage();
    }

    /* initialize tap interface */
    if ((tap_fd = tun_alloc(tap_name, IFF_TAP | IFF_NO_PI)) < 0)
    {
        str="Error connecting to tap interface ";
        str+=tap_name;
        output(1,str);
        printf("Error connecting to tap interface %s!\n", tap_name);
        close_tap(tap_fd);
        return;
        //exit(1);
    }
    output(2,"Successfully connected to interface!");
    printf("Successfully connected to interface %s\n", tap_name);


    if ((sock_fd = socket(AF_INET, SOCK_STREAM, 0)) < 0)
    {
        output(1,"socket error!");
        perror("socket()");
        close_tap(tap_fd);
        return;
        //exit(1);
    }


    /* assign the destination address */
    memset(&remote, 0, sizeof(remote));
    remote.sin_family = AF_INET;
    remote.sin_addr.s_addr = inet_addr(remote_ip);
    remote.sin_port = htons(remote_port);

    /* connection request */
    if (connect_fd(sock_fd, (struct sockaddr*) &remote, sizeof(remote)) < 0)
    {
      output(1,"connect error!");
      perror("connect()");
      close_tap(tap_fd);
      return;
      //exit(1);
    }

    net_fd = sock_fd;
    str="CLIENT: Connected to server ";
    str+=inet_ntoa(remote.sin_addr);
    output(2,str);
    printf("CLIENT: Connected to server %s\n", inet_ntoa(remote.sin_addr));

    char *prog = "client";
    int client_ciphers[] = { TLS_cipher_ecc_sm4_cbc_sm3, };
    char cacertfile[] = "../GateClient/rootcacert.pem";
    char certfile[] ="../GateClient/clientcert.pem";
    char keyfile[] = "../GateClient/clientkey.pem";
    char pass[] = "1234";
    //TLS_CTX ctx;
    //TLS_CONNECT conn;
    memset(&ctx, 0, sizeof(ctx));
    memset(&conn, 0, sizeof(conn));

    if (tls_ctx_init(&ctx, TLS_protocol_tlcp, TLS_client_mode) != 1
    || tls_ctx_set_cipher_suites(&ctx, client_ciphers, sizeof(client_ciphers)/sizeof(client_ciphers[0])) != 1)
    {
        str=prog;
        str+=": context init error\n";
        output(1,str);
        fprintf(stderr, "%s: context init error\n", prog);
        close_fd(net_fd,&ctx,&conn);
        close_tap(tap_fd);
        return;
        //exit(0);
    }
    if (cacertfile) {
        if (tls_ctx_set_ca_certificates(&ctx, cacertfile, TLS_DEFAULT_VERIFY_DEPTH) != 1) {
            perror("t");
            str=prog;
            str+=": context init error\n";
            output(1,str);
            fprintf(stderr, "%s: context init error\n", prog);
            close_fd(net_fd,&ctx,&conn);
            close_tap(tap_fd);
            return;
            //exit(0);
        }
    }
    if (certfile) {
        if (tls_ctx_set_certificate_and_key(&ctx, certfile, keyfile, pass) != 1) {
            perror("t");
            str=prog;
            str+=": context init error\n";
            output(1,str);
            fprintf(stderr, "%s: context init error\n", prog);
            close_fd(net_fd,&ctx,&conn);
            close_tap(tap_fd);
            return;
            //exit(0);
            //return (0);
        }
    }


    if (tls_init(&conn, &ctx) != 1
        || tls_set_socket(&conn, net_fd) != 1
        || tls_do_handshake(&conn) != 1)
    {
        str=prog;
        str+=": tls error\n";
        output(1,str);
        fprintf(stderr, "%s: error\n", prog);
        close_fd(net_fd,&ctx,&conn);
        close_tap(tap_fd);
        return;
        //exit(0);
        //return (0);
    }


    unsigned char eth_ip[4] = {0};
    // read_n(net_fd,eth_ip,4);
    temp=tls_read_n(&conn, (char *)eth_ip, 4);
    if(temp==0||temp>=65534)
    {
        close_fd(net_fd,&ctx,&conn);
        tls_shutdown(&conn);
        close_tap(tap_fd);
        return;
    }
        //goto end;
    str="get ip from server:"+QString::number((unsigned int)eth_ip[0])+"."+QString::number((unsigned int)eth_ip[1])+"."+
            QString::number((unsigned int)eth_ip[2])+"."+QString::number((unsigned int)eth_ip[3]);
    QString bri_src=QString::number((unsigned int)eth_ip[0])+"."+QString::number((unsigned int)eth_ip[1])+"."+
            QString::number((unsigned int)eth_ip[2])+"."+"1";
    strcpy(bridge_ip,bri_src.toLatin1());

    output(2,str);
    printf("get ip from server:%d.%d.%d.%d\n",eth_ip[0],eth_ip[1],eth_ip[2],eth_ip[3]);

    if(net_eth_set_ipv4_addr(tap_name,eth_ip,eth_mask)!=0)
    {
        output(1,"set tap ip failed!");
        perror("set tap ip failed");
        close_fd(net_fd,&ctx,&conn);
        close_tap(tap_fd);
        return;
        //exit(1);
    }
    int tap_state;
    if((tap_state=net_eth_state_is_up(tap_name))==0)
    {
        if(net_eth_state(tap_name,"up")!=0)
        {
            output(1,"up tap failed!");
            perror("up tap failed");
            close_fd(net_fd,&ctx,&conn);
            close_tap(tap_fd);
            return;
            //exit(1);
        }
    }
    else if(tap_state==-1)
    {
        output(1,"net_eth_state_is_up!");
        perror("net_eth_state_is_up()");
        close_fd(net_fd,&ctx,&conn);
        close_tap(tap_fd);
        return;
        //exit(1);
    }
    str=tap_name;
    str+=" is up now";
    output(2,str);
    printf("%s is up now\n", tap_name);

    maxfd = (tap_fd > net_fd)?tap_fd:net_fd;
    size_t sentlen;
    while (1)
    {
        int ret;
        fd_set rd_set;

        FD_ZERO(&rd_set);
        FD_SET(tap_fd, &rd_set);
        FD_SET(net_fd, &rd_set);

        ret = select(maxfd + 1, &rd_set, NULL, NULL, NULL);

        if (ret < 0 && errno == EINTR)
        {
            continue;
        }

        if (ret < 0)
        {
            output(1,"select error!");
            perror("select()");
            close_fd(net_fd,&ctx,&conn);
            close_tap(tap_fd);
            return;
            //exit(1);
        }

        if (FD_ISSET(tap_fd, &rd_set))
        {
            /* data from tun/tap: just read it and write it to the network */

            if((nread = cread(tap_fd, buffer, BUFSIZE))<0)
            {
                output(2,"stop connect");
                close_fd(net_fd,&ctx,&conn);
                close_tap(tap_fd);
                return;
            }
                //goto end;
            tap2net++;
            str="TAP2NET ";
            str=str+QString::number(tap2net)+"Read "+QString::number(nread)+" bytes to the tap interface";
            output(3,str);
            my_debug("TAP2NET %lu: Read %d bytes from the tap interface\n", tap2net, nread);

            /* write length + packet */
            plength = htons(nread);
            if(tls_send(&conn, (const uint8_t *)&plength, sizeof(plength), &sentlen) != 1)
            {
                output(2,"stop connect");
                close_fd(net_fd,&ctx,&conn);
                close_tap(tap_fd);
                return;
            }
                    //goto end;
            if(tls_send(&conn, (const uint8_t *)buffer, nread, &sentlen) != 1)
            {
                output(2,"stop connect");
                close_fd(net_fd,&ctx,&conn);
                close_tap(tap_fd);
                return;
            }
                //goto end;

            str="TAP2NET ";
            str=str+QString::number(tap2net)+"Written "+QString::number(nwrite)+" bytes to the tap interface";
            output(3,str);
            my_debug("TAP2NET %lu: Written %d bytes to the network\n", tap2net, nwrite);
        }

        if (FD_ISSET(net_fd, &rd_set))
        {
            /* data from the network: read it, and write it to the tun/tap interface.
             * We need to read the length first, and then the packet */

            /* Read length */
            nread = tls_read_n(&conn, (char *)&plength, sizeof(plength));
            if(nread>=65534)
            {
                output(2,"server disconnect");
                close_fd(net_fd,&ctx,&conn);
                close_tap(tap_fd);
                return;
            }
                //goto end;
            if (nread == 0)
            {
                output(2,"server disconnect");
                close_fd(net_fd,&ctx,&conn);
                close_tap(tap_fd);
                return;
                /* ctrl-c at the other end */
                //goto end;
            }
            // plength=256;

            str="ntohs(plength)=";
            str+=QString::number(ntohs(plength));
            output(3,str);
            my_debug("ntohs(plength)=%d\n", ntohs(plength));

            net2tap++;

            /* read packet */
            nread = tls_read_n(&conn, buffer, ntohs(plength));
            if(nread>=65534)
            {
                output(2,"server disconnect");
                close_fd(net_fd,&ctx,&conn);
                close_tap(tap_fd);
                return;
            }
                    //goto end;

            str="NET2TAP ";
            str=str+QString::number(net2tap)+"Read "+QString::number(nread)+" bytes to the tap interface";
            output(3,str);
            my_debug("NET2TAP %lu: Read %d bytes from the network\n", net2tap, nread);

            /* now buffer[] contains a full packet or frame, write it into the tun/tap interface */
            if((nwrite = cwrite(tap_fd, buffer, nread))<0)
            {
                output(2,"stop connect");
                close_fd(net_fd,&ctx,&conn);
                close_tap(tap_fd);
                return;
            }
                //goto end;
            str="NET2TAP ";
            str=str+QString::number(net2tap)+"Written "+QString::number(nwrite)+" bytes to the tap interface";
            output(3,str);
            my_debug("NET2TAP %lu: Written %d bytes to the tap interface\n", net2tap, nwrite);
        }
        if(stop_flag)
        {
            output(2,"stop connect!");
            close_fd(net_fd,&ctx,&conn);
            close_tap(tap_fd);
            return;
            //goto end;
        }

    }
/*
end:
    close_fd(net_fd,&ctx,&conn);
    close_tap(tap_fd);
    return;
    //exit(0);
*/
}


ClientThread::ClientThread(QObject *parent) : QObject(parent)
{

}


/*
void ClientThread::recv_option(QString ip, QString port, bool debug){

    if(!ip.isEmpty()){
        QByteArray ip_name=ip.toLatin1();
        strcpy(remote_ip,ip_name.data());
    }else strcpy(remote_ip,"192.168.159.170");


    if(!port.isEmpty()){
        remote_port=port.toUShort();
    }else remote_port=6000;

    if (debug==true) {
        debug=1;
    }
    else debug=0;
}
*/
void ClientThread::stopconnect(){
    //output(2,"stop connect!");
    //close_fd(net_fd,&ctx,&conn);
    stop_flag=1;
}

void ClientThread::resetconnect(){
    output(2,"stop connect and terminate thread!");
    close_fd(net_fd,&ctx,&conn);
    close_tap(tap_fd);
}
