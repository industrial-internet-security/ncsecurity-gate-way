HEADERS += \
    $$PWD/ClientWidget.h \
    $$PWD/CustomWidget.h \
    $$PWD/LoginWidget.h \
    $$PWD/clientthread.h \
    $$PWD/mainwindow.h \
    $$PWD/nclinkmsgwidget.h
           
SOURCES += \
    $$PWD/ClientWidget.cpp \
    $$PWD/CustomWidget.cpp \
    $$PWD/LoginWidget.cpp \
    $$PWD/clientthread.cpp \
    $$PWD/mainwindow.cpp \
    $$PWD/nclinkmsgwidget.cpp
	
FORMS += \
    $$PWD/ClientWidget.ui \
    $$PWD/CustomWidget.ui \
    $$PWD/LoginWidget.ui \
    $$PWD/mainwindow.ui \
    $$PWD/nclinkmsgwidget.ui
