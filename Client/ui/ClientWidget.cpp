﻿#include "ClientWidget.h"
#include "ui_ClientWidget.h"
//#include "../help/BinaryCvn.h"
#include "../help/UiSet.h"
#include "../help/AppCfg.h"
#include "clientthread.h"
#include <QObject>

//函数入口？
ClientWidget::ClientWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ClientWidget)
{
    ui->setupUi(this);
    setWindowFlags(Qt::FramelessWindowHint);   //去掉边框
    setAttribute(Qt::WA_StyledBackground);
    //server = new QTcpServer(this);
    //client = new QTcpSocket(this);   //实例化tcpClient
    InitWindow();
}

ClientWidget::~ClientWidget()
{
    //saveConfig();
    delete ui;
}


void ClientWidget::InitWindow()
{
    //IP输入框正则
    QRegExp regExp("^((2[0-4]\\d|25[0-5]|[01]?\\d\\d?)\\.){3}(2[0-4]\\d|25[0-5]|[01]?\\d\\d?)$");
    QValidator *validator = new QRegExpValidator(regExp, ui->ip);
    ui->ip->setValidator(validator);
    ui->port->setValidator(new QIntValidator(0, 65536,ui->port));

    ClientThread *thread1 =new ClientThread;

    QThread *t1=new QThread(this);
    thread1->moveToThread(t1);

    connect(this,&ClientWidget::send_option,thread1,&ClientThread::startconnect);
    connect(ui->startconnect,&QPushButton::clicked,this,[=](){
        if(ui->ip->text().isEmpty())ui->ip->setText("192.168.159.172");
        if(ui->port->text().isEmpty())ui->port->setText("6000");
        emit send_option(ui->ip->text(),ui->port->text(),ui->debugmode->isChecked());
        t1->start();
    });
    connect(thread1,&ClientThread::send_msg,this,[=](QVector<QString>msg){
        msg.data();
        ui->display->append(msg.at(0));
    });


    connect(ui->stopconnect, &QPushButton::clicked,this,[=](){
        if(t1->isRunning()){
            //t1->terminate();
            thread1->stopconnect();
        }

    });

    connect(ui->resetconnect, &QPushButton::clicked,this,[=](){
        if(t1->isRunning()){
            thread1->resetconnect();
            t1->terminate();
        }
        //t1->terminate();
    });
}


/*
void ClientWidget::output(quint8 type, QString exp)
{
    QString str;

    if (type == 1) {
        str = "[error] :";
        ui->display->setTextColor(QColor("red"));
    }else if (type == 2) {
        str = "[info] :";
        ui->display->setTextColor(QColor("dodgerblue"));
    }else if (type == 3) {
        str = "[trans] :";
        ui->display->setTextColor(QColor("black"));
    }
    ui->display->append(QString("time [%1] %2 %3").arg(DATETIME).arg(str).arg(exp));
}
*/
