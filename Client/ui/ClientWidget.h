﻿#ifndef TCPWIDGET_H
#define TCPWIDGET_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <net/if.h>
#include <linux/if_tun.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <arpa/inet.h>
#include <sys/select.h>
#include <sys/time.h>
#include <errno.h>
#include <stdarg.h>
#include <gmssl/tls.h>
#include <gmssl/error.h>
#include "../GateClient/ifManage.h"
#include "../GateClient/tuntapAPI.h"

#include <QMainWindow>
//#include <QtNetwork/QTcpServer>

namespace Ui {class ClientWidget;}

class ClientWidget : public QWidget
{
    Q_OBJECT

public:
    explicit ClientWidget(QWidget *parent = nullptr);
    ~ClientWidget();
    void output(quint8 type, QString exp);

signals:
    void send_option(QString ip,QString port,bool debug);

private slots:
    void InitWindow();
    //void saveConfig();

private:
    Ui::ClientWidget *ui;

};

/*
class TcpWidget : public QWidget
{
    Q_OBJECT

public:
    explicit TcpWidget(QWidget *parent = nullptr);
    ~TcpWidget();

private slots:
    void initWindow();
    void choose();
    void initData();
    void builtConnect();
    void change(bool b);
    void openTcp();
    void append(quint8 type, QString msg);
    void sendData();
    void on_autosend_stateChanged(int arg1);
    void autoSendRestart();

    void newConnection();       //sever
    void disconnected();
    void serverRead();

    void readError(QAbstractSocket::SocketError);//client
    void clientRead();
    void autCconnectRestart();
    void connectRestart();
    void on_autoconnect_stateChanged(int arg1);
    void on_sendcount_clicked();
    void on_recvcount_clicked();
    //void on_savedata_clicked();
    void on_cleardata_clicked();

    void saveConfig();

private:
    Ui::TcpWidget *ui;

    QTcpServer *server;          //server
    QList<QTcpSocket*> serverClient;
    QTcpSocket *serverCurrentClient;

    QTcpSocket *client;          //client
    QTimer *timeConnect;

    bool ok;
    int recvCount;                  //接收数据计数
    int sendCount;                  //发送计数
    QTimer *sendTime;               //定时发送串口数据
};
*/

#endif // TCPWIDGET_H
