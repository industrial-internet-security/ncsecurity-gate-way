#ifndef NCLINKMSGWIDGET_H
#define NCLINKMSGWIDGET_H

#include <QWidget>
#include <QTcpSocket>

namespace Ui {
class nclinkmsgwidget;
}

class nclinkmsgwidget : public QWidget
{
    Q_OBJECT

public:
    explicit nclinkmsgwidget(QWidget *parent = nullptr);
    ~nclinkmsgwidget();

private slots:
    void on_send_msg_clicked();
    void on_start_client_clicked();

private:
    Ui::nclinkmsgwidget *ui;
    QTcpSocket *tcp_socket;

};

#endif // NCLINKMSGWIDGET_H
