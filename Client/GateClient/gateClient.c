#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <net/if.h>
#include <linux/if_tun.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <arpa/inet.h>
#include <sys/select.h>
#include <sys/time.h>
#include <errno.h>
#include <stdarg.h>
#include <gmssl/tls.h>
#include <gmssl/error.h>
#include"tuntapAPI.h"
#include"ifManage.h"


/* buffer for reading from tun/tap interface, must be >= 1500 */
#define BUFSIZE 2000
#define PORT 6000

char *progname;
int debug=0;

static void my_debug(char *msg, ...)
{
  if(debug) 
  {
    va_list argp;
	va_start(argp, msg);
	vfprintf(stderr, msg, argp);
	va_end(argp);
  }
}

static size_t tls_read_n(TLS_CONNECT *conn, char *buf, size_t n)
{
    size_t nread=0, left = n;

    while (left > 0)
    {
        int rv;
        if ((rv = tls_recv(conn, (uint8_t *)buf, left, &nread)) != 1) 
        {
			if (rv < 0) 
                return rv;
			else 
                return 0;
		}
        else
        {
            if(nread==0)
                return 0;
            left -= nread;
            buf += nread;
        }
    }
    return n;
}

/**************************************************************************
 * cread: read routine that checks for errors and exits if an error is    *
 *        returned.                                                       *
 **************************************************************************/
int cread(int fd, char *buf, int n)
{

    int nread;

    if ((nread = read(fd, buf, n)) < 0)
    {
        perror("Reading data");
    }
    return nread;
}

/**************************************************************************
 * cwrite: write routine that checks for errors and exits if an error is  *
 *         returned.                                                      *
 **************************************************************************/
int cwrite(int fd, char *buf, int n)
{

    int nwrite;

    if ((nwrite = write(fd, buf, n)) < 0)
    {
        perror("Writing data");
    }
    return nwrite;
}

/**************************************************************************
 * read_n: ensures we read exactly n bytes, and puts them into "buf".     *
 *         (unless EOF, of course)                                        *
 **************************************************************************/
int read_n(int fd, char *buf, int n)
{

    int nread, left = n;

    while (left > 0)
    {
        if ((nread = cread(fd, buf, left)) == 0)
        {
            return 0;
        }
        else if(nread<0)
            return nread;
        else
        {
            left -= nread;
            buf += nread;
        }
    }
    return n;
}



/**************************************************************************
 * usage: prints usage and exits.                                         *
 **************************************************************************/
static void usage(void)
{
    fprintf(stderr, "Usage:\n");
    fprintf(stderr, "there is no help messsage yet\n");
    exit(1);
}




int start_client(int argc, char *argv[])
{

    int tap_fd, option;
    char tap_name[IFNAMSIZ] = "gatetap0";
    int maxfd;
    uint16_t nread, nwrite, plength;
    char buffer[BUFSIZE];
    struct sockaddr_in local, remote;
    char remote_ip[16] = "192.168.159.170"; /* dotted quad IP string */
    unsigned short int port = PORT;
    int sock_fd, net_fd, optval = 1;
    socklen_t remotelen;
    unsigned long int tap2net = 0, net2tap = 0;

    progname = argv[0];
    
    // unsigned char eth_mac[6] = {0};
    unsigned char eth_mask[4]={255,255,0,0};
    /* Check command line options */
    while ((option = getopt(argc, argv, "t:p:hg:d")) > 0)
    {
        switch (option)
        {
        case 'd':
            debug=1;
            break;
        case 'h':
            usage();
            break;
        case 't':
            strncpy(tap_name, optarg, IFNAMSIZ - 1);
            break;
        case 'p':
            port = atoi(optarg);
            break;
        case 'g':
            strncpy(remote_ip, optarg, 15);
            break;            
        default:
            printf("Unknown option %c\n", option);
            usage();
        }
    }

    argv += optind;
    argc -= optind;

    if (argc > 0)
    {
        printf("Too many options!\n");
        usage();
    }
    if (*remote_ip == '\0')
    {
        printf("Must specify gateserver IP!\n");
        usage();
    }
    if (*tap_name == '\0')
    {
        printf("Must specify tap interface name!\n");
        usage();
    }

    /* initialize tap interface */
    if ((tap_fd = tun_alloc(tap_name, IFF_TAP | IFF_NO_PI)) < 0)
    {
        printf("Error connecting to tap interface %s!\n", tap_name);
        exit(1);
    }
    printf("Successfully connected to interface %s\n", tap_name);
   
    
    if ((sock_fd = socket(AF_INET, SOCK_STREAM, 0)) < 0)
    {
        perror("socket()");
        exit(1);
    }


    /* assign the destination address */
    memset(&remote, 0, sizeof(remote));
    remote.sin_family = AF_INET;
    remote.sin_addr.s_addr = inet_addr(remote_ip);
    remote.sin_port = htons(port);

    /* connection request */
    if (connect(sock_fd, (struct sockaddr*) &remote, sizeof(remote)) < 0) {
      perror("connect()");
      exit(1);
    }

    net_fd = sock_fd;
    printf("CLIENT: Connected to server %s\n", inet_ntoa(remote.sin_addr));

    char *prog = argv[0];
    int client_ciphers[] = { TLS_cipher_ecc_sm4_cbc_sm3, };
    char cacertfile[] = "rootcacert.pem";
	char certfile[] ="clientcert.pem";
	char keyfile[] = "clientkey.pem";
	char pass[] = "1234";
    TLS_CTX ctx;
	TLS_CONNECT conn;
    memset(&ctx, 0, sizeof(ctx));
	memset(&conn, 0, sizeof(conn));

    if (tls_ctx_init(&ctx, TLS_protocol_tlcp, TLS_client_mode) != 1
    || tls_ctx_set_cipher_suites(&ctx, client_ciphers, sizeof(client_ciphers)/sizeof(client_ciphers[0])) != 1) 
    {
        fprintf(stderr, "%s: context init error\n", prog);
        goto end;
	}
	if (cacertfile) {
		if (tls_ctx_set_ca_certificates(&ctx, cacertfile, TLS_DEFAULT_VERIFY_DEPTH) != 1) {
            perror("t");
			fprintf(stderr, "%s: context init error\n", prog);
			goto end;
		}
	}
	if (certfile) {
		if (tls_ctx_set_certificate_and_key(&ctx, certfile, keyfile, pass) != 1) {
            perror("t");
			fprintf(stderr, "%s: context init error\n", prog);
			goto end;
		}
	}


	if (tls_init(&conn, &ctx) != 1
		|| tls_set_socket(&conn, net_fd) != 1
		|| tls_do_handshake(&conn) != 1) 
    {
		fprintf(stderr, "%s: error\n", prog);
		goto end;
	}


    unsigned char eth_ip[4] = {0};
    // read_n(net_fd,eth_ip,4);
    if((tls_read_n(&conn, eth_ip, 4))<=0)
        goto end;
    printf("get ip from server:%d.%d.%d.%d\n",eth_ip[0],eth_ip[1],eth_ip[2],eth_ip[3]);

    if(net_eth_set_ipv4_addr(tap_name,eth_ip,eth_mask)!=0)
    {
        perror("set tap ip failed");
        exit(1);
    }
    int tap_state;
    if((tap_state=net_eth_state_is_up(tap_name))==0)
    {
        if(net_eth_state(tap_name,"up")!=0)
        {
            perror("up tap failed");
            exit(1);
        }
    }
    else if(tap_state==-1)
    {
        perror("net_eth_state_is_up()");
        exit(1);
    }
    printf("%s is up now\n", tap_name);

    maxfd = (tap_fd > net_fd)?tap_fd:net_fd;
    size_t sentlen;
    while (1)
    {
        int ret;
        fd_set rd_set;

        FD_ZERO(&rd_set);
        FD_SET(tap_fd, &rd_set);
        FD_SET(net_fd, &rd_set);

        ret = select(maxfd + 1, &rd_set, NULL, NULL, NULL);

        if (ret < 0 && errno == EINTR)
        {
            continue;
        }

        if (ret < 0)
        {
            perror("select()");
            exit(1);
        }

        if (FD_ISSET(tap_fd, &rd_set))
        {
            /* data from tun/tap: just read it and write it to the network */

            if((nread = cread(tap_fd, buffer, BUFSIZE))<0)
                goto end;
            tap2net++;
            my_debug("TAP2NET %lu: Read %d bytes from the tap interface\n", tap2net, nread);

            /* write length + packet */
            plength = htons(nread);
            if(tls_send(&conn, (char *)&plength, sizeof(plength), &sentlen) != 1)
                    goto end;
            if(tls_send(&conn, buffer, nread, &sentlen) != 1)
                goto end;

            my_debug("TAP2NET %lu: Written %d bytes to the network\n", tap2net, nwrite);
        }

        if (FD_ISSET(net_fd, &rd_set))
        {
            /* data from the network: read it, and write it to the tun/tap interface.
             * We need to read the length first, and then the packet */

            /* Read length */
            if((nread = tls_read_n(&conn, (char *)&plength, sizeof(plength)))<0)
                goto end;
            if (nread == 0)
            {
                /* ctrl-c at the other end */
                goto end;
            }
            // plength=256;
            my_debug("ntohs(plength)=%d\n", ntohs(plength));

            net2tap++;

            /* read packet */
            if((nread = tls_read_n(&conn, buffer, ntohs(plength)))<0)
                    goto end;
            my_debug("NET2TAP %lu: Read %d bytes from the network\n", net2tap, nread);

            /* now buffer[] contains a full packet or frame, write it into the tun/tap interface */
            if((nwrite = cwrite(tap_fd, buffer, nread))<0)
                goto end;
            my_debug("NET2TAP %lu: Written %d bytes to the tap interface\n", net2tap, nwrite);
        }
    }

end:
	close(net_fd);
	tls_ctx_cleanup(&ctx);
	tls_cleanup(&conn);
	return 0;
    return (0);
}
