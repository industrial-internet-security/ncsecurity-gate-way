DISTFILES += \
    $$PWD/clientcert.pem \
    $$PWD/clientkey.pem \
    $$PWD/compile.sh \
    $$PWD/rootcacert.pem

HEADERS += \
    $$PWD/ifManage.h \
    $$PWD/tuntapAPI.h

SOURCES += \
    $$PWD/ifManage.c \
    $$PWD/tuntapAPI.c
