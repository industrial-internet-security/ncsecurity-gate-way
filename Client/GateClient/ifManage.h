#ifndef _IFMANAGE_H
#define _IFMANAGE_H	1
extern int net_eth_state(char *eth_name, char *state);
extern int net_eth_state_is_up(char *eth_name);
extern int net_eth_get_mac(char *eth_name, unsigned char *mac);
extern int net_eth_set_mac(char *eth_name, unsigned char *mac);
extern int net_eth_get_ipv4_addr(char *eth_name, unsigned char *ip, unsigned char *netmask);
extern int net_eth_set_ipv4_addr(char *eth_name, unsigned char *ip, unsigned char *netmask);
#endif   /* ifManage.h  */